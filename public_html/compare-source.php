<?php
/**
 * @author: Muhammad Khalil
 * @created_at May 22, 2016
 */

//region Source Config | Staging
//$sourceFile = 'http://local.giftcardbid.com/compare-source.php';
//$remoteFile = 'http://giftcardbid.com/compare-remote.php';
//
//// *** Define your host, username, and password
//define('FTP_HOST', 'giftcardbid.com');
//define('FTP_USER', 'bidcardgift');
//define('FTP_PASS', '');
//define('PASSIVE_MODE', TRUE);
//endregion

//region Source Config | Live
$sourceFile = 'http://local.giftcardbid.com/compare-source.php';
$remoteFile = 'http://giftcardbid.com/compare-remote.php';

// *** Define your host, username, and password
define('FTP_HOST', 'giftcardbid.com');
define('FTP_USER', 'bidcardgift');
define('FTP_PASS', 'N$CB_l+Lsn}f');
define('PASSIVE_MODE', TRUE);
//endregion

/******************* FOLLOWING CODE DOES NOT NEED TO BE CHANGED *******************/
require 'compare/Helper.php';
require 'compare/Compare.php';
require 'compare/FTPClient.php';

// Define all titles at one place
define( 'TOGGLE_TITLE', 'Toggle Selection of the files' );
define( 'DOWNLOAD_TITLE', 'Download From Remote' );
define( 'DOWNLOAD_ALL_TITLE', 'Download selected files from remote' );
define( 'UPLOAD_TITLE', 'Upload To Remote' );
define( 'UPLOAD_ALL_TITLE', 'Upload selected files to remote' );
define( 'DELETE_SOURCE_TITLE', 'Remove file from source' );
define( 'DELETE_ALL_SOURCE_TITLE', 'Remove selected files from remote' );
define( 'DELETE_REMOTE_TITLE', 'Remove file from remote' );
define( 'DELETE_ALL_REMOTE_TITLE', 'Remove selected files from remote' );
define( 'COPY_TITLE', 'Remove selected files from remote' );

// Remote Output
$remoteData = file_get_contents($remoteFile);
$remoteData = json_decode($remoteData, true);

$remoteFiles = [];
foreach ($remoteData['files'] as $row) {
    $remoteFiles[] = $row['path'];
}

// Remote Config
$remoteConfig = file_get_contents($remoteFile . '?json&request=config');
$remoteConfig = json_decode($remoteConfig, true);
// dump($remoteConfig);

// Assign remote config to source config variables
list($scanList, $ignoreList) = array_values($remoteConfig);

// Scan Source
$compare = new Compare();
$compare->setIgnoreList($ignoreList);
$compare->setScanList($scanList);
$sourceData = $compare->scanner();

$sourceFiles = [];
$files_matched = [];
$files_different = [];
$files_new_at_source = [];
$files_new_at_remote = [];
$counter = 0;
foreach ($sourceData['files'] as $key => $file) {
    $sourceFileKey = $key;
    $sourceFilePath = str_replace('./', '', $file['path']);
    $sourceFileHash = $file['hash'];
    $sourceFiles[] = $file['path'];

    if (isset($remoteData['files'][$sourceFileKey])) {
        if ($remoteData['files'][$sourceFileKey]['hash'] == $sourceFileHash) {
            $files_matched[$counter] = $sourceFilePath;
        } else {
            $files_different[$counter] = $sourceFilePath;
        }
        unset($remoteData['files'][$sourceFileKey]['hash']);
    } else {
        $files_new_at_source[$counter] = $sourceFilePath;
    }
    $counter++;
}

// d($files_matched);
// d($files_different);
// d($files_new_at_source);

// Get files exist on remote but not in source
$files_new_at_remote = array_diff($remoteFiles, $sourceFiles);
$files_new_at_remote = str_replace('./', '', $files_new_at_remote);

$final_output['different'] = $files_different;
$final_output['matched'] = $files_matched;
$final_output['new_at_source'] = $files_new_at_source;
$final_output['new_at_remote'] = $files_new_at_remote;
// d($final_output);


/**
 * Upload file to the remote
 */
if (isset($_POST['action']) && $_POST['action'] == 'upload') {
    $filePath = $_POST['file'];
    $remoteUrl = $remoteConfig['remoteUrl'];

    $ch = curl_init();

    $file = new CURLFile($filePath, getMimeType($filePath), $filePath);
    $data = array('file' => $file, 'path' => $filePath);

    curl_setopt($ch, CURLOPT_URL, $remoteUrl);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));

    $fp = fopen(dirname(__FILE__).'/errorlog.txt', 'w');

    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_STDERR, $fp);

    $response = curl_exec($ch);
    if ($response == true) {
        echo json_encode(['response' => true, 'msg' => 'File Uploaded']);
    } else {
        echo json_encode(['response' => false, 'msg' => curl_error($ch)]);
    }
    curl_close($ch);

    exit();
}

/**
 * Download file from the remote
 */
if (isset($_POST['action']) && $_POST['action'] == 'download') {
    // http://stackoverflow.com/questions/6409462/downloading-a-large-file-using-curl
    // https://www.youtube.com/watch?v=W_pC50LhBFQ&list=PLliWPavYPve3iodMy_eD0sv_ku33ljFQ7

//    $remoteUrl = $remoteConfig['remoteUrl'];
//    $remoteUrl = substr($remoteUrl, 0, strrpos( $remoteUrl, '/'));
//    $filePath = $_POST['file'];
//
//    $url = $remoteUrl .'/'. $filePath;
//    $downloaded = __DIR__ .DIRECTORY_SEPARATOR. $filePath;
//
//    $ch = curl_init();
//    curl_setopt($ch, CURLOPT_URL, $url);
//    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//    $st = curl_exec($ch);
//    $fd = fopen($downloaded, 'w');
//    fwrite($fd, $st);
//    if( fclose($fd) ) {
//        echo json_encode(['response' => true, 'msg' => 'File Downloaded']);
//    }
//
//    curl_close($ch);
//    exit();


    // *** Create the FTP object
    $ftpObj = new FTPClient();
    // *** Connect
    if ($ftpObj -> connect(FTP_HOST, FTP_USER, FTP_PASS, PASSIVE_MODE)) {
        // *** Then add FTP code here

        $filePath = $_POST['file'];
        $path = dirname($filePath);
        $fileName = basename($filePath);
        if (!file_exists($path)) {
            mkdir($path, 0755, true);
        }

        // *** Change to folder
        $ftpObj->changeDir($path);

        $fileFrom = $fileName; # The location on the remote
        $fileTo = $filePath; # Source dir to save to

        // *** Download file
        $ftpObj->downloadFile($fileFrom, $fileTo);

        echo json_encode(['response' => true, 'msg' => 'File Downloaded']);
    } else {
        echo json_encode(['response' => false, 'msg' => $ftpObj -> getMessages()]);
    }
    exit();
}
?>

<html>
<head>
    <title>Comparison Utility by Khalil</title>
    <style>
        body {
            font-family: Arial;
            position: relative;
        }

        hr {
            border: none;
        }

        #loader {
            background-color: rgba(0, 0, 0, 0.8);
            z-index: 999;
            position: absolute;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            display: none;
        }
        #loader span{
            background: #fff;
            padding: 10px;
            line-height: 40px;
            top: 45%;
            position: fixed;
            right: 50%;
            margin-right: -250px;
            width: 400px;
            display: block;
            text-align: center;
            border: solid 5px #A9A9A9;
            font-weight: bolder;
        }

        ol.files {
            list-style-type: none;
            list-style: none;
            padding: 0;
            display: block;
            margin-top: 8px;
        }

        ol.red {
            border: solid 5px #7c1212;
        }

        ol.pink {
            border: solid 5px #f23593;
        }

        ol.orange {
            border: solid 5px #d38704;
        }

        ol.green {
            border: solid 5px #16A085;
        }

        ol.files > li {
            height: 44px;
            line-height: 44px;
            padding-left: 10px;
            border-bottom: solid 1px #ccc;
        }

        ol.files > li:nth-child(even) {
            background: #e9e9e9;
        }

        ol.files > li:nth-child(odd) {
            background: #e7e7e7;
        }

        ol.files > li:hover {
            background: #e2e2e2;
        }

        .btn {
            border: none;
            margin-top: 4px;
            margin-right: 10px;
            float: right;
        }

        strong {
            -webkit-box-shadow: 6px 7px 7px -5px rgba(0, 0, 0, 0.75);
            -moz-box-shadow: 6px 7px 7px -5px rgba(0, 0, 0, 0.75);
            box-shadow: 6px 7px 7px -5px rgba(0, 0, 0, 0.75);
            background: #00FFFF;
            border-radius: 10px 10px 10px 10px;
            -moz-border-radius: 10px 10px 10px 10px;
            -webkit-border-radius: 10px 10px 10px 10px;
            border: 0px solid #000000;
            padding: 5px;
            text-transform: uppercase;

            color: #111;
            font-family: 'Helvetica Neue', sans-serif;
            font-size: 22px;
            font-weight: bold;
            letter-spacing: -1px;
            line-height: 1;
            text-align: center;
        }

        /** Tags Start */
        .tag-green {
            display: inline-block;
            -webkit-box-sizing: content-box;
            -moz-box-sizing: content-box;
            box-sizing: content-box;
            height: 22px;
            position: relative;
            content: "";
            cursor: pointer;
            margin: 0 13px 0 0;
            padding: 10px 28px 10px 20px;
            border: none;
            -webkit-border-radius: 4px 1px 1px 4px;
            border-radius: 4px 1px 1px 4px;
            font: normal 20px/24px "Antic", Helvetica, sans-serif;
            color: rgba(255, 255, 255, 1);
            text-align: center;
            text-transform: uppercase;
            -o-text-overflow: ellipsis;
            text-overflow: ellipsis;
            background: #1abc9c;
            -webkit-box-shadow: 0 5px 0 0 #16a085, 5px 5px 0 0 #16a085;
            box-shadow: 0 5px 0 0 #16a085, 5px 5px 0 0 #16a085;
        }

        .tag-green::before {
            display: inline-block;
            -webkit-box-sizing: content-box;
            -moz-box-sizing: content-box;
            box-sizing: content-box;
            z-index: 1;
            width: 30px;
            height: 30px;
            position: absolute;
            content: "";
            cursor: pointer;
            top: 6px;
            right: -16px;
            border: none;
            -webkit-border-radius: 1px 1px 4px;
            border-radius: 1px 1px 4px;
            font: normal medium/normal Arial, Helvetica, sans-serif;
            color: rgba(255, 255, 255, 0.9);
            -o-text-overflow: clip;
            text-overflow: clip;
            background: #1abc9c;
            -webkit-box-shadow: 0 6px 0 0 #16a085;
            box-shadow: 0 6px 0 0 #16a085;
            text-shadow: none;
            -webkit-transform: rotateY(1deg) rotateZ(-45deg);
            transform: rotateY(1deg) rotateZ(-45deg);
        }

        .tag-green::after {
            display: inline-block;
            -webkit-box-sizing: content-box;
            -moz-box-sizing: content-box;
            box-sizing: content-box;
            z-index: 2;
            width: 12px;
            height: 12px;
            position: absolute;
            content: "";
            cursor: pointer;
            top: 16px;
            right: 0;
            border: none;
            -webkit-border-radius: 10px;
            border-radius: 10px;
            font: normal medium/normal Arial, Helvetica, sans-serif;
            color: rgba(255, 255, 255, 0.9);
            -o-text-overflow: clip;
            text-overflow: clip;
            background: #fcfcfc;
            -webkit-box-shadow: 5px 5px 0 0 #16a085 inset;
            box-shadow: 5px 5px 0 0 #16a085 inset;
            text-shadow: none;
        }

        .tag-red {
            display: inline-block;
            -webkit-box-sizing: content-box;
            -moz-box-sizing: content-box;
            box-sizing: content-box;
            height: 22px;
            position: relative;
            content: "";
            cursor: pointer;
            margin: 0 13px 0 0;
            padding: 10px 28px 10px 20px;
            border: none;
            -webkit-border-radius: 4px 1px 1px 4px;
            border-radius: 4px 1px 1px 4px;
            font: normal 20px/24px "Antic", Helvetica, sans-serif;
            color: rgba(255, 255, 255, 1);
            text-align: center;
            text-transform: uppercase;
            -o-text-overflow: ellipsis;
            text-overflow: ellipsis;
            background: #ba1a1a;
            -webkit-box-shadow: 0 5px 0 0 #7c1212, 5px 5px 0 0 #7c1212;
            box-shadow: 0 5px 0 0 #7c1212, 5px 5px 0 0 #7c1212;
        }

        .tag-red::before {
            display: inline-block;
            -webkit-box-sizing: content-box;
            -moz-box-sizing: content-box;
            box-sizing: content-box;
            z-index: 1;
            width: 30px;
            height: 30px;
            position: absolute;
            content: "";
            cursor: pointer;
            top: 6px;
            right: -16px;
            border: none;
            -webkit-border-radius: 1px 1px 4px;
            border-radius: 1px 1px 4px;
            font: normal medium/normal Arial, Helvetica, sans-serif;
            color: rgba(255, 255, 255, 0.9);
            -o-text-overflow: clip;
            text-overflow: clip;
            background: #ba1a1a;
            -webkit-box-shadow: 0 6px 0 0 #7c1212;
            box-shadow: 0 6px 0 0 #7c1212;
            text-shadow: none;
            -webkit-transform: rotateY(1deg) rotateZ(-45deg);
            transform: rotateY(1deg) rotateZ(-45deg);
        }

        .tag-red::after {
            display: inline-block;
            -webkit-box-sizing: content-box;
            -moz-box-sizing: content-box;
            box-sizing: content-box;
            z-index: 2;
            width: 12px;
            height: 12px;
            position: absolute;
            content: "";
            cursor: pointer;
            top: 16px;
            right: 0;
            border: none;
            -webkit-border-radius: 10px;
            border-radius: 10px;
            font: normal medium/normal Arial, Helvetica, sans-serif;
            color: rgba(255, 255, 255, 0.9);
            -o-text-overflow: clip;
            text-overflow: clip;
            background: #fcfcfc;
            -webkit-box-shadow: 5px 5px 0 0 #7c1212 inset;
            box-shadow: 5px 5px 0 0 #7c1212 inset;
            text-shadow: none;
        }

        .tag-pink {
            display: inline-block;
            -webkit-box-sizing: content-box;
            -moz-box-sizing: content-box;
            box-sizing: content-box;
            height: 22px;
            position: relative;
            content: "";
            cursor: pointer;
            margin: 0 13px 0 0;
            padding: 10px 28px 10px 20px;
            border: none;
            -webkit-border-radius: 4px 1px 1px 4px;
            border-radius: 4px 1px 1px 4px;
            font: normal 20px/24px "Antic", Helvetica, sans-serif;
            color: rgba(255, 255, 255, 1);
            text-align: center;
            text-transform: uppercase;
            -o-text-overflow: ellipsis;
            text-overflow: ellipsis;
            background: #FF69B4;
            -webkit-box-shadow: 0 5px 0 0 #f23593, 5px 5px 0 0 #f23593;
            box-shadow: 0 5px 0 0 #f23593, 5px 5px 0 0 #f23593;
        }

        .tag-pink::before {
            display: inline-block;
            -webkit-box-sizing: content-box;
            -moz-box-sizing: content-box;
            box-sizing: content-box;
            z-index: 1;
            width: 30px;
            height: 30px;
            position: absolute;
            content: "";
            cursor: pointer;
            top: 6px;
            right: -16px;
            border: none;
            -webkit-border-radius: 1px 1px 4px;
            border-radius: 1px 1px 4px;
            font: normal medium/normal Arial, Helvetica, sans-serif;
            color: rgba(255, 255, 255, 0.9);
            -o-text-overflow: clip;
            text-overflow: clip;
            background: #FF69B4;
            -webkit-box-shadow: 0 6px 0 0 #f23593;
            box-shadow: 0 6px 0 0 #f23593;
            text-shadow: none;
            -webkit-transform: rotateY(1deg) rotateZ(-45deg);
            transform: rotateY(1deg) rotateZ(-45deg);
        }

        .tag-pink::after {
            display: inline-block;
            -webkit-box-sizing: content-box;
            -moz-box-sizing: content-box;
            box-sizing: content-box;
            z-index: 2;
            width: 12px;
            height: 12px;
            position: absolute;
            content: "";
            cursor: pointer;
            top: 16px;
            right: 0;
            border: none;
            -webkit-border-radius: 10px;
            border-radius: 10px;
            font: normal medium/normal Arial, Helvetica, sans-serif;
            color: rgba(255, 255, 255, 0.9);
            -o-text-overflow: clip;
            text-overflow: clip;
            background: #fcfcfc;
            -webkit-box-shadow: 5px 5px 0 0 #f23593 inset;
            box-shadow: 5px 5px 0 0 #f23593 inset;
            text-shadow: none;
        }

        .tag-orange {
            display: inline-block;
            -webkit-box-sizing: content-box;
            -moz-box-sizing: content-box;
            box-sizing: content-box;
            height: 22px;
            position: relative;
            content: "";
            cursor: pointer;
            margin: 0 13px 0 0;
            padding: 10px 28px 10px 20px;
            border: none;
            -webkit-border-radius: 4px 1px 1px 4px;
            border-radius: 4px 1px 1px 4px;
            font: normal 20px/24px "Antic", Helvetica, sans-serif;
            color: rgba(255, 255, 255, 1);
            text-align: center;
            text-transform: uppercase;
            -o-text-overflow: ellipsis;
            text-overflow: ellipsis;
            background: #FFA500;
            -webkit-box-shadow: 0 5px 0 0 #d38704, 5px 5px 0 0 #d38704;
            box-shadow: 0 5px 0 0 #d38704, 5px 5px 0 0 #d38704;
        }

        .tag-orange::before {
            display: inline-block;
            -webkit-box-sizing: content-box;
            -moz-box-sizing: content-box;
            box-sizing: content-box;
            z-index: 1;
            width: 30px;
            height: 30px;
            position: absolute;
            content: "";
            cursor: pointer;
            top: 6px;
            right: -16px;
            border: none;
            -webkit-border-radius: 1px 1px 4px;
            border-radius: 1px 1px 4px;
            font: normal medium/normal Arial, Helvetica, sans-serif;
            color: rgba(255, 255, 255, 0.9);
            -o-text-overflow: clip;
            text-overflow: clip;
            background: #FFA500;
            -webkit-box-shadow: 0 6px 0 0 #d38704;
            box-shadow: 0 6px 0 0 #d38704;
            text-shadow: none;
            -webkit-transform: rotateY(1deg) rotateZ(-45deg);
            transform: rotateY(1deg) rotateZ(-45deg);
        }

        .tag-orange::after {
            display: inline-block;
            -webkit-box-sizing: content-box;
            -moz-box-sizing: content-box;
            box-sizing: content-box;
            z-index: 2;
            width: 12px;
            height: 12px;
            position: absolute;
            content: "";
            cursor: pointer;
            top: 16px;
            right: 0;
            border: none;
            -webkit-border-radius: 10px;
            border-radius: 10px;
            font: normal medium/normal Arial, Helvetica, sans-serif;
            color: rgba(255, 255, 255, 0.9);
            -o-text-overflow: clip;
            text-overflow: clip;
            background: #fcfcfc;
            -webkit-box-shadow: 5px 5px 0 0 #d38704 inset;
            box-shadow: 5px 5px 0 0 #d38704 inset;
            text-shadow: none;
        }

        /** Tags End */

        .btn-3d {
            display: inline-block;
            -webkit-box-sizing: content-box;
            -moz-box-sizing: content-box;
            box-sizing: content-box;
            cursor: pointer;
            padding: 0 20px;
            border: none;
            -webkit-border-radius: 20px;
            border-radius: 20px;
            font: normal normal bold 20px/30px "Antic", Helvetica, sans-serif;
            color: black;
            -o-text-overflow: clip;
            text-overflow: clip;
            background: rgb(221, 221, 221);
            -webkit-box-shadow: 0 1px 0 0 rgb(119, 119, 119), 0 2px 0 0 rgb(119, 119, 119), 0 3px 0 0 rgb(119, 119, 119), 0 4px 0 0 rgb(119, 119, 119), 0 5px 0 0 rgb(119, 119, 119), 0 6px 0 0 rgb(119, 119, 119), 0 0 5px 0 rgba(0, 0, 0, 0.0980392), 0 1px 3px 0 rgba(0, 0, 0, 0.298039), 0 3px 5px 0 rgba(0, 0, 0, 0.2), 0 5px 10px 0 rgba(0, 0, 0, 0.247059), 0 10px 10px 0 rgba(0, 0, 0, 0.2), 0 20px 20px 0 rgba(0, 0, 0, 0.14902);
            box-shadow: 0 1px 0 0 rgb(119, 119, 119), 0 2px 0 0 rgb(119, 119, 119), 0 3px 0 0 rgb(119, 119, 119), 0 4px 0 0 rgb(119, 119, 119), 0 5px 0 0 rgb(119, 119, 119), 0 6px 0 0 rgb(119, 119, 119), 0 0 5px 0 rgba(0, 0, 0, 0.0980392), 0 1px 3px 0 rgba(0, 0, 0, 0.298039), 0 3px 5px 0 rgba(0, 0, 0, 0.2), 0 5px 10px 0 rgba(0, 0, 0, 0.247059), 0 10px 10px 0 rgba(0, 0, 0, 0.2), 0 20px 20px 0 rgba(0, 0, 0, 0.14902);
            text-shadow: 0 1px 0 #FFFFFF;
        }

        .btn-3d:hover {
            background: #FFFFFF;
        }

        .btn-3d:active {
            margin: 6px 0 0;
            background: rgb(221, 221, 221);
            -webkit-box-shadow: 0 -1px 10px 0 rgba(0, 0, 0, 0.247059), 0 4px 10px 0 rgba(0, 0, 0, 0.2), 0 14px 20px 0 rgba(0, 0, 0, 0.14902);
            box-shadow: 0 -1px 10px 0 rgba(0, 0, 0, 0.247059), 0 4px 10px 0 rgba(0, 0, 0, 0.2), 0 14px 20px 0 rgba(0, 0, 0, 0.14902);
        }

        .btn-3d:focus {
            background: rgb(221, 221, 221);
        }

        #tooltip {
            border: 1px solid #BFBFBF;
            float: left;
            font-size: 12px;
            padding: 1em 1em 1em 1em;
            position: absolute;
            z-index: 999;
        }

        .rounded {
            -moz-border-radius: 3px;
            -webkit-border-radius: 3px;
        }

        a {
            text-decoration: none;
            color: #000;
        }

        .compare-table {
            border: solid 1px #333;
            position: absolute;
            top: 25px;
            right: 25px;
            background: #e9e9e9;
            display: none;
            padding: 6px;

            -webkit-box-shadow: 3px 3px 18px 3px rgba(2, 11, 17, 0.91);
            box-shadow: 3px 3px 18px 3px rgba(2, 11, 17, 0.91);
        }

        .compare-table th {
            float: right;
        }

        a.config {
            position: absolute;
            top: 4px;
            right: 4px;
        }

        /* The alert message box */
        .alert {
            padding: 20px;
            background-color: #f44336;
            color: white;
            opacity: 0.83;
            transition: opacity 0.6s;
            margin-bottom: 15px;
        }
        .alert.success {
            background-color: #4CAF50;
        }
        .alert.info {
            background-color: #2196F3;
        }
        .alert.warning {
            background-color: #ff9800;
        }

        /* The close button */
        .closebtn {
            margin-left: 15px;
            color: white;
            font-weight: bold;
            float: right;
            font-size: 22px;
            line-height: 20px;
            cursor: pointer;
            transition: 0.3s;
        }

        /* When moving the mouse over the close button */
        .closebtn:hover {
            color: black;
        }

        div.tools{
            float:right; margin-right: 8px;
        }

        /*div.tools i{*/
            /*margin-top: 8px;*/
        /*}*/
        div.tools ul{
            display: block;
            background: #0c0c0c;
            margin: 0;
            padding: 0;
        }
        div.tools ul li{
            display: block;
            float: left;
            margin: 0;
            padding: 0;
            width: 34px;
        }
        div.tools ul li:hover{
            background: none;
        }
        div.tools li a{
            display: inline-block;
            padding-top: 12px;
        }
        div.tools li a i:hover{
            color: #009DFF;
        }


        #remote-only-files .tools i{
            color: #FFA500;
            font-size: 20px;
            padding: 0 10px;
        }

    </style>

    <link async href="http://fonts.googleapis.com/css?family=Antic" data-generated="http://enjoycss.com"
          rel="stylesheet"
          type="text/css"/>

</head>
<body>


<div id="loader">
    <span>Please wait...</span>
</div>

<div class="container">

    <h2>
        COMPARISON BETWEEN
        <a href="#" class="tooltip" title="<?php echo __DIR__; ?>">SOURCE </a>
        &
        <a href="#" class="tooltip" title="<?php echo $remoteConfig['remotePath']; ?>">REMOTE</a>
    </h2>

    <a href="#" class="config"><i class="fa fa-cog" aria-hidden="true" style="font-size: 22px;"></i></a>
    <table class="compare-table">
        <tr>
            <th>Source Path:</th>
            <td><?php echo __DIR__; ?></td>
        </tr>
        <tr>
            <th>Remote Path:</th>
            <td><?php echo $remoteConfig['remotePath']; ?></td>
        </tr>
        <tr>
            <th>Scan List:</th>
            <td>
                <ul>
                    <?php foreach ($remoteConfig['scanlist'] as $row) { ?>
                        <li><?php echo $row; ?></li>
                    <?php } ?>
                </ul>
            </td>
        </tr>
        <tr>
            <th>Ignore List:</th>
            <td>
                <ul>
                    <?php foreach ($remoteConfig['ignorelist'] as $row) { ?>
                        <li><?php echo $row; ?></li>
                    <?php } ?>
                </ul>
            </td>
        </tr>
    </table>

    <div class="row">
        <div class="alert" style="display: none;">
            <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
            <p></p>
        </div>

        <?php if ($files_different): ?>
            <div class="tag-red difference">
                <i class="fa fa-expand" aria-hidden="true"></i>
                Following files have difference ( <span class="qty"><?php echo count($files_different) ?></span> )
            </div>

            <ol class="files difference-files red">
                <?php foreach ($files_different as $file): ?>
                    <li>
                        <span><?php echo $file; ?></span>
                        <div class="tools">
                            <ul>
                                <li><a href="#" class="btn-download" title="Download from remote"><i class="fa fa-download" aria-hidden="true"></i></a></li>
                                <li><a href="#" class="btn-upload" title="Upload to remote"><i class="fa fa-upload" aria-hidden="true"></i></a></li>
                                <li><a href="#" class="btn-copy" title="Copy to clipboard"><i class="fa fa-clipboard" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </li>
                <?php endforeach; ?>
            </ol>
        <?php endif; ?>
    </div>

    <hr>

    <div class="row">
        <?php if ($files_new_at_source): ?>
            <div class="tag-pink source-only">
                <i class="fa fa-expand" aria-hidden="true"></i>
                New files at source ( <span class="qty"><?php echo count($files_new_at_source) ?></span> )
            </div>
            <ol class="files source-only-files pink">
                <?php foreach ($files_new_at_source as $file): ?>
                    <li>
                        <span><?php echo $file; ?></span>
                        <div class="tools">
                            <ul>
                                <li><a href="#" class="btn-upload" title="Upload to remote"><i class="fa fa-upload" aria-hidden="true"></i></a></li>
                                <li><a href="#" class="btn-copy" title="Copy to clipboard"><i class="fa fa-clipboard" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </li>
                <?php endforeach; ?>
            </ol>
        <?php endif; ?>
    </div>

    <hr>

    <div class="row">
        <?php if ($files_new_at_remote): ?>
            <div class="tag-orange remote-only">
                <i class="fa fa-compress" aria-hidden="true"></i>
                New files at source ( <span class="qty"><?php echo count($files_new_at_remote) ?></span> )
            </div>
            <ol class="files remote-only-files orange" style="display: none;">
                <?php foreach ($files_new_at_remote as $file): ?>
                    <li>
                        <span><?php echo $file; ?></span>
                        <div class="tools">
                            <ul>
                                <li><a href="#" class="btn-upload" title="Upload to remote"><i class="fa fa-upload" aria-hidden="true"></i></a></li>
                                <li><a href="#" class="btn-copy" title="Copy to clipboard"><i class="fa fa-clipboard" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </li>
                <?php endforeach; ?>
            </ol>
        <?php endif; ?>
    </div>

    <hr>

    <div class="row">
        <?php if ($files_matched): ?>
            <div class="tag-green matched">
                <i class="fa fa-compress" aria-hidden="true"></i>
                Following files are same ( <span class="qty"><?php echo count($files_matched) ?></span> )
            </div>
            <ol class="files matched-files green" style="display: none;">
                <?php foreach ($files_matched as $file): ?>
                    <li>
                        <span><?php echo $file; ?></span>
                        <div class="tools">
                            <ul>
                                <li><a href="#" class="btn-copy" title="Copy to clipboard"><i class="fa fa-clipboard" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </li>
                <?php endforeach; ?>
            </ol>
        <?php endif; ?>
    </div>

</div><!-- .container -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://use.fontawesome.com/3dc9168e7c.js"></script>
<script type="text/javascript">
    /**
     * A tooltip plugin taken from Jeffry-Way tutorial.
     */
    (function ($) {
        $.fn.tooltip = function (options) {

            var
                defaults = {
                    background: '#e3e3e3',
                    color: 'black',
                    rounded: false
                },
                settings = $.extend({}, defaults, options);

            this.each(function () {
                var $this = $(this);
                var title = this.title;

                if ($this.is('a') && $this.attr('title') != '') {
                    this.title = '';
                    $this.hover(function (e) {
                        // mouse over
                        $('<div id="tooltip" />')
                            .appendTo('body')
                            .text(title)
                            .hide()
                            .css({
                                backgroundColor: settings.background,
                                color: settings.color,
                                top: e.pageY + 10,
                                left: e.pageX + 20
                            })
                            .fadeIn(350);

                        if (settings.rounded) {
                            $('#tooltip').addClass('rounded');
                        }
                    }, function () {
                        // mouse out
                        $('#tooltip').remove();
                    });
                }

                $this.mousemove(function (e) {
                    $('#tooltip').css({
                        top: e.pageY + 10,
                        left: e.pageX + 20
                    });
                });
            });
            // returns the jQuery object to allow for chainability.
            return this;
        }
    })(jQuery);
</script>
<script type="text/javascript">
    $(function () {

        $('a.tooltip').tooltip({
            rounded: true
        });

        /**
         * Copy the current file path into clipboard
         */
        $('.btn-copy').click(function (e) {
            e.preventDefault();

            // Change the row color which has been copied just to remember
            $(this).parents('li').css({'background': '#D0FDDC', 'border-bottom': '1px solid #e9e9e9'});

            // Copy the text into clipboard
            var $temp = $("<input>");
            $("body").append($temp);
            $temp.val($(this).parents('li').find('span').text()).select();
            document.execCommand("copy");
            $temp.remove();
        });

        /**
         * Upload file to the remote-location
         */
        $('.btn-upload').click(function (e) {
            e.preventDefault();

            var sourceUrl = '<?php echo $sourceFile; ?>';
            var el = $(this);
            var file = el.parents('li').find('span').text();

            $('#loader').fadeIn('fast');

            $.ajax({
                url: sourceUrl,
                method: 'POST',
                data: {action: 'upload', file: file},
                dataType: 'json',
                success: function (response) {
                    console.log('response ', response);

                    if (response.response === true) {
                        el.parents('li').fadeOut();

                        var qty = el.parents('div.row').find('span.qty').text();
                        el.parents('div.row').find('span.qty').text(qty - 1);

                    } else {
                        $('.alert').fadeIn();
                        $('.alert p').html(response.msg);
                    }
                },
                complete: function() {
                    $('#loader').fadeOut('fast');
                }
            });

        });

        $('.btn-download').click(function (e) {
            e.preventDefault();
//            alert('Be patient, feature is coming soon');
//            return false;

            $('#loader').fadeIn('fast');

            var sourceUrl = '<?php echo $sourceFile; ?>';
            var el = $(this);
            var file = el.parents('li').find('span').text();
//            console.log(file);

            $.ajax({
                url: sourceUrl,
                method: 'POST',
                data: {action: 'download', file: file},
                dataType: 'json',
                success: function (response) {
                    console.log('response ', response);

                    if (response.response === true) {
                        el.parents('li').fadeOut();

                        var qty = el.parents('div.row').find('span.qty').text();
                        el.parents('div.row').find('span.qty').text(qty - 1);

                    } else {
                        $('.alert').fadeIn();
                        $('.alert p').html(response.msg);
                    }
                },
                complete: function() {
                    $('#loader').fadeOut('fast');
                }
            });

        });

        $('.difference').click(function () {
            $(this).children('i').toggleClass('fa-expand fa-compress');
            $('.difference-files').toggle();
            $('html, body').animate({
                scrollTop: $(".difference-files").offset().top
            }, 100);
        });
        $('.source-only').click(function () {
            $(this).children('i').toggleClass('fa-expand fa-compress');
            $('.source-only-files').toggle();
            $('html, body').animate({
                scrollTop: $(".source-only-files").offset().top
            }, 100);
        });
        $('.remote-only').click(function () {
            $(this).children('i').toggleClass('fa-expand fa-compress');
            $('.remote-only-files').toggle();
            $('html, body').animate({
                scrollTop: $(".remote-only-files").offset().top
            }, 100);
        });
        $('.matched').click(function () {
            $(this).children('i').toggleClass('fa-expand fa-compress');
            $('.matched-files').toggle();
            $('html, body').animate({
                scrollTop: $(".matched-files").offset().top
            }, 100);
        });

        $('.config').click(function (e) {
            e.preventDefault();
            $('.compare-table').fadeToggle();
        });

        $(".btn-toggle-all").click(function(e) {
            e.preventDefault();

            $(this).children().toggleClass('fa-toggle-on fa-toggle-off');

            var checkBoxes = $('#remote-only-files input[type="checkbox"]');
            checkBoxes.prop("checked", !checkBoxes.prop("checked"));
        });

    });
</script>

</body>
</html>
