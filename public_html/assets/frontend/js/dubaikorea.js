var main = 
{
    baseUrl: location.protocol + "//" + location.hostname + (location.port && ":" + location.port) + "/",

    /* Show Loader */
	showLoader: function () {
		$("#loading").css('display', 'table');
		return false;
	},

	/* Hide Loader */
	hideLoader: function () {
		$("#loading").fadeOut();
		return false;
	},

	/* Loading */
	loadingTo: function (speed) {
		var speed = speed || 20000;
		$("#loading").css('display', 'table');

		setTimeout(function () {
		$("#loading").fadeOut();
		}, speed);

		return false;
	},

    to_get_cookie: function(name)
    {
        var value = "; " + document.cookie;
        var parts = value.split("; " + name + "=");
        if (parts.length == 2) return parts.pop().split(";").shift();
    },

    to_scroll_to: function (div, offset, speed, callback)
    {
	    var speed  = speed || 'slow';
	    var offset = offset || '0'; // Default offset of header if any.
	    var callback = callback || function(){};
	    if ( $(div).length > 0 ) {
	        $('html, body').stop().animate({ scrollTop: ($(div).offset().top - offset) }, speed, 'swing', callback);
	        return $(div);
	    }
	    return false;
	},

    to_validate_email: function (email) 
    {
	    var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	    return re.test(email);
	},

	to_validate_phone: function (phone) {
	    var re = /[0-9-+]+$/;
	    return re.test(phone);
	},

	to_validate_post_code: function (postCode) {
	    var re = /[0-9-+]+$/;
	    return re.test(postCode);
	},

	to_validate_url: function(url) {
	    var urlregex = new RegExp(
	            "^(http|https|ftp)\://([a-zA-Z0-9\.\-]+(\:[a-zA-Z0-9\.&amp;%\$\-]+)*@)*((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])|([a-zA-Z0-9\-]+\.)*[a-zA-Z0-9\-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(\:[0-9]+)*(/($|[a-zA-Z0-9\.\,\?\'\\\+&amp;%\$#\=~_\-]+))*$");
	    return urlregex.test(url);
	},

	to_convert_fileto_base64: function(avatorId) {
	    filesSelected = document.getElementById("avatar").files;
	    if (filesSelected.length > 0) {
	        var fileToLoad = filesSelected[0];
	        var fileReader = new FileReader();

	        fileReader.onload = function(fileLoadedEvent) {
	            var srcData = fileLoadedEvent.target.result; // <--- data: base64
	            document.getElementById(avatorId).value = srcData.substr( srcData.indexOf('base64,')+7 );
	        }
	        fileReader.readAsDataURL(fileToLoad);
	    }
	},

	inputValidation: function( formSelector, errorClass ) {
	 
	    errorClass = errorClass || 'alert-error';

	    var errors = [];
	    $.each( formSelector, function(index, selector) {
	        $(selector).removeClass( errorClass );
	        $(selector).css( 'border', 'none' );

	        if ( $.trim($(selector).val()) == '' ) {
	            // $(selector).addClass( errorClass );
	            $(selector).css( 'border', '1px solid #c00' );
	            errorText = $(selector).data('title') || $(selector).attr('title') || $(selector).attr('placeholder');
	            errors.push( errorText );
	        }
	    });

	    return errors;
	},

	to_credit_card_type_from_number: function( num )
	{

	    // Sanitise number
	    num = num.replace(/[^\d]/g,'');

	    var regexps = {
	        'mastercard' : /^5[1-5][0-9]{5,}$/,
	        'visa' : /^4[0-9]{6,}$/,
	        'amex' : /^3[47][0-9]{5,}$/,
	        'discover' : /^6(?:011|5[0-9]{2})[0-9]{3,}$/,
	        'diners' : /^3(?:0[0-5]|[68][0-9])[0-9]{4,}$/,
	        'jcb' : /^(?:2131|1800|35[0-9]{3})[0-9]{3,}$/,
	        'unknown' : /.*/,
	    };

	    for( var card in regexps ) {
	        if ( num.match( regexps[ card ] ) ) {
	            // console.log( card );
	            $( '.cardsacceptedicon' ).removeClass( 'active' );
	            $( '.cardsacceptedicon.' + card ).removeClass( 'active' );
	            $( 'div.ccvalue' ).html( card );
	            return card;
	        }
	    }
	},

	to_credit_card_image_by_name: function(name)
	{
		var image_path = 'unknown_card.png';
		
		if ( name == 'visa' )
		{
			image_path = "visa.png";
		}
		else if ( name == 'mastercard' )
		{
			image_path = "master-card-blue.png";
		}
		else if ( name == 'jcb' )
		{
			image_path = "jcb.png";
		}
		else if ( name == 'discover' )
		{
			image_path = "discover.png";
		}
		else if ( name == 'diners' )
		{
			image_path = "diners-club-international.png";
		}
		else if ( name == 'amex' )
		{
			image_path = "amex_american_express.png";
		}

		return image_path;
	}
};


$('.validate_of_card_number').on('keyup', function(){
	
	var card_number 	 = $(this).val();
	var verify_card_name = main.to_credit_card_type_from_number( card_number );
	var display_of_card  = main.to_credit_card_image_by_name( verify_card_name );
	$(this).next().attr('src', main.baseUrl + 'storage/cards/' + display_of_card);

});

$('.payment_type').on('change', function(){

	if ( "credit_card" == $(this).val() )
	{
		// $("input[name='payment[card_number]'], input[name='payment[card_month]'], input[name='payment[card_year]'], input[name='payment[card_verification]']").addClass('required-field');
		// $('.payment_method').show();
	}
	else
	{
		// $("input[name='payment[card_number]'], input[name='payment[card_month]'], input[name='payment[card_year]'], input[name='payment[card_verification]']").removeClass('required-field');
		// $('.payment_method').hide();
	}
});

$('#billing_copy_to_shipping').on('change', function(){
	
	if ( $(this).prop('checked') )
	{
		$('#shipping_first_name').val( $('#bill_first_name').val() );
		$('#shipping_last_name').val( $('#bill_last_name').val() );
		$('#shipping_company_name').val( $('#bill_company_name').val() );
		$('#shipping_email_address').val( $('#bill_email_address').val() );
		$('#shipping_address').val( $('#bill_address').val() );
		$('#shipping_country').val( $('#bill_country').val() );
		$('#shipping_state').val( $('#bill_state').val() );
		$('#shipping_city').val( $('#bill_city').val() );
		$('#shipping_postal_code').val( $('#bill_postal_code').val() );
		$('#shipping_telephone').val( $('#bill_telephone').val() );
		$('#shipping_fax').val( $('#bill_fax').val() );
	}
	else
	{
		$('#shipping_first_name, #shipping_last_name, #shipping_company_name, #shipping_email_address, #shipping_address, #shipping_country, #shipping_state, #shipping_city, #shipping_postal_code, #shipping_telephone, #shipping_fax').val('');
	}
});

$("#checkout_form_id").submit(function(e) {

	var submit = true;

	$this = $(this);
	$('.alertMSG').hide();

	// Check for empty values first
	var errorInputs = main.inputValidation( $this.find('.required-field'));
	if (errorInputs.length > 0) {
	    var errorHTML = $.map(errorInputs, function(v) {
	        return v + ' cannot be left empty.';
	    }).join('<br />');

	    main.to_scroll_to( $('#checkout_form_id'), 30 );
	    submit = false;
	}

	if ( false == submit ) {
	    e.preventDefault();
	}

	return submit;
});

$("#whole_seller_form_id").submit(function(e) {

	var submit = true;

	$this = $(this);
	$('.alertMSG').hide();

	// Check for empty values first
	var errorInputs = main.inputValidation( $this.find('.required-field'));
	if (errorInputs.length > 0) {
	    var errorHTML = $.map(errorInputs, function(v) {
	        return v + ' cannot be left empty.';
	    }).join('<br />');

	    $("#whole_seller_form_model").animate({ scrollTop: 0 }, 'slow');
	    submit = false;
	}

	if ( false == submit ) {
	    e.preventDefault();
	}

	return submit;
});

$("#topHeaderSeachBar").submit(function(e) {

	$this = $(this);
	
	// Check for empty values first
	var errorInputs = main.inputValidation( $this.find('.required-field'));
	if (errorInputs.length > 0) {
		return false;
	}

	window.location = main.baseUrl + 'search/' + $this.find('select[name=category_id]').val() + '/' + $this.find('input[name=title]').val();
	return false;
});

$("#topResonsiveHeaderSeachBar").submit(function(e) {

	$this = $(this);
	
	// Check for empty values first
	var errorInputs = main.inputValidation( $this.find('.required-field'));
	if (errorInputs.length > 0) {
		return false;
	}

	window.location = main.baseUrl + 'search/all/' + $this.find('input[name=title]').val();
	return false;
});

$("#getSubscriberNow").submit(function(event) {
	
	$this = $(this);
	
	// Check for empty values first
	var errorInputs = main.inputValidation( $this.find('.required-field'));
	if (errorInputs.length > 0) {
		return false;
	}

	// alert( $this.find('.required-field').val() );
	if ( false == main.to_validate_email($this.find('.required-field').val()) )
	{
		$this.find('.required-field').css( 'border', '1px solid #c00' );
		return false;
	}

	return true;
});