<?php
/**
 * @author: Muhammad Khalil
 * @created_at May 22, 2016
 */


// Scan List of dirs and files
$scanList = array(
    '.', // root
//     'app', // specific folder
);

// Ignore List of dirs and files
$ignoreList = array(
    'cgi-bin',
    '.',
    '..',
    '._',
    './.DS_Store',
    './.idea',
    './.gitignore',
    './fileDiff.php',
    './compare-source.php',
    './TODO',
    './CI_phpStorm.php',
    './.gitattributes',
    './system',
    './application/logs',
    './application/cache',
    './application/third_party/stripe-php/.git',
    './images/brands',
    './errorlog.txt',
    //'./assets',
);

/******************* FOLLOWING CODE DOES NOT NEED TO BE CHANGED *******************/
require 'compare/Helper.php';
require 'compare/Compare.php';

// Scan Remote
$compare = new Compare();
$compare->setScanList( $scanList );
$compare->setIgnoreList( $ignoreList );

// Upload file
if ( isset( $_FILES[ 'file' ][ 'tmp_name' ] ) ) {

    // $path = 'uploads/' . $_FILES['file']['name'];
    $path = $_POST[ 'path' ];
    if ( file_exists( $path ) ) {
        unlink( $path );
    }

    move_uploaded_file( $_FILES[ 'file' ][ 'tmp_name' ], $path );
    exit;
}

// Output Remote Config
if ( isset( $_GET[ 'json' ] ) && $_GET[ 'request' ] == 'config' ) {
    header( 'Content-type: application/json' );
    echo json_encode( array(
        'scanlist'      => $compare->getScanList(),
        'ignorelist'    => $compare->getIgnoreList(),
        'remotePath'    => __DIR__,
        'baseUrl'       => "http://$_SERVER[SERVER_NAME]",
        'remoteUrlFull' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
        'remoteUrl'     => "http://" . $_SERVER[ "HTTP_HOST" ] . parse_url( $_SERVER[ 'REQUEST_URI' ], PHP_URL_PATH ),
    ) );
    exit;
}

echo json_encode( $compare->scanner() );
