<?php
define('PROJECT_TITLE', 'Dubai Korea');

error_reporting(-1);
ini_set('display_errors', 1);

mysql_connect( 'localhost', 'faq12fm3_korea', 'Admin@123', mysql_select_db('faq12fm3_dubaikorea'));


$result = mysql_query('select * from dc_categories') or die(mysql_error());

print_r($result); exit();


$php_version = 5.3;

$functions = array(
    'ini_get',
    'ini_set',
    'date_default_timezone_get',
    'mysqli_connect',
    'mysql_connect',
    'curl_init',
    'fsockopen',
    'stream_context_set_option',
);
$classes = array(
    'DateTime',
    'SimpleXMLElement',
);

$results = array();

# PHP VERSION
$results[] = 'PHP VERSION: ' . "<strong>$php_version</strong> <= " . ((version_compare( PHP_VERSION, $php_version ) >= 0) ? '<span class="passed">PASSED</span>' : '<span class="failed">FAILED</span>');

foreach ( $classes as $class ) {
    $results[] = 'CLASS <u>'.$class.'</u>: ' . (class_exists($class) ? '<span class="passed">PASSED</span>' : '<span class="failed">FAILED</span>');
}

foreach ( $functions as $function ) {
    $results[] = 'FUNCTION <u>'.$function.'</u>: ' . (function_exists($function) ? '<span class="passed">PASSED</span>' : '<span class="failed">FAILED</span>');
}

?><!DOCTYPE html>
<html>
<head>
    <title><?php echo PROJECT_TITLE; ?> - Compatibility Checker</title>
    <style type="text/css">
    body {
        font-family: monospace;
    }
    span.passed {
        color: green;
        font-weight: bold;
    }
    span.failed {
        color: red;
        font-weight: bold;
    }
    </style>
</head>
<body>

<?php
echo implode( "\n<br />", $results );
?>


</body>
</html>