<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @author: Junaid Ahmed
 * @package: Dubaikorea
 */

class Public_Controller extends Master_Controller {
    
    const alertMSGKey = 'alert';

    /**
     *  --------------------------------------
     *  Project configuration goes here
     *  --------------------------------------
     */
    public $primaryDomain = PRIMARY_DOMAIN;

    /**
     *  @var array $userLogin        Self explanatory.
     */
    public $auth = [];

    /**
     * Constructor
     */
    public function __construct() {

        parent::__construct();

        $this->load->helper('frontend');
        $this->load->model('productmodel');

        $this->load->library('cart');

        $this->auth = $this->session->userdata('auth');
    }

    public function loadTemplate( $view_name, $data = [] ) 
    {
        $this->load->library('cart');

        $categories = $this->productmodel->getAllCategories([
            'sortBy' => 'id',
            'orderBy' => 'DESC',
            'limit' => 100
        ]);
        if ( count($categories) )
        {
            $categories = buildTree($categories);
        }

        $brands = $this->triggermodel->all(TBL_BRANDS, [
            'select' => 'id, title, slug',
            'where' => [
                'active' => 1
            ],
            'orderBy' => 'DESC'
        ]);
        // rd($brands);

        $data['categories']         = $categories;
        $data['brands']             = $brands;
        $data['currencies']         = $this->currencies;
        $data['currency_active']    = $this->session->userdata('currency');
        $data['wishlist_in']        = $this->getCustomerWishlist();

        $idata = [
            'template' => APPPATH.'views/frontend/'.$view_name.'.php',
            'categories' => $categories
        ];

        $push = array_merge($data, $idata);

        $this->load->view( 'frontend/_layout/app.php', $push);
    }


     /**
     * Check wether user is logged in or not
     */
    public function isUserLogin() {
        return (bool) $this->session->userdata('logged_in');
    }

    /**
     * Extract Logged-in User Id
     */
    public function getUserId() {
        return $this->isUserLogin() ? $this->auth()->id : 0;
    }

    public function auth() {
        return $this->isUserLogin() ? $this->session->userdata('auth') : null;
    }

    public function getProperty( $property='' ) {
        return isset($this->{$property}) ? $this->{$property} : false;
    }

    public function getCustomerWishlist()
    {

        if ( $this->isUserLogin() )
        {
            $this->load->model('customermodel');
            return $this->customermodel->getCustomerWishlistProductIds();
        }

        return [];
    }


    protected function jhead() {
        header('Content-type: application/json');
    }

    protected function json($msg='', $status=true) {
        $data = array();
        $data['status'] = ($status===true) ? true : false;
        $message_key = ($status) ? 'data' : 'message';
        if ($msg!='') $data[ $message_key ] = $msg;
        echo json_encode( $data );
        return;
    }

}

/* End of file Public_Controller.php */
/* Location: ./application/core/Public_Controller.php */