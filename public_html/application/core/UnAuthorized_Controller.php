<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class UnAuthorized_Controller extends Master_Controller {


     /**
     *  @var array $header_assets        All css's file on header goes here..
     */
    public $header_assets = [];

    /**
     *  @var array $footer_assets        All Javascripts file on footer goes here..
     */
    public $footer_assets = [];

    /**
     *  @var array $footerJS        All Javascripts on footer goes here..
     */
    public $footerJS = '';
    
   
    protected $displayRecordListing = true;


    public function __construct() {

        parent::__construct();

        $this->load->helper(['form']);
    }

    public function loadLoginTemplate( $view_name, $data = [] ) {

        $template = [
            'template' => APPPATH.'views/backend/default/'.$view_name.'.php'
        ];

        $push = array_merge($data, $template);

        $this->load->view( 'backend/default/_layout/login-app.php', $push);
    }

    protected function isLoggedIn() {
        return $this->session->userdata('logged_in');
    }

    protected function checkLogin() {
        
        if( $this->isLoggedIn() )
        {
            redirect( link_to_backend('dashboard') );
        }
    }

}