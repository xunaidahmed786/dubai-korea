<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @author: Ehsaan Khatree
 * @description Web-Services
 * @package: Gift Card Bid
 */

class Service_Controller extends Master_Controller {
    /**
     * Define response type of success or false result
     */
    const SUCCESS_RETURN = TRUE;
    const FAIL_RETURN    = FALSE;

    /**
     *  --------------------------------------
     *  Project intial configuration goes here
     *  --------------------------------------
     *
     *  @var string $primaryDomain      Define primary domain here.
     */
    public $primaryDomain = PRIMARY_DOMAIN;

    /**
     *  @var array $userLogin        Self explanatory.
     */
    public $userLogin = array();

    /**
     * @param boolean isLogging TRUE/FALSE to enable disable logging
     */
    protected $isLogging = FALSE;

    const LOG_DIR        = 'application/logs/'; // Directory path with trailing slash.

    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct();

    }

    protected function jhead() {
        header('Content-type: application/json');
    }

    protected function json($msg='', $status=true) {
        $data = array();
        $data['status'] = ($status===true) ? self::SUCCESS_RETURN : self::FAIL_RETURN;
        $message_key = ($status) ? 'data' : 'message';
        if ($msg!='') $data[ $message_key ] = $msg;
        $this->json_encode( $data );
        return;
    }

    protected function setLoggingStatus($status) {
        $this->isLogging = (bool) $status;
    }

    protected function json_encode($value='') {
        $json = json_encode( $value );

        if ( TRUE === $this->isLogging ) {
            $this->myLogger(array(
                'log'   =>  'post|data',
                'data'  =>  $json
            ));
        }

        echo $json;
        return;
    }

    /*
     * Logging method Custom
     * Made for CODI environment, can be use in flat but need to change log directory path as well.
     */
    protected function myLogger($vars=array()) {
        date_default_timezone_set('Asia/Karachi');
        $filename = 'log_' . date('d-m-y') . '.txt';
        $append = true;
        $log = 'get|post'; // URL will also log (Supported: get, post, file, server)
        extract($vars);

        // Configure data to log file
        $data_log_array = explode('|', $log);
        $data_log = 'Time: ' . date("F j, Y, g:i a") . "\n";
        $data_log .= 'URL: ' . 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        $data_log .= (in_array('get', $data_log_array)) ? "\n" . 'GET: ' . ( (count($_GET)==0) ? 'Empty' : print_r($_GET, true) ) : '';
        $data_log .= (in_array('post', $data_log_array)) ? "\n" . 'POST: ' . ( (count($_POST)==0) ? 'Empty' : print_r($_POST, true) ) : '';
        $data_log .= (in_array('file', $data_log_array) || in_array('files', $data_log_array)) ? "\n" . 'FILES: ' . ( (count($_FILES)==0) ? 'Empty' : print_r($_FILES, true) ) : '';
        $data_log .= (in_array('data', $data_log_array)) ? "\n" . 'Output: ' . gettype($data) . ', ' . ( is_bool($data) ? var_export($data, true) : $data ) : '';
        $data_log .= (in_array('server', $data_log_array)) ? "\n" . 'SERVER: ' . ( (count($_SERVER)==0) ? 'Empty' : print_r($_SERVER, true) ) : '';
        $data_log .= (in_array('ip', $data_log_array)) ? "\n" . 'IP: ' . $_SERVER['REMOTE_ADDR'] : '';
        $data_log .= (in_array('useragent', $data_log_array) || in_array('user_agent', $data_log_array)) ? "\n" . 'Useragent: ' . $_SERVER['HTTP_USER_AGENT'] : '';

        // Write log file
        if ($append === true) {
            file_put_contents(self::LOG_DIR . $filename, $data_log . "\n" . str_repeat("=", 20) . "\n\n", FILE_APPEND);
        } else {
            file_put_contents(self::LOG_DIR . $filename,  $data_log . "\n" . str_repeat("=", 20) . "\n" . file_get_contents(self::LOG_DIR . $filename));
        }
    }

}

/* End of file Service_Controller.php */
/* Location: ./application/core/Service_Controller.php */