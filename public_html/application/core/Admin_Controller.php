<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_Controller extends Master_Controller {
   
    public $per_page                    = '10';
    public $permissionContainer         = [];
    protected $user_data                = [];


    public function __construct() {

        parent::__construct();
        
        $this->checkAdminLogin();

        $this->load->helper(['admin', 'form']);
    }

    public function loadTemplate( $view_name, $data = [] ) 
    {

        $template = [
            'template' => APPPATH.'views/backend/default/'.$view_name.'.php'
        ];

        $push = array_merge($data, $template);

        $this->load->view( 'backend/default/_layout/app.php', $push);
    }

    protected function isAdminLoggedIn() {
        return $this->session->userdata('logged_in');
    }

    protected function isAuth() {
        return $this->session->userdata('auth');
    }

    protected function isAdmin() {

        if($this->isAdminLoggedIn()===false)
        {
            redirect( link_to_backend('authorized/login') );
        }

        return ( (bool) $this->isAuth()->is_admin==true);
    }

	protected function checkAdminLogin() {
		
        if($this->isAdminLoggedIn()===false)
		{
			redirect( link_to_backend('authorized/login') );
		}

        if ( false == $this->isAdmin() )
        {
            redirect( base_url() );
        }
	}



}