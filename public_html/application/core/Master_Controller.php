<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Master_Controller extends CI_Controller {

     /**
     *  @var array $header_assets        All css's file on header goes here..
     */
    public $header_assets = [];

    /**
     *  @var array $footer_assets        All Javascripts file on footer goes here..
     */
    public $footer_assets = [];

    /**
     *  @var array $footerJS        All Javascripts on footer goes here..
     */
    public $headerCSS = NULL;

    /**
     *  @var array $footerJS        All Javascripts on footer goes here..
     */
    public $footerJS = NULL;

    public $is_admin_email  = NULL;
    
    public $is_no_reply     = NULL;

    public $currencies      = [];        


    public function __construct() {
        parent::__construct();

        $this->is_admin_email  = 'info@dubaikorea.com';
        $this->is_no_reply     = 'no-reply@dubaikorea.com';

        $this->currencies = $this->getAllCurrency();
    }

    public function getAllCurrency()
    {
        $curriencies = $this->triggermodel->all(TBL_CURRENCIES, ['where' => ['active' => 1 ]], true );

        return array_map(function($v){
            return [
                'description' => $v['description'],
                'symbols' => $v['symbols'],
                'amount' => $v['amount'],
            ];
        }, $curriencies);
    }

    public function send_email($to, $subject, $body = null, $template = null, $data = null )
    {

        #URL: http://www.anil2u.info/2015/04/how-to-send-email-using-html-templates-in-codeigniter/
        $config = array(        
            // 'protocol' => 'sendmail',
            // 'smtp_host' => 'your domain SMTP host',
            // 'smtp_port' => 25,
            // 'smtp_user' => 'SMTP Username',
            // 'smtp_pass' => 'SMTP Password',
            // 'smtp_timeout' => '4',
            'wordwrap'  => true, 
            'mailtype'  => 'html', 
            'charset'   => 'iso-8859-1'
        );

        $this->load->library('email');
        
        $this->email->initialize($config);

        $this->email->from($this->is_no_reply, PROJECT_TITLE );
        $this->email->to($to);
        
        $this->email->subject( PROJECT_TITLE . ' - ' . $subject);


        if ( $template )
        {
            $body = $this->load->view('emails/'.$template, $data, TRUE );
        }

        $this->email->message($body);
        
        $this->email->send();

        // $this->email->print_debugger();
    }

}

/* End of file Master_Controller.php */
/* Location: ./application/core/Master_Controller.php */