<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

/**
 * Backend URi
 */
$route['backend/logout'] 		= 'backend/authorized/getLogout';
$route['backend/(:any)'] 		= 'backend/$1';


/**
 * Frontend URi
 */
$route['page-view'] 							= 'homepage/getTest';
$route['currency-change/(:any)'] 				= 'homepage/getCurrency/$1';
$route['about-us'] 								= 'frontend/page/getAboutUs';
$route['privacy-policy'] 						= 'frontend/page/getPrivacyPolicy';
$route['terms-and-conditions'] 					= 'frontend/page/getTermsAndCondition';
$route['faqs'] 									= 'frontend/page/getFAQ';

$route['wholesale-enquiry'] 					= 'frontend/page/getWholeSellerInQuery';
$route['contact-us'] 							= 'frontend/page/getContactUS';
$route['contact-us/send'] 						= 'frontend/page/postContactUS';

$route['subscribe/now'] 						= 'frontend/page/postSubscriber';

$route['login'] 								= 'frontend/authorized/getLogin';
$route['authentication'] 						= 'frontend/authorized/getAuthentication';

$route['sign-up'] 								= 'frontend/authorized/getSignup';
$route['new/sign-up'] 							= 'frontend/authorized/postSignup';

$route['logout'] 								= 'frontend/authorized/getLogout';

$route['product/review/(:num)'] 				= 'frontend/products/postProductReviews/$1';
$route['whole-seller-send-inquiry'] 			= 'frontend/products/postWholeSellerSendInquiry';

$route['product/addtocart/(:num)'] 				= 'frontend/cart/getApiProductAddToCart/$1';
$route['cart/addtocart'] 						= 'frontend/cart/postAddToCart';
$route['shopping-cart/update/(:any)/(:any)'] 	= 'frontend/cart/getUpdateProductByCart/$1/$2';
$route['shopping-cart/remove/(:any)'] 			= 'frontend/cart/getRemoveProductByCart/$1';
$route['shopping-cart'] 						= 'frontend/cart/getCart';
$route['checkout'] 								= 'frontend/cart/getCheckout';
$route['order-place'] 							= 'frontend/order/postOrderPlace';
$route['place-order-confirmation'] 				= 'frontend/order/postPlaceOrderConfirms';
$route['success-order-place'] 					= 'frontend/order/getSuccessOrderPlace';

/**
 * Customers Account Panel Routes
 */
$route['my-account'] 							= 'frontend/customer/getMyAccount';
$route['my-orders'] 							= 'frontend/customer/getOrders';
$route['orders/order-summary/(:num)'] 			= 'frontend/customer/getOrderDetail/$1';
$route['add-to-wishlist/(:num)'] 				= 'frontend/customer/postWishlist/$1';
$route['wishlist/delete/(:num)'] 				= 'frontend/customer/deleteWishlist/$1';
$route['wishlist'] 								= 'frontend/customer/getWishlist';


$route['c/(:any)'] 								= 'frontend/products/getCategoriesProducts/$1';
$route['brand/(:any)'] 							= 'frontend/products/getBrandsProducts/$1';
$route['search/(:any)/(:any)'] 					= 'frontend/products/getCategoriesSearchProducts/$1/$2';
$route['(:any)/(:any)'] 						= 'frontend/products/getSingleProduct/$1/$2';

// $route['(:any)'] 				= 'frontend/$1/$2';


$route['default_controller'] 			= 'homepage';
$route['404_override'] 					= '';
$route['translate_uri_dashes'] 			= FALSE;
