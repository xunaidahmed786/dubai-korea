<?php defined('BASEPATH') OR exit('No direct script access allowed');

define('TBL_USERS', 'dc_users');
define('TBL_CONTACT_QUERY', 'dc_contact_query');
define('TBL_NEWSLETTERS', 'dc_subscribers');

define('TBL_CURRENCIES', 'dc_currencies');
define('TBL_SLIDERS', 'dc_sliders');

define('TBL_CATEGORIES', 'dc_categories');
define('TBL_BRANDS', 'dc_brands');
define('TBL_PRODUCTS', 'dc_products');
define('TBL_CATEGORY_PRODUCT', 'dc_category_products');
define('TBL_IMAGES', 'dc_images');
define('TBL_MEDIA', 'dc_media');

define('TBL_ATTRIBUTES', 'dc_attributes');
define('TBL_ATTRIBUTE_VALUES', 'dc_attributes_values');
define('TBL_PRODUCT_STOCKS', 'dc_product_stocks');
define('TBL_PRODUCT_REVIEWS', 'dc_products_reviews');

define('TBL_WHOLE_SELLER', 'dc_whole_seller_inquiries');
define('TBL_CUSTOMERS', 'dc_customers');
define('TBL_WISHLIST', 'dc_wishlist');

define('TBL_ORDERS', 'dc_orders');
define('TBL_ORDER_ITEMS', 'dc_order_items');
define('TBL_TRANSACTION', 'dc_transactions');
define('TBL_BILLING', 'dc_billing');
define('TBL_SHIPPING', 'dc_shipping');
