<!DOCTYPE html>
<html>
<head>
	<title>Invoice - Order</title>
<style>

body, p, table, span, h1 { margin: 0; padding: 0; font-family: arial; font-size: 14px;}

.row { width: 100%; float: left;}
/*.invoice-template { width: 90%; border: 1px solid #c00;}*/
.inovice-container { margin:10px auto; width: 90%; border: 1px solid #999; padding: 50px; display: table;  }

/*invoice main header*/
.inovice-container .header-1 .left-logo{ width: 30%; float: left; }
/*.inovice-container .header-1 .left-logo img{ width: 60%; float: left; }*/
.inovice-container .header-1 .right-inovice-info{  width: 70%; float: right; }

.inovice-container .header-1 .right-inovice-info .title h1{ font-size: 26px; }
.inovice-container .header-1 .right-inovice-info .email { font-size: 12px; text-decoration: underline; margin-bottom: 15px; }
.inovice-container .header-1 .right-inovice-info .inovice-number,
.inovice-container .header-1 .right-inovice-info .inovice-date { font-size: 13px; }

/*Invoice Header 2*/
.inovice-container .header-2 .left-side,
.inovice-container .header-2 .right-side{ width: 50%; float: left; padding: 20px 0; }

.inovice-container .header-2 .left-side table tr th,
.inovice-container .header-2 .right-side table tr th{ font-weight: bold; text-align: left; font-size: 13px;}

.inovice-container .header-2 .left-side table tr td,
.inovice-container .header-2 .right-side table tr td{ font-weight: normal; text-align: left; font-size: 13px;}

/*Invoice Body*/
.inovice-container .body table{ width: 100%; float: left; }
.inovice-container .body table tbody tr th,
.inovice-container .body table tbody tr td { border-bottom: 1px solid #e4e4e4; padding: 5px 2px; }
.inovice-container .body table tbody th{ background: #ed2617; color: #fff; }
.inovice-container .body table tbody td{ text-align: left; }
.inovice-container .body table tbody td:nth-child(2),
.inovice-container .body table tbody td:nth-child(3),
.inovice-container .body table tbody td:nth-child(4) { text-align: center;}
.inovice-container .body table tbody td:last-child { text-align: center; font-weight: bold;} /*td last child*/

.inovice-container .body table tbody tr.shipping td:nth-child(1),
.inovice-container .body table tbody tr.tax td:nth-child(1),
.inovice-container .body table tbody tr.discount td:nth-child(1),
.inovice-container .body table tbody tr.total td:nth-child(1) { font-weight: bold;}

.inovice-container .body table tbody tr.shipping td:nth-child(2),
.inovice-container .body table tbody tr.tax td:nth-child(2),
.inovice-container .body table tbody tr.discount td:nth-child(2) { text-align: left;}

.inovice-container .body table tbody tr.shipping td:nth-child(5),
.inovice-container .body table tbody tr.tax td:nth-child(5),
.inovice-container .body table tbody tr.discount td:nth-child(5) { background: #ff776d; color:#fff;}

.inovice-container .body table tbody tr.total { background: #ed2617; font-weight: bold; }
.inovice-container .body table tbody tr.total td:last-child  { color:#fff; }

/*Invoice Footer 1*/
.inovice-container .footer-1 { padding: 20px 0; }
.inovice-container .footer-1 p { width: 100%; float: left; padding: 2px 0;}
.inovice-container .footer-1 p span { text-decoration: underline; }

/*Invoice Footer 2*/
.inovice-container .footer-2 {}
.inovice-container .footer-2 p{ text-align: center; padding: 5px 0; color:#676767; font-size: 11px; }

</style>
</head>
<body>
	
	<div class="row invoice-template">
		
		<div class="inovice-container">
			
			<div class="row header-1">
				<div class="left-logo">
					<img src="http://dubaikorea.com/storage/images/logo.png">
				</div>
				<div class="right-inovice-info">
					<div class="title"><h1>Order Invoice</h1></div>
					<div class="email"><?php echo SALE_EMAIL;?></div>
					<div class="inovice-number">Invoice Number: <?php echo $order['order_number'];?></div>
					<div class="inovice-date">Invocie Date: <?php echo date('m/d/Y H:i:s', strtotime($order['created_at']));?></div>
				</div>
			</div>
			<?php
			if ( $customer ):
				$billing_name 		= $customer->first_name .' '.$customer->last_name;
				$billing_email 		= $customer->email;
				$billing_company 	= '-';
			else:
				$billing_name 		= $billing['first_name'] .' '.$billing['last_name'];
				$billing_email 		= $billing['email'];
				$billing_company 	= ($billing['company'] ? $billing['company'] : '-');
			endif;
			?>
			<div class="row header-2">
				<div class="left-side">
					<table>
						<tr>
							<th colspan="2">Customer Details:</th>
						</tr>
						<tr>
							<td>Full Name:</td>
							<td><?php echo $billing_name;?></td>
						</tr>
						<tr>
							<td>Email:</td>
							<td><?php echo $billing_email;?></td>
						</tr>
						<tr>
							<td>Company:</td>
							<td><?php echo $billing_company;?>.</td>
						</tr>
					</table>
				</div>
				<div class="right-side">
					<table>
						<tr>
							<th colspan="2">Shipping Details:</th>
						</tr>
						<tr>
							<td>Address:</td>
							<td><?php echo $shipping['address'];?></td>
						</tr>
						<tr>
							<td>Zip Code:</td>
							<td><?php echo ($shipping['zip_code'] ? $shipping['zip_code'] : '-');?></td>
						</tr>
						<tr>
							<td>City:</td>
							<td><?php echo ($shipping['city'] ? $shipping['city'] : '-');?></td>
						</tr>
						<tr>
							<td>Country:</td>
							<td><?php echo ($shipping['country'] ? $shipping['country'] : '-');?></td>
						</tr>
					</table>
				</div>
			</div>
			<div class="row body">
				<table>
					<tr>
						<th colspan="2">Description</th>
						<th>Quanity</th>
						<th>Unit Price</th>
						<th>Total Amount(<?php echo $order['currency'];?>)</th>
					</tr>
					<?php foreach( $orderProducts as $product ):?>
					<tr>
						<td colspan="2"><?php echo $product['name'];?></td>
						<td align="center"><?php echo $product['qty'];?></td>
						<td align="center"><?php echo ( ($product['sale_price'] == '0.00') ? round($product['price']) : round($product['sale_price']) );?></td>
						<td align="center"><?php echo round($product['subtotal'],0);?></td>
					</tr>
					<?php endforeach;?>
					<tr class="shipping">
						<td>Shipping:</td>
						<td>-</td>
						<td></td>
						<td align="center"><?php echo round($order['shipping_charges']);?></td>
						<td align="center" bgcolor="#ff776d"><font color="#fff"><?php echo round($order['shipping_charges']);?></font></td>
					</tr>
					<tr class="tax">
						<td>Tax:</td>
						<td></td>
						<td></td>
						<td align="center">0</td>
						<td align="center" bgcolor="#ff776d"><font color="#fff">0</font></td>
					</tr>
					<tr class="discount">
						<td>Discount:</td>
						<td></td>
						<td></td>
						<td align="center">0</td>
						<td bgcolor="#ff776d" align="center"><font color="#fff">0</font></td>
					</tr>
					<tr class="total">
						<td bgcolor="#ed2617" colspan="4"><font color="#fff">Total</font></td>
						<td bgcolor="#ed2617" align="center"><font color="#fff"><?php echo $order['total_amount'] .' '. $order['currency'];?></font><font color="#fff"></td>
					</tr>
				</table>
			</div>
			<div class="row footer-1">
				<div class="row"><p>Thankyou you for your order!</p></div>
				<div class="row"><p>If you have any question, please feel free to contact us at - <span><?php echo SUPPORT_EMAIL;?></span></p></div>
			</div>
			<div class="row footer-2">
				<div class="row">
					<p>
						This communication is for the exclusive use of the addresses and may contain properietary. Confidentaial or privileged information. If you are not the intended recepient any use, copyingm disclosue, dissemination or distribution is strictly prohibited.
					</p>
					<p><?php echo PROJECT_TITLE;?>. All Right Reserved</p>
				</div>
			</div>

		</div>

	</div>

</body>
</html>