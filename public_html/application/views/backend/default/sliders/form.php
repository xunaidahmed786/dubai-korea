<?php
$current_active_tab = 'Add New'; 
$button 			= 'Save';
$edit_id 			= 0; 
$title 				= NULL;
$is_active 			= 0;


$actions 			= link_to_backend('sliders/save');

if ( isset($edit_data) && count($edit_data) > 0 ){
	
	$edit_id 					= $edit_data->id;
	$title 						= $edit_data->title;
	$is_active 					= $edit_data->active;
	$actions 					= link_to_backend('sliders/update/'.$edit_id);
	
	$current_active_tab 		= 'Edit'; 
	$button 					= 'Update';
}

if ( $this->session->flashdata('data') ) {

	$title 						= $this->session->flashdata('data')['title'];
	$is_active 					= $this->session->flashdata('data')['active'];
}
?>

<ol class="breadcrumb">
	<li><a href="<?=link_to_backend('dashboard');?>">Dashboard</a></li>
	<li><a href="<?=link_to_backend('sliders/manage');?>">Sliders</a></li>
	<li class="active"><?=$current_active_tab;?></li>
</ol>

<!-- ============================================================== -->
<!-- Start Content here -->
<!-- ============================================================== -->
<div class="content">
	<div class="widget">
		<?php if( $this->session->flashdata('alert-message')):?>
			<div class="widget-content padding">
				<?=alert_messages( $this->session->flashdata('alert-message') );?>
			</div>
		<?php endif; ?>

		<div class="widget-header transparent">
			<h2><strong>Slider</strong> <?=$current_active_tab;?></h2>
		</div>
		<div class="widget-content padding">

			<?=form_open_multipart($actions, ['role' => 'form']);?>
				<div class="form-group">
					<label>Slider Name</label>
					<input type="text" class="form-control" name="title" value="<?=$title;?>">
				</div>
				<div class="form-group">
					<label>Status</label>
					<select class="form-control" name="active">
						<option value="1" <?=( ($is_active==1) ? 'selected=selected' : '' );?>>Yes</option>
						<option value="0" <?=( ($is_active==0) ? 'selected=selected' : '' );?>>No</option>
					</select>
				</div>
				<div class="form-group">
					<input name="images[]" type="file" />
				</div>
				<div class="form-group">
				<?php
				if( isset($edit_data) && $edit_data->source )
				{
					echo '<img src="'.storage('sliders/'.$edit_data->source).'" width="100" style="border:1px solid #ccc; border-radius:5px; margin-right:10px; ">';
					echo '<input type="hidden" name="source" value="'.$edit_data->source.'">';
				}
				?>
				</div>
				<button type="submit" class="btn btn-primary"><?=$button;?></button>
			<?=form_close();?>
		</div>
	</div>

	<?php include_backend_component('footer'); ?>

</div>