<ol class="breadcrumb">
	<li><a href="<?=link_to_backend('dashboard');?>">Dashboard</a></li>
	<li class="active">Sliders</li>
	<li class="active">Manage</li>
</ol>

<!-- ============================================================== -->
<!-- Start Content here -->
<!-- ============================================================== -->
<div class="content">
	
	<div class="row">

		<div class="col-md-12">
			<div class="widget">
				
				<?php if( $this->session->flashdata('alert-message')):?>
					<div class="widget-content padding">
						<?=alert_messages( $this->session->flashdata('alert-message') );?>
					</div>
				<?php endif; ?>

				<div class="widget-header">
					<h2><strong>Sliders</strong> Manage</h2>
				</div>
				<div class="widget-content">
				<br>					
					<div class="table-responsive">
						<div class="row">
							<div class="col-md-4"></div>
							<div class="col-md-8">
								<div class="toolbar-btn-action" style="margin-right:15px;">
									<a class="btn btn-primary" href="<?=link_to_backend('sliders/addNew');?>"><i class="fa fa-plus-circle"></i> Add New</a>
								</div>
							</div>
						</div>
						<br/>
						<form class='form-horizontal' role='form'>
							<table id="datatables-2" class="table table-striped table-bordered" cellspacing="0" width="100%">
						        <thead>
						            <tr>
						                <th><i class="fa fa-book"></i></th>
						                <th width="65%">Name</th>
						                <th>Display Image</th>
						                <th>Status</th>
						                <th>Created At</th>
						                <th>Action</th>
						            </tr>
						        </thead>
						        <tbody>
						        	<?php 
						        	if( count($data) > 0 ) {
						        		foreach( $data as $key => $row ){
						        	?>
						            <tr>
						                <td><i class="fa fa-book"></i></td>
						                <td><?=$row['title'];?></td>
						                <td><?='<img src="'.storage('sliders/'.$row['source']).'" width="100" style="border:1px solid #ccc; border-radius:5px; margin-right:10px; ">'?></td>
						                <td><?=is_status_visibility($row['active']);?></td>
						                <td><?=$row['created_at'];?></td>
						                <td><?=action_buttons('sliders', $row['id']);?></td>
						            </tr>
						            <?php 
						            	}
						            } ?>
						        </tbody>
						        </tbody>
							</table>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php include_backend_component('footer'); ?>

</div>
	
2011/04/25