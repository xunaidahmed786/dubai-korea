<ol class="breadcrumb">
	<li class="active">Dashboard</li>
</ol>

<!-- ============================================================== -->
<!-- Start Content here -->
<!-- ============================================================== -->
<div class="content">
	<!-- Start info box -->
	
	<?php if( $this->session->flashdata('alert-message')):?>
		<div class="widget-content padding">
			<?=alert_messages( $this->session->flashdata('alert-message') );?>
		</div>
	<?php endif; ?>

	<div class="row top-summary" style="padding-bottom:250px;">
		<div class="col-lg-3 col-md-6">
			<div class="widget lightblue-1">
				<div class="widget-content padding">
					<div class="widget-icon">

					</div>
					<div class="text-box">
						<p class="maindata">TOTAL <b>CATEGORIES</b> <small>(Active)</small></p>
						<h2><span class="animate-number" data-value="<?=$data['categories'];?>" data-duration="3000"><?=$data['categories'];?></span></h2>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="widget-footer" style="display:none;">
					<div class="row">
						<div class="col-sm-12">
							<i class="fa fa-caret-up rel-change"></i> <b>39%</b> <!-- increase in traffic -->
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>

		<div class="col-lg-3 col-md-6">
			<div class="widget green-1">
				<div class="widget-content padding">
					<div class="widget-icon">

					</div>
					<div class="text-box">
						<p class="maindata">TOTAL <b>PRODUCTS <small>(Active)</small></b></p>
						<h2><span class="animate-number" data-value="<?=$data['products'];?>" data-duration="3000"><?=$data['products'];?></span></h2>

						<div class="clearfix"></div>
					</div>
				</div>
				<div class="widget-footer" style="display:none;">
					<div class="row">
						<div class="col-sm-12">
							<i class="fa fa-caret-down rel-change"></i> <b>11%</b> <!-- decrease in sales -->
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>

		<div class="col-lg-3 col-md-6">
			<div class="widget orange-4">
				<div class="widget-content padding">
					<div class="widget-icon">

					</div>
					<div class="text-box">
						<p class="maindata">TOTAL <b>ORDERS <small>(Delivered)</small></b></p>
						<!-- <h2>$<span class="animate-number" data-value="70389" data-duration="3000">0</span></h2> -->
						<h2><span class="animate-number" data-value="<?=$data['orders'];?>" data-duration="3000"><?=$data['orders'];?></span></h2>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="widget-footer" style="display:none;">
					<div class="row">
						<div class="col-sm-12">
							<i class="fa fa-caret-down rel-change"></i> <b>7%</b> <!-- decrease in income -->
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>

		<div class="col-lg-3 col-md-6">
			<div class="widget darkblue-2">
				<div class="widget-content padding">
					<div class="widget-icon">

					</div>
					<div class="text-box">
						<p class="maindata">TOTAL <b>CUSTOMERS</b> <small>(Verify)</small></p>
						<h2><span class="animate-number" data-value="<?=$data['customers'];?>" data-duration="3000"><?=$data['customers'];?></span></h2>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="widget-footer" style="display:none;">
					<div class="row">
						<div class="col-sm-12">
							<i class="fa fa-caret-up rel-change"></i> <b>6%</b> <!-- increase in users -->
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
	<!-- End of info box -->

	<?php include_backend_component('footer'); ?>

</div>
	
