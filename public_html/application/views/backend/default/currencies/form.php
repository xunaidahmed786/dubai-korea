<?php
$current_active_tab = 'Add New'; 
$button 			= 'Save';
$edit_id 			= 0; 
$description 		= NULL;
$symbols 			= NULL;
$amount 			= NULL;
$is_active 			= 1;
$actions 			= link_to_backend('currency/save');

if ( isset($edit_data) && count($edit_data) > 0 ){
	$edit_id 			= $edit_data->id;
	$description 		= $edit_data->description;
	$symbols 			= $edit_data->symbols;
	$amount 			= $edit_data->amount;
	$is_active 			= $edit_data->active;
	$actions 			= link_to_backend('currency/update/'.$edit_id);
	
	$current_active_tab = 'Edit'; 
	$button 			= 'Update';
}

if ( $this->session->flashdata('data') ) {
	$description 	= $this->session->flashdata('data')['description'];
	$symbols 		= $this->session->flashdata('data')['symbols'];
	$amount 		= $this->session->flashdata('data')['amount'];
	$is_active 		= $this->session->flashdata('data')['active'];
}
?>

<ol class="breadcrumb">
	<li><a href="<?=link_to_backend('dashboard');?>">Dashboard</a></li>
	<li><a href="<?=link_to_backend('currency/manage');?>">Currencies</a></li>
	<li class="active"><?=$current_active_tab;?></li>
</ol>

<!-- ============================================================== -->
<!-- Start Content here -->
<!-- ============================================================== -->
<div class="content">
	<div class="widget">
		<?php if( $this->session->flashdata('alert-message')):?>
			<div class="widget-content padding">
				<?=alert_messages( $this->session->flashdata('alert-message') );?>
			</div>
		<?php endif; ?>

		<div class="widget-header transparent">
			<h2><strong>Currency</strong> <?=$current_active_tab;?></h2>
		</div>
		<div class="widget-content padding">
			<?=form_open( $actions, ['role' => 'form' ]); ?>
				<div class="form-group">
					<label>Currency Name</label>
					<input type="text" class="form-control" name="description" value="<?=$description;?>">
				</div>
				<div class="form-group">
					<label>Curreny Symbols</label>
					<input type="text" class="form-control" name="symbols" value="<?=$symbols;?>">
				</div>
				<div class="form-group">
					<label>Curreny Amount</label>
					<input type="text" class="form-control" name="amount" value="<?=$amount;?>">
				</div>
				<div class="form-group">
					<label>Status</label>
					<select class="form-control" name="active">
						<option value="1" <?=( ($is_active==1) ? 'selected=selected' : '' );?>>Yes</option>
						<option value="0" <?=( ($is_active==0) ? 'selected=selected' : '' );?>>No</option>
					</select>
				</div>
				<button type="submit" class="btn btn-primary"><?=$button;?></button>
			<?=form_close();?>
		</div>
	</div>

	<?php include_backend_component('footer'); ?>

</div>