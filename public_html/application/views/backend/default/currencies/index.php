<ol class="breadcrumb">
	<li><a href="<?=link_to_backend('dashboard');?>">Dashboard</a></li>
	<li class="active">Currencies</li>
	<li class="active">Manage</li>
</ol>

<!-- ============================================================== -->
<!-- Start Content here -->
<!-- ============================================================== -->
<div class="content">
	
	<div class="row">

		<div class="col-md-12">
			<div class="widget">
				
				<?php if( $this->session->flashdata('alert-message')):?>
					<div class="widget-content padding">
						<?=alert_messages( $this->session->flashdata('alert-message') );?>
					</div>
				<?php endif; ?>

				<div class="widget-header">
					<h2><strong>Currencies</strong> Manage</h2>
				</div>
				<div class="widget-content">
				<br>					
					<div class="table-responsive">
						<div class="row">
							<div class="col-md-4"></div>
							<div class="col-md-8">
								<div class="toolbar-btn-action" style="margin-right:15px;">
									<a class="btn btn-primary" href="<?=link_to_backend('currency/addNew');?>"><i class="fa fa-plus-circle"></i> Add New</a>
								</div>
							</div>
						</div>
						<br/>
						<form class='form-horizontal' role='form'>
						<table id="datatables-2" class="table table-striped table-bordered" cellspacing="0" width="100%">
						        <thead>
						            <tr>
						                <th><i class="fa fa-book"></i></th>
						                <th width="40%">Name</th>
						                <th>Symbols</th>
						                <th>Amount</th>
						                <th>Active</th>
						                <th>Created At</th>
						                <th>Updated At</th>
						                <th>Action</th>
						            </tr>
						        </thead>
						        <tbody>
						        	<?php 
						        	if( count($data) > 0 ) {
						        		foreach( $data as $key => $row ){
						        	?>
						            <tr>
						                <td><i class="fa fa-book"></i></td>
						                <td><?=$row['description'];?></td>
						                <td><?=$row['symbols'];?></td>
						                <td><?=$row['amount'];?></td>
						                <td><?=is_status_visibility($row['active']);?></td>
						                <td><?=$row['created_at'];?></td>
						                <td><?=$row['updated_at'];?></td>
						                <td><?=action_buttons('currency', $row['id']);?></td>
						            </tr>
						            <?php 
						            	}
						            } ?>
						        </tbody>
						    </table>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php include_backend_component('footer'); ?>

</div>
	
2011/04/25