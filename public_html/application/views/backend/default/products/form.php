<?php
$current_active_tab = 'Add New'; 
$button 			= 'Save';
$pivot_categories 	= [];
$brand_id 			= $edit_id = 0; 
$title 				= 
					$code 						= 
					$extra 						= 
					$code 						= 
					$short_description 			= 
					$description 				= 
					$price 						= 
					$sale_price 				= 
					$available_stock 			= 
					$whole_sale_price 			= 
					$whole_sale_available_stock = 
					$is_featured 				= 
					$expired_at 				= 
					$is_active 					= 
					NULL;
					$is_sale 					= 0;
					$is_deals 					= 0;
					$is_best_seller 			= 1;
$images 			= [];
$exists_thumbnail_ids = null; 
$exists_thumbnail_product_desc_ids = null; 


$actions 			= link_to_backend('products/save');

if ( isset($edit_data) && count($edit_data) > 0 ){
	
	$get_exists_thumbnail_ids 		= array_map(function($v){
		return $v['id'];
	}, $edit_data['products']['images'] );

	$get_exists_thumbnail_product_desc_ids = array_map(function($v){
		return $v['id'];
	}, $edit_data['products']['pro_desc'] );


	$images 					= (object) $edit_data['products']['images'];
	$pro_desc_media 			= (object) $edit_data['products']['pro_desc'];
	$edit_data 					= (object) $edit_data['products']['data'];

	$exists_thumbnail_ids 				= count($get_exists_thumbnail_ids) ? implode(',', $get_exists_thumbnail_ids) : null;
	$exists_thumbnail_product_desc_ids 	= count($get_exists_thumbnail_product_desc_ids) ? implode(',', $get_exists_thumbnail_product_desc_ids) : null;

	
	// dump($edit_data);
	$edit_id 					= $edit_data->id;
	
	$pivot_categories 			= $edit_pivot_categories;
	$brand_id 					= $edit_data->brand_id;
	$title 						= $edit_data->title;
	$code 						= $edit_data->code;
	$extra 						= $edit_data->extra;
	$short_description 			= $edit_data->short_description;
	$description 				= $edit_data->description;
	$price 						= $edit_data->price;
	$is_sale 					= $edit_data->is_sale;
	// rd($is_sale);
	$sale_price 				= $edit_data->sale_price;
	$available_stock 			= $edit_data->available_stock;
	$whole_sale_price 			= $edit_data->whole_sale_price;
	$whole_sale_available_stock = $edit_data->whole_sale_available_stock;
	
	$is_best_seller 			= $edit_data->is_best_seller;
	$is_deals 					= $edit_data->is_deals;
	$expired_at 				= $edit_data->expired_at;
	$is_active 					= $edit_data->active;

	$actions 					= link_to_backend('products/update/'.$edit_id);
	
	$current_active_tab 		= 'Edit'; 
	$button 					= 'Update';
}

if ( $this->session->flashdata('data') ) {

	// rd($this->session->flashdata('data'));
	$title 						= $this->session->flashdata('data')['title'];
	$pivot_categories 			= $this->session->flashdata('data')['category_id'];
	$brand_id 					= $this->session->flashdata('data')['brand_id'];
	$title 						= $this->session->flashdata('data')['title'];
	$code 						= $this->session->flashdata('data')['code_1'];
	$extra 						= $this->session->flashdata('data')['extra'];
	$short_description 			= $this->session->flashdata('data')['short_description'];
	$description 				= $this->session->flashdata('data')['description'];
	$price 						= $this->session->flashdata('data')['price'];
	$is_sale 					= $this->session->flashdata('data')['is_sale'];
	$sale_price 				= $this->session->flashdata('data')['sale_price'];
	$available_stock 			= $this->session->flashdata('data')['available_stock'];
	$whole_sale_price 			= $this->session->flashdata('data')['whole_sale_price'];
	$whole_sale_available_stock = $this->session->flashdata('data')['whole_sale_available_stock'];
	
	$is_best_seller 			= $this->session->flashdata('data')['is_best_seller'];
	$is_deals 					= $this->session->flashdata('data')['is_deals'];
	$expired_at 				= $this->session->flashdata('data')['expired_at'];
	$is_active 					= $this->session->flashdata('data')['active'];
}
?>

<ol class="breadcrumb">
	<li><a href="<?=link_to_backend('dashboard');?>">Dashboard</a></li>
	<li><a href="<?=link_to_backend('products/manage');?>">Products</a></li>
	<li class="active"><?=$current_active_tab;?></li>
</ol>

<!-- ============================================================== -->
<!-- Start Content here -->
<!-- ============================================================== -->
<div class="content">
	<div class="widget">
		<?php if( $this->session->flashdata('alert-message')):?>
			<div class="widget-content padding">
				<?=alert_messages( $this->session->flashdata('alert-message') );?>
			</div>
		<?php endif; ?>

		<div class="widget-header transparent">
			<h2><strong>Product</strong> <?=$current_active_tab;?></h2>
		</div>
		<div class="widget-content padding">

			<?=form_open_multipart($actions, ['id'=>'NotEmptyValidatorForProduct', 'role' => 'form']);?>
				<div class="form-group">
					<label>Category</label>
					<select class="form-control" id="category_id" name="category_id[]" multiple="multiple" style="border:none !important;padding: 6px 0px !important" >
						<?php 
						if ( count($categories) ) {
							foreach( buildTree($categories) as $value ) {
								if ( isset($value['children']) && is_array($value['children']) ){
									echo '<option value="'.$value['id'].'" '.(in_array($value['id'], (isset($pivot_categories) ? $pivot_categories : [] )) ? 'selected=selected' : '').'>'.$value['title'].'</option>';	
									
									foreach ($value['children'] as $child) {
										echo '<option value="'.$child['id'].'" '.(in_array($child['id'], (isset($pivot_categories) ? $pivot_categories : [] )) ? 'selected=selected' : '').'>-----'.$child['title'].'-----</option>';
										// echo '<option value="'.$child['id'].'" '.(in_array($child['id'], (isset($pivot_categories) ? $pivot_categories : [] )) ? 'selected=selected' : '').'>'.$child['title'].'</option>';
									}
								} else {
									echo '<option value="'.$value['id'].'" '.((in_array($value['id'], (isset($pivot_categories) ? $pivot_categories : [] ))) ? 'selected=selected' : '').'>'.$value['title'].'</option>';	
								}
							}
						}
						?>
					</select>
				</div>
				<div class="form-group">
					<label>Brand</label>
					<select class="form-control" name="brand_id">
						<option value="">-- Select Brand --</option>
						<?php 
						if ( count($brands) ) {
							foreach( $brands as $brand ) {
								echo '<option value="'.$brand['id'].'" '.(($brand['id']==$brand_id) ? 'selected=selected' : '').'>'.$brand['title'].'</option>';		
							}
						}
						?>
					</select>
				</div>
				<div class="form-group">
					<label>Product Name</label>
					<input type="text" class="form-control" name="title" value="<?=$title;?>">
				</div>
				<div class="form-group">
					<label>Product Code</label>
					<input type="text" class="form-control" name="code_1" value="<?=$code;?>">
				</div>
				<div class="form-group">
					<label>Product Extra</label>
					<input type="text" class="form-control" name="extra" value="<?=$extra;?>">
				</div>
				<div class="form-group">
					<label>Product Stock</label>
					<input type="text" class="form-control" name="available_stock" value="<?=$available_stock;?>">
				</div>
				<div class="form-group">
					<label>Short Description</label>
					<textarea class="form-control" name="short_description" rows="5"><?=$short_description;?></textarea>
				</div>
				<div class="form-group">
					<label>Description</label>
					<textarea name="description" class="summernote"><?=$description;?></textarea>
				</div>
				<div class="form-group">
					<label>Price</label>
					<input type="text" class="form-control" name="price" value="<?=$price;?>">
				</div>
				<div class="form-group">
					<label>
						<input type="checkbox" name="is_sale" value="<?=$is_sale;?>" <?php echo ($is_sale ? 'checked' : null); ?> class="is_sale_class">Sale Price
					</label>
				</div>
				<div class="form-group is_sale_price_txt" <?php echo ($is_sale ? null : 'style="display:none;"'); ?> >
					<input type="text" class="form-control" name="sale_price" value="<?=$sale_price;?>">
				</div>
				<div class="form-group">
					<label>Whole Sale Price</label>
					<input type="text" class="form-control" name="whole_sale_price" value="<?=$whole_sale_price;?>">
				</div>
				<div class="form-group">
					<label>Whole Sale Min Quantity</label>
					<input type="text" class="form-control" name="whole_sale_available_stock" value="<?=$whole_sale_available_stock;?>">
				</div>
				<div class="form-group">
					<label>
						<input type="checkbox" name="is_deals" value="<?=$is_deals;?>" <?php echo ($is_deals ? 'checked' : null); ?> class="is_deal_class">Product Deals
					</label>
				</div>
				<div class="form-group is_expire_at" <?php echo ($is_deals ? null : 'style="display:none;"'); ?>>
					<label>Product Expire Date</label>
					<input type="text" class="form-control datepicker-input" name="expired_at">
				</div>
				<div class="form-group">
					<label>Product Best Seller</label>
					<select class="form-control" name="is_best_seller">
						<option value="">-- Select --</option>
						<option value="1" <?=( ($is_best_seller==1) ? 'selected=selected' : '' );?>>Yes</option>
						<option value="0" <?=( ($is_best_seller==0) ? 'selected=selected' : '' );?>>No</option>
					</select>
				</div>
				<div class="form-group">
					<label>Status</label>
					<select class="form-control" name="active">
						<option value="1" <?=( ($is_active==1) ? 'selected=selected' : '' );?>>Yes</option>
						<option value="0" <?=( ($is_active==0) ? 'selected=selected' : '' );?>>No</option>
					</select>
				</div>

				<input type="text" name="exists_thumbnail_ids" value="<?php echo $exists_thumbnail_ids;?>">
				<input type="text" name="exists_thumbnail_product_desc_ids" value="<?php echo $exists_thumbnail_product_desc_ids;?>">

				<div class="form-group ">
				    <label class="control-label">Product Thumbnail: Product Description Upload Images</label>
				    <div id="my-product-desc-dropzone" data-type="-" class="dropzone dropzone-file-area">
				        <div class="dz-default dz-message" data-dz-message></div>
				    </div>
				    <span class="help-block">Upload Image: Click anywhere in the box to upload</span>
				    <span class="help-block">Maximum Upload Size: (1 MB)</span>
				    <!-- <span class="help-block">Allowed Dimension: 990px by 765px</span> -->
				    <span class="help-block">Allowed Extensions: jpg,jpeg,png,gif</span> <br/>
				</div>

				<div class="form-group ">
				    <label class="control-label">Product Thumbnail: Upload Images</label>
				    <div id="my-dropzone" data-type="-" class="dropzone dropzone-file-area">
				        <div class="dz-default dz-message" data-dz-message></div>
				    </div>
				    <span class="help-block">Upload Image: Click anywhere in the box to upload</span>
				    <span class="help-block">Maximum Upload Size: (1 MB)</span>
				    <!-- <span class="help-block">Allowed Dimension: 990px by 765px</span> -->
				    <span class="help-block">Allowed Extensions: jpg,jpeg,png,gif</span> <br/>
				</div>

				<button type="submit" class="btn btn-primary"><?=$button;?></button>
			<?=form_close();?>
		</div>
	</div>

	<?php include_backend_component('footer'); ?>

</div>