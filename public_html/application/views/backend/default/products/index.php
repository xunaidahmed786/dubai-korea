<ol class="breadcrumb">
	<li><a href="<?=link_to_backend('dashboard');?>">Dashboard</a></li>
	<li class="active">Products</li>
	<li class="active">Manage</li>
</ol>

<!-- ============================================================== -->
<!-- Start Content here -->
<!-- ============================================================== -->
<div class="content">
	
	<div class="row">

		<div class="col-md-12">
			<div class="widget">
				
				<?php if( $this->session->flashdata('alert-message')):?>
					<div class="widget-content padding">
						<?=alert_messages( $this->session->flashdata('alert-message') );?>
					</div>
				<?php endif; ?>

				<div class="widget-header">
					<h2><strong>Products</strong> Manage</h2>
				</div>
				<div class="widget-content">
				<br>					
					<div class="table-responsive">
						<div class="row">
							<div class="col-md-4"></div>
							<div class="col-md-8">
								<div class="toolbar-btn-action" style="margin-right:15px;">
									<a class="btn btn-primary" href="<?=link_to_backend('products/addNew');?>"><i class="fa fa-plus-circle"></i> Add New</a>
								</div>
							</div>
						</div>
						<br/>
						<form class='form-horizontal' role='form'>
							<table id="datatables-2" class="table table-striped table-bordered" cellspacing="0" width="100%">
						        <thead>
						            <tr>
						                <th>ID</th>
						                <th>Cateory</th>
						                <th>Product</th>
						                <th>Price</th>
						                <th>Whole Sale</th>
						                <th>Deal</th>
						                <th>Best Seller</th>
						                <th>Status</th>
						                <th>Created At</th>
						                <th>Action</th>
						            </tr>
						        </thead>
						        <tbody>
						        	<?php 
						        	if( count($data) > 0 ) {
						        		foreach( $data as $key => $row ){
						        	?>
						            <tr>
						                <td><?=$row['id'];?></td>
						                <td><?=$row['c_name'];?></td>
						                <td>
						                	<?php 
						                	echo $row['title'];

						                	if ( $row['code'] ) {
						                		echo '<br><br> <small style="font-size:12px;">Item Code: ' . $row['code'] .'</small>';
						                	}

						                	if ( $row['available_stock'] ) {
						                		echo '<br><small style="font-size:12px;">Availability: ' . $row['available_stock'] .' Item(s) Available</small>';
						                	}
						                	?>
						                </td>
						                <td>
						                <?php
						                	if ( check_amount($row['sale_price']) ){
						                		echo '<del>'.backend_amount_format($row['price']).'</del> / ' . backend_amount_format($row['sale_price']);
						                	} else {
						                		echo backend_amount_format($row['price']);
						                	}
						                ?>
						                </td>
						                <td>
						                <?php
						                	if ( check_amount($row['whole_sale_price']) ){
						                		echo '<small style="font-size:12px;">Price: ' . backend_amount_format($row['whole_sale_price']) .'</small>';
						                		echo '<br> <small style="font-size:12px;">Min Quantity: ' . $row['whole_sale_available_stock'] .'</small>';
						                	} else {
						                		not_available_for_table();
						                	}
						                ?>
						                </td>
						                <td><?=is_status_visibility($row['is_deals']);?></td>
						                <td><?=is_status_visibility($row['is_best_seller']);?></td>
						                <td><?=is_status_visibility($row['active']);?></td>
						                <td><?=$row['created_at'];?></td>
						                <td><?=action_buttons('products', $row['id']);?></td>
						            </tr>
						            <?php 
						            	}
						            } ?>
						        </tbody>
						        </tbody>
							</table>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php include_backend_component('footer'); ?>

</div>
	
2011/04/25