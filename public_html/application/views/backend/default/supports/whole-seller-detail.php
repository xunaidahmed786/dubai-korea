<ol class="breadcrumb">
	<li><a href="<?=link_to_backend('dashboard');?>">Dashboard</a></li>
	<li class="active">Whole Seller Enquiry</li>
</ol>
<?php //rd($order);?>
<!-- ============================================================== -->
<!-- Start Content here -->
<!-- ============================================================== -->
<div class="content">
	
	<div class="page-heading">
		<h1>Whole Seller Enquiry</h1>
	</div>

	<div class="row">
	<div class="col-sm-12">
		<div class="widget">
			<div class="widget-header">
				<h2>Whole Seller Enquiry Details</h2>
			</div>
			<div class="widget-content padding">
				
				<h3><?=$data->product_name?></h3>
				<table class="table table-bordered table-striped" style="clear: both">
					<tbody>
						<tr>
							<th colspan="4">Denotes Fields</th>
						</tr>
						<tr>
							<td width="35%">Contact Name</td>
							<td width="65%"><?=$data->seller_name?></td>
						</tr>
						<tr>
							<td width="35%">Email</td>
							<td width="65%"><?=$data->seller_email?></td>
						</tr>
						<tr>
							<td width="35%">Phone</td>
							<td width="65%"><?=$data->seller_phone?></td>
						</tr>
						<tr>
							<td width="35%">Company Name</td>
							<td width="65%"><?=$data->company_name?></td>
						</tr>
						<tr>
							<td width="35%">Store or Website Name</td>
							<td width="65%"><?=$data->store_website_name?></td>
						</tr>
						<tr>
							<td width="35%">Year Established</td>
							<td width="65%"><?=($data->year_established ? $data->year_established : '<i>Not Available</i>' )?></td>
						</tr>
					</tbody>
				</table>
				<br/>
				<table class="table table-bordered table-striped" style="clear: both">
					<tbody>
						<tr>
							<th colspan="4">Company location</th>
						</tr>
						<tr>
							<td width="35%">Address 1</td>
							<td width="65%"><?=$data->address1?></td>
						</tr>
						<tr>
							<td width="35%">Address 2</td>
							<td width="65%"><?=($data->address2 ? $data->address2 : '<i>Not Available</i>')?></td>
						</tr>
						<tr>
							<td width="35%">City</td>
							<td width="65%"><?=$data->city?></td>
						</tr>
						<tr>
							<td width="35%">State</td>
							<td width="65%"><?=($data->state ? $data->state : '<i>Not Available</i>');?></td>
						</tr>
						<tr>
							<td width="35%">Country</td>
							<td width="65%"><?=$data->country?></td>
						</tr>
						<tr>
							<td width="35%">Post Code/Zip Code</td>
							<td width="65%"><?=($data->zip_code ? $data->zip_code : '<i>Not Available</i>');?></td>
						</tr>
						<tr>
							<td width="35%">Country you wish to distribute to or sell to</td>
							<td width="65%"><?=$data->country_distribute_sell?></td>
						</tr>
						<tr>
							<td width="35%">Type of business</td>
							<td width="65%"><?=$data->type_of_business?></td>
						</tr>
						<tr>
							<td width="35%">Which other skincare brands do you currently sell?</td>
							<td width="65%"><?=$data->currently_sell?></td>
						</tr>
						<tr>
							<td width="35%">Created At</td>
							<td width="65%"><?=$data->created_at?></td>
						</tr>
					</tbody>
				</table>

			</div>
		</div>
	</div>
	</div>
	
	<?php include_backend_component('footer'); ?>

</div>