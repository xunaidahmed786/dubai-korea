<ol class="breadcrumb">
	<li><a href="<?=link_to_backend('dashboard');?>">Dashboard</a></li>
	<li class="active">Supports</li>
	<li class="active">Contact Us Queries</li>
</ol>

<!-- ============================================================== -->
<!-- Start Content here -->
<!-- ============================================================== -->
<div class="content">
	
	<div class="row">

		<div class="col-md-12">
			<div class="widget">
				
				<?php if( $this->session->flashdata('alert-message')):?>
					<div class="widget-content padding">
						<?=alert_messages( $this->session->flashdata('alert-message') );?>
					</div>
				<?php endif; ?>

				<div class="widget-header">
					<h2><strong>Contact Us</strong> Manage</h2>
				</div>
				<div class="widget-content">
				<br>					
					<div class="table-responsive">
						<div class="row">
							<div class="col-md-4"></div>
							<div class="col-md-8">
								<div class="toolbar-btn-action" style="margin-right:15px;"></div>
							</div>
						</div>
						<br/>
						<form class='form-horizontal' role='form'>
							<table id="datatables-2" class="table table-striped table-bordered" cellspacing="0" width="100%">
						        <thead>
						            <tr>
						                <th>#</th>
						                <th>Name</th>
						                <th>Email</th>
						                <th>Telephone</th>
						                <th>Message</th>
						                <th>Track</th>
						                <th>Created At</th>
						                <th>Action</th>
						            </tr>
						        </thead>
						        <tbody>
						        	<?php 
						        	if( count($data) > 0 ) {
						        		foreach( $data as $key => $row ){
						        	?>
						            <tr>
						                <td>#</td>
						                <td><?=$row->name;?></td>
						                <td><?=$row->email;?></td>
						                <td><?=$row->telephone;?></td>
						                <td><?=$row->message;?></td>
						                <td><?=$row->ip_address;?></td>
						                <td><?=$row->created_at;?></td>
						                <td>#</td>
						            </tr>
						            <?php 
						            	}
						            } ?>
						        </tbody>
						        </tbody>
							</table>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php include_backend_component('footer'); ?>

</div>