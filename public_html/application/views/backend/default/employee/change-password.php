<ol class="breadcrumb">
	<li><a href="<?=link_to_backend('dashboard');?>">Dashboard</a></li>
	<li class="active">Change Password</li>
</ol>

<!-- ============================================================== -->
<!-- Start Content here -->
<!-- ============================================================== -->
<div class="content">

	<div class="widget">
		<div class="widget-header transparent">
			<h2><strong>Change Password</strong></h2>
		</div>
		<div class="widget-content padding">
			<form role="form" id="IdenticalValidator" novalidate="novalidate" class="bv-form">
				<div class="form-group">
					<label>Current Password</label>
					<input type="text" class="form-control" name="password" data-bv-field="password">
				</div>
				<div class="form-group">
					<label>New Password</label>
					<input type="text" class="form-control" name="password" data-bv-field="password">
				</div>
				<div class="form-group">
					<label>Retype password</label>
					<input type="text" class="form-control" name="confirmPassword" data-bv-field="confirmPassword">
				</div>
				<button type="submit" class="btn btn-primary">Submit</button>
			</form>
		</div>
	</div>

	<?php include_backend_component('footer'); ?>

</div>