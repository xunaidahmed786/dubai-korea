<!-- Begin page -->
<div class="container">
	<div class="full-content-center">
		<p class="text-center"><a href="#"><img src="<?=storage('/images/logo.png');?>" alt="Logo"></a></p>
		<div class="login-wrap animated flipInX">
			<div class="login-block">
				<br/><br/>
				<!-- <img src="<?=base_url('authorized/login');?>/images/users/default-user.png" class="img-circle not-logged-avatar"> -->
				<?php if( $this->session->flashdata('alert-message')):?>
					<div class="widget-content">
						<div class="alert alert-danger nomargin">
					        <?php echo $this->session->flashdata('alert-message') ;?>
					    </div><br/>
					</div>
				<?php endif; ?>

				<?=form_open( link_to_backend('authorized/auth'));?>
					<div class="form-group login-input">
						<i class="fa fa-user overlay"></i>
						<input type="text" class="form-control text-input" name="email" placeholder="Email Address">
					</div>
					<div class="form-group login-input">
						<i class="fa fa-key overlay"></i>
						<input type="password" class="form-control text-input" name="password" placeholder="********">
					</div>
					<div class="row">
						<div class="col-sm-12">
							<button type="submit" class="btn btn-success btn-block">LOGIN</button>
						</div>
					</div>
				<?=form_close();?>
				<br/>
			</div>
		</div>
		
	</div>
</div>