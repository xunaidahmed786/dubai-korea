<ol class="breadcrumb">
	<li><a href="<?=link_to_backend('dashboard');?>">Dashboard</a></li>
	<li class="active">Order Details</li>
</ol>
<?php //rd($order);?>
<!-- ============================================================== -->
<!-- Start Content here -->
<!-- ============================================================== -->
<div class="content">
	
	<div class="page-heading">
		<h1>Order Details</h1>
	</div>

	<div class="row">
	<div class="col-sm-12">
		<div class="widget">
			<div class="widget-header">
				<h2>Order Details</h2>
			</div>
			<div class="widget-content padding">
				<ul id="demo2" class="nav nav-tabs">
					<li class="active">
						<a href="#order-details" data-toggle="tab">Order</a>
					</li>
					<li class="">
						<a href="#cart-details" data-toggle="tab">Cart Details</a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Billing &Shipping<i class="fa fa-caret-down"></i></a>
						<ul class="dropdown-menu">
							<li><a href="#billing-details" data-toggle="tab">Billing Details</a></li>
							<li><a href="#shipping-details" data-toggle="tab">Shippping Details</a></li>
						</ul>
					</li> <!-- / .dropdown -->
				</ul>

				
				<div class="tab-content tab-boxed">
					<div class="tab-pane fade active in" id="order-details">
						
						<?php if ( isset($order['customer']['name']) && $order['customer']['name'] ): ?>
						<h3>Customer</h3>
						<table class="table table-bordered table-striped" style="clear: both">
							<tbody>
								<tr>
									<th colspan="4">Customer Details</th>
								</tr>
								<tr>         
									<td width="35%">Full Name</td>
									<td width="65%"><?=$order['customer']['name'];?></td>
								</tr>
								<tr>         
									<td width="35%">Email</td>
									<td width="65%"><?=$order['customer']['email'];?></td>
								</tr>
								<tr>         
									<td width="35%">Telephone</td>
									<td width="65%"><?=$order['customer']['telephone'];?></td>
								</tr>
								<tr>         
									<td width="35%">Address</td>
									<td width="65%">
										<?=$order['customer']['address'];?>
										<?=$order['customer']['city'];?>, 
										<?=$order['customer']['state'];?>, 
										<?=$order['customer']['country'];?>, 
										<?=$order['customer']['postal_code'];?>
									</td>
								</tr>
							</tbody>
						</table>
						<br/><hr/>
						<?php endif;?>

						<h3>Order</h3>
						<table class="table table-bordered table-striped" style="clear: both">
							<tbody> 
								<tr>
									<th colspan="4">Order Details</th>
								</tr>
								<tr>
									<td width="20%">Order Number</td>
									<td width="30%"><?=$order['order']['order_number'];?></td>
									<td width="20%">Shipping Charges</td>
									<td width="30%"><?=formatCurrencyByOrders($order['order']['shipping_charges'], $order['order']['currency']);?></td>
								</tr>
								<tr>
									<td width="20%">Discount Amount</td>
									<td width="30%"><?=formatCurrencyByOrders($order['order']['discount_amount'], $order['order']['currency']);?></td>
									<td width="20%">Total Amount</td>
									<td width="30%"><?=$order['order']['total_amount'];?></td>
								</tr>
								<tr>
									<td width="20%">Order Place Date</td>
									<td width="30%"><?=$order['order']['created_at'];?></td>
									<td width="20%">Order Shipped Date</td>
									<td width="30%"><?=$order['order']['shipped_at'];?></td>
								</tr>
								<tr>
									<td width="20%">Order Status</td>
									<td width="30%"><?=ucfirst($order['order']['order_status']);?></td>
									<td width="20%">Payment Method</td>
									<td width="30%"><?=ucfirst($order['order']['payment_method']);?></td>
								</tr>
								<tr>
									<td width="20%" colspan="1">Notes/Shipping Tracking</td>
									<td width="80%" colspan="3"><?=($order['order']['notes'] ? $order['order']['notes'] : '<i>Not Available</i>');?></td>
								</tr>
							</tbody>
						</table>
						<br/><hr/>

						<h3>Order Transaction</h3>
						<table class="table table-bordered table-striped" style="clear: both">
							<tbody> 
								<tr>
									<th colspan="4">Order Transaction Details</th>
								</tr>
								<?php if ( $order['order']['payment_method'] == CASH_ON_DELIVERY_METHOD ) : ?>
								<tr>
									<td width="100%" colspan="4">Cash On Delivery</td>
								</tr>
								<?php else: ?>
								<tr>
									<td width="20%">Account Number</td>
									<td width="30%"><?=$order['transactions']['account_id'];?></td>
									<td width="20%">Transaction Secret</td>
									<td width="30%">******</td>
								</tr>
								<tr>
									<td width="20%">Transaction Email</td>
									<td width="30%"><?=$order['transactions']['email'];?></td>
									<td width="20%">Total Amount</td>
									<td width="30%"><?=$order['order']['total_amount'];?></td>
								</tr>
								<tr>
									<td width="20%">Transaction Date</td>
									<td width="30%"><?=$order['transactions']['created_at'];?></td>
									<td width="20%">Transaction Status</td>
									<td width="30%"><?=$order['transactions']['status'];?></td>
								</tr>
								<?php endif;?>
							</tbody>
						</table>

					</div> <!-- / .tab-pane -->
					<div class="tab-pane fade" id="cart-details">
						
						<h3>Cart Summary</h3>
						<table class="table table-bordered table-striped" style="clear: both;">
							<tbody>
								<tr>
									<th>Name</th>
									<th>Description</th>
									<th width="10%">Price</th>
									<th width="10%">Qty</th>
									<th width="10%">Subtotal</th>
								</tr>
								<?php
								$total = 0;
								if ( count($order['order_items']) ):
									foreach($order['order_items'] as $iKey => $items):
									$total += $items['subtotal'];
								?>
								<tr>
									<td><?=$items['name'];?></td>
									<td><?=$items['name'];?></td>
									<td><?=(
										($items['sale_price'] == '0.00') ? 
										formatCurrencyByOrders($items['price'], $order['order']['currency']) : 
										formatCurrencyByOrders($items['sale_price'], $order['order']['currency']) );?></td>
									<td><?=$items['qty'];?></td>
									<td><?=formatCurrencyByOrders($items['subtotal'], $order['order']['currency']);?></td>
								</tr>
								<?php 
									endforeach;

									$total = $total + $order['order']['shipping_charges'];
								endif;
								?>
							</tbody>
						</table>
						<br/><br/>
						
						<table class="table table-bordered table-striped" style="clear: both; width:40%;" >
							<tbody>
								<tr>
									<th width="70%">Shippment (Tax).</th>
									<td><?=formatCurrencyByOrders($order['order']['shipping_charges'],  $order['order']['currency']);?></td>
								</tr>
								<tr>
									<th width="70%">Total</th>
									<td><?=formatCurrencyByOrders($total,  $order['order']['currency']);?></td>
								</tr>
							</tbody>
						</table>
						<br/><br/>

					</div> <!-- / .tab-pane -->
					<div class="tab-pane fade" id="billing-details">
						
						<h3>Customer Billing</h3>
						<table class="table table-bordered table-striped" style="clear: both">
							<tbody>
								<tr>
									<th colspan="4">Billing Information</th>
								</tr>
								<tr>
									<td width="35%">Full Name</td>
									<td width="65%"><?=$order['billing']['name'];?></td>
								</tr>
								<tr>
									<td width="35%">Email</td>
									<td width="65%"><?=$order['billing']['email'];?></td>
								</tr>
								<tr>
									<td width="35%">Address</td>
									<td width="65%"><?=$order['billing']['address'];?></td>
								</tr>
								<tr>
									<td width="35%">Telephone</td>
									<td width="65%"><?=$order['billing']['phone'];?></td>
								</tr>
								<tr>
									<td width="35%">Country</td>
									<td width="65%"><?=$order['billing']['country'];?></td>
								</tr>
								<tr>
									<td width="35%">State</td>
									<td width="65%"><?=$order['billing']['state'];?></td>
								</tr>
								<tr>
									<td width="35%">City</td>
									<td width="65%"><?=$order['billing']['city'];?></td>
								</tr>
								<tr>
									<td width="35%">Company</td>
									<td width="65%"><?=($order['billing']['company'] ? $order['billing']['company'] : '---');?></td>
								</tr>
								<tr>
									<td width="35%">Fax</td>
									<td width="65%"><?=($order['billing']['fax'] ? $order['billing']['fax'] : '---');?></td>
								</tr>
							</tbody>
						</table>
					</div> <!-- / .tab-pane -->
					<div class="tab-pane fade" id="shipping-details">
						<h3>Customer Shipping</h3>
						<table class="table table-bordered table-striped" style="clear: both">
							<tbody>
								<tr>
									<th colspan="2">Shipping Information</th>
								</tr>
								<?php if ( isset($order['shipping']) ) : ?>
								<tr>
									<td width="35%">Full Name</td>
									<td width="65%"><?=$order['shipping']['name'];?></td>
								</tr>
								<tr>
									<td width="35%">Email</td>
									<td width="65%"><?=$order['shipping']['email'];?></td>
								</tr>
								<tr>
									<td width="35%">Address</td>
									<td width="65%"><?=$order['shipping']['address'];?></td>
								</tr>
								<tr>
									<td width="35%">Telephone</td>
									<td width="65%"><?=$order['shipping']['phone'];?></td>
								</tr>
								<tr>
									<td width="35%">Country</td>
									<td width="65%"><?=$order['shipping']['country'];?></td>
								</tr>
								<tr>
									<td width="35%">State</td>
									<td width="65%"><?=$order['shipping']['state'];?></td>
								</tr>
								<tr>
									<td width="35%">City</td>
									<td width="65%"><?=$order['shipping']['city'];?></td>
								</tr>
								<tr>
									<td width="35%">Company</td>
									<td width="65%"><?=($order['shipping']['company'] ? $order['shipping']['company'] : '---');?></td>
								</tr>
								<tr>
									<td width="35%">Fax</td>
									<td width="65%"><?=($order['shipping']['fax'] ? $order['shipping']['fax'] : '---');?></td>
								</tr>
								<?php else: ?>
								<tr>
									<td width="100%">No shipping details</td>
								</tr>
								<?php endif;?>
							</tbody>
						</table>
					</div> <!-- / .tab-pane -->
				</div> <!-- / .tab-content -->
			</div>
		</div>
	</div>
	</div>
	
	<?php include_backend_component('footer'); ?>

</div>