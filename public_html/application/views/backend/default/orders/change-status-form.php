<ol class="breadcrumb">
	<li><a href="<?=link_to_backend('dashboard');?>">Dashboard</a></li>
	<li><a href="<?=link_to_backend('orders/confirmed');?>">Confirmed Orders</a></li>
	<li class="active">Change Order Status</li>
</ol>

<!-- ============================================================== -->
<!-- Start Content here -->
<!-- ============================================================== -->
<div class="content">
	<div class="widget">
		<?php if( $this->session->flashdata('alert-message')):?>
			<div class="widget-content padding">
				<?=alert_messages( $this->session->flashdata('alert-message') );?>
			</div>
		<?php endif; ?>

		<div class="widget-header transparent">
			<h2><strong>Orders</strong> Status</h2>
		</div>
		<div class="widget-content padding">
			<?=form_open(link_to_backend('orders/statusnow/'.$order->id));?>
				<div class="form-group">
					<div class="col-md-6">
						<label>Order Number</label>
						<p><?=$order->order_number;?></p>
					</div>
					<div class="col-md-6">
						<label>Payment Method</label>
						<p><?=$order->payment_method;?></p>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-6">
						<label>Shippment Charges</label>
						<p><?=formatCurrencyByOrders($order->shipping_charges, $order->currency);?></p>
					</div>
					<div class="col-md-6">
						<label>Discoun Amount</label>
						<p><?=formatCurrencyByOrders($order->discount_amount, $order->currency);?></p>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-6">
						<label>Total Amount</label>
						<p><?=formatCurrencyByOrders($order->total_amount, $order->currency);?></p>
					</div>
					<div class="col-md-6">
						<label>Order Date</label>
						<p><?=$order->created_at;?></p>
					</div>
				</div>
					
				
				<div class="form-group row">
					<div class="col-md-12">
						<label>Order Status</label>
						<select class="form-control" name="order_status">
							<option value="">-- Select --</option>
							<option value="confirmed" <?=( ($order->order_status=='confirmed') ? 'selected=selected' : '' );?>>Confirmed</option>
							<option value="in-transition" <?=( ($order->order_status=='in-transition') ? 'selected=selected' : '' );?>>In-Transition</option>
							<option value="delivered" <?=( ($order->order_status=='delivered') ? 'selected=selected' : '' );?>>Delivered</option>
						</select>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-md-12">
						<label>Notes/Shipping Tracking</label>
						<textarea name="notes" class="input form-control"><?=$order->notes;?></textarea>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-md-12">
						<button type="submit" class="btn btn-primary">Change Status Now</button>
					</div>
				</div>
			<?=form_close();?>
		</div>
	</div>

	<?php include_backend_component('footer'); ?>

</div>