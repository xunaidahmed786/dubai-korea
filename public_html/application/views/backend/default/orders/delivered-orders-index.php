<ol class="breadcrumb">
	<li><a href="<?=link_to_backend('dashboard');?>">Dashboard</a></li>
	<li class="active">Orders</li>
	<li class="active">Delivered Orders</li>
</ol>

<!-- ============================================================== -->
<!-- Start Content here -->
<!-- ============================================================== -->
<div class="content">
	
	<div class="row">

		<div class="col-md-12">
			<div class="widget">
				
				<?php if( $this->session->flashdata('alert-message')):?>
					<div class="widget-content padding">
						<?=alert_messages( $this->session->flashdata('alert-message') );?>
					</div>
				<?php endif; ?>

				<div class="widget-header">
					<h2><strong>Delivered Orders</strong> Manage</h2>
				</div>
				<div class="widget-content">
				<br>					
					<div class="table-responsive">
						<div class="row">
							<div class="col-md-4"></div>
							<div class="col-md-8">
								<div class="toolbar-btn-action" style="margin-right:15px;"></div>
							</div>
						</div>
						<br/>
						<form class='form-horizontal' role='form'>
							<table id="datatables-2" class="table table-striped table-bordered" cellspacing="0" width="100%">
						        <thead>
						            <tr>
						                <th>#</th>
						                <th>Customer Name</th>
						                <th>Order Number</th>
						                <th>Payment Type</th>
						                <th>Discount Amount</th>
						                <th>Total Amount</th>
						                <th>Total Items</th>
						                <th>Order Date</th>
						                <th>Status</th>
						                <th>Shipped date</th>
						                <th>Action</th>
						            </tr>
						        </thead>
						        <tbody>
						        	<?php 
						        	if( count($data) > 0 ) {
						        		foreach( $data as $key => $row ){
						        	?>
						            <tr>
						                <td>#</td>
						                <td><?=$row['first_name'];?> <?=$row['last_name'];?></td>
						                <td><?=$row['order_number'];?></td>
						                <td><?=$row['payment_method'];?></td>
						                <td><?=formatCurrency($row['discount_amount']);?></td>
						                <td><?=formatCurrency($row['total_amount']);?></td>
						                <td><?=$row['total_items'];?></td>
						                <td><?=$row['created_at'];?></td>
						                <td><?=ucfirst($row['order_status']);?></td>
						                <td><?=$row['shipped_at'];?></td>
						                <td>
											<div class="btn-group">
											  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
												<i class="fa fa-cog"></i> Action <span class="caret"></span>
											  </button>
											  <ul class="dropdown-menu primary" role="menu">
												<li><a href="<?=link_to_backend('orders/details/'.$row['id']);?>">Order Details</a></li>
											  </ul>
											</div>
						                </td>
						            </tr>
						            <?php 
						            	}
						            } ?>
						        </tbody>
						        </tbody>
							</table>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php include_backend_component('footer'); ?>

</div>