<!-- Left Sidebar Start -->
<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">
       <!-- Search form -->
        <form role="search" class="navbar-form">
            <div class="form-group">
                <input type="text" placeholder="Search" class="form-control">
                <button type="submit" class="btn search-button"><i class="fa fa-search"></i></button>
            </div>
        </form>
        <div class="clearfix"></div>
        <!--- Profile -->
        <div class="profile-info">
            <div class="col-xs-4">
              <a href="<?=link_to_backend('dashboard');?>" class="rounded-image profile-image" style="background:#fff">
                <img src="<?=storage('/images/logo.png');?>">
              </a>
            </div>
            <div class="col-xs-8">
                <div class="profile-text">Welcome <b><?=first_name();?></b></div>
                <div class="profile-buttons">
                  <!-- <a href="javascript:;"><i class="fa fa-envelope-o pulse"></i></a> -->
                  <!-- <a href="#connect" class="open-right"><i class="fa fa-comments"></i></a> -->
                  <a href="javascript:void(0)" data-modal="logout-modal" class="md-trigger" title="Sign Out"><i class="fa fa-power-off text-red-1"></i></a>
                </div>
            </div>
        </div>
        <!--- Divider -->
        <div class="clearfix"></div>
        <hr class="divider" />
        <div class="clearfix"></div>
        <!--- Divider -->
        <div id="sidebar-menu">
            <ul>
                <li>
                    <a href="<?php echo link_to_backend('dashboard');?>" class="active">
                        <span>Dashboard</span>
                    </a>
                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);">
                        <span>Categories</span>
                        <span class="pull-right"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul style="display: none;">
                        <li>
                            <a href="<?=link_to_backend('category/addNew');?>">
                                <span>Add New Category</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?=link_to_backend('category/manage');?>">
                                <span>Manage Categories</span>
                            </a>
                        </li>
                        <li style="display:none">
                            <a href="javascript:void(0);">
                                <span>Hierarchy Categories</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);">
                        <span>Brands</span>
                        <span class="pull-right"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul style="display: none;">
                        <li>
                            <a href="<?=link_to_backend('brands/addNew');?>">
                                <span>Add New Brand</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?=link_to_backend('brands/manage');?>">
                                <span>Manage Brands</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="">
                        <span>Products</span>
                        <span class="pull-right"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul style="display: none;">
                        <li>
                            <a href="<?=link_to_backend('products/addNew');?>">
                                <span>Add New Product</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?=link_to_backend('products/manage');?>">
                                <span>Manage Products</span>
                            </a>
                        </li>
                        <li style="display:none;">
                            <a href="javascript:void(0);">
                                <span>Attributes</span>
                            </a>
                            <ul style="display: none;">
                                <li>
                                    <a href="javascript:void(0);">
                                        <span>Add New Attributes</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">
                                        <span>Manage Attributes</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="">
                        <span>Orders</span>
                        <span class="pull-right"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul style="display: none;">
                        <li>
                            <a href="<?=link_to_backend('orders/confirmed');?>">
                                <span>Pending Orders</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?=link_to_backend('orders/delivered');?>">
                                <span>Delivered Orders</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="">
                        <span>Customers</span>
                        <span class="pull-right"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul style="display: none;">
                        <li>
                            <a href="<?=link_to_backend('customers/addNew');?>">
                                <span>Add New Customer</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?=link_to_backend('customers/manage');?>">
                                <span>Manage Customers</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="">
                        <span>Supports</span>
                        <span class="pull-right"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul>
                        <li>
                            <a href="<?=link_to_backend('support/wholeseller');?>">
                                <span>Whole Sellers</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?=link_to_backend('support/contactus');?>">
                                <span>Contact Us</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="">
                        <span>Sliders</span>
                        <span class="pull-right"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul style="display: none;">
                        <li>
                            <a href="<?=link_to_backend('sliders/addNew');?>">
                                <span>Add New Slider</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?=link_to_backend('sliders/manage');?>">
                                <span>Manage Sliders</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <!-- <li class='has_sub'>
                    <a href='javascript:void(0);'>
                        <i class='icon-home-3'></i>
                        <span>Administration</span>
                        <span class="pull-right"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul>
                        <li><a href='javascript:void(0)'><span>User Roles</span></a></li>
                    </ul>
                </li> -->
                <li class='has_sub'>
                    <a href='javascript:void(0);'>
                        <span>Administration</span>
                        <span class="pull-right"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul>
                        <li>
                            <a href="javascript:void(0);">
                                <span>Currencies</span>
                            </a>
                            <ul style="display: none;">
                                <li>
                                    <a href="<?=link_to_backend('currency/addNew');?>">
                                        <span>Add New Curreny</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?=link_to_backend('currency/manage');?>">
                                        <span>Manage Currencies</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span>Users</span>
                            </a>
                            <ul style="display: none;">
                                <li>
                                    <a href="<?=link_to_backend('users/addNew');?>">
                                        <span>Add New Users</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?=link_to_backend('users/manage');?>">
                                        <span>Manage Users</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li><a href='<?=link_to_backend('employee/change_password');?>'><span>Change Password</span></a></li>
                    </ul>
                </li>
                <div class="clearfix"></div>
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!-- Left Sidebar End -->