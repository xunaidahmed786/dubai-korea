<div class="widget-content padding">
    
    <div class="alert alert-success">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. <a href="#" class="alert-link">Alert Link</a>.
    </div>
    <div class="alert alert-info">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. <a href="#" class="alert-link">Alert Link</a>.
    </div>
    <div class="alert alert-warning">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. <a href="#" class="alert-link">Alert Link</a>.
    </div>
    <div class="alert alert-danger nomargin">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. <a href="#" class="alert-link">Alert Link</a>.
    </div>

</div>
<br/>
