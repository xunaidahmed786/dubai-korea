<?php
$current_active_tab = 'Add New'; 
$button 			= 'Save';
$parent_id 			= $edit_id = 0; 
$title 				= NULL;
$actions 			= link_to_backend('category/save');

if ( isset($edit_data) && count($edit_data) > 0 ){
	$edit_id 			= $edit_data->id;
	$parent_id 			= $edit_data->parent_id;
	$title 				= $edit_data->title;
	$actions 			= link_to_backend('category/update/'.$edit_id);
	
	$current_active_tab = 'Edit'; 
	$button 			= 'Update';
}

if ( $this->session->flashdata('data') ) {
	$parent_id 	= $this->session->flashdata('data')['parent_id'];
	$title 		= $this->session->flashdata('data')['title'];
}
?>

<ol class="breadcrumb">
	<li><a href="<?=link_to_backend('dashboard');?>">Dashboard</a></li>
	<li><a href="<?=link_to_backend('category/manage');?>">Categories</a></li>
	<li class="active"><?=$current_active_tab;?></li>
</ol>

<!-- ============================================================== -->
<!-- Start Content here -->
<!-- ============================================================== -->
<div class="content">
	<div class="widget">
		<?php if( $this->session->flashdata('alert-message')):?>
			<div class="widget-content padding">
				<?=alert_messages( $this->session->flashdata('alert-message') );?>
			</div>
		<?php endif; ?>

		<div class="widget-header transparent">
			<h2><strong>Category</strong> <?=$current_active_tab;?></h2>
		</div>
		<div class="widget-content padding">
			<?=form_open( $actions, ['id'=>'NotEmptyValidatorForCategory1', 'role' => 'form' ]); ?>
				<div class="form-group">
					<label>Parent Category</label>
					<select class="form-control" name="parent_id">
						<option value="0">-- Select Parent --</option>
						<?php 
						if ( count($categories) ) {
							foreach( buildTree($categories) as $value ) {
								if ( isset($value['children']) && is_array($value['children']) ){
									echo '<option value="'.$value['id'].'" '.(($value['id']==$parent_id) ? 'selected=selected' : '').'>'.$value['title'].'</option>';	
									
									foreach ($value['children'] as $child) {
										echo '<option value="'.$child['id'].'" '.(($child['id']==$parent_id) ? 'selected=selected' : '').'>--'.$child['title'].'</option>';
									}
								} else {
									echo '<option value="'.$value['id'].'" '.(($value['id']==$parent_id) ? 'selected=selected' : '').'>'.$value['title'].'</option>';	
								}
							}
						}
						?>
					</select>
				</div>
				<div class="form-group">
					<label>Title</label>
					<input type="text" class="form-control" name="title" value="<?=$title;?>">
				</div>
				<button type="submit" class="btn btn-primary"><?=$button;?></button>
			<?=form_close();?>
		</div>
	</div>

	<?php include_backend_component('footer'); ?>

</div>