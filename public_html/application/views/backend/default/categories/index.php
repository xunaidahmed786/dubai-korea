<ol class="breadcrumb">
	<li><a href="<?=link_to_backend('dashboard');?>">Dashboard</a></li>
	<li class="active">Categories</li>
	<li class="active">Manage</li>
</ol>

<!-- ============================================================== -->
<!-- Start Content here -->
<!-- ============================================================== -->
<div class="content">
	
	<div class="row">

		<div class="col-md-12">
			<div class="widget">
				
				<?php if( $this->session->flashdata('alert-message')):?>
					<div class="widget-content padding">
						<?=alert_messages( $this->session->flashdata('alert-message') );?>
					</div>
				<?php endif; ?>

				<div class="widget-header">
					<h2><strong>Categories</strong> Manage</h2>
				</div>
				<div class="widget-content">
				<br>					
					<div class="table-responsive">
						<form class='form-horizontal' role='form'>
						<table id="datatables-2" class="table table-striped table-bordered" cellspacing="0" width="100%">
						        <thead>
						            <tr>
						                <th>ID</th>
						                <th>Title</th>
						                <th>Created At</th>
						                <th>Updated At</th>
						                <th>Action</th>
						            </tr>
						        </thead>
						 
						        <!-- <tfoot>
						            <tr>
						                <th>Name</th>
						                <th>Position</th>
						                <th>Office</th>
						                <th>Age</th>
						                <th>Start date</th>
						                <th>Salary</th>
						            </tr>
						        </tfoot> -->
						 
						        <tbody>
						        	<?php 
						        	if( count($data) > 0 ) {
						        		foreach( $data as $key => $row ){
						        	?>
						            <tr>
						                <td><?=$row['id'];?></td>
						                <td><?=$row['title'];?></td>
						                <td><?=$row['created_at'];?></td>
						                <td><?=$row['updated_at'];?></td>
						                <td><?=action_buttons('category', $row['id']);?></td>
						            </tr>
						            <?php 
						            	}
						            } ?>
						        </tbody>
						    </table>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php include_backend_component('footer'); ?>

</div>
	
2011/04/25