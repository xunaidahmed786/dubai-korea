<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Admin - </title>   
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        
        <!-- Base Css Files -->
        <link href="<?=base_url('assets/backend');?>/assets/libs/jqueryui/ui-lightness/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />
        <link href="<?=base_url('assets/backend');?>/assets/libs/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
        <link href="<?=base_url('assets/backend');?>/assets/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
        <link href="<?=base_url('assets/backend');?>/assets/libs/fontello/css/fontello.css" rel="stylesheet" />
        <link href="<?=base_url('assets/backend');?>/assets/libs/animate-css/animate.min.css" rel="stylesheet" />
        <link href="<?=base_url('assets/backend');?>/assets/libs/nifty-modal/css/component.css" rel="stylesheet" />
        <link href="<?=base_url('assets/backend');?>/assets/libs/magnific-popup/magnific-popup.css" rel="stylesheet" /> 
        <link href="<?=base_url('assets/backend');?>/assets/libs/ios7-switch/ios7-switch.css" rel="stylesheet" /> 
        <link href="<?=base_url('assets/backend');?>/assets/libs/pace/pace.css" rel="stylesheet" />
        <link href="<?=base_url('assets/backend');?>/assets/libs/sortable/sortable-theme-bootstrap.css" rel="stylesheet" />
        <link href="<?=base_url('assets/backend');?>/assets/libs/bootstrap-datepicker/css/datepicker.css" rel="stylesheet" />
        <link href="<?=base_url('assets/backend');?>/assets/libs/jquery-icheck/skins/all.css" rel="stylesheet" />
        <link href="<?=base_url('assets/backend');?>/assets/css/style.css" rel="stylesheet" />
        <!-- Code Highlighter for Demo -->
        <link href="<?=base_url('assets/backend');?>/assets/libs/prettify/github.css" rel="stylesheet" />
        
        <!-- Extra CSS Libraries End -->
        <link href="<?=base_url('assets/backend');?>/assets/css/style-responsive.css" rel="stylesheet" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <!-- Page Specific JS Libraries -->
        <?php if( count($this->header_assets) ): foreach( $this->header_assets as $script ) {
        echo '<link href="'.base_url('assets/backend/'. $script).'" rel="stylesheet" type="text/css" />' . "\n";
        }
        endif; ?>
        
        <link rel="shortcut icon" href="<?=base_url('assets/backend');?>/assets/img/favicon.ico">
        <link rel="apple-touch-icon" href="<?=base_url('assets/backend');?>/assets/img/apple-touch-icon.png" />
        <link rel="apple-touch-icon" sizes="57x57" href="<?=base_url('assets/backend');?>/assets/img/apple-touch-icon-57x57.png" />
        <link rel="apple-touch-icon" sizes="72x72" href="<?=base_url('assets/backend');?>/assets/img/apple-touch-icon-72x72.png" />
        <link rel="apple-touch-icon" sizes="76x76" href="<?=base_url('assets/backend');?>/assets/img/apple-touch-icon-76x76.png" />
        <link rel="apple-touch-icon" sizes="114x114" href="<?=base_url('assets/backend');?>/assets/img/apple-touch-icon-114x114.png" />
        <link rel="apple-touch-icon" sizes="120x120" href="<?=base_url('assets/backend');?>/assets/img/apple-touch-icon-120x120.png" />
        <link rel="apple-touch-icon" sizes="144x144" href="<?=base_url('assets/backend');?>/assets/img/apple-touch-icon-144x144.png" />
        <link rel="apple-touch-icon" sizes="152x152" href="<?=base_url('assets/backend');?>/assets/img/apple-touch-icon-152x152.png" />
    </head>
    <body class="fixed-left">

    <!-- Begin page -->
	<div id="wrapper">
		
        <!-- Top Bar Start -->
        <div class="topbar">
            <div class="topbar-left">
                <div class="logo">
                    <!-- <h1><a  style="background:#fff" href="<?=link_to_backend('dashboard');?>"><img src="<?=storage('/images/logo.png');?>"></a></h1> -->
                </div>
                <button class="button-menu-mobile open-left">
                <i class="fa fa-bars"></i>
                </button>
            </div>
            <!-- Button mobile view to collapse sidebar menu -->
            <div class="navbar navbar-default" role="navigation">
                <div class="container">
                    <div class="navbar-collapse2">
                        <ul class="nav navbar-nav navbar-right top-navbar">
                            <li class="dropdown iconify hide-phone">
                                <a href="#" onclick="javascript:toggle_fullscreen()"><i class="icon-resize-full-2"></i></a>
                            </li>
                            <li class="dropdown topbar-profile">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <span class="rounded-image topbar-profile-image">
                                        <img src="<?=storage('/images/logo.png');?>">
                                    </span> <?=first_name();?> <strong><?=last_name();?></strong> 
                                    <i class="fa fa-caret-down"></i>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?=link_to_backend('employee/change_password');?>">Change Password</a></li>
                                    <li><a href="javascript:void(0)">Account Setting</a></li>
                                    <li class="divider"></li>
                                    <!-- <li><a href="lockscreen.h-tml"><i class="icon-lock-1"></i> Lock me</a></li> -->
                                    <li><a class="md-trigger" data-modal="logout-modal"><i class="icon-logout-1"></i> Logout</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
        </div>
        <!-- Top Bar End -->

        <!-- Modal Logout -->
        <div class="md-modal md-just-me" id="logout-modal" style="background: #fff;border: 1px solid #eee;">
            <div class="md-content">
                <h3><strong>Logout</strong> Confirmation</h3>
                <div>
                    <p class="text-center">Are you sure want to logout from this account system?</p>
                    <p class="text-center">
                    <button class="btn btn-danger md-close">Nope!</button>
                    <a href="<?=link_to_backend('logout');?>" class="btn btn-success md-close">Yeah, I'm sure</a>
                    </p>
                </div>
            </div>
        </div>
        <!-- Modal End -->  
        
        <?php include_backend_component('left_sidebar'); ?>

        <!-- Start right content -->
        <div class="content-page">
            <?php include( $template ); ?>
        </div>
        <!-- End right content -->
  	</div>
	<!-- End of page -->
	
    <!-- the overlay modal element -->
	<div class="md-overlay"></div>
	<!-- End of eoverlay modal -->
	<script>
		var resizefunc = [];
	</script>
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="<?=base_url('assets/backend');?>/assets/libs/jquery/jquery-1.11.1.min.js"></script>
	<script src="<?=base_url('assets/backend');?>/assets/libs/bootstrap/js/bootstrap.min.js"></script>
	<script src="<?=base_url('assets/backend');?>/assets/libs/jqueryui/jquery-ui-1.10.4.custom.min.js"></script>
	<script src="<?=base_url('assets/backend');?>/assets/libs/jquery-ui-touch/jquery.ui.touch-punch.min.js"></script>
	<script src="<?=base_url('assets/backend');?>/assets/libs/jquery-detectmobile/detect.js"></script>
	<script src="<?=base_url('assets/backend');?>/assets/libs/jquery-animate-numbers/jquery.animateNumbers.js"></script>
	<script src="<?=base_url('assets/backend');?>/assets/libs/ios7-switch/ios7.switch.js"></script>
	<script src="<?=base_url('assets/backend');?>/assets/libs/fastclick/fastclick.js"></script>
	<script src="<?=base_url('assets/backend');?>/assets/libs/jquery-blockui/jquery.blockUI.js"></script>
	<script src="<?=base_url('assets/backend');?>/assets/libs/bootstrap-bootbox/bootbox.min.js"></script>
	<script src="<?=base_url('assets/backend');?>/assets/libs/jquery-slimscroll/jquery.slimscroll.js"></script>
	<script src="<?=base_url('assets/backend');?>/assets/libs/jquery-sparkline/jquery-sparkline.js"></script>
	<script src="<?=base_url('assets/backend');?>/assets/libs/nifty-modal/js/classie.js"></script>
	<script src="<?=base_url('assets/backend');?>/assets/libs/nifty-modal/js/modalEffects.js"></script>
	<script src="<?=base_url('assets/backend');?>/assets/libs/sortable/sortable.min.js"></script>
	<script src="<?=base_url('assets/backend');?>/assets/libs/bootstrap-fileinput/bootstrap.file-input.js"></script>
	<script src="<?=base_url('assets/backend');?>/assets/libs/bootstrap-select/bootstrap-select.min.js"></script>
	<script src="<?=base_url('assets/backend');?>/assets/libs/bootstrap-select2/select2.min.js"></script>
	<script src="<?=base_url('assets/backend');?>/assets/libs/magnific-popup/jquery.magnific-popup.min.js"></script> 
	<script src="<?=base_url('assets/backend');?>/assets/libs/pace/pace.min.js"></script>
	<script src="<?=base_url('assets/backend');?>/assets/libs/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script src="<?=base_url('assets/backend');?>/assets/libs/jquery-icheck/icheck.min.js"></script>

	<!-- Demo Specific JS Libraries -->
	<script src="<?=base_url('assets/backend');?>/assets/libs/prettify/prettify.js"></script>
	<script src="<?=base_url('assets/backend');?>/assets/js/init.js"></script>

    <!-- Page Specific JS Libraries -->
    <?php if( count($this->footer_assets) ): foreach( $this->footer_assets as $script ) { ?>
    <script src="<?=base_url('assets/backend/' . $script);?>"></script>
    <?php }
    endif; ?>

    <?php if ( $this->footer_assets ) { ?>
    <script>
    $(function(){
        <?php echo $this->footerJS;?>
    });
    </script>
    <?php } ?>

	</body>
</html>