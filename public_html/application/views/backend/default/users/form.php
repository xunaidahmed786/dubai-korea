<?php
$current_active_tab = 'Add New'; 
$button 			= 'Save';
$edit_id 		= 0; 
$first_name     = 
$middle_name    = 
$last_name      =
$email      	=
$phone          = 
$country        = 
$state          = 
$city           = 
$postalcode     = 
$address        = null; 

$actions 			= link_to_backend('users/save');

if ( isset($edit_data) && count($edit_data) > 0 ){
	$edit_id 			= $edit_data->id;
    $first_name         = $edit_data->first_name;
	// $middle_name 		= $edit_data->middle_name;
	$last_name 			= $edit_data->last_name;
	$email 				= $edit_data->email;
	// $phone 			    = $edit_data->phone;
	// $address 			= $edit_data->address_1;
	// $country 			= $edit_data->country;
	// $state 				= $edit_data->state;
	// $city 				= $edit_data->city;
	// $active 			= $edit_data->active;
	$actions 			= link_to_backend('users/update/'.$edit_id);
	
	$current_active_tab = 'Edit'; 
	$button 			= 'Update';
}

if ( $this->session->flashdata('data') ) {
    $first_name     = $this->session->flashdata('data')['first_name'];
	// $middle_name    = $this->session->flashdata('data')['middle_name'];
    $last_name      = $this->session->flashdata('data')['last_name'];
    $email          = $this->session->flashdata('data')['email'];
    // $telephone      = $this->session->flashdata('data')['telephone'];
    // $address        = $this->session->flashdata('data')['address']; 
    // $country        = $this->session->flashdata('data')['country'];
    // $state          = $this->session->flashdata('data')['state'];
    // $city           = $this->session->flashdata('data')['city'];
    // $active         = $this->session->flashdata('data')['active'];
}
?>

<ol class="breadcrumb">
	<li><a href="<?=link_to_backend('dashboard');?>">Dashboard</a></li>
	<li><a href="<?=link_to_backend('users/manage');?>">Users</a></li>
	<li class="active"><?=$current_active_tab;?></li>
</ol>

<!-- ============================================================== -->
<!-- Start Content here -->
<!-- ============================================================== -->
<div class="content">
	<div class="widget">
		<?php if( $this->session->flashdata('alert-message')):?>
			<div class="widget-content padding">
				<?=alert_messages( $this->session->flashdata('alert-message') );?>
			</div>
		<?php endif; ?>

		<div class="widget-header transparent">
			<h2><strong>User</strong> <?=$current_active_tab;?></h2>
		</div>
		<div class="widget-content padding">
			<?=form_open( $actions, ['id'=>'NotEmptyValidatorForCustomer1', 'role' => 'form' ]); ?>
			
                <div class="col-sm-6">
                    <label for="first_name" class="required">First Name <strong>*</strong></label>
                    <input class="input form-control" name="first_name" value="<?=$first_name;?>" id="first_name" type="text">
                </div>
                <div class="col-sm-6">
                    <label for="last_name" class="required">Last Name <strong>*</strong></label>
                    <input name="last_name" class="input form-control" value="<?=$last_name;?>" id="last_name" type="text">
                </div>
                <div class="col-sm-12">
                    <label for="email_address" class="required">Email Address <strong>*</strong></label>
                    <input class="input form-control" name="email" value="<?=$email;?>" id="email_address" type="text">
                    <br/>
                </div>
                <div class="col-sm-6">
                    <label for="password" class="required">Password <?=($edit_id?null:'<strong>*</strong>');?></label>
                    <input class="input form-control" name="password" id="password" type="password">
                </div>
                <div class="col-sm-6">
                    <label for="confirm" class="required">Confirm Password <?=($edit_id?null:'<strong>*</strong>');?></label>
                    <input class="input form-control" name="confirm" id="confirm" type="password">
                    <br/>
                </div>
				<div class="col-sm-6">
					<button type="submit" class="btn btn-primary"><?=$button;?></button><br/><br/>
				</div>

			<?=form_close();?>
		</div>
	</div>

	<?php include_backend_component('footer'); ?>

</div>