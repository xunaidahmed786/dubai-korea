<?php
$current_active_tab = 'Add New'; 
$button 			= 'Save';
$edit_id 		= 0; 
$first_name     = 
$last_name      =
$email      	=
$telephone      = 
$country        = 
$state          = 
$city           = 
$postalcode     = 
$address        = null; 

$actions 			= link_to_backend('customers/save');

if ( isset($edit_data) && count($edit_data) > 0 ){
	$edit_id 			= $edit_data->id;
	$first_name 		= $edit_data->first_name;
	$last_name 			= $edit_data->last_name;
	$email 				= $edit_data->email;
	$telephone 			= $edit_data->telephone;
	$address 			= $edit_data->address;
	$country 			= $edit_data->country;
	$state 				= $edit_data->state;
	$city 				= $edit_data->city;
	$postal_code 		= $edit_data->postal_code;
	$active 			= $edit_data->active;
	$actions 			= link_to_backend('customers/update/'.$edit_id);
	
	$current_active_tab = 'Edit'; 
	$button 			= 'Update';
}

if ( $this->session->flashdata('data') ) {
	$first_name = $this->session->flashdata('data')['first_name'];
    $last_name  = $this->session->flashdata('data')['last_name'];
    $email      = $this->session->flashdata('data')['email'];
    $telephone  = $this->session->flashdata('data')['telephone'];
    $address    = $this->session->flashdata('data')['address']; 
    $postalcode = $this->session->flashdata('data')['postal_code']; 
    $country    = $this->session->flashdata('data')['country'];
    $state      = $this->session->flashdata('data')['state'];
    $city       = $this->session->flashdata('data')['city'];
}
?>

<ol class="breadcrumb">
	<li><a href="<?=link_to_backend('dashboard');?>">Dashboard</a></li>
	<li><a href="<?=link_to_backend('customers/manage');?>">Customers</a></li>
	<li class="active"><?=$current_active_tab;?></li>
</ol>

<!-- ============================================================== -->
<!-- Start Content here -->
<!-- ============================================================== -->
<div class="content">
	<div class="widget">
		<?php if( $this->session->flashdata('alert-message')):?>
			<div class="widget-content padding">
				<?=alert_messages( $this->session->flashdata('alert-message') );?>
			</div>
		<?php endif; ?>

		<div class="widget-header transparent">
			<h2><strong>Customer</strong> <?=$current_active_tab;?></h2>
		</div>
		<div class="widget-content padding">
			<?=form_open( $actions, ['id'=>'NotEmptyValidatorForCustomer1', 'role' => 'form' ]); ?>
			
                <div class="col-sm-6">
                    <label for="first_name" class="required">First Name <strong>*</strong></label>
                    <input class="input form-control" name="first_name" value="<?=$first_name;?>" id="first_name" type="text">
                </div>
                <div class="col-sm-6">
                    <label for="last_name" class="required">Last Name <strong>*</strong></label>
                    <input name="last_name" class="input form-control" value="<?=$first_name;?>" id="last_name" type="text">
                </div>
                <div class="col-sm-6">
                    <label for="email_address" class="required">Email Address <strong>*</strong></label>
                    <input class="input form-control" name="email" value="<?=$email;?>" id="email_address" type="text">
                </div>
                <div class="col-sm-6">
                    <label for="telephone" class="required">Telephone <strong>*</strong></label>
                    <input class="input form-control" name="telephone" value="<?=$telephone;?>" id="telephone" type="text">
                </div> 
                <div class="col-xs-12">
                    <label for="address" class="required">Address <strong>*</strong></label>
                    <input class="input form-control" name="address" value="<?=$address;?>" id="address" type="text">
                </div>
                <div class="col-sm-6">
                    <label for="city" class="required">City</label>
                    <input class="input form-control" name="city" id="city" value="<?=$city;?>" type="text">
                </div>
                <div class="col-sm-6">
                    <label class="required">State/Province</label>
                    <input class="input form-control" name="state" value="<?=$state;?>" type="text">
                </div>
                <div class="col-sm-6">
                    <label for="postal_code" class="required">Zip/Postal Code</label>
                    <input class="input form-control" name="postal_code" value="<?=$postalcode;?>" id="postal_code" type="text">
                </div>
                <div class="col-sm-6">
                    <label class="required">Country</label>
                    <input class="input form-control" name="country" value="<?=$country;?>" type="text">
                </div>
                <div class="col-sm-6">
                    <label for="password" class="required">Password <?=($edit_id?null:'<strong>*</strong>');?></label>
                    <input class="input form-control" name="password" id="password" type="password">
                </div>
                <div class="col-sm-6">
                    <label for="confirm" class="required">Confirm Password <?=($edit_id?null:'<strong>*</strong>');?></label>
                    <input class="input form-control" name="confirm" id="confirm" type="password">
                </div>
				
				<div class="col-sm-6">
					<br/>
					<button type="submit" class="btn btn-primary"><?=$button;?></button>
				</div>

			<?=form_close();?>
		</div>
	</div>

	<?php include_backend_component('footer'); ?>

</div>