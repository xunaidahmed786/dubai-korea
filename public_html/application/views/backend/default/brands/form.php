<?php
$current_active_tab = 'Add New'; 
$button 			= 'Save';
$edit_id 			= 0; 
$title        		= null;
$is_active 			= 1;

$actions 			= link_to_backend('brands/save');

if ( isset($edit_data) && count($edit_data) > 0 ){
	$edit_id 			= $edit_data->id;
    $title         		= $edit_data->title;
	$is_active 			= $edit_data->active;
	$actions 			= link_to_backend('brands/update/'.$edit_id);
	
	$current_active_tab = 'Edit'; 
	$button 			= 'Update';
}

if ( $this->session->flashdata('data') ) {
    
    $title     		= $this->session->flashdata('data')['title'];
	$is_active     	= $this->session->flashdata('data')['active'];
}
?>

<ol class="breadcrumb">
	<li><a href="<?=link_to_backend('dashboard');?>">Dashboard</a></li>
	<li><a href="<?=link_to_backend('brands/manage');?>">Brands</a></li>
	<li class="active"><?=$current_active_tab;?></li>
</ol>

<!-- ============================================================== -->
<!-- Start Content here -->
<!-- ============================================================== -->
<div class="content">
	<div class="widget">
		<?php if( $this->session->flashdata('alert-message')):?>
			<div class="widget-content padding">
				<?=alert_messages( $this->session->flashdata('alert-message') );?>
			</div>
		<?php endif; ?>

		<div class="widget-header transparent">
			<h2><strong>Brand</strong> <?=$current_active_tab;?></h2>
		</div>
		<div class="widget-content padding">
			<?=form_open( $actions, [ 'role' => 'form' ]); ?>
				
				<div class="form-group">
					<label>Name</label>
					<input type="text" class="form-control" name="title" value="<?=$title;?>">
				</div>

                <div class="form-group col-md-6" style="margin:0; padding:0;">
					<label>Status</label>
					<select class="form-control" name="active">
						<option value="1" <?=( ($is_active==1) ? 'selected=selected' : '' );?>>Active</option>
						<option value="0" <?=( ($is_active==0) ? 'selected=selected' : '' );?>>Inactive</option>
					</select>
					<br/>
				</div>
	
				<div class="form-group col-md-12" style="margin:0; padding:0;">
					<button type="submit" style="float:left;" class="btn btn-primary"><?=$button;?></button><br/><br/><br/><br/>
				</div>

			<?=form_close();?>
		</div>
	</div>

	<?php include_backend_component('footer'); ?>

</div>