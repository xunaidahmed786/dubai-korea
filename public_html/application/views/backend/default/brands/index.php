<ol class="breadcrumb">
	<li><a href="<?=link_to_backend('dashboard');?>">Dashboard</a></li>
	<li class="active">Brands</li>
	<li class="active">Manage</li>
</ol>

<!-- ============================================================== -->
<!-- Start Content here -->
<!-- ============================================================== -->
<div class="content">
	
	<div class="row">

		<div class="col-md-12">
			<div class="widget">
				
				<?php if( $this->session->flashdata('alert-message')):?>
					<div class="widget-content padding">
						<?=alert_messages( $this->session->flashdata('alert-message') );?>
					</div>
				<?php endif; ?>

				<div class="widget-header">
					<h2><strong>Brands</strong> Manage</h2>
				</div>
				<div class="widget-content">
				<br>					
					<div class="table-responsive">
						<form class='form-horizontal' role='form'>
						<table id="datatables-2" class="table table-striped table-bordered" cellspacing="0" width="100%">
						        <thead>
						            <tr>
						                <th>#</th>
						                <th width="60%">Name</th>
						                <th>Status</th>
						                <th>Created At</th>
						                <th>Action</th>
						            </tr>
						        </thead>
						        <tbody>
						        	<?php 
						        	if( count($data) > 0 ) {
						        		foreach( $data as $key => $row ){
						        	?>
						            <tr>
						                <td><i class="fa fa-book"></i></td>
						                <td><?=$row['title'];?></td>
						                <td><?=$row['active'];?></td>
						                <td><?=$row['created_at'];?></td>
						                <td><?=action_buttons('brands', $row['id']);?></td>
						            </tr>
						            <?php 
						            	}
						            } ?>
						        </tbody>
						    </table>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php include_backend_component('footer'); ?>

</div>
	
2011/04/25