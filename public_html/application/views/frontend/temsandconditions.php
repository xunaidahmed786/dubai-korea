<!-- MAIN -->
<main class="site-main">
    <div class="columns container">
        <!-- Block  Breadcrumb-->
        <ol class="breadcrumb no-hide">
            <li><a href="<?=base_url();?>">Home</a></li>
            <li class="active">Terms & Conditions</li>
        </ol><!-- Block  Breadcrumb-->

        <div class="row">
            <!-- Main Content -->
            <div class="col-md-12">
                <h2 class="page-heading">
                    <span class="page-heading-title2">Terms & Conditions</span>
                </h2>
                <div class="content-text clearfix">

                            

<p>By using Dubaikorea Services, you agree to these conditions. Please read them carefully. Dubaikorea reserves the right to amend or update the Terms &amp; Conditions at any time without providing notice to you.

</p><p><strong>1) Important Information:</strong></p><p>
</p><p>• By accessing, browsing or placing an order on DubaiKorea you agree to be bound by the associated terms, conditions, disclaimers and limitations of liability (Terms &amp; Conditions).</p><p>
</p><p>• By using Dubaikorea, you acknowledge that you have read and understood the Terms &amp; Conditions.</p><p>
</p><p>• Any dispute or claim arising out of or in connection with DubaiKorea shall be governed and construed in accordance with the laws of UAE.</p><p>
</p><p>• United Arab of Emirates is our country of domicile.</p><p>
</p><p>• Minors under the age of 18 shall are prohibited to register as a User of DubaiKorea and are not allowed to transact or use DubaiKorea.</p><p>
</p><p>• If you make a payment for our products or services on DubaiKorea, the details you are asked to submit will be provided directly to our payment provider via a secured connection.</p><p>
</p><p>• The cardholder must retain a copy of transaction records and Merchant policies and rules.</p><p>

</p><p><strong>2) Product and Purchasing:</strong></p><p>
</p><p>a) Availability</p><p>
</p><p>• Offers on special product mark downs, sale items are available until stocks sell out.</p><p>
</p><p>• All orders are subject to availability of stock and confirmation of the order price.</p><p>
</p><p>• All efforts will be taken to process orders within 24 hours from time and date the order is lodged.</p><p>

</p><p>b) Product Pictures</p><p>
</p><p>All colors of products are reproduced as accurately as possible, however a slight variation may occur in color and size specifications. Colors may appear slightly different via this web site due to computer picture resolution and individual monitor settings.</p><p>

</p><p>c) Pricing and Payment</p><p>
</p><p>Dubaikorea does not guarantee the price of any item listed on this website until your order has been processed. Whilst Dubaikorea takes steps to ensure the price and details of products listed are correct, if they are incorrect, Dubaikorea reserves the right to cancel the order and refund.</p><p>
</p><p>Dubaikorea reserves the right to take reasonable steps to verify that the order and credit details are bona fide. Delays may occur as a consequence of this process.</p><p>

</p><p>d) Sale Items and Special Product Mark Downs</p><p>
</p><p>Special product mark downs, sample products or sale items that appear on this website are only available when ordering online and are only available until such stocks sell out.

</p><p>e) Changing an Order</p><p>
</p><p>Changes can be made to orders after they have been submitted. If you are unable to cancel an order you have made, please write to us at (info@dubaikorea.com)</p><p>
</p><p>If you are unhappy with your purchase once you receive it, you may return it to us. See below for further information regarding returns and exchanges.</p><p> 

</p><p><strong>How to Return an Item:</strong></p><p>
</p><p>To Return an item to us please view the return process and please feel free to contact us at 00971-4-224-4999/info@dubaikorea.com and we will assign you a tracking number.</p><p>

</p><p><strong>Refund, Return &amp; Exchange Policy:</strong></p><p>
</p><p>The item(s) must be returned in a new and unused condition, unless there is a manufacturer defect. You must return the item within 30 days of your purchase. All original tags must still be attached. Please also include the original invoice. Delivery costs will only be reimbursed if the wrong item was delivered or if the item was unknowingly defective.</p><p>
</p><p>Dubaikorea <strong>WILL NOT</strong> at any of our retail stores allow an exchange or return of any items ordered online using this website.
</p><p><strong>PLEASE NOTE:<strong> We </strong>DO NOT</strong> accept exchange requests for special product mark downs, sample products, sale item or items that are no longer stocked unless the goods are unknowingly defective.</p><p> 
</p><p>We reserve the right to refuse a returned product that is damaged by the recipient due to lack of care or disregard of care or usage.</p><p>
</p><p>The item(s) remains your responsibility until it reaches us, so for your own protection we recommend that you send the parcel using a delivery service that insures you for the value of the goods. We will not return items which are damaged or lost in the post. And items that are opened cannot be returned.</p><p>
</p><p>Refunds will be done only through the Original Mode of Payment.</p><p>

</p><p><strong>Payment Policy</strong></p><p>
</p><p>Accepted methods of payments</p><p>
</p><p>1. Credit card (Visa, Master Card, American Express, Discover)</p><p>
</p><p>2. Cash On Delivery</p><p>
</p><p>“We accept payments online using Visa and MasterCard credit/debit card in AED (or any other agreed currency)”.</p><p>

</p><p><strong>Shipping &amp; Delivery Policy:</strong></p><p>
</p><p>1)Turnaround:</p><p>
</p><p>All orders are shipped within 48 hours Sunday- Thursday 10am- 6pm</p><p>

</p><p>2) Carriers:</p><p>
</p><p>We use the following carriers to deliver our orders:</p><p>
</p><p>1) FedEx</p><p>
</p><p>2) DHL</p><p>
</p><p>3) Aramex</p><p>

</p><p><strong>3) Order Tracking:</strong></p><p>
</p><p>If a tracking number is provided by a shipping carrier, we will update your order with the tracking information.</p><p>

</p><p><strong>4)Shipping Rates:</strong></p><p>
</p><p>Before the final checkout page, you will be shown what the cost of shipping will be, and you will have a chance to not place your order if you decide not to. The rate charges for the shipping of your order is based on the purchase of your products and your location. So you have purchased-</p><p>
</p><p>Above 299DHMS- delivery will be free</p><p>
</p><p>(Above $80- delivery will be free)</p><p>
</p><p>Order equal to 100DHMS- 15DHMS delivery charges</p><p>
</p><p>(Order Equal to $27- $4 delivery charges)</p><p>
</p><p>Order more than 299DHMS and less than 100DHMS- 12DHMS delivery charges</p><p>
</p><p>(Order more than $80 and less than $27- $3 delivery charges)</p><p>

</p><p>DubaiKorea will NOT deal or provide any services or products to any of OFAC (Office of Foreign Assets Control) sanctions countries in accordance with the law of UAE”.</p><p>

</p><p>Multiple shipments/delivery may result in multiple postings to the cardholder’s monthly statement.</p><p>
</p><p><strong>Disclaimer:</strong></p><p>
</p><p>The information contained on the website is provided by Dubaikorea in good faith. To the best of Dubaikorea’s knowledge, the information is accurate and current. However, Dubaikorea, its related bodies corporate and any of their respective directors, officers, employees, consultants or shareholders do not make any representation or warranty as to the accuracy or completeness of the information.</p><p>

</p><p><strong>Limitation of Liability:</strong></p><p>
</p><p>Subject to any applicable law that cannot be excluded, Dubaikorea and its related bodies corporate will not be liable to you or any other person for any non-excludable direct, indirect, incidental, special, consequential or exemplary damages, including but not limited to damages for product liability, personal injury or negligence resulting from or in connection with products or services available or supplied to you, or on your behalf, through the website.</p><p>
</p><p>Dubaikorea or its related bodies corporate will not in any case be liable for any lost profit (direct or indirect), deletion or corruption of electronically or digitally stored information or incidental, consequential or special damages arising out of or in connection with this website, the content and the products and services available or accessible on the website. </p><p>

</p>

               
            </div>
            </div><!-- Main Content -->
        </div>
    </div>
</main><!-- end MAIN -->