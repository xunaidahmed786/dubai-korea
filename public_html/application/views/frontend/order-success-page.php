<!-- MAIN -->
<main class="site-main">
    <div class="columns container">
        
        <br/><br/>

        <div class="row">
            
            <?php if ($status): ?>
            <!-- Main Content -->
            <div class="col-md-12">
                <h2 class="page-heading">
                    <span class="page-heading-title2"><center>Your Order has been received</center></span>
                </h2>
                <div class="content-text clearfix">
                    <p><center>Thank you for your purchase!</center></p>
                    <p><center>Your Order # is: <?=$order_details['order_number'];?></center></p>
                    <p><center>You will receive an order confirmation email with details of your order and a link to track to progress.</center></p>
                    <p>
                        <center>
                        Your order of <strong><?=getSelectFormatCurrency($order_details['amount']);?></strong> at Filterized has been confirmed. A confirmation has been sent to you at 
                        <strong><?=$order_details['email'];?></strong>.
                        </center>
                    </p>
                    <p>
                        <center>
                            <button class="btn btn-checkout" onclick="window.location='<?=base_url();?>'" type="button" title="Continue Shopping">
                            <span>Continue Shopping</span>
                            </button>
                        </center>
                    </p>
                </div>
            </div><!-- Main Content -->
            <?php else: ?>
            <!-- Main Content -->
            <div class="col-md-12">
                <h2 class="page-heading">
                    <span class="page-heading-title2"><center><?=$errors['error_title'];?></center></span>
                </h2>
                <div class="content-text clearfix">
                    <p style="display:none;"><center>Error Code: <?=$errors['error_code'];?></center></p>
                    <p><center><?=$errors['error_message'];?></center></p>
                    <p>
                        <center>
                            <button class="btn btn-checkout" onclick="window.location='<?=base_url();?>'" type="button" title="Back to Home">
                            <span>Back to Home</span>
                            </button>
                        </center>
                    </p>
                </div>
            </div><!-- Main Content -->
            <?php endif;?>

        </div>
    </div>
</main><!-- end MAIN -->