<!DOCTYPE html>
<html lang="en">
<head>
    <?php
    $site_title = 'Undefined Site Title';
    if ( isset($meta) && count($meta) ){
        $site_title = $meta['site_title'];
    }
    ?>
    <title><?php echo PROJECT_TITLE;?> - <?=$site_title;?></title>
    <link rel="shortcut icon" href="<?=storage('/images/favicon.ico');?>">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta http-equiv="cache-control" content="no-cache">
    <!-- Style CSS -->
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/frontend/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/frontend/css/custom.css">
    
    <!-- for Google -->
    <?php if ( isset($meta['properties']['description']) && $meta['properties']['description'] ){ ?>
    <meta name="description" content="<?=$meta['properties']['description'];?>" />
    <?php } ?>
    <?php if ( isset($meta['properties']['keywords']) && $meta['properties']['keywords'] ){ ?>
    <meta name="keywords" content="<?=$meta['properties']['keywords'];?>" />
    <?php } ?>

    <!-- for Facebook -->          
    <meta property="og:title" content="<?=$site_title;?>" />
    <meta property="og:type" content="website">
    <?php if ( isset($meta['properties']['image']) && $meta['properties']['image'] ){ ?>
    <meta property="og:image" content="<?=$meta['properties']['image'];?>" />
    <?php } ?>
    <?php if ( isset($meta['properties']['url']) && $meta['properties']['url'] ){ ?>
    <meta property="og:url" content="<?=$meta['properties']['url'];?>" />
    <?php } ?>
    <?php if ( isset($meta['properties']['description']) && $meta['properties']['description'] ){ ?>
    <meta name="og:description" content="<?=$meta['properties']['description'];?>" />
    <?php } ?>

    <!-- for Twitter -->          
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:title" content="<?=$site_title;?>" />
    <?php if ( isset($meta['properties']['description']) && $meta['properties']['description'] ){ ?>
    <meta name="twitter:description" content="<?=$meta['properties']['description'];?>" />
    <?php } ?>
    <?php if ( isset($meta['properties']['image']) && $meta['properties']['image'] ){ ?>
    <meta property="twitter:image" content="<?=$meta['properties']['image'];?>" />
    <?php } ?>
    <?php if ( isset($meta['properties']['url']) && $meta['properties']['url'] ){ ?>
    <meta property="twitter:url" content="<?=$meta['properties']['url'];?>" />
    <?php } ?>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <!-- Page Script -->
    <?php 
    if( count($this->header_assets) ): foreach( $this->header_assets as $script ) {
        echo '<link href="'.base_url('assets/frontend/'. $script).'" rel="stylesheet" type="text/css" />' . "\n";
    }
    endif; ?>
    <?php if ( $this->headerCSS ) { ?>
    <style type="text/css">
        <?php echo $this->headerCSS;?>
    </style>
    <?php } ?>
</head>
<body class="index-opt-1 catalog-category-view catalog-view_op1">

    <div id="loading">
        <div class="spinner">
            <div class="rect1"></div>
            <div class="rect2"></div>
            <div class="rect3"></div>
            <div class="rect4"></div>
            <div class="rect5"></div>
        </div>
    </div>

    <div class="wrapper"></div>

    <?php
    $name               = null;
    $is_alert_show      = (($this->router->fetch_method() == 'index') ? TRUE : FALSE);

    if ( $this->session->flashdata('data') )
    {
        $email              = $this->session->flashdata('data')['email'];
    }
    ?>
    
    <?php if ( $is_alert_show ): ?>
        <?php if( $this->session->flashdata('alert-danger')):?>
        <div class="widget-content margin-none padding-none">
            <div class="alert alert-danger margin-none padding-none">
                <?php echo $this->session->flashdata('alert-danger') ;?>
            </div>
        </div>
        <?php endif; ?>

        <?php if( $this->session->flashdata('alert-success')):?>
        <div class="widget-content margin-none padding-none">
            <div class="alert alert-success margin-none padding-none">
                <?php echo $this->session->flashdata('alert-success') ;?>
            </div>
        </div>
        <?php endif; ?>
    <?php endif;?>

        <!-- HEADER -->
        <header class="site-header header-opt-1 cate-show">

            <div class="header-top">
                <div class="container">

                    <!-- nav-left -->
                    <ul class="nav-left">
                        <?php if ( check_login() ){ ?>
                        <li class="mobile_responsive_account">
                            <a href="<?=site_url('my-orders');?>"><?php echo first_name();?></a>
                        </li>
                        <?php } ?>
                        <li class="mobile_responsive_account dropdown setting">
                            <a data-toggle="dropdown" role="button" href="<?=site_url('my-orders');?>" class="dropdown-toggle " aria-expanded="false">
                                <span>My Account</span> <i aria-hidden="true" class="fa fa-angle-down"></i>
                            </a>
                            <div class="dropdown-menu" style="right: 0px; left: auto;">
                                <ul class="account">
                                    <?php if ( check_login() ){ ?>
                                    <li><a href="<?=site_url('my-orders');?>">My Orders</a></li>
                                    <li><a href="<?=site_url('wishlist');?>">Wishlistt</a></li>
                                    <li><a href="<?=site_url('logout');?>">Logout</a></li>
                                    <?php } else { ?>
                                    <li><a href="<?=site_url('wholesale-enquiry');?>">Whole Seller Query</a></li>
                                    <li><a href="<?=site_url('login');?>">Customer Login</a></li>
                                    <li><a href="<?=site_url('login');?>">Register</a></li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </li>
                        <li class="top_contact_us"><span><i class="fa fa-phone" aria-hidden="true"></i>+971-4-2244999</span></li>
                        <li class="top_contact_us">
                            <a href="<?=site_url('contact-us');?>">
                                <span><i class="fa fa-envelope" aria-hidden="true"></i> Contact us today !</span>
                            </a>
                        </li>
                        <li class="dropdown switcher switcher-currency">
                            <?php 
                            $curreny_icons = 
                            [
                                'USD' => '<i class="fa fa-usd" aria-hidden="true"></i>',
                                'EUR' => '<i class="fa fa-eur" aria-hidden="true"></i>',
                            ]; 
                            ?>
                            <a data-toggle="dropdown" role="button" href="#" class="dropdown-toggle switcher-trigger" aria-expanded="false">
                                <?php if ( $currency_active ) : ?>
                                    <span>
                                        <?php echo ( isset($curreny_icons[$currency_active['symbols']]) ? $curreny_icons[$currency_active['symbols']] : DEFAULT_CURRENCY_ICON );?>
                                        <?=$currency_active['symbols'];?>
                                    </span> 
                                    <i aria-hidden="true" class="fa fa-angle-down"></i>
                                <?php else:?>
                                    <span> <?=DEFAULT_CURRENCY_ICON;?> <?=DEFAULT_CURRENCY_NAME;?></span> 
                                    <i aria-hidden="true" class="fa fa-angle-down"></i>
                                <?php endif;?>
                            </a>
                            <ul class="dropdown-menu switcher-options " style="right: 0px; left: auto; min-width:80px; width: 110px;">
                                <li class="switcher-option">
                                    <a  style="padding: 6px 20px;" href="<?=site_url('currency-change/'.DEFAULT_CURRENCY_NAME);?>">
                                        <?=DEFAULT_CURRENCY_ICON;?>  <?=DEFAULT_CURRENCY_NAME;?>
                                    </a>
                                </li>
                                <?php
                                if ( count($currencies) ) :
                                    // symbols
                                    foreach($currencies as $currency ):
                                ?>
                                    <li class="switcher-option">
                                        <a  style="padding: 6px 20px;" href="<?=site_url('currency-change/'.$currency['symbols']);?>">
                                            <?php echo ( isset($curreny_icons[$currency['symbols']]) ? $curreny_icons[$currency['symbols']] : DEFAULT_CURRENCY_ICON );?> <?=$currency['symbols'];?>
                                        </a>
                                    </li>
                                <?php
                                    endforeach;
                                endif;
                                ?>
                            </ul>
                        </li>
                        <li class="dropdown switcher switcher-language">
                            <a data-toggle="dropdown" role="button" href="#" class="dropdown-toggle switcher-trigger" aria-expanded="false">
                                <img class="switcher-flag" alt="flag" src="<?=base_url();?>assets/frontend/images/flags/flag_english.png">
                                <span>English</span> 
                                <i aria-hidden="true" class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu switcher-options " style="right: 0px; left: auto; width: 130px; min-width: 60px;">
                                <li class="switcher-option">
                                    <a href="javascript:void(0)" class="lang_online" data-lang="English">
                                        <img class="switcher-flag" src="<?=base_url('assets/frontend/images/flags/flag_english.png');?>">English
                                    </a>
                                </li>
                                <li class="switcher-option">
                                    <a href="javascript:void(0)" class="lang_online" data-lang="Arabic">
                                        <img class="switcher-flag" src="<?=base_url('assets/frontend/images/flags/flag_UAE.png');?>">Arabic
                                    </a>
                                </li>
                                <li class="switcher-option">
                                    <a href="javascript:void(0)" class="lang_online" data-lang="Russian">
                                        <img class="switcher-flag" src="<?=base_url('assets/frontend/images/flags/flag_russia.png');?>">Russian
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <div id="google_translate_element"></div>
                        </li>
                    </ul><!-- nav-left -->

                    <!-- nav-right -->
                    <ul class=" nav-right">
                        <?php if ( check_login() ){ ?>
                        <li>
                            <a href="<?=site_url('my-orders');?>"><?php echo first_name();?>  <?php echo last_name();?> </a>
                        </li>
                        <?php } ?>
                        <li class="dropdown switcher switcher-language">
                            <a data-toggle="dropdown" role="button" href="<?=site_url('my-orders');?>" class="dropdown-toggle switcher-trigger" aria-expanded="false">
                                <span>My Account</span> 
                                <i aria-hidden="true" class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu switcher-options " style="right: 0px; left: auto; width: 150px; min-width: 60px;">
                                <?php if ( check_login() ){ ?>
                                    <li class="switcher-option"><a href="<?=site_url('my-orders');?>">My Orders</a></li>
                                    <li class="switcher-option"><a href="<?=site_url('wishlist');?>">Wishlistt</a></li>
                                    <li class="switcher-option"><a href="<?=site_url('logout');?>">Logout</a></li>
                                <?php } else { ?>
                                    <li class="switcher-option"><a href="<?=site_url('wholesale-enquiry');?>">Whole Seller Query</a></li>
                                    <li class="switcher-option"><a href="<?=site_url('login');?>">Customer Login</a></li>
                                    <li class="switcher-option"><a href="<?=site_url('login');?>">Register</a></li>
                                <?php } ?>
                            </ul>
                        </li>
                        <li><a href="<?=site_url('contact-us');?>">Support </a></li>
                    </ul><!-- nav-right -->

                </div>
            </div>

            <!-- header-content -->
            <div class="header-content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3 nav-left">
                            <!-- logo -->
                            <strong class="logo">
                                <a href="<?=base_url();?>"><img src="<?=storage('/images/logo.png');?>" alt="<?=$site_title;?>"></a>
                            </strong>
                        </div>
                        <div class="nav-right" style="display:block;">
                            <!-- block mini cart -->  
                            <div class="block-minicart dropdown">
                                <a class="dropdown-toggle" role="button" data-toggle="dropdown">
                                    <span class="cart-icon" onclick="javascript:window.location='<?=site_url('shopping-cart');?>'"></span>
                                    <span class="counter qty" style="border:none;">
                                        <?php $items_total      = currencyConvertToAmount($this->cart->total());?>
                                        <?php $shipping_charges = (round($items_total) ? shippingPolicyRules($items_total) : formatCurrency(0.00));?>

                                        <span class="cart-text" style="display:none;">Basket</span>
                                        <span class="counter-number"><?php echo $this->cart->total_items();?></span>
                                        <span class="counter-label display-none"><?php echo count($this->cart->contents());?> <span>Items</span></span>
                                        <span class="counter-price display-none"><?=getSelectFormatCurrency($items_total+$shipping_charges);?></span>
                                    </span>
                                </a>
                                <div class="dropdown-menu" style="right: 0px; left: auto;">
                                    <form>
                                        <div class="minicart-content-wrapper">
                                            <div class="subtitle">
                                                You have <?php echo count($this->cart->contents());?> item(s) in your cart
                                            </div>
                                            <div class="minicart-items-wrapper">
                                                <ol class="minicart-items">
                                                   <?php 
                                                    $is_cart    = false;
                                                    if ( count($this->cart->contents()) ):
                                                        $i = 1;
                                                        $is_cart = true;
                                                        foreach ($this->cart->contents() as $items):
                                                    ?>
                                                    <li class="product-item">
                                                        <a class="product-item-photo" href="<?php echo $items['options']['product_url']; ?>">
                                                            <?php if ( $items['options']['product_image'] ) : ?>
                                                                <img class="product-image-photo" src="<?php echo storage('products/'.$items['options']['product_image']); ?>" alt="<?php echo $items['name']; ?>">
                                                            <?php else: ?>
                                                                <small><i>(Not available)</i></small>
                                                            <?php endif;?>
                                                        </a>
                                                        <div class="product-item-details">
                                                            <strong class="product-item-name">
                                                                <a href="<?php echo $items['options']['product_url']; ?>"><?php echo $items['name']; ?></a>
                                                            </strong>
                                                            <div class="product-item-price">
                                                                <?php if ( $items['sale_price'] == 0.00 ) : ?>
                                                                    <span class="price" style="text-align:left;"><?php echo formatCurrency($items['original_price']); ?></span>
                                                                <?php else : ?>
                                                                    <span class="price" style="text-align:left;"><del><?php echo formatCurrency($items['original_price']); ?></del></span>
                                                                    <span class="price" style="text-align:left;"><?php echo formatCurrency($items['sale_price']); ?></span>
                                                                <?php endif;?>
                                                            </div>
                                                            <div class="product-item-qty">
                                                                <span class="label">Qty: </span><span class="number"><?php echo $items['qty']; ?></span>
                                                            </div>
                                                            <div class="product-item-actions">
                                                                <a class="action delete" href="<?php echo site_url('shopping-cart/remove/'.$items['rowid']); ?>" title="Remove item">
                                                                    <span>Remove</span>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <?php
                                                        $i++;
                                                        endforeach;
                                                    else:
                                                    ?>
                                                    <li>No items</li>
                                                    <?php endif;?>
                                                </ol>
                                            </div>
                                            <div class="subtotal">
                                                <span class="label">Total</span>
                                                <span class="price"><?=getSelectFormatCurrency($items_total+$shipping_charges);?></span>
                                            </div>
                                            <div class="actions">
                                            <?php if ( $is_cart ) : ?>
                                                <button class="btn btn-checkout" onclick="window.location='<?php echo site_url('checkout');?>'" type="button" title="Check Out">
                                                    <span>Checkout</span>
                                                </button>
                                            <?php endif;?><br/>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="nav-mind">
                            <div class="block-search">
                                <div class="block-title">
                                    <span>Search</span>
                                </div>
                                <div class="block-content">
                                    <form method="POST" id="topHeaderSeachBar">
                                        <div class="categori-search display-none">
                                            <select data-placeholder="All Categories" name="category_id" class="categori-search-option">
                                                <option value="all">All Categories</option>
                                                <?php
                                                if ( count($categories) ) {
                                                    foreach ( $categories as $category) {
                                                ?>
                                                    <option value="<?=$category['slug'];?>"><?=$category['title'];?></option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="form-search pull-right">
                                            <div class="box-group">
                                                <input type="text" name="title" class="form-control required-field" placeholder="i'm Searching for...">
                                                <button class="btn btn-search" type="submit"><span>search</span></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div><!-- header-content -->

            <!-- header-nav -->
            <div class="header-nav mid-header">
                <div class="container">
                    <div class="box-header-nav">

                        <!-- btn categori mobile -->
                        <span data-action="toggle-nav-cat" class="nav-toggle-menu nav-toggle-cat"><span>Categories</span></span>

                        <!--categori  -->
                        <?php load_frontend_view('components/top_nav_categories', ['categories'=>$categories]);?>
                        <!--categori  -->

                        <!-- mini cart -->
                        <div class="block-minicart dropdown">
                            <a class="dropdown-toggle" href="javascript:void(0)" role="button" data-toggle="dropdown">
                                <span class="cart-icon"></span>
                            </a>
                            <?php include_frontend_component('mini_carts');?>
                        </div>

                        <!-- search -->
                        <div class="block-search">
                            <div class="block-title">
                                <span>Search</span>
                            </div>
                            <div class="block-content">
                                <div class="form-search">
                                    <form method="POST" id="topResonsiveHeaderSeachBar">
                                        <div class="box-group">
                                            <input type="text" name="title" class="form-control" placeholder="i'm Searching for...">
                                            <button class="btn btn-search" type="button"><span>search</span></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div><!-- search -->

                        <!--setting  -->
                        <div class="dropdown setting">
                            <a data-toggle="dropdown" role="button" href="#" class="dropdown-toggle "><span>Settings</span> <i aria-hidden="true" class="fa fa-user"></i></a>
                            <div class="dropdown-menu  ">
                                <div class="switcher  switcher-language">
                                    <strong class="title">Select language</strong>
                                    <ul class="switcher-options">
                                        <li class="switcher-option">
                                            <a href="javascript:void(0)" class="lang_online" data-lang="English">
                                                <img class="switcher-flag" src="<?=base_url('assets/frontend/images/flags/flag_english.png');?>">
                                            </a>
                                        </li>
                                        <li class="switcher-option">
                                            <a href="javascript:void(0)" class="lang_online" data-lang="Arabic">
                                                <img class="switcher-flag" src="<?=base_url('assets/frontend/images/flags/flag_UAE.png');?>">
                                            </a>
                                        </li>
                                        <li class="switcher-option">
                                            <a href="javascript:void(0)" class="lang_online" data-lang="Russian">
                                                <img class="switcher-flag" src="<?=base_url('assets/frontend/images/flags/flag_russia.png');?>">
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="switcher  switcher-currency">
                                    <strong class="title">SELECT CURRENCIES</strong>
                                    <ul class="switcher-options ">
                                        <li class="switcher-option <?php echo ($currency_active == "" ? 'switcher-active' : null);?>">
                                            <a style="height: 20px;padding-right:3px;line-height: 18px;" href="<?=site_url('currency-change/'.DEFAULT_CURRENCY_NAME);?>">
                                                <i class="fa fa-aed" aria-hidden="true"></i> <?=DEFAULT_CURRENCY_NAME;?>
                                            </a>
                                        </li>
                                        <?php
                                        if ( count($currencies) ) :
                                            foreach($currencies as $currency ):
                                        ?>
                                            <li class="switcher-option <?php echo ( ( isset($currency_active['symbols']) && $currency_active['symbols'] == $currency['symbols']) ? 'switcher-active' : null );?> ">
                                                <a style="height: 20px;padding-right:3px;line-height: 18px;"  href="<?=site_url('currency-change/'.$currency['symbols']);?>">
                                                    <i class="fa fa-aed"aria-hidden="true" ></i> <?=$currency['symbols'];?>
                                                </a>
                                            </li>
                                        <?php
                                            endforeach;
                                        endif;
                                        ?>
                                    </ul>
                                </div>
                                <ul class="account">
                                    <li><a href="<?=site_url('login');?>">Login/Register</a></li>
                                    <li><a href="<?=site_url('my-orders');?>">My Account</a></li>
                                    <li><a href="<?=site_url('wishlist');?>">Wishlist</a></li>
                                    <li><a href="<?=site_url('checkout');?>">Checkout</a></li>
                                </ul>
                            </div>
                        </div><!--setting  -->
                        
                    </div>
                </div>
            </div><!-- header-nav -->
        </header><!-- end HEADER -->
           
		<?php include( $template ); ?>
		
		<!-- FOOTER -->
        <footer class="site-footer footer-opt-1">
            <div class="container">
                <div class="footer-column">
                    <div class="row">
                        <div class="col-md-3 col-lg-3 col-xs-12 col">
                            <strong class="logo-footer notranslate">
                                <a href="<?=base_url();?>"><img src="<?=storage('/images/logo.png');?>" alt="<?=$site_title;?>"></a>
                            </strong>

                            <table class="address notranslate">
                                <tr>
                                    <td><b>Address:  </b></td>
                                    <td>Silicon Oasis</td>
                                </tr>
                                <tr>
                                    <td><b>Phone: </b></td>
                                    <td><a href="tel:+971-4-2244999">+971-4-2244999</a></td>
                                </tr>
                                <tr>
                                    <td><b>Email:</b></td>
                                    <td><a href="mailto:support@dubaikorea.com">Support@dubaikorea.com</a></td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-2 col-lg-2 col-xs-12 col policy_footer_links">
                            <div class="links">
                            <h3 class="title">Company</h3>
                            <ul>
                                <li><a href="<?=site_url('about-us');?>">About Us</a></li>
                                <li><a href="<?=site_url('privacy-policy');?>">Privacy Policy</a></li>
                                <li><a href="<?=site_url('terms-and-conditions');?>">Terms &amp; Conditions</a></li>
                                <li><a href="<?=site_url('contact-us');?>">Contact Us</a></li>
                            </ul>
                            </div>
                        </div>
                        <div class="col-md-2 col-lg-2 col-xs-12 col myaccount_footer_links">
                            <div class="links">
                            <h3 class="title">My Account</h3>
                            <ul>
                                <li><a href="<?=site_url('my-orders');?>">My Order</a></li>
                                <li><a href="<?=site_url('wishlist');?>">My Wishlist</a></li>
                                <li><a href="<?=site_url('my-orders');?>">My Account</a></li>
                            </ul>
                            </div>
                        </div>
                        <div class="col-md-2 col-lg-2 col-xs-12 col support_footer_links">
                            <div class="links">
                            <h3 class="title">Support</h3>
                            <ul>
                                <li><a href="<?=site_url('contact-us');?>">Help Center</a></li>
                                <li><a href="<?=site_url('terms-and-conditions');?>">Refund Policy</a></li>
                                <li><a href="<?=site_url('contact-us');?>">Report Spam</a></li>
                                <li><a href="<?=site_url('terms-and-conditions');?>">FAQs</a></li>
                            </ul>
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3 col-xs-12 col">
                            <div class="block-newletter">
                                <div class="block-title">NEWSLETTER</div>
                                <div class="block-content">
                                    <form method="POST" id="getSubscriberNow" action="<?php echo site_url('subscribe/now');?>">
                                    <div class="input-group">
                                        <input type="text" class="form-control required-field" name="email" placeholder="Your Email Address">
                                        <span class="input-group-btn">
                                            <button class="btn btn-subcribe" type="submit"><span>Now</span></button>
                                        </span>
                                    </div>
                                    </form>
                                </div>
                            </div>
                            <div class="block-social">
                                <div class="block-title">Let’s Socialize </div>
                                <div class="block-content">
                                    <a href="https://www.facebook.com/DKdubaikorea/" target="_blank" class="sh-facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                    <a href="https://www.pinterest.com/Dubaikorea/" target="_blank" class="sh-pinterest"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a>
                                    <a href="https://www.youtube.com/channel/UC0PeGt2PhQouwa-fMbPGHIg" target="_blank" class="sh-youtube"><i class="fa fa-youtube" aria-hidden="true"></i></a>
                                    <a href="https://www.instagram.com/dubaikorea/" target="_blank" class="sh-instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="payment-methods">
                    <div class="block-title">
                        Accepted Payment Methods
                    </div>
                    <div class="block-content">
                        <img src="<?=base_url('assets/frontend/images/payments/visa.png');?>">
                        <img src="<?=base_url('assets/frontend/images/payments/mastercard.png');?>">
                        <img src="<?=base_url('assets/frontend/images/payments/paypal.png');?>">
                        <img src="<?=base_url('assets/frontend/images/payments/cash-on-delivery.png');?>">
                    </div>
                </div>
                <div class="footer-bottom">
                    <div class="links">
                        <ul>
                            <li><a href="<?=base_url();?>">Dubai Korea</a></li>
                        </ul>
                        <ul>
                            <li><a href="javascript:void(0)">Promotions</a></li>
                            <li><a href="<?=site_url('orders');?>">My Orders </a></li>
                            <li><a href="<?=site_url('contact-us');?>">Help</a></li>
                            <li><a href="javascript:void(0)">Site Map</a></li>
                            <li><a href="<?=site_url('contact-us');?>">Customer Service</a></li>
                            <li><a href="<?=site_url('terms-and-conditions');?>">Support</a></li>
                        </ul>
                        <ul>
                            <li><a href="javascript:void(0)">Most Popular</a></li>
                            <li><a href="javascript:void(0)">Best Sellers</a></li>
                            <li><a href="javascript:void(0)">New Arrivals</a></li>
                            <li><a href="<?=site_url('about-us');?>">Our Store</a></li>
                            <li><a href="<?=site_url('contact-us');?>">Refunds</a></li>
                        </ul>
                        <ul>
                            <li><a href="<?=site_url('terms-and-conditions');?>">Terms &amp; Conditions </a></li>
                            <li><a href="<?=site_url('privacy-policy');?>">Privacy Policy</a></li>
                            <li><a href="<?=site_url('terms-and-conditions');?>">Shipping</a></li>
                            <li><a href="<?=site_url('terms-and-conditions');?>">Returns</a></li>
                            <li><a href="<?=site_url('terms-and-conditions');?>">Warrantee</a></li>
                            <li><a href="<?=site_url('terms-and-conditions');?>">FAQs</a></li>
                            <li><a href="<?=site_url('contact-us');?>">Contact Us</a></li>
                        </ul>
                    </div>
                </div>
                <div class="copyright">
                    Copyright &copy; 2017 DubaiKorea. All Rights Reserved.
                </div>
            </div>
        </footer><!-- end FOOTER -->        
        <!--back-to-top  -->
        <a href="#" class="back-to-top">
            <i aria-hidden="true" class="fa fa-angle-up"></i>
        </a>
    </div>

    <!-- jQuery -->    
    <script type="text/javascript" src="<?=base_url();?>assets/frontend/js/jquery.min.js"></script>
    <!-- sticky -->
    <script type="text/javascript" src="<?=base_url();?>assets/frontend/js/jquery.sticky.js"></script>
    <!-- OWL CAROUSEL Slider -->    
    <script type="text/javascript" src="<?=base_url();?>assets/frontend/js/owl.carousel.min.js"></script>
    <!-- Boostrap --> 
    <script type="text/javascript" src="<?=base_url();?>assets/frontend/js/bootstrap.min.js"></script>
    <!-- Countdown --> 
    <script type="text/javascript" src="<?=base_url();?>assets/frontend/js/jquery.countdown.min.js"></script>
    <!--jquery Bxslider  -->
    <script type="text/javascript" src="<?=base_url();?>assets/frontend/js/jquery.bxslider.min.js"></script>
    <!-- actual --> 
    <script type="text/javascript" src="<?=base_url();?>assets/frontend/js/jquery.actual.min.js"></script>
    <!-- jQuery UI -->
    <script type="text/javascript" src="<?=base_url();?>assets/frontend/js/jquery-ui.min.js"></script>
    <!-- Chosen jquery-->    
    <script type="text/javascript" src="<?=base_url();?>assets/frontend/js/chosen.jquery.min.js"></script>
    <!-- parallax jquery--> 
    <script type="text/javascript" src="<?=base_url();?>assets/frontend/js/jquery.parallax-1.1.3.js"></script>
    <!-- elevatezoom --> 
    <script type="text/javascript" src="<?=base_url();?>assets/frontend/js/jquery.elevateZoom.min.js"></script>
    <!-- fancybox -->
    <script src="<?=base_url();?>assets/frontend/js/fancybox/source/jquery.fancybox.pack.js"></script>
    <script src="<?=base_url();?>assets/frontend/js/fancybox/source/helpers/jquery.fancybox-media.js"></script>
    <script src="<?=base_url();?>assets/frontend/js/fancybox/source/helpers/jquery.fancybox-thumbs.js"></script>

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <!-- arcticmodal -->
    <script src="<?=base_url();?>assets/frontend/js/arcticmodal/jquery.arcticmodal.js"></script>
    <script src="<?=base_url();?>assets/frontend/js/dubaikorea.js"></script>
    <!-- Main -->  
    <script type="text/javascript" src="<?=base_url();?>assets/frontend/js/main.js"></script>
    <script type="text/javascript">
    $(function(){

        $( document ).tooltip({ 
            content: function(callback) { 
                callback($(this).prop('title').replace('|', '<br />')); 
            }
        });

        // onmouseout
        // block-content
        // has-open
        // block-title //active
        $('.block-nav-categori').hover(function(){
           $(this).addClass('has-open');
           $(this).find('.block-title').addClass('active');
           $(this).find('.block-content').css('display', 'block');
        });

        $('.block-nav-categori').mouseleave(function(){
           $(this).removeClass('has-open');
           $(this).find('.block-title').removeClass('active');
           $(this).find('.block-content').css('display', 'none');
        });
    });
    </script>
    <script type="text/javascript">
      function googleTranslateElementInit() {
        new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false}, 'google_translate_element');
      }
    </script>
    <script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit" type="text/javascript"></script>

    <!-- Flag click handler -->
    <script type="text/javascript">
        $('.lang_online').click(function() {
          var lang = $(this).data('lang');
          var $frame = $('.goog-te-menu-frame:first');
          if (!$frame.size()) {
            alert("Error: Could not find Google translate frame.");
            return false;
          }
          $frame.contents().find('.goog-te-menu2-item span.text:contains('+lang+')').get(0).click();
          return false;
        });

        /*Deals*/
        $(".latest-block-deals .owl-carousel").owlCarousel({
            items : 1, //10 items above 1000px browser width
            nav: true,
            navText : ["<i class='fa'></i>","<i class='fa'></i>"],
            loop:true,
            autoplayTimeout:4000,
            autoplayHoverPause:true,
            itemsDesktop : [400,5], //5 items between 1000px and 901px
            itemsDesktopSmall : [900,3], // betweem 900px and 601px
            itemsTablet: [600,2], //2 items between 600 and 0;
            itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
            responsive:{ 
                0:{
                  items:1 // Two item from 0px up to 479px
                },
                480:{
                  items:1 // Three items from 480px up to 677px
                },
                678:{
                  items:1 // Four items from 677px up to 959px
                },
                960:{
                  items:1 // Five items from 960px up
                }
            }
        });

        /*Top Latest Products*/
        $(".latest-block-content .owl-carousel").owlCarousel({
            items : 4, //10 items above 1000px browser width
            nav: true,
            navText : ["<i class='fa'></i>","<i class='fa'></i>"],
            autoplay:true,
            autoplayTimeout:4000,
            autoplayHoverPause:true,
            itemsDesktop : [400,5], //5 items between 1000px and 901px
            itemsDesktopSmall : [900,3], // betweem 900px and 601px
            itemsTablet: [600,2], //2 items between 600 and 0;
            itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
            responsive:{ 
                0:{
                  items:2 // Two item from 0px up to 479px
                },
                480:{
                  items:3 // Three items from 480px up to 677px
                },
                678:{
                  items:3 // Four items from 677px up to 959px
                },
                960:{
                  items:4 // Five items from 960px up
                }
            }
        });

        /*Floor Products*/
        $(".block-content .owl-carousel").owlCarousel({
            items : 5, //10 items above 1000px browser width
            nav: true,
            navText : ["<i class='fa'></i>","<i class='fa'></i>"],
            autoplay:true,
            autoplayTimeout:4000,
            autoplayHoverPause:true,
            itemsDesktop : [400,5], //5 items between 1000px and 901px
            itemsDesktopSmall : [900,3], // betweem 900px and 601px
            itemsTablet: [600,2], //2 items between 600 and 0;
            itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
            responsive:{ 
                0:{
                  items:2 // Two item from 0px up to 479px
                },
                480:{
                  items:3 // Three items from 480px up to 677px
                },
                678:{
                  items:4 // Four items from 677px up to 959px
                },
                960:{
                  items:5 // Five items from 960px up
                }
            }
        });

    </script>

    <!-- Page Specific JS Libraries -->
    <?php if( count($this->footer_assets) ): foreach( $this->footer_assets as $script ) { ?>
    <script src="<?=base_url('assets/frontend/' . $script);?>"></script>
    <?php }
    endif; ?>

    <?php if ( $this->footerJS ) { ?>
    <script>
    $(function(){
        <?php echo $this->footerJS;?>
    });
    </script>
    <?php } ?>
</body>
</html>