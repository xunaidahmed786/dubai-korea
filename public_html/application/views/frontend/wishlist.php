<!-- MAIN -->
<main class="site-main">
    <div class="columns container">
        <!-- Block  Breadcrumb-->
        <ol class="breadcrumb no-hide">
            <li><a href="<?=base_url();?>">Home</a></li>
            <li><a href="<?=site_url('my-account');?>">My Account</a></li>
            <li class="active">Wishlist</li>
        </ol><!-- Block  Breadcrumb-->

      
        <?php if( $this->session->flashdata('alert-success')):?>
        <div class="widget-content">
            <div class="alert alert-success nomargin">
                <?php echo $this->session->flashdata('alert-success') ;?>
            </div><br/>
        </div>
        <?php endif; ?>

        <?php if( $this->session->flashdata('alert-danger')):?>
        <div class="widget-content">
            <div class="alert alert-danger nomargin">
                <?php echo $this->session->flashdata('alert-danger') ;?>
            </div><br/>
        </div>
        <?php endif; ?>

        <div class="row">

            <!-- Main Content -->
            <div class="col-md-9 col-md-push-3 col-main">
                <h2 class="page-heading">
                    <span class="page-heading-title2">My wishlist</span>
                </h2>
                <ul class="row list-wishlist">
                    <?php
                    if( count($wishlists) ):
                        foreach( $wishlists as $product):
                        
                        $single_link = site_url($product['category']['slug'].'/'.$product['products']['slug']);
                    ?>
                    <li class="col-sm-3">
                        <div class="product-img">
                            <a href="<?=$single_link;?>"><img alt="<?=$product['products']['title'];?>" src="<?=storage('products/'.$product['images'][0]['image']);?>"></a>
                        </div>
                        <h5 class="product-name">
                            <a href="<?=$single_link;?>"><?=$product['products']['title'];?></a>
                        </h5>
                        <div class="button-action">
                            <button class="button button-sm" onclick="javascript:window.location='<?=$single_link;?>'">Click to detail</button>
                            <a href="<?=site_url('wishlist/delete/' . $product['products']['id']);?>"><i class="fa fa-close"></i></a>
                        </div>
                    </li>
                    <?php
                        endforeach;
                    else:
                    ?>
                    <li class="col-sm-12">No wishlists history</li>
                    <?php endif;?>
                </ul>
            </div><!-- Main Content -->
                
            <!-- Sidebar -->
            <div class="col-md-3 col-md-pull-9 col-sidebar">
               <?php load_frontend_view('customers/sidebar.php'); ?>
            </div><!-- Sidebar -->

        </div>
        
    </div>
</main><!-- end MAIN -->