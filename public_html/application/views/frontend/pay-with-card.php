<!-- MAIN -->
<main class="site-main">
    <div class="columns container">
        <!-- Block  Breadcrumb-->
        <ol class="breadcrumb no-hide">
            <li><a href="<?=base_url();?>">Home</a></li>
            <li><a href="<?=site_url('checkout');?>">Checkout</a></li>
            <li class="active">Pay with Card</li>
        </ol><!-- Block  Breadcrumb-->

        <h2 class="page-heading">
            <span class="page-heading-title2">Pay with Card</span>
        </h2>
        <div class="page-content checkout-page">
            <div class="box-border">
                <?php
                $total              = currencyConvertToAmount($this->cart->total());
                $shipping_charges   = shippingPolicyRules($total);
                $total_amount       = ($total + $shipping_charges);
                ?>
                <form method="post" action="<?=site_url('place-order-confirmation');?>">
                    <script src="https://beautiful.start.payfort.com/checkout.js"
                        data-key="<?php echo OPEN_KEY;?>"
                        data-currency="<?php echo ($currency_active ? $currency_active['symbols'] : DEFAULT_CURRENCY_NAME);?>"
                        data-amount="<?php echo calculatOfPayTo($total_amount); ?>"
                        data-email="<?php echo $billing['email_address']; ?>">
                  </script>
                </form>
            </div>
        </div>
        
    </div>
</main><!-- end MAIN -->