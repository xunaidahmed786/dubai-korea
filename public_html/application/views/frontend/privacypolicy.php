<!-- MAIN -->
<main class="site-main">
    <div class="columns container">
        <!-- Block  Breadcrumb-->
        <ol class="breadcrumb no-hide">
            <li><a href="<?=base_url();?>">Home</a></li>
            <li class="active">Privacy Policy</li>
        </ol><!-- Block  Breadcrumb-->

        <div class="row">
            <!-- Main Content -->
            <div class="col-md-12">
                <h2 class="page-heading">
                    <span class="page-heading-title2">Privacy Policy</span>
                </h2>
                <div class="content-text clearfix">
                    <!-- <img width="310" alt="" class="alignleft" src="images/media/detail/about-us.jpg"> -->
                    <h4>What We Collect:</h4>
                    <p>We shall collect the following information:</p>
                    <ul>
                        <li>Name</li>
                        <li>Contact information including email address</li>
                        <li>Demographic information such as postcode, preferences and interests</li>
                        <li>Other information relevant to customer surveys and/or offers</li>
                    </ul>
                    <br/><br/>
                    
                    <h4>What We Do with The Information We Gather:</h4>
                    <p>We require this information to understand your needs and provide you with a better service, and in particular for the following reasons:</p>
                    <ul>
                        <li>Internal record keeping</li>
                        <li>We may use the information to improve our products and services</li>
                        <li>We may periodically send promotional emails about new products, special offers or other information which we think you may find interesting using the email address which you have provided</li>
                        <li>From time to time, we may also use your information to contact you for market research purposes. We may contact you by email or phone. We may use the information to customize the website according to your interests</li>
                    </ul>
                    <br/>
                    <p>Please note that the information collected from you does not include your credit card details.</p>
                    <br/>

                    <h4>Security</h4>
                    <p>We are committed to ensuring that your information is secure. In order to prevent unauthorized access or disclosure, 
                    we have put in place suitable physical, electronic and managerial procedures to safeguard and secure the information 
                    we collect online. Controlling Your Personal Information.</p>
                    <p>You may choose to restrict the collection or use of your personal information in the following ways:</p>
                    <ul>
                        <li>Whenever you are asked to fill in a form on the website, look for the box that you can click to indicate that you do not want the information to be used by anybody for direct marketing purposes</li>
                        <li>If you have previously agreed to us by using your personal information for direct marketing purposes, you may change your mind at any time by writing or emailing us at <a href="mailto:info@dubaikorea.com"><u>info@dubaikorea.com</u></li>
                    </ul>

                    <br/><br/>
                    <p>
                        We will not sell, distribute or lease your personal information to third parties unless we have your permission or 
                        are required by law to do so. We may use your personal information to send you promotional information about third parties 
                        which we think you may find interesting.
                        <br/><br/>
                        
                        If you agree on doing so, you may request details of personal information which we hold about you under the Data Protection 
                        Act 1998. A small fee will be payable. If you would like a copy of the information held under your name, then please write 
                        to us at <a href="mailto:info@dubaikorea.com"><u>info@dubaikorea.com</u></a>.
                        <br/><br/>
                        
                        If you believe that any information we are holding on you is incorrect or incomplete, please write to or email us as soon as possible, 
                        at the above address. We will promptly correct any incorrect information.
                        <br/><br/>
                        
                        All credit/debit cards detail and personally identifiable information will NOT be stored, sold, shared, rented or leased to any third parties.
                        <br/><br/>
                        
                        DubaiKorea Policies and Terms & Conditions may be changed or updated occasionally to meet the requirements and standards. 
                        Therefore, the Customers’ are encouraged to frequently visit these sections in order to be updated about the changes on 
                        DubaiKorea. Modifications will be effective on the day they are posted.
                        <br/><br/>

                        Some of the advertisements you see on the Site are selected and delivered by third parties, such as ad networks, advertising agencies, advertisers, and audience segment providers.
                        <br/><br/>

                        These third parties may collect information about you and your online activities, either on the Site or on other websites, 
                        through cookies, web beacons, and other technologies in an effort to understand your interests and deliver to your 
                        advertisements that are tailored to your interests.
                        <br/><br/>
                        
                        Please remember that we do not have access to, or control over, the information these third parties may collect. 
                        The information practices of these third parties are not covered by this privacy policy.
                    </p>
                    <br/>
                    
                    <h4>Intellectual Property</h4>
                    <p>
                        Images, text, site design, logos, graphics and any other content and software on this site, including their arrangement, 
                        selection and assembly, are the property of Dubaikorea, unless specified otherwise.
                        <br/><br/>
                        
                        You may use DubaiKorea only for your personal and non-commercial purposes. Except to the extent permitted by relevant 
                        legislation, you must not use, copy, modify, transmit, store, publish or distribute any material DubaiKorea or 
                        create any other material using material on DubaiKorea, without obtaining our prior written consent (which you can obtain 
                        by emailing us.
                        <br/><br/>

                        Unauthorized use of the images, designs and other materials appearing on this site may violate copyright, trademark and other 
                        applicable laws and may result in criminal or civil proceedings and penalties.
                    </p>
                    <br/>
                    
                    <h4>Use of Images</h4>
                    <p>When Dubaikorea collects pictures, photos and images from you it is because you are voluntarily submitting those images 
                    to us if you choose to participate in Dubaikorea activities like contests, games and surveys, etc., or because you want us 
                    to furnish you with products, services, newsletters, or information, or in connection with content or suggestions you submit 
                    to Dubaikorea for review.
                    <br/><br/>

                    Upon providing Dubaikorea with such images, you will be deemed to acknowledge and agree to the use by Dubaikorea of your 
                    images on this website for marketing purposes associated with Dubaikorea products. Dubaikorea will not otherwise share your 
                    images with third parties unless you have given Dubaikorea permission to do so.
                    </p>
                    <br/>

                    <h4>Customer Guarantee</h4>
                    <p>Dubaikorea will always mission to provide the best price, quality and customer service whenever possible to ensure you the customer will always be delighted!</p>
                    <br/>
                </div>
            </div><!-- Main Content -->
        </div>
    </div>
</main><!-- end MAIN -->