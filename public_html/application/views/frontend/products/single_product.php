<?php
$single_link = site_url($product['category']['slug'].'/'.$product['products']['slug']);
?>
<div class="product-item  product-item-opt-1 ">
    <div class="product-item-info">
        <div class="product-item-photo">
            <a class="product-item-img" href="<?=$single_link;?>">
                <img alt="<?=$product['products']['title'];?>" src="<?=storage('products/'.$product['images'][0]['image']);?>">
            </a>
            <div class="product-item-actions">
                <a class="btn btn-quickview" href="<?=$single_link;?>"><span>quickview</span></a>
                <?php if ( in_array($product['products']['id'], $wishlist_in) ) : ?>
                    <a class="btn btn-wishlist" href="<?=site_url('wishlist/delete/' . $product['products']['id'] );?>" style="color: #fff;background-color: #f36;">
                        <span>wishlist</span>
                    </a>
                <?php else:?>
                    <a class="btn btn-wishlist" href="<?=site_url('add-to-wishlist/' . $product['products']['id'] );?>">
                        <span>wishlist</span>
                    </a>
                <?php endif;?>
                <a class="btn btn-compare" href="<?=$single_link;?>#related-bookmark"><span>compare</span></a>
            </div>
            <button type="button" class="btn btn-cart" onclick="javascript:window.location='<?=base_url('product/addtocart/'. $product['products']['id'] );?>'"><span>Add to Cart</span></button>
            
            <?php if ( isset($product['products']['product_type']) && (bool) $product['products']['product_type'] ) { ?>
                <span class="product-item-label label-new" style="display:none;">New</span>
            <?php } else { ?>

                <?php if ( $product['products']['is_sale'] ) { ?>
                    <span class="product-item-label label-price" style="display:none;"><?=calculateOfSavePercentage($product['products']['sale_price'], $product['products']['price']);?> <span>off</span></span>
                <?php } ?>
            
            <?php } ?>
            
        </div>
        <div class="product-item-detail">
            <strong class="product-item-name notranslate">
                <a href="<?=$single_link;?>"><?=$product['products']['title'];?></a>
            </strong>
            <div class="clearfix">
                <div class="product-item-price">
                    <?php
                    if ( $product['products']['is_sale'] )
                    {
                        echo '<span class="price">'.formatCurrency($product['products']['sale_price']).'</span><br/><span class="old-price">'.formatCurrency($product['products']['price']).'</span>';
                    } else 
                    {
                        echo '<span class="price">'.formatCurrency($product['products']['price']).'</span>';
                    }
                    ?>
                </div>
                <?php
                $set_rate = 0;
                if ( isset($product['review']))
                {
                    $set_rate = calculateOfProductRating($product['review']['total'], ($product['review']['items']*5) );
                }
                ?>
                <div class="product-reviews-summary">
                    <div class="rating-summary">
                        <div title="<?=$set_rate;?>%" class="rating-result">
                            <span style="width:<?=$set_rate;?>%">
                                <span><span><?=$set_rate;?></span>% of <span>100</span></span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>