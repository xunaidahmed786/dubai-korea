<div class="form-group">
    <label for="name">Contact Name <strong style="color:red;">*</strong></label>
    <input type="text" name="name" class="form-control required-field" id="name">
</div>
<div class="form-group">
    <label for="email">Email <strong style="color:red;">*</strong></label>
    <input type="email" name="email" class="form-control required-field" id="email">
</div>
<div class="form-group">
    <label for="phone">Phone <strong style="color:red;">*</strong></label>
    <input type="text" name="phone" class="form-control required-field" id="phone">
</div>
<div class="form-group">
    <label for="company-name">Company Name <strong style="color:red;">*</strong></label>
    <input type="text" name="company_name" class="form-control required-field" id="company-name">
</div>
<div class="form-group">
    <label for="store-website-name">Store or Website Name <strong style="color:red;">*</strong></label>
    <input type="text" name="store_website_name" class="form-control required-field" id="store-website-name">
</div>
<div class="form-group">
    <label for="year-established">Year Established</label>
    <input type="text" name="year_established" class="form-control" id="year-established">
</div>
<br/>
<h3>Company location</h3><br/>
<div class="form-group">
    <label for="name">Address 1 <strong style="color:red;">*</strong></label>
    <textarea class="form-control required-field" name="address1" id="address1"></textarea>
</div>
<div class="form-group">
    <label for="name">Address 2</label>
    <textarea class="form-control" name="address2" id="address2"></textarea>
</div>
<div class="form-group">
    <label for="name">City <strong style="color:red;">*</strong></label>
    <input type="text" name="city" class="form-control required-field" id="city">
</div>
<div class="form-group">
    <label for="name">State</label>
    <input type="text" name="state" class="form-control" id="state">
</div>
<div class="form-group">
    <label for="name">Post Code/Zip Code</label>
    <input type="text" name="zip_code" class="form-control" id="zip_code">
</div>
<div class="form-group">
    <label for="name">Country <strong style="color:red;">*</strong></label>
    <input type="text" name="country" class="form-control required-field" id="country">
</div>
<div class="form-group">
    <label for="name">Country you wish to distribute to or sell to <strong style="color:red;">*</strong></label>
    <input type="text" name="country_distribute_sell" class="form-control required-field" id="country_distribute_sell">
</div>
<div class="form-group">
    <label for="name">Type of business <strong style="color:red;">*</strong></label>
    <textarea class="form-control required-field" name="type_of_business" id="type_of_business"></textarea>
</div>
<div class="form-group">
    <label for="name">Which other skincare brands do you currently sell? <strong style="color:red;">*</strong></label>
    <textarea class="form-control required-field" name="currently_sell" id="currently_sell"></textarea>
</div>

<button type="submit" class="btn btn-default">Submit</button>
