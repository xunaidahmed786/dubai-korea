<?php
$is_stock       = false;
$stock          = $single_product['product']['available_stock'];
$fk_product_url = site_url($single_product['category']['slug'].'/'.$single_product['product']['slug']);
if ( $stock > 0  )
{
    $is_stock = true;
}
?>
<!-- MAIN -->
<main class="site-main">

    <div class="columns container">
        <!-- Block  Breadcrumb-->

        <ol class="breadcrumb no-hide">
            <li class="notranslate"><a href="<?=base_url();?>">Home</a></li>
            <li class="notranslate"><a href="<?=site_url('c/'.$single_product['category']['slug']);?>"><?=$single_product['category']['title'];?></a></li>
            <li class="active notranslate"><?=$single_product['product']['title'];?></li>
        </ol><!-- Block  Breadcrumb-->

        <div class="row">

            <?php if( $this->session->flashdata('alert-success')):?>
            <div class="widget-content">
                <div class="alert alert-success nomargin">
                    <?php echo $this->session->flashdata('alert-success') ;?>
                </div><br/>
            </div>
            <?php endif; ?>

            <!-- Main Content -->
            <div class="col-md-12  col-main">
                
                <div class="row">
                    <div class="col-sm-6 col-md-4 col-lg-4">
                        <div class="product-media media-horizontal">
                            <div class="image_preview_container images-large" style="margin-bottom:10px;">
                                <?php 
                                $default_image = '';
                                if ( isset($single_product['images'][0]['image']) && $single_product['images'][0]['image'] )
                                {
                                    $default_image = storage('products/'.$single_product['images'][0]['image']);
                                }
                                ?>
                                <img 
                                    id="img_zoom" 
                                    data-zoom-image="<?=$default_image;?>" 
                                    src="<?=$default_image;?>"
                                />
                            </div>
                            
                            <div class="product_preview images-small" style="padding:0px;">
                                <div class="owl-carousel thumbnails_carousel" id="thumbnails"  data-nav="true" data-dots="false" data-margin="10" data-responsive='{"0":{"items":3},"480":{"items":4},"600":{"items":5},"768":{"items":4}}'>
                                    <?php if ( isset($single_product['images']) && count($single_product['images']) ) {
                                        foreach($single_product['images'] as $image ) {
                                    ?>
                                        <a href="#" data-image="<?=storage('products/'.$image['image']);?>" data-zoom-image="<?=storage('products/'.$image['image']);?>">
                                            <img src="<?=storage('products/'.$image['image']);?>" data-large-image="<?=storage('products/'.$image['image']);?>" alt="">
                                        </a>
                                    <?php 
                                        }
                                    } ?>
                                </div><!--/ .owl-carousel-->
                            </div><!--/ .product_preview-->
                        </div><!-- image product -->
                    </div>

                    <div class="col-sm-6 col-md-8 col-lg-8 single-product-on-right"> 

                        <div class="product-info-main">
                            <h1 class="page-title notranslate"><?=$single_product['product']['title'];?></h1>
                            <div class="product-reviews-summary">
                                <div class="rating-summary">
                                    <div class="rating-result" title="0%">
                                        <span style="width:0%">
                                            <span><span>0</span>% of <span>100</span></span>
                                        </span>
                                    </div>
                                </div>
                                <div class="reviews-actions">
                                    <a href="javascript:void(0)" class="action view">Based  on 0 ratings</a>
                                    <a href="javascript:void(0)" class="action add write-review">
                                        <img alt="img" src="<?=base_url('assets/frontend/images/icon/write.png');?>">&#160;&#160;write a review
                                    </a>
                                </div>
                            </div>
                           
                            <div class="product-info-price">
                                <div class="price-box">
                                	<?php if ( $single_product['product']['sale_price'] == '0.00') { ?>
				                        <span class="price"><?=formatCurrency($single_product['product']['price']);?></span>
				                    <?php } else { ?>
				                        <span class="price"><?=formatCurrency($single_product['product']['sale_price']);?></span>
				                        <span class="old-price"><?=formatCurrency($single_product['product']['price']);?></span>
				                        <span class="label-sale"><?=calculateOfSavePercentage($single_product['product']['sale_price'], $single_product['product']['price']);?></span>
				                    <?php } ?>
                                </div>
                            </div>  
                            <div class="product-code">
                                Item Code: <?=$single_product['product']['code'];?> :  
                            </div> 
                            <div class="product-info-stock">
                                <div class="stock available">
                                    <span class="label">Availability: </span>
                                    <label style="font-weight:none; <?php echo (!$is_stock ? 'color:#ff0505' : '');?>"><?php echo ( $is_stock ? 'In stock' : '<i>Not Available</i>' );?></label>
                                </div>
                            </div>
                            <div class="product-condition" style="display:none;">
                                Condition: New
                            </div>
                            <?php if ( $single_product['product']['short_description'] ) { ?>
                            <div class="product-overview">
                                <div class="overview-content">
                                    <?=$single_product['product']['short_description'];?>
                                </div>
                            </div>
                            <?php } ?>

                            <div class="product-add-form">
                                <p>Available Options:</p>
                                    
                                <?=form_open( site_url('cart/addtocart'));?>
                                <div class="product-options-wrapper">
                                    <div class="form-qty">
                                        <label class="label">Qty: </label>
                                        <div class="control">
                                            <input type="text" class="form-control input-qty" value='1' id="qty" name="qty"  maxlength="<?=$stock;?>"  minlength="1">
                                            <button class="btn-number qtyminus" data-type="minus" data-field="qty"><span>-</span></button>
                                            <button class="btn-number qtyplus" data-type="plus" data-field="qty"><span>+</span></button>
                                        </div>
                                    </div>
                                </div>

                                <div class="product-options-bottom clearfix">  
                                    <div class="actions">
                                        <?php if ( $is_stock ) : ?>
                                        <button type="submit" title="Add to Cart" class="action btn-cart">
                                            <span>Add to Cart</span>
                                        </button>

                                        <button type="button" title="Whole Seller Contact" class="action btn-cart" data-toggle="modal" data-target="#whole_seller_form_model">
                                            <span>Whole Seller Contact</span>
                                        </button>
                                        <?php endif;?>
                                        <div class="product-addto-links">
                                            <a href="<?=site_url('add-to-wishlist/' . $single_product['product']['id'] );?>" class="action btn-wishlist" title="Wish List">
                                                <span>Wishlist</span>
                                            </a>
                                            <a href="#related-bookmark" class="action btn-compare" title="Compare">
                                                <span>Compare</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <?php if ( $is_stock ) : ?>
                                    <input type="hidden" value="<?=$single_product['product']['id'];?>" name="product_id">
                                <?php endif;?>
                                <?=form_close();?>
                            </div>

                            <?php if ( $is_stock ) : ?>
                                <div id="whole_seller_form_model" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Whole Seller Enquiry for <strong><?php echo $single_product['product']['title'];?></strong></h4>
                                            </div>
                                            <div class="modal-body">
                                                <form method="post" id="whole_seller_form_id" action="<?=site_url('whole-seller-send-inquiry');?>">
                                                    <h3>Denotes mandatory field</h3>
                                                    <?php load_frontend_view('products/whole_seller_form');?>
                                                    <input type="hidden" value="<?=$single_product['product']['id'];?>" name="product_id">
                                                    <input type="hidden" value="<?=$single_product['product']['title'];?>" name="product_name">
                                                    <input type="hidden" value="<?=$fk_product_url;?>" name="product_url">
                                                </form>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            <?php endif;?>
                            
                            </div>

                            <div class="product-addto-links-second">
                                <a href="javascript:window.print();" class="action action-print">Print</a>
                                <a href="javascript:void(0)" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(location.href),'facebook-share-dialog', 'width=626,height=436');return false;" class="action action-friend">Send to a friend</a>
                            </div>
                        </div><!-- detail- product -->
                    </div><!-- Main detail -->
                </div>

                <!-- product tab info -->            
                <div class="product-info-detailed">
                    <?php 
                    $select_tabs = 'description';
                    if ( $this->session->userdata('select-tab') )
                    {
                        $select_tabs = $this->session->userdata('select-tab');
                    }
                    ?>

                    <!-- Nav tabs -->
                    <ul class="nav nav-pills" role="tablist">
                        <?php if (  $single_product['product']['description'] ) { ?>
                        <li role="presentation" <?=(($select_tabs=='description')?'class="active"':null);?> >
                            <a href="#description" role="tab" data-toggle="tab">Product Details</a>
                        </li>
                        <?php } ?>
                        <?php if (  $single_product['product']['extra'] ) { ?>
                        <li role="presentation" <?=(($select_tabs=='additional')?'class="active"':null);?> >
                            <a href="#additional"  role="tab" data-toggle="tab">Extra Tabs</a>
                        </li>
                        <?php } ?>
                        <li role="presentation" <?=(($select_tabs=='reviews')?'class="active"':null);?> >
                            <a href="#reviews"  role="tab" data-toggle="tab">reviews</a>
                        </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <?php if ( $single_product['product']['description'] ) { ?>
                        <div role="tabpanel" class="tab-pane <?=(($select_tabs=='description')?'active':null);?>" id="description">
                            <div class="block-title">Product Details</div>
                            <div class="block-content">
                                <?=$single_product['product']['description'];?>
                                

                                <?php 
                                if ( isset($single_product['pro_desc_images']) && count($single_product['pro_desc_images']) )
                                    foreach ($single_product['pro_desc_images'] as $img ) {
                                ?>
                                    <center>
                                        <img src="<?php echo storage('products/desc/'.$img['image']);?>" />
                                    </center>
                                <?php      
                                    }
                                ?>
                            </div>
                        </div>
                        <?php } ?>
                        <?php if ( $single_product['product']['extra'] ) { ?>
                        <div role="tabpanel" class="tab-pane <?=(($select_tabs=='additional')?'active':null);?>" id="additional">
                            <div class="block-title">information</div>
                            <div class="block-content">
                                <?=$single_product['product']['extra'];?>
                            </div>
                        </div>
                        <?php } ?>
                        <div role="tabpanel" class="tab-pane <?=(($select_tabs=='reviews')?'active':null);?>" id="reviews">
                            <div class="block-title">reviews</div>
                            <div class="block-content">

                                <?php if ( count($single_product['product']) ):?>
                                <div class="comment-list">
                                    <ul>
                                        <?php foreach($single_product['reviews'] as $review):?>
                                        <li>
                                            <div class="avartar">
                                                <img src="http://www.barcelonaskingenomic.com/images/nophotouser.png" alt="<?=$review['customer_name'];?>">
                                            </div>
                                            <div class="comment-body">
                                                <div class="comment-meta">
                                                    <span class="author"><a href="javascript:void(0)"><?=$review['customer_name'];?></a></span>
                                                    <span class="date"><?=formatDate('Y-m-d', $review['created_at']);?></span>
                                                </div>
                                                <div class="comment">
                                                    <?=$review['reviews'];?>
                                                </div>
                                                <div class="product-reviews-summary">
                                                    <div class="rating-summary">
                                                        <div title="<?=calculateOfProductRating($review['review_rating']);?>%" class="rating-result">
                                                            <span style="width:<?=calculateOfProductRating($review['review_rating']);?>%">
                                                                <span><span><?=calculateOfProductRating($review['review_rating']);?></span>% of <span>100</span></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <?php endforeach;?>
                                    </ul>
                                </div>
                                <hr/>
                                <?php endif;?>
                                <div class="contact-form-box">
                                    <?php if ( check_login() ) : ?>
                                    <form method="post" action="<?=site_url('product/review/'.$single_product['product']['id']);?>">
                                        <div class="form-selector">
                                            <label>Review/Comments <strong>*</strong></label>
                                            <textarea name="comments" rows="5" class="form-control input-sm" required ></textarea>
                                        </div>
                                        <br/>
                                        <div class="form-selector">
                                            <label>Rate: </label>
                                            <label>
                                                <input type="radio" name="rate" value="1" checked="checked"> 1
                                            </label>
                                            <label>
                                                <input type="radio" name="rate" value="2" > 2
                                            </label>
                                            <label>
                                                <input type="radio" name="rate" value="3" > 3
                                            </label>
                                            <label>
                                                <input type="radio" name="rate" value="4" > 4
                                            </label>
                                            <label>
                                                <input type="radio" name="rate" value="5" > 5
                                            </label>
                                        </div>
                                        <br/>
                                        <input type="hidden" name="fk_product_url" value="<?=$fk_product_url;?>">
                                        <div class="form-selector">
                                            <button class="btn" style="color:#fff;background-color: #f36;" type="submit">Review</button>
                                        </div>
                                    </form>
                                    <?php else: ?>
                                        <button class="btn" onclick="javascript:window.location='<?=site_url('login');?>';" style="color:#fff;background-color: #f36;" type="button">Sign in</button>
                                    <?php endif;?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
                <!-- product tab info -->
                
                <?php if ( isset($related_products) && count($related_products) ) { ?>
                <!-- block-related product -->
                <div class="block-related " id="related-bookmark">
                    <div class="block-title">
                        <strong class="title">RELATED products</strong>
                    </div>
                    <div class="block-content ">
                        <ol class="product-items owl-carousel " data-nav="true" data-dots="false" data-margin="30" data-responsive='{"0":{"items":1},"480":{"items":2},"600":{"items":3},"992":{"items":3},"1200":{"items":4}}'>
                            <?php 
                            foreach ($related_products as $product){
                            ?>
                            <li class="product-item product-item-opt-2">
                                <?php load_frontend_view('products/single_product', ['product' => $product] ); ?>
                            </li>
                            <?php } ?>
                        </ol>
                    </div>
                </div><!-- block-related product -->
                <?php } ?>

            </div><!-- Main Content -->
            
        </div>
    </div>

</main><!-- end MAIN -->