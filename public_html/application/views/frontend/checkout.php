<!-- MAIN -->
<main class="site-main">
    <div class="columns container">
        <!-- Block  Breadcrumb-->
        <ol class="breadcrumb no-hide">
            <li><a href="<?=base_url();?>">Home</a></li>
            <li><a href="<?=site_url('shopping-cart');?>">Shopping Cart</a></li>
            <li class="active">Checkout</li>
        </ol><!-- Block  Breadcrumb-->

        <h2 class="page-heading">
            <span class="page-heading-title2">Checkout</span>
        </h2>

        <div class="page-content checkout-page">
            <form method="post" action="<?=site_url('order-place');?>" id="checkout_form_id">
                
                <?php
                $bill_first_name     = 
                $bill_last_name      =
                $bill_email          =
                $bill_company        =
                $bill_address        =
                $bill_country        = 
                $bill_state          = 
                $bill_city           = 
                $bill_postalcode     = 
                $bill_telephone      = 
                $bill_fax            = null; 
                
                if ( $this->session->flashdata('data') )
                {
                    $bill_first_name = $this->session->flashdata('data')['billing']['first_name'];
                    $bill_last_name  = $this->session->flashdata('data')['billing']['last_name'];
                    $bill_email      = $this->session->flashdata('data')['billing']['email_address'];
                    $bill_company    = $this->session->flashdata('data')['billing']['company_name']; 
                    $bill_address    = $this->session->flashdata('data')['billing']['address']; 
                    $bill_country    = $this->session->flashdata('data')['billing']['country'];
                    $bill_state      = $this->session->flashdata('data')['billing']['state'];
                    $bill_city       = $this->session->flashdata('data')['billing']['city'];
                    $bill_postalcode = $this->session->flashdata('data')['billing']['postal_code']; 
                    $bill_telephone  = $this->session->flashdata('data')['billing']['telephone'];
                    $bill_fax        = $this->session->flashdata('data')['billing']['fax'];
                }

                if ( $this->session->userdata('logged_in') )
                {
                    $auth            = $this->session->userdata('auth');
                    $bill_first_name = $auth->first_name;
                    $bill_last_name  = $auth->last_name;
                    $bill_email      = $auth->email;
                    $bill_address    = $auth->address; 
                    $bill_country    = $auth->country;
                    $bill_state      = $auth->state;
                    $bill_city       = $auth->city;
                    $bill_postalcode = $auth->postal_code; 
                    $bill_telephone  = $auth->telephone;
                }
                ?>
                <?php if( $this->session->flashdata('alert-danger')):?>
                <div class="widget-content">
                    <div class="alert alert-danger nomargin">
                        <?php echo $this->session->flashdata('alert-danger') ;?>
                    </div><br/>
                </div>
                <?php endif; ?>

                <h3 class="checkout-sep">1. Billing Infomations</h3>
                <div class="box-border">
                    <ul>
                        <li class="row">
                            <div class="col-sm-6">
                                <label for="first_name" class="required">First Name *</label>
                                <input class="input form-control required-field" name="billing[first_name]" value="<?=$bill_first_name;?>" id="bill_first_name" type="text">
                            </div>
                            <div class="col-sm-6">
                                <label for="last_name" class="required">Last Name *</label>
                                <input name="billing[last_name]" class="input form-control required-field" id="bill_last_name" value="<?=$bill_last_name;?>" type="text">
                            </div>
                        </li>
                        <li class="row">
                            <div class="col-sm-6">
                                <label for="company_name">Company Name</label>
                                <input name="billing[company_name]" class="input form-control" id="bill_company_name" value="<?=$bill_company;?>" type="text">
                            </div>
                            <div class="col-sm-6">
                                <label for="email_address" class="required">Email Address *</label>
                                <input class="input form-control required-field" name="billing[email_address]" id="bill_email_address" value="<?=$bill_email;?>" type="text">
                            </div>
                        </li>
                        <li class="row"> 
                            <div class="col-xs-12">
                                <label for="address" class="required">Address *</label>
                                <input class="input form-control required-field" name="billing[address]" id="bill_address" value="<?=$bill_address;?>" type="text">
                            </div>
                        </li>
                        <li class="row">
                            <div class="col-sm-6">
                                <label for="country" class="required">Country *</label>
                                <input class="input form-control required-field" name="billing[country]" id="bill_country" value="<?=$bill_country;?>" type="text">
                            </div>
                            <div class="col-sm-6">
                                <label for="state" class="required">State *</label>
                                <input class="input form-control required-field" name="billing[state]" id="bill_state" value="<?=$bill_state;?>" type="text">
                            </div>
                        </li>
                        <li class="row">
                            <div class="col-sm-6">
                                <label for="city" class="required">City *</label>
                                <input class="input form-control required-field" name="billing[city]" value="<?=$bill_city;?>" id="bill_city" type="text">
                            </div>
                            <div class="col-sm-6">
                                <label for="postal_code" class="required">Zip/Postal Code</label>
                                <input class="input form-control" name="billing[postal_code]" value="<?=$bill_postalcode;?>" id="bill_postal_code" type="text">
                            </div>
                        </li>
                        <li class="row">
                            <div class="col-sm-6">
                                <label for="telephone" class="required">Telephone *</label>
                                <input class="input form-control required-field" name="billing[telephone]" value="<?=$bill_telephone;?>" id="bill_telephone" type="text">
                            </div>
                            <div class="col-sm-6">
                                <label for="fax">Fax</label>
                                <input class="input form-control" name="billing[fax]" value="<?=$bill_fax;?>" id="bill_fax" type="text">
                            </div>
                        </li>
                        <li class="row">
                            <br/>
                            <div class="col-sm-12">
                                <input name="billing[shipping]" id="billing_copy_to_shipping" type="checkbox">
                                <label for="billing_copy_to_shipping" style="font-weight:normal;">Billing information is the same as shipping information</label>
                            </div>
                        </li>
                    </ul>
                </div>

                <h3 class="checkout-sep">2. Shipping Information</h3>
                <div class="box-border">
                    <ul>
                        <li class="row">
                            <div class="col-sm-6">
                                <label for="first_name" class="required">First Name *</label>
                                <input class="input form-control required-field" name="shipping[first_name]" id="shipping_first_name" type="text">
                            </div>
                            <div class="col-sm-6">
                                <label for="last_name" class="required">Last Name*</label>
                                <input name="shipping[last_name]" class="input form-control required-field" id="shipping_last_name" type="text">
                            </div>
                        </li>
                        <li class="row">
                            <div class="col-sm-6">
                                <label for="company_name">Company Name</label>
                                <input name="shipping[company_name]" class="input form-control" id="shipping_company_name" type="text">
                            </div>
                            <div class="col-sm-6">
                                <label for="email_address" class="required">Email Address *</label>
                                <input class="input form-control required-field" name="shipping[email_address]" id="shipping_email_address" type="text">
                            </div>
                        </li>
                        <li class="row"> 
                            <div class="col-xs-12">
                                <label for="address" class="required">Address *</label>
                                <input class="input form-control required-field" name="shipping[address]" id="shipping_address" type="text">
                            </div>
                        </li>
                        <li class="row">
                            <div class="col-sm-6">
                                <label for="country" class="required">Country *</label>
                                <input class="input form-control required-field" name="shipping[country]" id="shipping_country" type="text">
                            </div>
                            <div class="col-sm-6">
                                <label for="state" class="required">State *</label>
                                <input class="input form-control required-field" name="shipping[state]" id="shipping_state" type="text">
                            </div>
                        </li>
                        <li class="row">
                            <div class="col-sm-6">
                                <label for="city" class="required">City *</label>
                                <input class="input form-control required-field" name="shipping[city]" id="shipping_city" type="text">
                            </div>
                            <div class="col-sm-6">
                                <label for="postal_code" class="required">Zip/Postal Code</label>
                                <input class="input form-control" name="shipping[postal_code]" id="shipping_postal_code" type="text">
                            </div>
                        </li>
                        <li class="row">
                            <div class="col-sm-6">
                                <label for="telephone" class="required">Telephone *</label>
                                <input class="input form-control required-field" name="shipping[telephone]" id="shipping_telephone" type="text">
                            </div>
                            <div class="col-sm-6">
                                <label for="fax">Fax</label>
                                <input class="input form-control" name="shipping[fax]" id="shipping_fax" type="text">
                            </div>
                        </li>
                    </ul>
                </div>
                <h3 class="checkout-sep">3. Payment Information</h3>
                <div class="box-border">
                    <ul>
                        <li>
                            <label for="cash_checkout">
                                <input name="payment[info]" class="payment_type" checked="checked" value="cash_checkout" id="cash_checkout" type="radio"> Cash On Delivery
                            </label>
                        </li>
                        <li>
                            <label for="credit_card">
                                <input name="payment[info]" class="payment_type"  value="credit_card" id="credit_card" type="radio"> Credit card (saved)
                            </label>
                        </li>
                    </ul>
                </div>
                
                <div class="payment_method" style="display:none;">
                    <h3 class="checkout-sep">Payment Method</h3>
                    <div class="box-border" style="margin:0px;">
                        <ul>
                            <li class="row">
                                <div class="col-sm-6">
                                    <label for="card_number" class="required width-100">Card Number *</label>
                                    <input class="input form-control col-sm-8 width-90 validate_of_card_number" name="payment[card_number]" type="text">
                                    <img src="<?php echo storage('cards/unknown_card.png');?>" style="width:7%"class="pull-right">
                                </div>
                                <div class="col-sm-6">
                                    <label for="card_holder_name">Card Holder's Name</label>
                                    <input name="payment[card_holder_name]" class="input form-control" type="text">
                                </div>
                            </li>
                            <li class="row">
                                <div class="col-sm-6">
                                    <label for="card_expiry" class="required">Expiry Date</label><br/> 
                                    <input class="input form-control pull-left width-20" style="margin-right:10px;" name="payment[card_month]" placeholder="mm" type="text">
                                    <input class="input form-control width-30" name="payment[card_year]" placeholder="yyyy" type="text">
                                
                                </div>
                                <div class="col-sm-6">
                                    <label for="card_verification" class="required">Card verification code *</label>
                                    <input class="input form-control width-20" name="payment[card_verification]" type="text">
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>

                <h3 class="checkout-sep">4. Order Review</h3>
                <div class="box-border">
                    <div class="table-responsive">
                        <table class="table table-bordered  cart_summary">
                            <thead>
                                <tr>
                                    <th class="cart_product">Product</th>
                                    <th width="55%">Description</th>
                                    <th>Unit price</th>
                                    <th>Qty</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $total      = 0;
                                $is_cart    = false;
                                if ( count($this->cart->contents()) ):
                                    $i = 1;
                                    $is_cart = true;
                                    foreach ($this->cart->contents() as $items):
                                    $total += $items['subtotal'];
                                ?>
                                <tr>
                                    <td class="cart_product">
                                        <a href="<?php echo $items['options']['product_url']; ?>">
                                            <?php if ( $items['options']['product_image'] ) : ?>
                                            <img alt="<?php echo $items['name']; ?>" src="<?php echo storage('products/'.$items['options']['product_image']); ?>">
                                            <?php else: ?>
                                                <small><i>(Not available)</i></small>
                                            <?php endif;?>
                                        </a>
                                    </td>
                                    <td class="cart_description">
                                        <p class="product-name">
                                            <a href="<?php echo $items['options']['product_url']; ?>"><?php echo $items['name']; ?></a>
                                        </p>
                                        <small class="cart_ref">Code : <?php echo $items['code']; ?></small><br><br>
                                        <p class="product-name" style="font-size:13px; font-weight:bold;">Short Description</p>
                                        <small>
                                            <?php echo $items['short_description']; ?>
                                        </small>
                                    </td>
                                    <td class="price">
                                        <?php if ( $items['sale_price'] == 0.00 ) : ?>
                                            <span style="text-align:left;"><?php echo formatCurrency($items['original_price']); ?></span>
                                        <?php else : ?>
                                            <span style="text-align:left;"><del><?php echo formatCurrency($items['original_price']); ?></del></span>
                                            <span style="text-align:left;"><?php echo formatCurrency($items['sale_price']); ?></span>
                                        <?php endif;?>
                                    </td>
                                    <td class="qty">
                                        <?php echo $items['qty']; ?>
                                    </td>
                                    <td class="price">
                                        <span><?php echo formatCurrency($items['subtotal']); ?></span>
                                    </td>
                                </tr>
                                <?php
                                    $i++;
                                    endforeach;
                                else:
                                ?>
                                <tr><td colspan="6">Your cart is empty.</td></tr>
                                <?php endif;?>
                            </tbody>
                            <tfoot>
                            <?php 
                            if ( count($this->cart->contents()) ):
                            $total              = currencyConvertToAmount($this->cart->total());
                            $shipping_charges   = shippingPolicyRules($total);
                            ?>
                                <tr>
                                    <td colspan="2"></td>
                                    <td colspan="2">Shippment Charges (tax incl.)</td>
                                    <td colspan="1"><?=getSelectFormatCurrency($shipping_charges);?></td>
                                </tr>
                                <tr>
                                    <td colspan="2"><strong>Total</strong></td>
                                    <td colspan="3"><strong><?=getSelectFormatCurrency($total+$shipping_charges);?></strong></td>
                                </tr>
                            </tfoot>
                            <?php endif;?>
                        </table>
                        <button type="submit" class="button pull-right">Place Order</button>
                    </div>
                </div>
            </form>
        </div>

       
    </div>
</main><!-- end MAIN -->