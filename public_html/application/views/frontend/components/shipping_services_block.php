<!-- block  service-->
<div class="container">
    <div class="block-service-opt1">
        <div class="clearfix">
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="item" data-placement="top" title="
- Above 299DHMS- delivery will be free <br/>
- Above $80- delivery will be free) <br/>
- Order equal to 100DHMS- 15DHMS delivery charges <br/>
- Order Equal to $27- $4 delivery charges <br/>
- Order more than 299DHMS and less than 100DHMS - 12DHMS delivery charges <br/>
- Order more than $80 and less than $27- $3 delivery charges" data-html="true">
                    <span class="icon">
                        <img src="<?=base_url('assets/frontend/images/dummy/airplan.png');?>">
                    </span>
                    <div class="box-shipping-info">
                        <strong class="title">Free Shipping </strong>
                        <span>On order over 299DHMS ($80)</span>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="item">
                    <span class="icon">
                        <img src="<?=base_url('assets/frontend/images/dummy/timer.png');?>" alt="service">
                    </span>
                    <div class="box-shipping-info">
                        <strong class="title">30-day return</strong>
                        <span>Moneyback guarantee</span>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="item">
                    <span class="icon">
                        <img src="<?=base_url('assets/frontend/images/dummy/247.png');?>" alt="service">
                    </span>
                    <div class="box-shipping-info">
                        <strong class="title">24/7 support</strong>
                        <span>Online consultations</span>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="item">
                    <span class="icon">
                        <img src="<?=base_url('assets/frontend/images/dummy/ambrella.png');?>" alt="service">
                    </span>
                    <div class="box-shipping-info">
                        <strong class="title">SAFE SHOPPING </strong>
                        <span>Safe Shopping Guarantee</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- block  service-->