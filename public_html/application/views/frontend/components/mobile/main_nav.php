<div class="block-nav-menu">
    <div class="clearfix"><span data-action="close-nav" class="close-nav"><span>close</span></span></div>
    <ul class="ui-menu">
        <li class="parent parent-megamenu active">
            <a >Home</a>
            <span class="toggle-submenu"></span>
            <div class="megamenu drop-menu">
                <ul>
                    <li class="col-md-3">
                        <strong class="title"><a ><span>Home </span></a></strong>
                        <ul class="list-submenu">
                            <li><a href="home1.html">Home page 1</a></li>
                            <li><a href="home2.html">Home page 2</a></li>
                            <li><a href="home3.html">Home page 3</a></li>
                            <li><a href="home4.html">Home page 4</a></li>
                            <li><a href="home5.html">Home page 5</a></li>
                            <li><a href="home6.html">Home page 6</a></li>
                            <li><a href="home7.html">Home page 7</a></li>
                        </ul>
                    </li>
                    <li class="col-md-3">
                        <strong class="title"><a ><span>Home </span></a></strong>
                        <ul class="list-submenu">
                            <li><a href="home8.html">Home page 8</a></li>
                            <li><a href="home9.html">Home page 9</a></li>
                            <li><a href="home10.html">Home page 10</a></li>
                            <li><a href="home11.html">Home page 11</a></li>
                            <li><a href="home12.html">Home page 12</a></li>
                            <li><a href="home13.html">Home page 13</a></li>
                            <li><a href="home14.html">Home page 14</a></li>
                        </ul>
                    </li>
                    <li class="col-md-3">
                        <strong class="title"><a ><span>Page </span></a></strong>
                        <ul class="list-submenu">
                            <li><a href="Login.html">Login</a></li>
                            <li><a href="About.html">About</a></li>
                            <li><a href="Contact.html">Contact</a></li>
                            <li><a href="Blog.html">Blog</a></li>
                            <li><a href="Blog_Post.html">Blog Post</a></li>
                            <li><a href="Checkout.html">Checkout</a></li>
                            <li><a href="Order.html">Order</a></li>
                        </ul>
                    </li>
                    <li class="col-md-3">
                        <strong class="title"><a ><span>Page </span></a></strong>
                        <ul class="list-submenu">
                            <li><a href="Category1.html">Category 1</a></li>
                            <li><a href="Category2.html">Category 2</a></li>
                            <li><a href="Product1.html">Product 1</a></li>
                            <li><a href="Product2.html">Product 2</a></li>
                            <li><a href="Product3.html">Product 3</a></li>
                            <li><a href="WishList.html">WishList </a></li>
                            <li><a href="Compare.html">Compare</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </li>
        <li><a href="#">Sports</a></li>
        <li class="parent parent-submenu">
            <a > Fashion  </a>
            <span class="toggle-submenu"></span>
            <div class="submenu drop-menu">
                <ul >
                    <li><a href="">Shoes</a></li>
                    <li><a href="">Clothing</a></li>
                    <li><a href="">Accessories</a></li>
                    <li><a href="">Sunglasses</a></li>
                </ul>
            </div>
        </li>
        
        <li class="parent parent-megamenu">
            <a >Electronics  <span class="label-menu">New</span></a>
            <span class="toggle-submenu"></span>
            <div class="megamenu drop-menu">
                <ul>
                    <li class="col-md-3">
                        <div class="img-categori">
                            <a href=""><img alt="img" src="<?=base_url();?>assets/frontend/images/media/index1/img-categori1.jpg"></a>
                        </div>
                        <strong class="title"><a href=""><span>Women's </span></a></strong>
                        <ul class="list-submenu">
                            <li><a href="">Shoes</a></li>
                            <li><a href="">Clothing</a></li>
                            <li><a href="">Accessories</a></li>
                            <li><a href="">Sunglasses</a></li>
                        </ul>
                    </li>
                    <li class="col-md-3">
                       
                        <div class="img-categori">
                            <a href=""><img  alt="img"  src="<?=base_url();?>assets/frontend/images/media/index1/img-categori2.jpg"></a>
                        </div>
                        <strong class="title"><a href=""><span>Women's </span></a></strong>
                        <ul class="list-submenu">
                            <li><a href="">Shoes</a></li>
                            <li><a href="">Clothing</a></li>
                            <li><a href="">Accessories</a></li>
                            <li><a href="">Sunglasses</a></li>
                        </ul>
                    </li>
                    <li class="col-md-3">
                        
                        <div class="img-categori">
                            <a href=""><img alt="img"  src="<?=base_url();?>assets/frontend/images/media/index1/img-categori3.jpg"></a>
                        </div>
                        <strong class="title"><a href=""> <span>Kid's</span></a></strong>
                        <ul class="list-submenu">
                            <li><a href="">Shoes</a></li>
                            <li><a href="">Clothing</a></li>
                            <li><a href="">Accessories</a></li>
                            <li><a href="">Sunglasses</a></li>
                        </ul>
                    </li>
                    <li class="col-md-3">
                        
                        <div class="img-categori">
                            <a href=""><img alt="img"  src="<?=base_url();?>assets/frontend/images/media/index1/img-categori4.jpg"></a>
                        </div>
                        <strong class="title"><a href=""><span>Trending</span> </a></strong>
                        <ul class="list-submenu">
                            <li><a href="">Shoes</a></li>
                            <li><a href="">Clothing</a></li>
                            <li><a href="">Accessories</a></li>
                            <li><a href="">Sunglasses</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </li>
        <li class="parent parent-megamenu">
            <a >Digital </a>
            <span class="toggle-submenu"></span>
            <div class="megamenu drop-menu">
                <ul>
                    <li class="col-md-3">
                        
                        <strong class="title"><a href=""><span>Women's </span></a></strong>
                        <ul class="list-submenu">
                            <li><a href="">Shoes</a></li>
                            <li><a href="">Clothing</a></li>
                            <li><a href="">Accessories</a></li>
                            <li><a href="">Sunglasses</a></li>
                        </ul>

                        <strong class="title"><a href=""><span>Women's </span></a></strong>
                        <ul class="list-submenu">
                            <li><a href="">Shoes</a></li>
                            <li><a href="">Clothing</a></li>
                            <li><a href="">Accessories</a></li>
                            <li><a href="">Sunglasses</a></li>
                        </ul>
                    </li>
                    <li class="col-md-3">
                        <strong class="title"><a href=""><span>Women's </span></a></strong>
                        <ul class="list-submenu">
                            <li><a href="">Shoes</a></li>
                            <li><a href="">Clothing</a></li>
                            <li><a href="">Accessories</a></li>
                            <li><a href="">Sunglasses</a></li>
                        </ul>

                        <strong class="title"><a href=""><span>Women's </span></a></strong>
                        <ul class="list-submenu">
                            <li><a href="">Shoes</a></li>
                            <li><a href="">Clothing</a></li>
                            <li><a href="">Accessories</a></li>
                            <li><a href="">Sunglasses</a></li>
                        </ul>
                    </li>
                    <li class="col-md-3">
                        
                        <strong class="title"><a href=""> <span>Kid's</span></a></strong>
                        <ul class="list-submenu">
                            <li><a href="">Shoes</a></li>
                            <li><a href="">Clothing</a></li>
                            <li><a href="">Accessories</a></li>
                            <li><a href="">Sunglasses</a></li>
                        </ul>

                        <strong class="title"><a href=""><span>Women's </span></a></strong>
                        <ul class="list-submenu">
                            <li><a href="">Shoes</a></li>
                            <li><a href="">Clothing</a></li>
                            <li><a href="">Accessories</a></li>
                            <li><a href="">Sunglasses</a></li>
                        </ul>
                    </li>
                    <li class="col-md-3">
                        <div class="img-categori">
                            <a href=""><img alt="img" src="<?=base_url();?>assets/frontend/images/media/index1/img-categori5.jpg"></a>
                        </div>
                       
                    </li>
                </ul>
            </div>
        </li>
        <li><a href="#"> Furniture </a></li>
        <li><a href="#"> Jewelry  </a></li>
        <li><a href="#">Blog</a></li>
    </ul>
</div>