<?php if ( count($categories) || count($brands) ) { ?>
<!-- categori -->
<div class="block-nav-categori">
    <div class="block-title">
        <span>Categories</span>
    </div>
    <div class="block-content">
        <ul class="ui-categori">
            <?php if ( count($brands) ): ?>
            <li class="parent">
                <a href="<?=site_url('brand/all');?>">
                    <span class="icon"><img src="<?=base_url('assets/frontend/images/icon/index1/nav-cat8.png');?>" alt="nav-cat"></span>
                    Brands
                </a>
                <span class="toggle-submenu"></span>
                <div class="submenu" style="background-image: url(images/media/index1/bgmenu.jpg);">
                    <?php foreach ( $brands as $brand ): ?>
                    <div class="custom-submenu"><a href="<?=site_url('brand/'.$brand->slug);?>"><?php echo $brand->title;?></a></div>
                    <?php endforeach; ?>
                </div>
            </li>
            <?php endif; ?>
            
            <?php if ( count($categories) ): ?>
                <?php foreach ($categories as $category): ?>
                <li <?=( isset($category['children']) ? 'class="parent"' : '' ); ?> >
                    <a href="<?=site_url('c/'.$category['slug']);?>">
                        <span class="icon"><img src="<?=base_url('assets/frontend/images/icon/index1/nav-cat8.png');?>" alt="nav-cat"></span>
                        <?=$category['title'];?>
                    </a>
                    <?php if( isset($category['children']) ) { ?>
                        <span class="toggle-submenu"></span>
                        <div class="submenu" style="background-image: url(images/media/index1/bgmenu.jpg);">
                            <?php foreach ( $category['children'] as $children ) { ?>
                            <div class="custom-submenu"><a href="<?=site_url('c/'.$children['slug']);?>"><?php echo $children['title'];?></a></div>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </li>
                <?php endforeach; ?>
            <?php endif; ?>
        </ul>
        <div class="view-all-categori">
            <a  href="javascript:void(0)" class="open-cate btn-view-all">All Categories</a>
        </div>
    </div>
</div><!-- categori -->
<?php } ?>