<?php if ( isset($sliders) && count($sliders) ): ?>
<!-- slide -->
<div class="owl-carousel dotsData" 
    data-nav="true" 
    data-dots="true" 
    data-margin="0" 
    data-items='1' 
    data-autoplayTimeout="100" 
    data-autoplay="true" 
    data-loop="true">
    <?php foreach( $sliders as $sk => $slider ){ ?>
    <div class="item item<?=($sk+1);?>" style="background-image: url(<?=storage('/sliders/'.$slider->source) ?>);" data-dot="<?=($sk+1);?>">
        <div class="description">
            <!-- <span class="title"><?=$slider->title;?></span> -->
            <!-- <span class="subtitle">BIG SALE</span> -->
            <!-- <span class="des"> ENJOY UP TO 35% OFF</span> -->
            <!-- <a href="" class="btn">SHOP NOW</a> -->
        </div>
    </div>
    <?php } ?>
</div> <!-- slide -->
<?php endif;?>