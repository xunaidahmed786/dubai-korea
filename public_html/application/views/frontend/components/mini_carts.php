<div class="dropdown-menu">
    <div  class="minicart-content-wrapper" >
        <div class="subtitle">
            You have <?php echo count($this->cart->contents());?> item(s) in your cart
        </div>
        <div class="minicart-items-wrapper">
            <ol class="minicart-items">
                <?php 
                $is_cart    = false;
                $items_total        = formatCurrency(0.00);
                $shipping_charges   = formatCurrency(0.00);
                if ( count($this->cart->contents()) ):
                    
                    $i                  = 1;
                    $is_cart            = true;
                    $items_total        = currencyConvertToAmount($this->cart->total());
                    $shipping_charges   = (round($items_total) ? shippingPolicyRules($items_total) : formatCurrency(0.00));

                    foreach ($this->cart->contents() as $items):
                ?>
                <li class="product-item">
                    <a class="product-item-photo" href="<?php echo $items['options']['product_url']; ?>">
                        <?php if ( $items['options']['product_image'] ) : ?>
                            <img class="product-image-photo" src="<?php echo storage('products/'.$items['options']['product_image']); ?>" alt="<?php echo $items['name']; ?>">
                        <?php else: ?>
                            <small><i>(Not available)</i></small>
                        <?php endif;?>
                    </a>
                    <div class="product-item-details">
                        <strong class="product-item-name">
                            <a href="<?php echo $items['options']['product_url']; ?>"><?php echo $items['name']; ?></a>
                        </strong>
                        <div class="product-item-price">
                            <?php if ( $items['sale_price'] == 0.00 ) : ?>
                                <span class="price" style="text-align:left;"><?php echo formatCurrency($items['original_price']); ?></span>
                            <?php else : ?>
                                <span class="price" style="text-align:left;"><del><?php echo formatCurrency($items['original_price']); ?></del></span>
                                <span class="price" style="text-align:left;"><?php echo formatCurrency($items['sale_price']); ?></span>
                            <?php endif;?>
                        </div>
                        <div class="product-item-qty">
                            <span class="label">Qty: </span><span class="number"><?php echo $items['qty']; ?></span>
                        </div>
                        <div class="product-item-actions">
                            <a class="action delete" href="<?php echo site_url('shopping-cart/remove/'.$items['rowid']); ?>" title="Remove item">
                                <span>Remove</span>
                            </a>
                        </div>
                    </div>
                </li>
                <?php
                    $i++;
                    endforeach;
                else:
                ?>
                <li>No items</li>
                <?php endif;?>
            </ol>
        </div>
        <div class="subtotal">
            <span class="label">Total</span>
            <span class="price"><?=getSelectFormatCurrency($items_total+$shipping_charges);?></span>
        </div>
        <div class="actions">
            <?php if ( $is_cart ) : ?>
                <button class="btn btn-checkout" onclick="window.location='<?php echo site_url('checkout');?>'" type="button" title="Check Out">
                    <span>Checkout</span>
                </button>
            <?php endif;?>
        </div>
    </div>
</div>