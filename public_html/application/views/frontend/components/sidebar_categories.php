<!-- Block  bestseller products-->
<div class="block-sidebar block-sidebar-categorie">
    <div class="block-title">
        <strong>PRODUCT TYPES</strong>
    </div>
    <div class="block-content">
        <ul class="items">
            <?php foreach ($categories_of_sidebar as $category) { ?>
            <li <?=(isset($category['children']) ? 'class="parent"' : '');?>>
                <a href="<?=site_url('c/'.$category['slug']);?>"><?=$category['title'];?></a>
                <?php if ( isset($category['children']) ){?>
                <span class="toggle-submenu"></span>
                    <?php foreach($category['children'] as $children) { ?>
                    <ul class="subcategory">
                        <li><a href="<?=site_url('c/'.$children['slug']);?>"><?=$children['title'];?></a></li>
                    </ul>
                    <?php }?>
                <?php } ?>
            </li>
            <?php } ?>
        </ul>
    </div>
</div><!-- Block  bestseller products-->

<!-- block filter products -->
<div id="layered-filter-block" class="block-sidebar block-filter no-hide" style="display:none;">
    <div class="close-filter-products"><i class="fa fa-times" aria-hidden="true"></i></div>
    <div class="block-title">
        <strong>FILTER SELECTION</strong>
    </div>
    <div class="block-content">

        <!-- Filter Item  categori-->
        <div class="filter-options-item filter-options-categori">
            <div class="filter-options-title">Categories</div>
            <div class="filter-options-content">
                <ol class="items">
                    <?php foreach ($categories_of_sidebar as $category) { ?>
                    <li class="item">
                        <label>
                            <input type="checkbox"><span><?=$category['title'];?> <span class="count">(0)</span></span>
                        </label>
                    </li>
                    <?php } ?>
                    <li class="item ">
                        <label>
                            <input type="checkbox"><span>Headphone & Headset <span class="count">(20)</span></span>
                        </label>
                    </li>
                </ol>
            </div>
        </div><!-- Filter Item  categori-->

        <!-- filter price -->
        <div class="filter-options-item filter-options-price">
            <div class="filter-options-title">Price</div>
            <div class="filter-options-content">
                <div class="slider-range">
                    
                    <div id="slider-range" ></div>
                    
                    <div class="action">
                        <span class="price">
                            <span>Range:</span>
                            $<span id="amount-left"></span>
                            -
                            $<span id="amount-right"></span>
                        </span>
                    </div>
                </div>
                <ol class="items">
                    <li class="item ">
                        <label>
                            <input type="checkbox"><span>$20 - $50 <span class="count">(20)</span>  </span>
                        </label>
                    </li>
                    <li class="item ">
                        <label>
                            <input type="checkbox"><span>$50 - $100 <span class="count">(20)</span> </span>
                        </label>
                    </li>
                    <li class="item ">
                        <label>
                            <input type="checkbox"><span>$100 - $250 <span class="count">(20)</span> </span>
                        </label>
                    </li>
                   
                </ol>
            </div>
        </div><!-- filter price -->

        <!-- filter brad-->
        <div class="filter-options-item filter-options-brand">
            <div class="filter-options-title">BRAND</div>
            <div class="filter-options-content">
                <ol class="items">
                    <li class="item ">
                        <label>
                            <input type="checkbox"><span>Channelo <span class="count">(20)</span>  </span>
                        </label>
                    </li>
                    <li class="item ">
                        <label>
                            <input type="checkbox"><span>Mamypokon <span class="count">(20)</span> </span>
                        </label>
                    </li>
                    <li class="item ">
                        <label>
                            <input type="checkbox"><span>Pamperson <span class="count">(20)</span> </span>
                        </label>
                    </li>
                    <li class="item ">
                        <label>
                            <input type="checkbox"><span>Pradano <span class="count">(20)</span> </span>
                        </label>
                    </li>
                    <li class="item ">
                        <label>
                            <input type="checkbox"><span>Pumano <span class="count">(20)</span> </span>
                        </label>
                    </li>
                    <li class="item ">
                        <label>
                            <input type="checkbox"><span>AME <span class="count">(20)</span> </span>
                        </label>
                    </li>
                </ol>
            </div>
        </div><!-- Filter Item -->

        <!-- filter color-->
        <div class="filter-options-item filter-options-color">
            <div class="filter-options-title">COLOR</div>
            <div class="filter-options-content">
                <ol class="items">
                    <li class="item">
                        <label>
                            <input type="checkbox">
                            <span>
                                <span class="img" style="background-color: #fca53c;"></span>          
                                <span class="count">(30)</span>
                            </span>
                            
                        </label>
                    </li>
                    <li class="item">
                        <label>
                            <input type="checkbox">
                            <span>
                                <span class="img" style="background-color: #6b5a5c;"></span>          
                                <span class="count">(20)</span>
                            </span>
                            
                        </label>
                    </li>
                    <li class="item">
                        <label>
                            <input type="checkbox">
                            <span>
                                <span class="img" style="background-color: #000000;"></span>          
                                <span class="count">(20)</span>
                            </span>
                            
                        </label>
                    </li>
                    <li class="item">
                        <label>
                            <input type="checkbox">
                            <span>
                                <span class="img" style="background-color: #3063f2;"></span>          
                                <span class="count">(20)</span>
                            </span>
                            
                        </label>
                    </li>
                    
                    <li class="item">
                        <label>
                            <input type="checkbox">
                            <span>
                                <span class="img" style="background-color: #f9334a;"></span>          
                                <span class="count">(20)</span>
                            </span>
                            
                        </label>
                    </li>
                    
                    <li class="item">
                        <label>
                            <input type="checkbox">
                            <span>
                                <span class="img" style="background-color: #964b00;"></span>          
                                <span class="count">(20)</span>
                            </span>
                            
                        </label>
                    </li>
                    <li class="item">
                        <label>
                            <input type="checkbox">
                            <span>
                                <span class="img" style="background-color: #faebd7;"></span>          
                                <span class="count">(20)</span>
                            </span>
                            
                        </label>
                    </li>
                    <li class="item">
                        <label>
                            <input type="checkbox">
                            <span>
                                <span class="img" style="background-color: #e84c3d;"></span>          
                                <span class="count">(20)</span>
                            </span>
                            
                        </label>
                    </li>
                    <li class="item">
                        <label>
                            <input type="checkbox">
                            <span>
                                <span class="img" style="background-color: #fccacd;"></span>          
                                <span class="count">(20)</span>
                            </span>
                            
                        </label>
                    </li>
                   
                </ol>
            </div>
        </div><!-- Filter Item -->

        <!-- Filter Item  size-->
        <div class="filter-options-item filter-options-size">
            <div class="filter-options-title">SIZE</div>
            <div class="filter-options-content">
                <ol class="items">
                    <li class="item ">
                        <label>
                            <input type="checkbox"><span>X <span class="count">(20)</span></span>
                        </label>
                    </li>
                    <li class="item ">
                        <label>
                            <input type="checkbox"><span>XXL <span class="count">(20)</span></span>
                        </label>
                    </li>
                    <li class="item ">
                        <label>
                            <input type="checkbox"><span>XXL <span class="count">(20)</span></span>
                        </label>
                    </li>
                    <li class="item ">
                        <label>
                            <input type="checkbox"><span>m <span class="count">(20)</span></span>
                        </label>
                    </li>
                    <li class="item ">
                        <label>
                            <input type="checkbox"><span>L <span class="count">(20)</span></span>
                        </label>
                    </li>
                    <li class="item ">
                        <label>
                            <input type="checkbox"><span>32 <span class="count">(20)</span></span>
                        </label>
                    </li>
                    <li class="item ">
                        <label>
                            <input type="checkbox"><span>36 <span class="count">(20)</span></span>
                        </label>
                    </li>
                    <li class="item ">
                        <label>
                            <input type="checkbox"><span>37 <span class="count">(20)</span></span>
                        </label>
                    </li>
                    <li class="item ">
                        <label>
                            <input type="checkbox"><span>X <span class="count">(20)</span></span>
                        </label>
                    </li>
                    <li class="item ">
                        <label>
                            <input type="checkbox"><span>XXL <span class="count">(20)</span></span>
                        </label>
                    </li>
                    <li class="item ">
                        <label>
                            <input type="checkbox"><span>XXL <span class="count">(20)</span></span>
                        </label>
                    </li>
                    <li class="item ">
                        <label>
                            <input type="checkbox"><span>m <span class="count">(20)</span></span>
                        </label>
                    </li>
                    
                </ol>
            </div>
        </div><!-- Filter Item  size-->

    </div>
</div><!-- Filter -->

<!-- Block  bestseller products-->
<div class="block-sidebar block-sidebar-products" style="display:none;">
    <div class="block-title">
        <strong>SPECIAL PRODUCTS</strong>
    </div>
    <div class="block-content">
        <div class="product-item product-item-opt-1">
            <div class="product-item-info">
                <div class="product-item-photo">
                    <a class="product-item-img" href=""><img alt="product name" src="images/media/floor5-1.jpg"></a>
                </div>
                <div class="product-item-detail">
                    <strong class="product-item-name"><a href="">Security Camera Size Flared</a></strong>
                    <div class="clearfix">
                        <div class="product-item-price">
                            <span class="price">$45.00</span>
                        </div>
                        <div class="product-reviews-summary">
                            <div class="rating-summary">
                                <div title="70%" class="rating-result">
                                    <span style="width:70%">
                                        <span><span>70</span>% of <span>100</span></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-center">
            <a href="" class="btn all-products">All products <i aria-hidden="true" class="fa fa-angle-double-right"></i></a>
        </div>
    </div>
</div><!-- Block  bestseller products-->

<!-- Block  tags-->
<div class="block-sidebar block-sidebar-tags" style="display:none;">
    <div class="block-title">
        <strong>product tags</strong>
    </div>
    <div class="block-content">
        <ul>              
            <li><a href="" class="lv2">actual</a></li>
            <li><a href="" class="lv1">adorable</a></li>
            <li><a href="" class="lv3">amaze</a></li>
            <li><a href="" class="lv5">change</a></li>
            <li><a href="" class="lv2">consider</a></li>
            <li><a href="" class="lv1">delivery</a></li>
            <li><a href="" class="lv1">Top</a></li>
            <li><a href="" class="lv4">flexib</a></li>
            <li><a href="" class="lv1">phenomenon </a></li>
        </ul>
    </div>
</div><!-- Block  tags-->

<!-- block slide top -->
<div class="block-sidebar block-sidebar-testimonials" style="display:none;">
    <div class="block-title">
        <strong>Testimonials</strong>
    </div>
    <div class="block-content">
        <div class="owl-carousel" 
            data-nav="false" 
            data-dots="true" 
            data-margin="0" 
            data-items='1' 
            data-autoplayTimeout="700" 
            data-autoplay="true" 
            data-loop="true">
            <div class="item " >
               <strong class="name">Roverto & Maria</strong>
               <div class="avata">
                    <img src="images/media/avata.jpg" alt="avata">
               </div>
               <div class="des">
                "Your product needs to improve more. To suit the needs and update your image up"
               </div>
            </div>
            <div class="item " >
               <strong class="name">Roverto & Maria</strong>
               <div class="avata">
                    <img src="images/media/avata.jpg" alt="avata">
               </div>
               <div class="des">
                "Your product needs to improve more. To suit the needs and update your image up"
               </div>
            </div>
            <div class="item " >
               <strong class="name">Roverto & Maria</strong>
               <div class="avata">
                    <img src="images/media/avata.jpg" alt="avata">
               </div>
               <div class="des">
                "Your product needs to improve more. To suit the needs and update your image up"
               </div>
            </div>
        </div>
    </div>
</div><!-- block slide top -->