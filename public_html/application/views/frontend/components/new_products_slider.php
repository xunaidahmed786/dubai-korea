<div class="container">
    <div class="block-title">
        <span class="title"><i class="fa fa-shopping-bag" style="color:#fff" aria-hidden="true"></i> Products</span>
        <div class="actions">
            <a href="#floor0-1" class="action action-up"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
            <a href="#floor0-3" class="action action-down"><i class="fa fa-angle-down" aria-hidden="true"></i></a>
        </div>
    </div>

    <!-- Banner -->
    <div class="block-banner-floor">
        <div class="col-sm-6">
            <a href="javascript:void(0)" class="box-img"><img src="<?=base_url('assets/frontend/images/dummy/new-product-banner-1.jpg');?>"></a>
        </div>
        <div class="col-sm-6">
            <a href="javascript:void(0)" class="box-img"><img src="<?=base_url('assets/frontend/images/dummy/new-product-banner-2.jpg');?>"></a>
        </div>
    </div><!-- Banner -->

    <div class="block-content">
        <div class="col-products tab-content">
            <!-- tab 1 -->
            <div class="tab-pane active in  fade " id="floor2-1" role="tabpanel">
                <div class="owl-carousel" 
                    data-nav="true" 
                    data-margin="0"
                    data-nav="true" 
                    data-dots="true" 
                    data-items='1' 
                    data-autoplayTimeout="400" 
                    data-autoplay="true"
                    data-responsive='{
                    "0":{"items":1},
                    "420":{"items":2},
                    "600":{"items":3},
                    "768":{"items":3},
                    "992":{"items":3},
                    "1200":{"items":4}
                }'>
                <?php 
                foreach ($new_products as $product){
                    load_frontend_view('products/single_product', ['product' => $product, 'typeOfproduct' => 1] );
                }
                ?> 
                </div>
            </div>
        </div>
    </div>
</div>