<div class="container">
    <div class="block-title ">
        <span class="title"><i class="fa fa-star-half-o" style="color:#fff" aria-hidden="true"></i> Best Seller</span>
        <div class="actions">
            <a href="#floor0-2" class="action action-up"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
            <a href="#floor0-4" class="action action-down"><i class="fa fa-angle-down" aria-hidden="true"></i></a>
        </div>
    </div>

    <!-- Banner -->
    <div class="block-banner-floor">
        <div class="col-sm-6">
            <a href="javascript:void(0)" class="box-img"><img src="<?=base_url('assets/frontend/images/dummy/best-seller-product-banner-1.jpg');?>"></a>
        </div>
        <div class="col-sm-6">
            <a href="javascript:void(0)" class="box-img"><img src="<?=base_url('assets/frontend/images/dummy/best-seller-product-banner-2.jpg');?>"></a>
        </div>
    </div><!-- Banner -->

    <div class="block-content">
        <div class="col-products tab-content">
            <div class="tab-pane active in  fade " id="floor3-1" role="tabpanel">
                <div class="owl-carousel" 
                    data-nav="true" 
                    data-margin="0"
                    data-nav="true" 
                    data-dots="true" 
                    data-items='1' 
                    data-autoplayTimeout="400" 
                    data-autoplay="true"
                    data-responsive='{
                    "0":{"items":1},
                    "420":{"items":2},
                    "600":{"items":3},
                    "768":{"items":3},
                    "992":{"items":3},
                    "1200":{"items":4}
                }'>
                    <?php 
                    foreach ($best_seller as $product):
                        load_frontend_view('products/single_product', ['product' => $product] );
                    endforeach;
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>