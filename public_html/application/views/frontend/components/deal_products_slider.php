<?php foreach ($deal_products as $product): ?>
<div class="product-item  product-item-opt-1 ">
    <div class="deals-of-countdown">
        <div class="count-down-time" data-countdown="<?=$product['products']['expired_at'];?>"></div>
    </div>
    
    <?php load_frontend_view('products/single_product', ['product' => $product, 'typeOfproduct' => 0] ); ?>
</div>
<?php endforeach;?>