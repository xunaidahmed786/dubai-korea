<!-- MAIN -->
<main class="site-main">
    <div class="columns container">
        <!-- Block  Breadcrumb-->
        <ol class="breadcrumb no-hide">
            <li><a href="<?=base_url();?>">Home</a></li>
            <li class="active">404 Error Page</li>
        </ol><!-- Block  Breadcrumb-->

        <div class="row">
            <!-- Main Content -->
            <div class="col-md-12">
                <h2 class="page-heading">
                    <span class="page-heading-title2">404 Error Page</span>
                </h2>
                <div class="content-text clearfix">
                    <p>
                        404 Page not found
                        <br/><br/>
                    </p>
                </div>
            </div><!-- Main Content -->
        </div>
    </div>
</main><!-- end MAIN -->