<!-- MAIN -->
<main class="site-main">
    
    <div class="block-section-top block-section-top1">
        <div class="container">
            <div class="box-section-top" style="padding-left:0">

                <!-- block slide top -->
                <div class="block-slide-main slide-opt-1" style="width: 870px;">
                <?php load_frontend_components('main_slider', $sliders);?>
                </div><!-- block slide top -->

                <!-- banner -->
                <div class="banner-slide">
                    <a href="javascript:void(0)" class="box-img" data-toggle="modal" data-target="#xKd9DdiRfiw">
                        <iframe width="300" height="224" src="https://www.youtube.com/embed/xKd9DdiRfiw" frameborder="0" allowfullscreen></iframe>
                    </a>
                    <a href="javascript:void(0)" class="box-img" data-toggle="modal" data-target="#mB1vxHqagvw">
                        <iframe width="300" height="222" src="https://www.youtube.com/embed/mB1vxHqagvw" frameborder="0" allowfullscreen></iframe>
                    </a>
                </div><!-- banner -->
            </div>
        </div>
    </div>

    <!-- Modal - xKd9DdiRfiw -->
    <div id="xKd9DdiRfiw" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">CELRANICO- Blackhead Removing Products</h4>
                </div>
                <div class="modal-body">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/xKd9DdiRfiw" frameborder="0" allowfullscreen></iframe>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal - mB1vxHqagvw -->
    <div id="mB1vxHqagvw" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">How to use Lindsay Modelling Masks, Easy and quicker way!! Korean skincare Products</h4>
                </div>
                <div class="modal-body">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/mB1vxHqagvw" frameborder="0" allowfullscreen></iframe>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-9">                
                <!-- block tab products -->
                <div class="block-tab-products-opt1">
                    <div class="block-title">
                        <ul class="nav" role="tablist">
                            <?php if ( count($new_products) ) : ?>
                            <li role="presentation" class="active">
                                <a href="#new-products"  role="tab" data-toggle="tab">PRODUCTS </a>
                            </li>
                            <?php endif;?>
                            <?php if ( count($sale_products) ) : ?>
                            <li role="presentation">
                                <a href="#on-sale-products" role="tab" data-toggle="tab">ON SALE</a>
                            </li>
                            <?php endif;?>
                            <?php if ( count($best_seller) ) : ?>
                            <li role="presentation">
                                <a href="#best-seller-products" role="tab" data-toggle="tab">BEST SELLER</a>
                            </li>
                            <?php endif;?>
                        </ul>
                    </div>
                    
                    <div class="block-content latest-block-content tab-content">
                        <?php if ( count($new_products) ): ?>
                        <!-- tab 1 -->
                        <div role="tabpanel" class="tab-pane active fade in " id="new-products">
                            <div class="owl-carousel" 
                                data-nav="true" 
                                data-dots="false" 
                                data-margin="30" 
                                data-responsive='{
                                "0":{"items":1},
                                "480":{"items":2},
                                "480":{"items":2},
                                "768":{"items":3},
                                "992":{"items":3}
                            }'>
                                <?php 
                                foreach ($new_products as $product){
                                    load_frontend_view('products/single_product', ['product' => $product] );
                                }
                                ?>
                            </div>
                        </div><!-- tab 1 -->
                        <?php endif;?>
                        
                        <?php if ( count($sale_products) ): ?>
                        <!-- tab 2 -->
                        <div role="tabpanel" class="tab-pane fade" id="on-sale-products">
                            <div class="owl-carousel" 
                                data-nav="true" 
                                data-dots="false" 
                                data-margin="30" 
                                data-responsive='{
                                "0":{"items":1},
                                "480":{"items":2},
                                "480":{"items":2},
                                "768":{"items":3},
                                "992":{"items":3}
                            }'>
                                
                                <?php 
                                foreach ($sale_products as $product):
                                    load_frontend_view('products/single_product', ['product' => $product] );    
                                endforeach;
                                ?>
                            </div>
                        </div><!-- tab 2 -->
                        <?php endif;?>
                        
                        <?php if ( count($best_seller) ): ?>
                        <!-- tab 3 -->
                        <div role="tabpanel" class="tab-pane fade" id="best-seller-products">
                            <div class="owl-carousel" 
                                data-nav="true" 
                                data-dots="false" 
                                data-margin="30" 
                                data-responsive='{
                                "0":{"items":1},
                                "480":{"items":2},
                                "480":{"items":2},
                                "768":{"items":3},
                                "992":{"items":3}
                            }'>
                                <?php 
                                foreach ($best_seller as $product){
                                    load_frontend_view('products/single_product', ['product' => $product] );
                                }
                                ?>
                            </div>
                        </div/><!-- tab 3 -->
                        <?php endif;?>

                    </div>
                </div><!-- block tab products -->
            </div>

            <div class="col-md-3">
            
                <?php if ( count($deal_products) ): ?>
                <!-- block deals  of -->
                <div class="block-deals-of block-deals-of-opt1">
                    <div class="block-title ">
                        <span class="icon"></span>
                        <div class="heading-title">latest deals</div>
                    </div>
                    <div class="block-content latest-block-deals latest-block-content">
                        
                        <div class="owl-carousel" 
                            data-nav="false" 
                            data-dots="false" 
                            data-margin="30" 
                            data-responsive='{
                            "0":{"items":1},
                            "480":{"items":2},
                            "768":{"items":3},
                            "992":{"items":1},
                            "1200":{"items":1}
                            }'>
                            <?php load_frontend_view('components/deal_products_slider', ['deal_products' => $deal_products ]);?>
                        </div>    
                                    
                    </div>
                </div><!-- block deals  of -->
                <?php endif;?>
            </div>
        </div>
    </div>

    <div class="clearfix" style="background-color: #eeeeee;">
        <!-- block -new-products / floor 1 :NewProducts-->
        <div class="block-floor-products block-floor-products-opt1 floor-products1" id="floor0-1">
            <?php load_frontend_view('components/new_products_slider', ['new_products']);?>
        </div><!-- block -new-products / floor 1 :NewProducts-->
        <br/>
        <?php if ( count($sale_products) ) : ?>
        <!-- block -on-sale-products / floor 2 :OnSaleProducts-->
        <div class="block-floor-products block-floor-products-opt1 floor-products2" id="floor0-2">
            <?php load_frontend_view('components/on_sale_products_slider', ['sale_products' => $sale_products ]);?>
        </div><!-- block -on-sale-products / floor 2:OnSaleProducts-->
        <?php endif;?>

        <?php if ( count($best_seller) ) : ?>
        <br/>
        <!-- block -best-seller-products / floor 3 :BestSellerProducts-->
        <div class="block-floor-products block-floor-products-opt1 floor-products3" id="floor0-3">
            <?php load_frontend_view('components/best_seller_products_slider', ['best_seller' => $best_seller ]);?>
        </div><!-- block -best-seller-products / floor 3 :BestSellerProducts-->
        <?php endif; ?>
    </div>
    
    <br/>
    <?php include_frontend_component('shipping_services_block');?>

</main><!-- end MAIN -->