<!-- MAIN -->
<main class="site-main">
    <div class="columns container">
        <!-- Block  Breadcrumb-->
        
        <?php //rd($category); ?>
        <ol class="breadcrumb no-hide">
            <li><a href="<?=base_url();?>">Home</a></li>
            <li class="active"><?=$category['title'];?></li>
        </ol><!-- Block  Breadcrumb-->

        <div class="row">

            <!-- Main Content -->
            <div class="col-md-9 col-md-push-3  col-main">

                

            </div><!-- Main Content -->
            
            <!-- Sidebar -->
            <div class="col-md-3 col-md-pull-9  col-sidebar">
                <?php load_frontend_view('components/sidebar_categories', ['categories_of_sidebar'=>$categories_of_sidebar]);?>
            </div><!-- Sidebar -->
            
        </div>
    </div>
</main><!-- end MAIN -->
