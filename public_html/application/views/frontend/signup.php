<!-- MAIN -->
<main class="site-main">
    <div class="columns container">
        <!-- Block  Breadcrumb-->
        <ol class="breadcrumb no-hide">
            <li><a href="<?=base_url();?>">Home</a></li>
            <li class="active">Sign Up</li>
        </ol><!-- Block  Breadcrumb-->

        <h2 class="page-heading">
            <span class="page-heading-title2">Sign Up</span>
        </h2>
        <?php

        $first_name     = 
        $last_name      =
        $telephone      = 
        $country        = 
        $state          = 
        $city           = 
        $postalcode     = 
        $address        = null; 
        $email          = isset($email) ? $email : null; 
        if ( $this->session->flashdata('data') )
        {
            $first_name = $this->session->flashdata('data')['first_name'];
            $last_name  = $this->session->flashdata('data')['last_name'];
            $email      = $this->session->flashdata('data')['email'];
            $telephone  = $this->session->flashdata('data')['telephone'];
            $address    = $this->session->flashdata('data')['address']; 
            $postalcode = $this->session->flashdata('data')['postal_code']; 
            $country    = $this->session->flashdata('data')['country'];
            $state      = $this->session->flashdata('data')['state'];
            $city       = $this->session->flashdata('data')['city'];
        }
        ?>

        <?php if( $this->session->flashdata('alert-message')):?>
        <div class="widget-content">
            <div class="alert alert-danger nomargin">
                <?php echo $this->session->flashdata('alert-message') ;?>
            </div><br/>
        </div>
        <?php endif; ?>

        <div class="page-content checkout-page">
            <div class="box-border">
                <form method="post" action="<?=site_url('new/sign-up');?>">
                    <ul>
                        <li class="row">
                            <div class="col-sm-6">
                                <label for="first_name" class="required">First Name <strong>*</strong></label>
                                <input class="input form-control" name="first_name" value="<?=$first_name;?>" id="first_name" type="text">
                            </div>
                            <div class="col-sm-6">
                                <label for="last_name" class="required">Last Name <strong>*</strong></label>
                                <input name="last_name" class="input form-control" value="<?=$first_name;?>" id="last_name" type="text">
                            </div>
                        </li>
                        <li class="row">
                            <div class="col-sm-6">
                                <label for="email_address" class="required">Email Address <strong>*</strong></label>
                                <input class="input form-control" name="email" value="<?=$email;?>" id="email_address" type="text">
                            </div>
                            <div class="col-sm-6">
                                <label for="telephone" class="required">Telephone <strong>*</strong></label>
                                <input class="input form-control" name="telephone" value="<?=$telephone;?>" id="telephone" type="text">
                            </div>
                        </li>
                        <li class="row"> 
                            <div class="col-xs-12">
                                <label for="address" class="required">Address <strong>*</strong></label>
                                <input class="input form-control" name="address" value="<?=$address;?>" id="address" type="text">
                            </div>
                        </li>
                        <li class="row">
                            <div class="col-sm-6">
                                <label for="city" class="required">City</label>
                                <input class="input form-control" name="city" id="city" value="<?=$city;?>" type="text">
                            </div>
                            <div class="col-sm-6">
                                <label class="required">State/Province</label>
                                <input class="input form-control" name="state" value="<?=$state;?>" type="text">
                            </div>
                        </li>
                        <li class="row">
                            <div class="col-sm-6">
                                <label for="postal_code" class="required">Zip/Postal Code</label>
                                <input class="input form-control" name="postal_code" value="<?=$postalcode;?>" id="postal_code" type="text">
                            </div>
                            <div class="col-sm-6">
                                <label class="required">Country</label>
                                <input class="input form-control" name="country" value="<?=$country;?>" type="text">
                            </div>
                        </li>
                        <li class="row">
                            <div class="col-sm-6">
                                <label for="password" class="required">Password <strong>*</strong></label>
                                <input class="input form-control" name="password" id="password" type="password">
                            </div>
                            <div class="col-sm-6">
                                <label for="confirm" class="required">Confirm Password <strong>*</strong></label>
                                <input class="input form-control" name="confirm" id="confirm" type="password">
                            </div>
                        </li>
                        <li>
                            <button class="button">Continue</button>
                        </li>
                    </ul>
                </form>
            </div>
        </div>
        
    </div>
</main><!-- end MAIN -->