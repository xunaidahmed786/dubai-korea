<!-- MAIN -->
<main class="site-main">
    <div class="columns container">
        <!-- Block  Breadcrumb-->
        <ol class="breadcrumb no-hide">
            <li><a href="<?=base_url();?>">Home</a></li>
            <li><a href="<?=site_url('my-account');?>">My Account</a></li>
            <li><a href="<?=site_url('my-orders');?>">Orders</a></li>
            <li class="active">Order Summary</li>
        </ol><!-- Block  Breadcrumb-->

      
        <?php if( $this->session->flashdata('alert-message')):?>
        <div class="widget-content">
            <div class="alert alert-danger nomargin">
                <?php echo $this->session->flashdata('alert-message') ;?>
            </div><br/>
        </div>
        <?php endif; ?>

        <div class="row">

            <!-- Main Content -->
            <div class="col-md-9 col-md-push-3 col-main">
                <h2 class="page-heading">
                    <span class="page-heading-title2">Order Summary</span>
                </h2>
                
                <div class="page-content page-order">
                    <ul class="step">
                        <li data-id="order-summary" class="current-step"><span>01. Order Summary</span></li>
                        <li data-id="payment-information"><span>02. Payment Information</span></a></li>
                        <li data-id="cart-summary"><span>03. Cart Summary</span></li>
                        <li data-id="billing-information"><span>04. Billing Information</span></li>
                        <li data-id="shipping-information"><span>05. Shipping Information</span></li>
                    </ul>
                </div>
                
                <div class="content" id="order-summary">
                    <div class="table-responsive">
                        <table class="table table-bordered cart_summary">
                            <tbody> 
                                <tr>
                                    <th colspan="4">Order Details</th>
                                </tr>
                                <tr>
                                    <td width="20%">Order Number</td>
                                    <td width="30%"><?=$order['order']['order_number'];?></td>
                                    <td width="20%">Payment Method</td>
                                    <td width="30%"><?=ucfirst($order['order']['payment_method']);?></td>
                                </tr>
                                <tr>
                                    <td width="20%">Order Status</td>
                                    <td width="30%"><?=ucfirst($order['order']['order_status']);?></td>
                                    <td width="20%">Shipping Amount</td>
                                    <td width="30%"><?=formatCurrencyByOrders($order['order']['shipping_charges'], $order['order']['currency']);?></td>
                                </tr>
                                <tr>
                                    <td width="20%">Discount Amount</td>
                                    <td width="30%"><?=formatCurrencyByOrders($order['order']['discount_amount'], $order['order']['currency']);?></td>
                                    <td width="20%">Total Amount</td>
                                    <td width="30%"><?=$order['order']['total_amount'];?></td>
                                </tr>
                                <tr>
                                    <td width="20%">Order Place Date</td>
                                    <td width="30%"><?=date('F d, Y h:i A', strtotime($order['order']['created_at']));?></td>
                                    <td width="20%">Order Shipped Date</td>
                                    <td width="30%"><?=($order['order']['shipped_at'] ? date('F d, Y h:i A', strtotime($order['order']['shipped_at'])) : '---');?></td>
                                </tr>
                                <tr>
                                    <td width="20%">Tracking Status</td>
                                    <td width="30%" colspan="3"><?=($order['order']['notes'] ? $order['order']['notes'] : '---');?></td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="content" id="payment-information">
                    <div class="table-responsive">
                        <table class="table table-bordered cart_summary">
                            <tbody> 
                                <tr>
                                    <th colspan="4">Order Transaction Details</th>
                                </tr>
                                <?php if ( $order['order']['payment_method'] == CASH_ON_DELIVERY_METHOD ) : ?>
                                <tr>
                                    <td width="100%" colspan="4">Cash On Delivery</td>
                                </tr>
                                <?php else: ?>
                                <tr>
                                    <td width="20%">Account Number</td>
                                    <td width="30%"><?=$order['transactions']['account_id'];?></td>
                                    <td width="20%">Transaction Secret</td>
                                    <td width="30%">******</td>
                                </tr>
                                <tr>
                                    <td width="20%">Transaction Email</td>
                                    <td width="30%"><?=$order['transactions']['email'];?></td>
                                    <td width="20%">Total Amount</td>
                                    <td width="30%"><?=$order['order']['total_amount'];?></td>
                                </tr>
                                <tr>
                                    <td width="20%">Transaction Date</td>
                                    <td width="30%"><?=$order['transactions']['created_at'];?></td>
                                    <td width="20%">Transaction Status</td>
                                    <td width="30%"><?=$order['transactions']['status'];?></td>
                                </tr>
                                <?php endif;?>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="content" id="cart-summary">
                    <div class="table-responsive">
                        <table class="table table-bordered cart_summary">
                            <tbody>
                                <tr>
                                    <th>Name</th>
                                    <th width="50%">Description</th>
                                    <th>Price</th>
                                    <th>Qty</th>
                                    <th>Subtotal</th>
                                </tr>
                                <?php
                                $total = 0;
                                if ( count($order['order_items']) ):
                                    foreach($order['order_items'] as $iKey => $items):
                                    $total += $items['subtotal'];
                                ?>
                                <tr>
                                    <td><?=$items['name'];?></td>
                                    <td widtd="60%"><?=$items['name'];?></td>
                                    <td width="15%"><?=(
                                        ($items['sale_price'] == '0.00') ? 
                                        formatCurrencyByOrders($items['price'], $order['order']['currency']) : 
                                        formatCurrencyByOrders($items['sale_price'], $order['order']['currency'])
                                        );?></td>
                                    <td><?=$items['qty'];?></td>
                                    <td><?=formatCurrencyByOrders($items['subtotal'], $order['order']['currency']);?></td>
                                </tr>
                                <?php 
                                    endforeach;
                                    $total = ($total + $order['order']['shipping_charges']);
                                endif;
                                ?>
                            </tbody>
                        </table>
                        <br/><br/>
                        
                        <table class="table table-bordered table-striped" style="clear: both; width:40%; float:right;" >
                            <tbody>
                                <tr>
                                    <th width="70%">Total products (tax incl.)</th>
                                    <td><?=formatCurrencyByOrders($order['order']['shipping_charges'], $order['order']['currency']);?></td>
                                </tr>
                                <tr>
                                    <th width="70%">Total</th>
                                    <td><?=formatCurrencyByOrders($total, $order['order']['currency']);?></td>
                                </tr>
                            </tbody>
                        </table>
                        <br/><br/>
                    </div>
                </div>

                <div class="content" id="billing-information">
                    <div class="table-responsive">
                        <table class="table table-bordered cart_summary">
                            <tbody>
                                <tr>
                                    <th colspan="4">Billing Information</th>
                                </tr>
                                <tr>
                                    <td width="35%">Full Name</td>
                                    <td width="65%"><?=$order['billing']['name'];?></td>
                                </tr>
                                <tr>
                                    <td width="35%">Email</td>
                                    <td width="65%"><?=$order['billing']['email'];?></td>
                                </tr>
                                <tr>
                                    <td width="35%">Address</td>
                                    <td width="65%"><?=$order['billing']['address'];?></td>
                                </tr>
                                <tr>
                                    <td width="35%">Telephone</td>
                                    <td width="65%"><?=$order['billing']['phone'];?></td>
                                </tr>
                                <tr>
                                    <td width="35%">Country</td>
                                    <td width="65%"><?=$order['billing']['country'];?></td>
                                </tr>
                                <tr>
                                    <td width="35%">State</td>
                                    <td width="65%"><?=$order['billing']['state'];?></td>
                                </tr>
                                <tr>
                                    <td width="35%">City</td>
                                    <td width="65%"><?=$order['billing']['city'];?></td>
                                </tr>
                                <tr>
                                    <td width="35%">Company</td>
                                    <td width="65%"><?=($order['billing']['company'] ? $order['billing']['company'] : '---');?></td>
                                </tr>
                                <tr>
                                    <td width="35%">Fax</td>
                                    <td width="65%"><?=($order['billing']['fax'] ? $order['billing']['fax'] : '---');?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="content" id="shipping-information">
                    <div class="table-responsive">
                        <table class="table table-bordered cart_summary">
                            <tbody>
                                <tr>
                                    <th colspan="2">Shipping Information</th>
                                </tr>
                                <?php if ( isset($order['shipping']) ) : ?>
                                <tr>
                                    <td width="35%">Full Name</td>
                                    <td width="65%"><?=$order['shipping']['name'];?></td>
                                </tr>
                                <tr>
                                    <td width="35%">Email</td>
                                    <td width="65%"><?=$order['shipping']['email'];?></td>
                                </tr>
                                <tr>
                                    <td width="35%">Address</td>
                                    <td width="65%"><?=$order['shipping']['address'];?></td>
                                </tr>
                                <tr>
                                    <td width="35%">Telephone</td>
                                    <td width="65%"><?=$order['shipping']['phone'];?></td>
                                </tr>
                                <tr>
                                    <td width="35%">Country</td>
                                    <td width="65%"><?=$order['shipping']['country'];?></td>
                                </tr>
                                <tr>
                                    <td width="35%">State</td>
                                    <td width="65%"><?=$order['shipping']['state'];?></td>
                                </tr>
                                <tr>
                                    <td width="35%">City</td>
                                    <td width="65%"><?=$order['shipping']['city'];?></td>
                                </tr>
                                <tr>
                                    <td width="35%">Company</td>
                                    <td width="65%"><?=($order['shipping']['company'] ? $order['shipping']['company'] : '---');?></td>
                                </tr>
                                <tr>
                                    <td width="35%">Fax</td>
                                    <td width="65%"><?=($order['shipping']['fax'] ? $order['shipping']['fax'] : '---');?></td>
                                </tr>
                                <?php else: ?>
                                <tr>
                                     <td colspan="2">No shipping details</td>
                                </tr>
                                <?php endif;?>
                            </tbody>
                        </table>
                    </div>
                </div>
                
            </div><!-- Main Content -->
                
            <!-- Sidebar -->
            <div class="col-md-3 col-md-pull-9 col-sidebar">
               <?php load_frontend_view('customers/sidebar.php'); ?>
            </div><!-- Sidebar -->

        </div>
        
    </div>
</main><!-- end MAIN -->