<!-- MAIN -->
<main class="site-main">
    <div class="columns container">
        <!-- Block  Breadcrumb-->
        <ol class="breadcrumb no-hide">
            <li><a href="<?=base_url();?>">Home</a></li>
            <li class="active">My Account</li>
        </ol><!-- Block  Breadcrumb-->

      
        <?php if( $this->session->flashdata('alert-message')):?>
        <div class="widget-content">
            <div class="alert alert-danger nomargin">
                <?php echo $this->session->flashdata('alert-message') ;?>
            </div><br/>
        </div>
        <?php endif; ?>

        <div class="row">

            <!-- Main Content -->
            <div class="col-md-9 col-md-push-3 col-main">
                
                <h3 class="checkout-sep">Confirm Orders</h3>
                <div class="table-responsive">
                    <table class="table table-bordered cart_summary">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Order Number</th>
                                <th>Payment Type</th>
                                <th>Discount Amount</th>
                                <th>Total Amount</th>
                                <th>Total Items</th>
                                <th>Order Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            if ( count($confirmed_orders) ): 
                                foreach($confirmed_orders as $order):
                            ?>
                            <tr>
                                <td>#</td>
                                <td><?=$order['order_number'];?></td>
                                <td><?=$order['payment_method'];?></td>
                                <td><?=formatCurrency($order['discount_amount']);?></td>
                                <td><?=formatCurrency($order['total_amount']);?></td>
                                <td><?=$order['total_items'];?> <small>(item's)</small></td>
                                <td><?=formatDate('F d, Y', $order['created_at']);?></td>
                            </tr>
                            <?php 
                                endforeach;
                            else:
                            ?>
                            <tr>
                                <td colspan="7">No orders</td>
                            </tr>
                            <?php endif;?>
                        </tbody> 
                    </table>
                </div>

                <br/><br/>
                <h3 class="checkout-sep">Delivered Orders</h3>
                <div class="table-responsive">
                    <table class="table table-bordered cart_summary">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Order Number</th>
                                <th>Payment Type</th>
                                <th>Discount Amount</th>
                                <th>Total Amount</th>
                                <th>Total Items</th>
                                <th>Order Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tbody>
                            <?php 
                            if ( count($delivered_orders) ): 
                                foreach($delivered_orders as $order):
                            ?>
                            <tr>
                                <td>#</td>
                                <td><?=$order['order_number'];?></td>
                                <td><?=$order['payment_method'];?></td>
                                <td><?=formatCurrency($order['discount_amount']);?></td>
                                <td><?=formatCurrency($order['total_amount']);?></td>
                                <td><?=$order['total_items'];?> <small>(item's)</small></td>
                                <td><?=formatDate('F d, Y', $order['created_at']);?></td>
                            </tr>
                            <?php 
                                endforeach;
                            else:
                            ?>
                            <tr>
                                <td colspan="7">No orders</td>
                            </tr>
                            <?php endif;?>
                        </tbody> 
                        </tbody> 
                    </table>
                </div>
                <br/><br/>
                
            </div><!-- Main Content -->
                
            <!-- Sidebar -->
            <div class="col-md-3 col-md-pull-9 col-sidebar">
               <?php load_frontend_view('customers/sidebar.php'); ?>
            </div><!-- Sidebar -->

        </div>
        
    </div>
</main><!-- end MAIN -->