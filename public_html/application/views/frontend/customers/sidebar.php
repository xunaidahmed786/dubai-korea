<div class="block-sidebar block-sidebar-categorie">
    <div class="block-title">
        <strong>MY ACCOUNT</strong>
    </div>
    <div class="block-content">
        <ul class="items">
            <!-- <li><a href="<?=site_url('my-account');?>">My Account</a></li> -->
            <li><a href="<?=site_url('my-orders');?>">My Orders</a></li>
            <li><a href="<?=site_url('wishlist');?>">Wishlist</a></li>
            <li><a href="<?=site_url('logout');?>">Logout</a></li>
        </ul>
    </div>
</div>