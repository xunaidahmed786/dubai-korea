<!-- MAIN -->
<main class="site-main">
    <div class="columns container">
        <!-- Block  Breadcrumb-->
        <ol class="breadcrumb no-hide">
            <li><a href="<?=base_url();?>">Home</a></li>
            <li><a href="<?=site_url('my-account');?>">My Account</a></li>
            <li class="active">My Orders</li>
        </ol><!-- Block  Breadcrumb-->

      
        <?php if( $this->session->flashdata('alert-message')):?>
        <div class="widget-content">
            <div class="alert alert-danger nomargin">
                <?php echo $this->session->flashdata('alert-message') ;?>
            </div><br/>
        </div>
        <?php endif; ?>

        <div class="row">

            <!-- Main Content -->
            <div class="col-md-9 col-md-push-3 col-main">
                <h2 class="page-heading">
                    <span class="page-heading-title2">My Orders</span>
                </h2>

                <div class="table-responsive">
                    <table class="table table-bordered cart_summary">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Order Number</th>
                                <th>Payment Type</th>
                                <th>Shipping Amount</th>
                                <th>Discount Amount</th>
                                <th>Total Amount</th>
                                <th>Total Items</th>
                                <th>Status</th>
                                <th>Order Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            if ( count($orders) ):
                                foreach($orders as $order):
                            ?>
                            <tr>
                                <td>#</td>
                                <td><?=$order['order_number'];?></td>
                                <td><?=$order['payment_method'];?></td>
                                <td><?=$order['shipping_charges'];?></td>
                                <td><?=$order['discount_amount'];?></td>
                                <td><?=$order['total_amount'];?></td>
                                <td><?=$order['total_items'];?> <small>(item's)</small></td>
                                <td><?=ucfirst($order['order_status']);?></td>
                                <td><?=formatDate('F d, Y', $order['created_at']);?></td>
                                <td>
                                    <a href="<?=site_url('orders/order-summary/'. $order['id']);?>">Order Summary</a>
                                </td>
                            </tr>
                            <?php 
                                endforeach;
                            else:
                            ?>
                            <tr>
                                <td colspan="8">No orders</td>
                            </tr>
                            <?php endif;?>
                        </tbody> 
                    </table>
                </div>
                
            </div><!-- Main Content -->
                
            <!-- Sidebar -->
            <div class="col-md-3 col-md-pull-9 col-sidebar">
               <?php load_frontend_view('customers/sidebar.php'); ?>
            </div><!-- Sidebar -->

        </div>
        
    </div>
</main><!-- end MAIN -->