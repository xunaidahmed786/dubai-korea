<!-- MAIN -->
<main class="site-main">
    <div class="columns container">
        <!-- Block  Breadcrumb-->
        <ol class="breadcrumb no-hide">
            <li><a href="<?=base_url();?>">Home</a></li>
            <li class="active">Contact Us</li>
        </ol><!-- Block  Breadcrumb-->

        <h2 class="page-heading">
            <span class="page-heading-title2">Contact Us</span>
        </h2>

        <div class="page-content" id="contact">
            <div class="row">
                <div class="col-sm-6">
                    <?php
                    $name               = 
                    $email              =
                    $telephone          = 
                    $message            = null;

                    if ( $this->session->flashdata('data') )
                    {
                        $name               = $this->session->flashdata('data')['name'];
                        $email              = $this->session->flashdata('data')['email'];
                        $telephone          = $this->session->flashdata('data')['telephone'];
                        $message            = $this->session->flashdata('data')['message'];
                    }
                    ?>
                    <?php if( $this->session->flashdata('alert-danger')):?>
                    <div class="widget-content">
                        <div class="alert alert-danger nomargin">
                            <?php echo $this->session->flashdata('alert-danger') ;?>
                        </div>
                    </div><br/>
                    <?php endif; ?>

                    <?php if( $this->session->flashdata('alert-success')):?>
                    <div class="widget-content">
                        <div class="alert alert-success nomargin">
                            <?php echo $this->session->flashdata('alert-success') ;?>
                        </div>
                    </div><br/>
                    <?php endif; ?>

                    <h3 class="page-subheading">Drop Us a Query</h3>
                    <div class="contact-form-box">
                        <form method="post" action="<?=site_url('contact-us/send');?>">
                            <div class="form-selector">
                                <label>Name <strong>*</strong></label>
                                <input type="text" name="name" value="<?=$name;?>" class="form-control input-sm">
                            </div>
                            <div class="form-selector">
                                <label>Email address <strong>*</strong></label>
                                <input type="text" name="email" value="<?=$email;?>" class="form-control input-sm">
                            </div>
                            <div class="form-selector">
                                <label>Telephone <strong>*</strong></label>
                                <input type="text" name="telephone" class="form-control input-sm">
                            </div>
                            <div class="form-selector">
                                <label>Message</label>
                                <textarea name="message" rows="10" class="form-control input-sm"><?=$message;?></textarea>
                            </div>
                            <div class="form-selector">
                                <button class="btn" type="submit" id="btn-send-contact">Send</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div id="contact_form_map" class="col-xs-12 col-sm-6">
                    <h3 class="page-subheading">Contact Us</h3>
                    <p>
                        Don’t be a stranger!<br><br>
                        It's our customers across GCC who make us what we are, so we're always eager to hear from you! The more feedback you give us, 
                        the more personalized your Dubaikorea experience can become. If you have any questions, comments, complaints or queries, 
                        please don’t hesitate to contact us at the following addresses.
                    </p>
                    <br>
                    <h4>Office</h4>
                    <ul class="store_info">
                        <li><i class="fa fa-home"></i>You can come and see us at Silicon Oasis, Palace Tower, Dubai, P.O.BOX: 299357</li>
                        <li><i class="fa fa-phone"></i><span>+ 971(4)-224-4999</span></li>
                        <li><i class="fa fa-envelope"></i>Email: <span><a href="mailto:info@dubaikorea.com">info@dubaikorea.com</a></span></li>
                    </ul><br>
                    
                    <h4>Support</h4>
                    <p>
                        For everything relating to your buying experience, including how to use the site, which products are right for you and anything 
                        you aren’t satisfied about, contact: <a href="mailto:support@dubaikorea.com">support@dubaikorea.com</a>
                    </p><br>

                    <h4>Sales</h4>
                    <p>For questions and queries relating to how to become a partner or associate of Dubaikorea, 
                    contact our Sales Department at <a href="mailto:sales@dubaikorea.com">sales@dubaikorea.com</a>.
                    </p><br>
                    
                    <h4>Customer Service Office Hours</h4>
                    <ul>
                        <li>Sunday - Thursday 10 AM to 6PM (UAE time)</li>
                        <li>Friday &amp; Saturday we will ensure to respond within 24 hours</li>
                    </ul>
                    <br><br>
                </div>
            </div>
        </div>
    </div>
</main><!-- end MAIN -->