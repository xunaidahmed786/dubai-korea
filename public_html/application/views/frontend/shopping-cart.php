<!-- MAIN -->
<main class="site-main">
    <div class="columns container">
        <!-- Block  Breadcrumb-->
        <ol class="breadcrumb no-hide">
            <li><a href="<?=base_url();?>">Home</a></li>
            <li class="active">Shopping Cart</li>
        </ol><!-- Block  Breadcrumb-->

        <h2 class="page-heading">
            <span class="page-heading-title2">Shopping Cart</span>
        </h2>
        <div class="page-content page-order">
            
            <?php if( $this->session->flashdata('alert-danger')):?>
                <div class="widget-content">
                    <div class="alert alert-danger nomargin">
                        <?php echo $this->session->flashdata('alert-danger') ;?>
                    </div><br/>
                </div>
            <?php endif; ?>

            <?php if( $this->session->flashdata('alert-success')):?>
                <div class="widget-content">
                    <div class="alert alert-success nomargin">
                        <?php echo $this->session->flashdata('alert-success') ;?>
                    </div><br/>
                </div>
            <?php endif; ?>

            <div class="order-detail-content">
                <div class="table-responsive">
                    <table class="table table-bordered cart_summary">
                        <thead>
                            <tr>
                                <th class="cart_product">Product</th>
                                <th>Description</th>
                                <th>Unit price</th>
                                <th>Qty</th>
                                <th>Total</th>
                                <th class="action"><i class="fa fa-trash-o"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            $total      = 0;
                            $is_cart    = false;
                            if ( count($this->cart->contents()) ):
                                $i = 1;
                                $is_cart = true;
                                foreach ($this->cart->contents() as $items):
                                $total += $items['subtotal'];
                            ?>
                            <tr>
                                <?php echo form_hidden($i.'[rowid]', $items['rowid']); ?>
                                <td class="cart_product">
                                    <a href="<?php echo $items['options']['product_url']; ?>">
                                        <?php if ( $items['options']['product_image'] ) : ?>
                                        <img alt="<?php echo $items['name']; ?>" src="<?php echo storage('products/'.$items['options']['product_image']); ?>">
                                        <?php else: ?>
                                            <small><i>(Not available)</i></small>
                                        <?php endif;?>
                                    </a>
                                </td>
                                <td class="cart_description">
                                    <p class="product-name">
                                        <a href="<?php echo $items['options']['product_url']; ?>"><?php echo $items['name']; ?></a>
                                    </p>
                                    <small class="cart_ref">Code : <?php echo $items['code']; ?></small><br><br>
                                    <p class="product-name" style="font-size:13px; font-weight:bold;">Short Description</p>
                                    <small>
                                        <?php echo $items['short_description']; ?>
                                    </small>
                                </td>
                                <td class="price">
                                    <?php if ( $items['sale_price'] == 0.00 ) : ?>
                                        <span style="text-align:left;"><?php echo formatCurrency($items['original_price']); ?></span>
                                    <?php else : ?>
                                        <span style="text-align:left;"><del><?php echo formatCurrency($items['original_price']); ?></del></span>
                                        <span style="text-align:left;"><?php echo formatCurrency($items['sale_price']); ?></span>
                                    <?php endif;?>
                                </td>
                                <td class="qty">
                                    <input type="text" minlength="1" data-row-id="<?=$items['rowid'];?>" maxlength="<?php echo $items['available_stock']; ?>" name="qty" id="qty" value="<?php echo $items['qty']; ?>" class="form-control input-sm">
                                    <button type="button" class="btn qtyclick">Update</button>
                                    <!-- <span  data-field="qty" data-type="minus" class="btn-number"><i class="fa fa-caret-up"></i></span> -->
                                    <!-- <span  data-field="qty" data-type="plus" class="btn-number"><i class="fa fa-caret-down"></i></span> -->
                                </td>
                                <td class="price">
                                    <span><?php echo formatCurrency($items['subtotal']); ?></span>
                                </td>
                                <td class="action">
                                    <a href="<?php echo site_url('shopping-cart/remove/'.$items['rowid']); ?>">Delete item</a>
                                </td>
                            </tr>
                            <?php
                                $i++;
                                endforeach;
                            else:
                            ?>
                            <tr><td colspan="6">Your cart is empty.</td></tr>
                            <?php endif;?>
                        </tbody>
                        <tfoot>
                            <?php 
                            if ( count($this->cart->contents()) ):
                            $total              = currencyConvertToAmount($this->cart->total());
                            $shipping_charges   = shippingPolicyRules($total);
                            ?>
                            <tr>
                                <td rowspan="1" colspan="2"></td>
                                <td colspan="3">Shippment Charges (tax incl.)</td>
                                <td colspan="2"><?=getSelectFormatCurrency($shipping_charges);?></td>
                            </tr>
                            <tr>
                                <td colspan="2"><strong>Total</strong></td>
                                <td colspan="4"><strong><?=getSelectFormatCurrency($total+$shipping_charges);?></strong></td>
                            </tr>
                            <?php endif;?>
                        </tfoot>    
                    </table>
                </div>
                <br/><br/>
                <div class="cart_navigation">
                    <a href="<?=base_url();?>" class="prev-btn">Continue shopping</a>
                    <?php if ( $is_cart ) : ?>
                    <a href="<?=site_url('checkout');?>" class="next-btn">Proceed to checkout</a>
                    <?php endif;?>
                </div>
            </div>

        </div>
    <br/><br/><br/><br/><br/><br/>
    </div>
</main><!-- end MAIN -->