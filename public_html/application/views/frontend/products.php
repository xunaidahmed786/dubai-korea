<!-- MAIN -->
<main class="site-main">
    <div class="columns container">
        <!-- Block  Breadcrumb-->
        
        <?php //rd($category); ?>
        <ol class="breadcrumb no-hide">
            <li><a href="<?=base_url();?>">Home</a></li>
            <li class="active"><?=$breakcrum_active;?></li>
        </ol><!-- Block  Breadcrumb-->

        <div class="row">
            <!-- Main Content -->
            <div class="col-md-9 col-md-push-3  col-main">
               
                <!-- List Products -->
                <div class="products  products-grid">
                    <ol class="product-items row">
                        <?php 
                        if ( count($products) ):
                            foreach ($products as $product):?>
                            <li class="col-sm-3 product-item ">
                                <?php load_frontend_view('products/single_product', ['product' => $product] ); ?>
                            </li>
                        <?php endforeach;?>
                        <?php else:?>
                        <li class="col-sm-3 product-item ">No product(s) available. </li>
                        <?php endif; ?>
                    </ol><!-- list product -->
                </div> <!-- List Products -->

            </div><!-- Main Content -->
            
            <!-- Sidebar -->
            <div class="col-md-3 col-md-pull-9  col-sidebar">
                <?php load_frontend_view('components/sidebar_categories', ['categories_of_sidebar'=>$categories_of_sidebar]);?>
            </div><!-- Sidebar -->

        </div>
    </div>

</main><!-- end MAIN -->