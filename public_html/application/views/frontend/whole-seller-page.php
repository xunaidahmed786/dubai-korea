<!-- MAIN -->
<main class="site-main">
    <div class="columns container">
        <!-- Block  Breadcrumb-->
        <ol class="breadcrumb no-hide">
            <li><a href="<?=base_url();?>">Home</a></li>
            <li class="active">Wholesale Enquiry</li>
        </ol><!-- Block  Breadcrumb-->

        <h2 class="page-heading">
            <span class="page-heading-title2">Wholesale Enquiry</span>
        </h2>

        <div class="page-content" style="margin:0;">
            <div class="row">
                <div class="col-sm-6">
                   
                    <?php if( $this->session->flashdata('alert-danger')):?>
                    <div class="widget-content">
                        <div class="alert alert-danger nomargin">
                            <?php echo $this->session->flashdata('alert-danger') ;?>
                        </div>
                    </div>
                    <?php endif; ?>

                    <?php if( $this->session->flashdata('alert-success')):?>
                    <div class="widget-content">
                        <div class="alert alert-success nomargin">
                            <?php echo $this->session->flashdata('alert-success') ;?>
                        </div>
                    </div>
                    <?php endif; ?>
                    
                    <form method="post" id="whole_seller_form_id" action="<?=site_url('whole-seller-send-inquiry');?>">
                        <input type="hidden" value="<?=site_url('wholesale-enquiry');?>" name="product_url">
                        <input type="hidden" name="product_name">
                        <h3>Denotes mandatory field</h3>
                        <div class="form-group">
                            <label for="name">Product <strong style="color:red;">*</strong></label>
                            <select name="product_id" class="form-control required-field">
                                <option value="">Select Product</option>
                                <?php foreach( $products as $product ):?>
                                <option value="<?php echo $product['id'];?>"><?php echo $product['title'];?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                        <?php load_frontend_view('products/whole_seller_form');?>
                        
                    </form>
                    
                </div>
            </div>
        </div>
    </div>
</main><!-- end MAIN -->