<!-- MAIN -->
<main class="site-main">
    <div class="columns container">
        <!-- Block  Breadcrumb-->
        <ol class="breadcrumb no-hide">
            <li><a href="<?=base_url();?>">Home</a></li>
            <li class="active">Authentication</li>
        </ol><!-- Block  Breadcrumb-->

        <h2 class="page-heading">
            <span class="page-heading-title2">Authentication</span>
        </h2>

        <div class="page-content">
            <div class="row">
                <?php if( $this->session->flashdata('alert-success')):?>
                <div class="widget-content">
                    <div class="alert alert-success nomargin">
                        <?php echo $this->session->flashdata('alert-success') ;?>
                    </div><br/>
                </div>
                <?php endif; ?>

                <div class="col-sm-6">
                    <div class="box-border">
                        <h4>Checkout as a Guest or Register</h4>
                        <p>Register with us for future convenience:</p>
                        <ul>
                            <li>
                                <label><input name="checkout_type" checked="checked" value="register" type="radio"> Register</label>
                            </li>
                            <li>
                                <label><input name="checkout_type" value="guest" type="radio"> Checkout as Guest</label>
                            </li>
                        </ul>
                        <br>
                        <h4>Register and save time!</h4>
                        <p>Register with us for future convenience:</p>
                        <p><i class="fa fa-check-circle text-primary"></i> Fast and easy check out</p>
                        <p><i class="fa fa-check-circle text-primary"></i> Easy access to your order history and status</p>
                        <button class="button" id="checkout_btn">Continue</button>
                    </div>
                </div>
                <div class="col-sm-6">
                
                    <div class="box-authentication">

                        <?php if( $this->session->flashdata('alert-danger')):?>
                        <div class="widget-content">
                            <div class="alert alert-danger nomargin">
                                <?php echo $this->session->flashdata('alert-danger') ;?>
                            </div><br/>
                        </div>
                        <?php endif; ?>

                        <form method="post" action="<?=site_url('authentication');?>">
                            <h3>Already registered?</h3>
                            <label for="emmail_login">Email address</label>
                            <input type="text" class="form-control" name="email">
                            <label for="password_login">Password</label>
                            <input type="password" class="form-control" name="password">
                            <p class="forgot-pass"><a href="javascript:void(0)">Forgot your password?</a></p>
                            <button type="submit" class="button"><i class="fa fa-lock"></i> Sign in</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
       
    </div>
</main><!-- end MAIN -->