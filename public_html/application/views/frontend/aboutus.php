<!-- MAIN -->
<main class="site-main">
    <div class="columns container">
        <!-- Block  Breadcrumb-->
        <ol class="breadcrumb no-hide">
            <li><a href="<?=base_url();?>">Home</a></li>
            <li class="active">About Us</li>
        </ol><!-- Block  Breadcrumb-->

        <div class="row">
            <!-- Main Content -->
            <div class="col-md-12">
                <h2 class="page-heading">
                    <span class="page-heading-title2">About Us</span>
                </h2>
                <div class="content-text clearfix">
                    <!-- <img width="310" alt="" class="alignleft" src="images/media/detail/about-us.jpg"> -->
                    <p>
                        Winke General Trading FZE founded in 2016, are a skincare and haircare company that currently receives attention from a lot of 
                        customers with skincare products. Currently communicating with customers through social commerce, online and offline 
                        store.DubaiKorea is owned and managed by Winke General Treading FZE.
                        <br/><br/>
                        We always do our best to provide customer satisfaction and premium value, we are obsessed with customer service and would 
                        further like to share happiness with customers through unique products and sincere services.
                        <br/><br/>
                        We are committed to delivering a superior site experience, accurate search results, 
                        accurate inventory and great service across every touch point on the website. Our technology today is ahead of the 
                        game yet we are nowhere close to finished work here. We will continue to improve, update and find better ways to be more 
                        efficient in delivering more.
                        <br/><br/>
                        Enjoy your experience with Dubaikorea,
                        <br/><br/>
                        Happy Shopping!!!
                        <br/><br/>

                    </p>
                </div>
            </div><!-- Main Content -->
        </div>
    </div>
</main><!-- end MAIN -->