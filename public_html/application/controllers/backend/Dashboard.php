<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Admin_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function index()
	{

		$this->header_assets = [
			'assets/libs/rickshaw/rickshaw.min.css',
			'assets/libs/morrischart/morris.css',
			'assets/libs/jquery-jvectormap/css/jquery-jvectormap-1.2.2.css',
			'assets/libs/jquery-clock/clock.css',
			'assets/libs/bootstrap-calendar/css/bic_calendar.css',
			'assets/libs/jquery-weather/simpleweather.css',
			'assets/libs/bootstrap-xeditable/css/bootstrap-editable.css'
		];

		$this->footer_assets = [
			'assets/libs/d3/d3.v3.js',
			'assets/libs/rickshaw/rickshaw.min.js',
			'assets/libs/raphael/raphael-min.js',
			'assets/libs/morrischart/morris.min.js',
			'assets/libs/jquery-knob/jquery.knob.js',
			'assets/libs/jquery-jvectormap/js/jquery-jvectormap-1.2.2.min.js',
			'assets/libs/jquery-jvectormap/js/jquery-jvectormap-world-mill-en.js',
			'assets/libs/jquery-clock/clock.js',
			'assets/libs/jquery-easypiechart/jquery.easypiechart.min.js',
			'assets/libs/jquery-weather/jquery.simpleWeather-2.6.min.js',
			'assets/libs/bootstrap-xeditable/js/bootstrap-editable.min.js',
			'assets/libs/bootstrap-calendar/js/bic_calendar.min.js',
			'assets/js/apps/calculator.js',
			'assets/js/apps/todo.js',
			'assets/js/apps/notes.js',
			'assets/js/pages/index2.js'
		];
		
		$this->load->model('productmodel');
		$categories = $this->triggermodel->count(TBL_CATEGORIES, ['parent_id' => 0, 'active' => 1]);
		$products 	= $this->productmodel->getProductsCountByActive();
		$orders 	= $this->triggermodel->count(TBL_ORDERS, ['order_status' => 'delivered']);
		$customers 	= $this->triggermodel->count(TBL_CUSTOMERS, ['active' => 1]);
		
		$this->loadTemplate('dashboard', 
		[
			'data' =>
			[
				'categories'	=> $categories,
				'products' 		=> $products,
				'orders' 		=> $orders,
				'customers' 	=> $customers,
			]
		]);
	}
}
