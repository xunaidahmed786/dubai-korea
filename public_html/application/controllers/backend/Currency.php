<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Currency extends Admin_Controller {

	public function __construct(){
		parent::__construct();
	}
	
	private function formAssets(){
		$this->header_assets = [
			'assets/libs/bootstrap-validator/css/bootstrapValidator.min.css',
		];

		$this->footer_assets = [
			'assets/libs/bootstrap-validator/js/bootstrapValidator.min.js',
			'assets/js/pages/form-validation.js',
		];
	}

	public function addNew(){
		
		$this->formAssets();
		
		$this->loadTemplate('currencies/form');
	}

	public function save(){

		$this->__validation();

		$data = [
			'description' 	=> $this->input->post('description'),
			'symbols' 		=> $this->input->post('symbols'),
			'amount' 		=> $this->input->post('amount'),
			'active' 		=> $this->input->post('active'),
			'created_at' 	=> date('Y-m-d H:i:s'),
			'updated_at' 	=> date('Y-m-d H:i:s')
		];

		$this->triggermodel->insertOrFail(TBL_CURRENCIES, $data);
		
		$this->session->set_flashdata('alert-message', [
			'alert-type' => 'success',
			'message' 	 => 'Your record has been successfully added.',
			'link_name' => 'View Currencies Manage',
			'link_url' => link_to_backend('currency/manage'),
		]);

		redirect( link_to_backend('currency/addNew') );
	}

	public function edit( $id ){
		
		$this->formAssets();

		$edit_data = $this->triggermodel->firstOrFail(TBL_CURRENCIES, ['where' => ['id' => $id ]]);

		$this->loadTemplate('currencies/form', ['edit_data' => $edit_data]);
	}

	public function update($id){

		$this->__validation($id);

		$data = [
			'description' 	=> $this->input->post('description'),
			'symbols' 		=> $this->input->post('symbols'),
			'amount' 		=> $this->input->post('amount'),
			'active' 		=> $this->input->post('active'),
			'updated_at' 	=> date('Y-m-d H:i:s')
		];

		$this->triggermodel->updateOrFail(TBL_CURRENCIES, $data, ['id' => $id]);
		
		$this->session->set_flashdata('alert-message', [
			'alert-type' => 'success',
			'message' 	 => 'Your record has been successfully updated.'
		]);

		redirect( link_to_backend('currency/manage') );
	}

	public function delete( $id )
	{

		$this->triggermodel->use_soft = true;
		$this->triggermodel->delete(TBL_CURRENCIES, $id);

		$this->session->set_flashdata('alert-message', [
			'alert-type' => 'success',
			'message' 	 => 'Your record has been successfully deleted.'
		]);
		redirect( link_to_backend('currency/manage') );
	}

	public function manage()
	{
		$this->header_assets = [
			'assets/libs/jquery-datatables/css/dataTables.bootstrap.css',
			'assets/libs/jquery-datatables/extensions/TableTools/css/dataTables.tableTools.css'
		];

		$this->footer_assets = [
			'assets/libs/jquery-datatables/js/jquery.dataTables.min.js',
			'assets/libs/jquery-datatables/js/dataTables.bootstrap.js',
			'assets/libs/jquery-datatables/extensions/TableTools/js/dataTables.tableTools.min.js',
			'assets/js/pages/datatables.js'
		];

		$categories = $this->triggermodel->all(TBL_CURRENCIES, [], TRUE);

		if ( count($categories) ){
			$categories = array_map( function($v){
				
				$row = $v;

				if ( $row['created_at'] ) {
					$row['created_at'] = formatDate(ADMIN_DATE_FORMAT, $row['created_at'] );
				};

				if ( $row['updated_at'] ) {
					$row['updated_at'] = formatDate(ADMIN_DATE_FORMAT, $row['updated_at'] );
				};

				return $row;

			}, $categories );
		}

		$this->loadTemplate('currencies/index', [
			'data' => $categories
		]);
	}

	private function __validation( $id = NULL ){

		$this->load->library('form_validation');

		$config = [
	        [
	        	'field' => 'description',
	            'label' => 'description',
	            'rules' => 'trim|required|max_length[50]|is_unique['.TBL_CURRENCIES.'.description]',
	            'errors' => [
	            	'required' 	=> 'You must provide a %s.',
	            	'is_unique' => 'This %s already exists.'
	            ],
	        ]
		];

		/* categories email validation */
		if ( $id ) { 
			$config[0] = [
				'field'   => 'description', 
				'label'   => 'description', 
				'rules'   => 'trim|required|max_length[50]|unique_other_than['.TBL_CURRENCIES.'.description, id='.$id.']',
				'rules'   => 'trim|required|max_length[50]',
				'errors' => [
	            	'required' 	=> 'You must provide a %s.',
	            	'is_unique' => 'This %s already exists.'
	            ]
			];
		}

		// rd($config);

		$this->form_validation->set_rules($config);

		if ( $this->form_validation->run() == FALSE ) {

			$this->session->set_flashdata('data', $this->input->post() );
			$this->session->set_flashdata('alert-message', [
				'alert-type' => 'danger',
				'message' 	 => validation_errors()
			]);

			redirect( link_to_backend('currency/' . ($id ? ('edit/'.$id)  : 'addNew' ) ) );
		}
	}
}
