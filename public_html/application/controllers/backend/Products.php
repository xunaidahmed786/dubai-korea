<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends Admin_Controller {

	private $_uploaded;
	private $max_width 	= '1024';
	private $max_height = '768';
	private $location 	= './storage/products/';

	public function __construct(){
		parent::__construct();

		$this->load->model('productmodel');
	}

	public function dropzoneDoUpload()
	{
		$config['upload_path'] 		= $this->location;
	    $config['allowed_types'] 	= 'gif|jpg|png|jpeg';
	    $config['max_size'] 		= 1024 * 8;
	    // $config['max_width']  		= $this->max_width;
	    // $config['max_height']  		= $this->max_height;
	    $config['encrypt_name'] 	= TRUE;
	    
	    $this->load->library('upload', $config);
	    
	    if (!$this->upload->do_upload('file'))
        {
            $status 	= false;
            $message 	= $this->upload->display_errors('', '');
            r($msg);
        }
        else
        {
        	$status = true;
            $data = $this->upload->data();

            $this->triggermodel->insertOrFail(TBL_IMAGES, [
				'product_id' => 0,
				'image' 	 => $data['file_name'],
				'is_default' => 0
			]);

            $collection = [
				'file_id' 	=> $this->triggermodel->last_insert_id(),
				'file_name' => $data['file_name'],
				'file_size' => filesize( './storage/products/'. $data['file_name']),
			];

			
			$message = 'Successfully Upload';
        }
		
		echo json_encode([
			'status' 	=> $status,
			'message' 	=> $message, 
			'collection' => [
				'images' => count($collection) ? $collection : null
			]
		]);
	}

	public function dropzoneExistsDoUpload()
	{
		$thumnail = $this->db->where_in('id', $this->input->post('files_ids'))->select('id, image')->get(TBL_IMAGES);
		
		$medias = array_map(function($v){
			return [
				'file_id' 	=> $v['id'],
				'file_name' => $v['image'],
				'file_size' => filesize( './storage/products/'. $v['image']),
			];
		}, $thumnail->result_array() );
		
		echo json_encode([
			'status' => TRUE,
			'collection' => [
				'images' => $medias
			]
		]);
	}

	public function dropzoneExistsRemoveImage()
	{	
		$file_id 	= $this->input->post('file_id');
		$file_name 	= $this->input->post('file_name');

		@unlink( $this->location . $file_name );
		$this->triggermodel->delete(TBL_IMAGES, $file_id);

		echo json_encode([
			'status' 	=> TRUE,
			'message' 	=> 'Delete Image',
		]);
	}
	
	private function formAssets(){
		$this->header_assets = [
			'assets/libs/summernote/summernote.css',
			
			'assets/libs/dropzone/css/dropzone.css',
			'assets/libs/bootstrap-select2/select2.css',
		];

		$this->footer_assets = [
			
			'assets/libs/bootstrap-inputmask/inputmask.js',
			'assets/libs/summernote/summernote.js',
			
			'assets/js/pages/forms.js',
			'assets/libs/dropzone/dropzone.min.js',
			'assets/libs/bootstrap-select2/select2.min.js',
		];

		$base_url = base_url();
	
$this->footerJS = <<<JAVASCRIPT

		//http://mrbool.com/building-a-file-upload-form-using-dropzonejs-and-php/34395
		
		Dropzone.autoDiscover = false;
		
		var myDropzone = new Dropzone("div#my-dropzone", { 
			url: "$base_url"+"backend/products/dropzoneDoUpload",
			addRemoveLinks: true,
			maxFilesize: 1,
			acceptedFiles: ".jpeg,.jpg,.png,.gif",
			init: function() {
				
				thisMyDropzone = this;

				this.on("removedfile", function (file) {

					var file_id 				= file.file_id;
					var file_name 				= file.name;

					$.post( "$base_url"+"backend/products/dropzoneExistsRemoveImage", { file_id: file_id, file_name: file_name }, function(data) {
						
						var collection_of_parse = JSON.parse(data);
						if ( collection_of_parse.status )
						{
							var existing_thumbnail_ids 	= $('input[name=exists_thumbnail_ids]').val().split(',').filter(function(v){ return v!='';});
							var newArr 					= $(existing_thumbnail_ids).not([file_id]).get();
							
							$('input[name=exists_thumbnail_ids]').val(newArr);
							(newArr=="") ? $('.dz-message').show() : $('.dz-message').hide()
						}
					});

				});			
			},
			uploadprogress: function(file, progress, bytesSent) {
                $(":submit").prop('disabled', true); // Display the progress
            },
			success: function( file, response ) {
				
				$(":submit").prop('disabled', false); // Display the progress

				var collection_of_parse = JSON.parse(response);
			
				if ( collection_of_parse.status )
				{
					var value 		= collection_of_parse.collection.images;
					var mockFile 	= { 
						name: value.file_name, 
						size:value.file_size, 
						file_id: value.file_id,
						accepted: true
					};

					var tmp_store =  $('input[name=exists_thumbnail_ids]').val();
					$('input[name=exists_thumbnail_ids]').val( tmp_store+','+value.file_id );

					myDropzone.options.addedfile.call(myDropzone, mockFile );
					myDropzone.options.thumbnail.call(myDropzone, mockFile, "$base_url"+"storage/products/"+value.file_name);
				}
            },
            confirm: function(question, accepted, rejected) {
                console.log( question );
                console.log( accepted );
                console.log( rejected );
            }

		});

		myDropzone.on("complete", function(file) {
		  myDropzone.removeFile(file);
		});

	var to_load_dropzone_exists_images = function()
	{
		var existing_thumbnail_ids = $('input[name=exists_thumbnail_ids]').val().split(',').filter(function(v){ return v!='';});
		if ( existing_thumbnail_ids.length ) {

			$.post( "$base_url"+"backend/products/dropzoneExistsDoUpload", { files_ids: existing_thumbnail_ids }, function(data) {
				
				var collection_of_parse = JSON.parse(data);
				
				if ( collection_of_parse.status )
				{
					$.each(collection_of_parse.collection.images, function(key,value) {
						
						var mockFile = { 
							name: value.file_name, 
							size:value.file_size, 
							file_id: value.file_id,
							accepted: true
						};

						myDropzone.options.addedfile.call(myDropzone, mockFile, '34' );
						myDropzone.options.thumbnail.call(myDropzone, mockFile, "$base_url"+"storage/products/"+value.file_name);
					});
				}
			});
		}
	}

	to_load_dropzone_exists_images();


	var myProductDescDropzone = new Dropzone("div#my-product-desc-dropzone", { 
		url: "$base_url"+"backend/products/dropzoneDoUploadProductDesc",
		addRemoveLinks: true,
		maxFilesize: 1,
		acceptedFiles: ".jpeg,.jpg,.png,.gif",
		init: function() {
			
			thisMyDropzone = this;

			this.on("removedfile", function (file) {

				var file_id 				= file.file_id;
				var file_name 				= file.name;

				$.post( "$base_url"+"backend/products/dropzoneExistsRemoveImageProductDesc", { file_id: file_id, file_name: file_name }, function(data) {
					
					var collection_of_parse = JSON.parse(data);
					if ( collection_of_parse.status )
					{
						var existing_thumbnail_ids 	= $('input[name=exists_thumbnail_product_desc_ids]').val().split(',').filter(function(v){ return v!='';});
						var newArr 					= $(existing_thumbnail_ids).not([file_id]).get();
						
						$('input[name=exists_thumbnail_product_desc_ids]').val(newArr);
						(newArr=="") ? $('.dz-message').show() : $('.dz-message').hide()
					}
				});

			});			
		},
		uploadprogress: function(file, progress, bytesSent) {
            $(":submit").prop('disabled', true); // Display the progress
        },
		success: function( file, response ) {
			
			$(":submit").prop('disabled', false); // Display the progress

			var collection_of_parse = JSON.parse(response);
		
			if ( collection_of_parse.status )
			{
				var value 		= collection_of_parse.collection.images;
				var mockFile 	= { 
					name: value.file_name, 
					size:value.file_size, 
					file_id: value.file_id,
					accepted: true
				};

				var tmp_store =  $('input[name=exists_thumbnail_product_desc_ids]').val();
				$('input[name=exists_thumbnail_product_desc_ids]').val( tmp_store+','+value.file_id );

				myProductDescDropzone.options.addedfile.call(myProductDescDropzone, mockFile );
				myProductDescDropzone.options.thumbnail.call(myProductDescDropzone, mockFile, "$base_url"+"storage/products/desc/"+value.file_name);
			}
        },
        confirm: function(question, accepted, rejected) {
            console.log( question );
            console.log( accepted );
            console.log( rejected );
        }

	});

	myProductDescDropzone.on("complete", function(file) {
	  myProductDescDropzone.removeFile(file);
	});



	var to_load_dropzone_product_desc_exists_images = function()
	{
		var existing_thumbnail_pro_desc_ids = $('input[name=exists_thumbnail_product_desc_ids]').val().split(',').filter(function(v){ return v!='';});
		if ( existing_thumbnail_pro_desc_ids.length ) {

			$.post( "$base_url"+"backend/products/dropzoneExistsDoUploadProductDesc", { files_ids: existing_thumbnail_pro_desc_ids }, function(data) {
				
				var collection_of_parse = JSON.parse(data);
				
				if ( collection_of_parse.status )
				{
					$.each(collection_of_parse.collection.images, function(key,value) {
						
						var mockFile = { 
							name: value.file_name, 
							size:value.file_size, 
							file_id: value.file_id,
							accepted: true
						};

						myProductDescDropzone.options.addedfile.call(myProductDescDropzone, mockFile, '34' );
						myProductDescDropzone.options.thumbnail.call(myProductDescDropzone, mockFile, "$base_url"+"storage/products/desc/"+value.file_name);
					});
				}
			});
		}
	}

	to_load_dropzone_product_desc_exists_images();


$('.is_sale_class').on('ifChanged', function(event) {
    
    if ( event.target.checked )
    {
		$('.is_sale_price_txt').css('display', 'block');
    }
    else
    {
		$('.is_sale_price_txt').css('display', 'none');
    }
});

$('.is_deal_class').on('ifChanged', function(event) {
    
    if ( event.target.checked )
    {
		$('.is_expire_at').css('display', 'block');
    }
    else
    {
		$('.is_expire_at').css('display', 'none');
    }
});

$('#summernote').summernote({
    height: 200,
    onImageUpload: function(files, editor, welEditable) {
        sendFile(files[0], editor, welEditable);
    }
});

$("#category_id").select2({
  placeholder: "Select Categories/Subcategories",
  allowClear: true
});

function sendFile(file, editor, welEditable) {
    data = new FormData();
    data.append("file", file);
    $.ajax({
        data: data,
        type: "POST",
        url: '$base_url' + 'backend/products/uploadFileSummerNote',
        cache: false,
        contentType: false,
        processData: false,
        dataType	: 'json',
        success: function(response) {
        	var link = '$base_url' + 'storage/summernote/'+response.data.file_name;
            editor.insertImage(welEditable, link);
        }
    });
}

JAVASCRIPT;
	}

	public function addNew(){
		
		$this->formAssets();
		
		$categories = $this->triggermodel->all(TBL_CATEGORIES, [
			'select' 	=> 'id, parent_id, title'
		], TRUE);

		$brands = $this->triggermodel->all(TBL_BRANDS, [
			'select' 	=> 'id, title'
		], TRUE);
		
		$this->loadTemplate('products/form', ['categories' => $categories, 'brands' => $brands]);
	}

	public function save(){

		$this->__validation();

		/**
		 * Table of Products
		 */
		$data = 
		[
			'title' 						=> $this->input->post('title'),
			'slug' 							=> friendly_url($this->input->post('title')),
			
			// 'category_id' 					=> $this->input->post('category_id'),
			'brand_id' 						=> $this->input->post('brand_id'),
			'code' 							=> $this->input->post('code_1'),
			'extra' 						=> $this->input->post('extra'),
			
			'short_description' 			=> $this->input->post('short_description'),
			'description' 					=> $this->input->post('description'),
			
			'price' 						=> $this->input->post('price'),
			
			'is_sale' 						=> ($this->input->post('is_sale') ? 1 : 0),
			'sale_price' 					=> $this->input->post('sale_price'),
			
			'available_stock' 				=> $this->input->post('available_stock'),
			
			'whole_sale_price' 				=> $this->input->post('whole_sale_price'),
			'whole_sale_available_stock' 	=> $this->input->post('whole_sale_available_stock'),
			
			'is_deals' 						=> ($this->input->post('is_deals') ? 1 : 0),
			'expired_at' 					=> ( $this->input->post('expired_at') ? date('Y-m-d', strtotime($this->input->post('expired_at'))) : null),
			
			'is_best_seller' 				=> $this->input->post('is_best_seller'),
			'active' 						=> $this->input->post('active'),
			
			'created_at' 					=> date('Y-m-d H:i:s'),
			'updated_at' 					=> date('Y-m-d H:i:s')
		];

		$this->triggermodel->insertOrFail(TBL_PRODUCTS, $data);
		$last_insert_id  = $this->triggermodel->last_insert_id();

		if ( $this->input->post('category_id') )
		{
			$this->insertOfPivotCategories( $this->input->post('category_id'), $last_insert_id );	
		}

		/**
		 * Table of Images
		 * @var array
		 */
		/*$images = array_map(function($v) use ($last_insert_id) {

			return [
				'product_id' => $last_insert_id,
				'image' 	 => $v['file_name'],
			];

		}, $this->_uploaded );

		$this->triggermodel->insertBatch(TBL_IMAGES, $images);*/
		$this->triggermodel->updateOrFail(TBL_IMAGES, ['product_id' => $last_insert_id], ['product_id' => 0]);
		$this->triggermodel->updateOrFail(TBL_MEDIA, ['product_id' => $last_insert_id], ['product_id' => 0, 'type' => 'pro-desc']);

		$this->session->set_flashdata('alert-message', [
			'alert-type' => 'success',
			'message' 	 => 'Your record has been successfully added.',
			'link_name' => 'View Products Manage',
			'link_url' => link_to_backend('products/manage'),
		]);

		redirect( link_to_backend('products/addNew') );
	}

	public function edit( $id ){
		
		$this->formAssets();

		$categories = $this->triggermodel->all(TBL_CATEGORIES, [
			'select' 	=> 'id, parent_id, title'
		], TRUE);

		$brands = $this->triggermodel->all(TBL_BRANDS, [
			'select' 	=> 'id, title'
		], TRUE);

		$pivot_categories = $this->triggermodel->all(TBL_CATEGORY_PRODUCT, [
			'where' 	=> [
				'product_id' => $id
			],
			'select' 	=> 'category_id'
		], TRUE, FALSE);

		$pivot_categories = array_map(function($v){
			return $v['category_id'];
		}, $pivot_categories);

		// dump($pivot_categories);

		$edit_data = $this->productmodel->getSingleProduct($id);

		$this->loadTemplate('products/form', [
			'edit_data' 		=> $edit_data, 
			'categories' 		=> $categories, 
			'brands' 			=> $brands,
			'edit_pivot_categories'	=> $pivot_categories
		]);
	}

	public function update($id){

		// rd($this->input->post('is_sale'));
		$this->__validation($id);

		$is_sale = 0;
		if ( isset($_POST['is_sale']) )
		{
			$is_sale = ($_POST['is_sale'] ? 0 : 1);	
		}

		$is_deals = 0;
		if ( isset($_POST['is_deals']) )
		{
			$is_deals = ($_POST['is_deals'] ? 0 : 1);	
		}

		$data = 
		[
			'title' 						=> $this->input->post('title'),
			'slug' 							=> friendly_url($this->input->post('title')),
			
			// 'category_id' 					=> $this->input->post('category_id'),
			'brand_id' 						=> $this->input->post('brand_id'),
			'code' 							=> $this->input->post('code_1'),
			'extra' 						=> $this->input->post('extra'),
			
			'short_description' 			=> $this->input->post('short_description'),
			'description' 					=> $this->input->post('description'),
			
			'price' 						=> $this->input->post('price'),

			'is_sale' 						=> $is_sale,
			'sale_price' 					=> $this->input->post('sale_price'),
			
			'available_stock' 				=> $this->input->post('available_stock'),
			
			'whole_sale_price' 				=> $this->input->post('whole_sale_price'),
			'whole_sale_available_stock' 	=> $this->input->post('whole_sale_available_stock'),
			
			'is_deals' 						=> $is_deals,
			'expired_at' 					=> ($this->input->post('expired_at') ? date('Y-m-d', strtotime($this->input->post('expired_at'))) : null),
			
			'is_best_seller' 				=> $this->input->post('is_best_seller'),
			'active' 						=> $this->input->post('active'),
			
			'updated_at' 					=> date('Y-m-d H:i:s')
		];

		$this->triggermodel->updateOrFail(TBL_PRODUCTS, $data, ['id' => $id]);

		if ( $this->input->post('category_id') )
		{
			$this->removeOfCategoriesByProductId($id);
			$this->insertOfPivotCategories( $this->input->post('category_id'), $id );	
		}

		$this->triggermodel->updateOrFail(TBL_IMAGES, ['product_id' => $id], ['product_id' => 0]);
		$this->triggermodel->updateOrFail(TBL_MEDIA, ['product_id' => $id], ['product_id' => 0, 'type' => 'pro-desc']);
		
		/*if ( count( $this->_uploaded ) )
		{
			$this->triggermodel->delete(TBL_IMAGES, null, ['product_id' => $id]);
			remove_image_from_dir($this->input->post('image_source'), 'products');

			*
			 * Table of Images
			 * @var array
			 
			$images = array_map(function($v) use ($id) {

				return [
					'product_id' => $id,
					'image' 	 => $v['file_name'],
				];

			}, $this->_uploaded );

			$this->triggermodel->insertBatch(TBL_IMAGES, $images);
		}*/
		


		$this->session->set_flashdata('alert-message', [
			'alert-type' => 'success',
			'message' 	 => 'Your record has been successfully updated.'
		]);

		redirect( link_to_backend('products/manage') );
	}

	public function delete( $id )
	{

		$this->triggermodel->use_soft = true;
		$this->triggermodel->delete(TBL_PRODUCTS, $id);

		$this->session->set_flashdata('alert-message', [
			'alert-type' => 'success',
			'message' 	 => 'Your record has been successfully deleted.'
		]);
		redirect( link_to_backend('products/manage') );
	}

	public function manage()
	{
		$this->header_assets = [
			'assets/libs/jquery-datatables/css/dataTables.bootstrap.css',
			'assets/libs/jquery-datatables/extensions/TableTools/css/dataTables.tableTools.css'
		];

		$this->footer_assets = [
			'assets/libs/jquery-datatables/js/jquery.dataTables.min.js',
			'assets/libs/jquery-datatables/js/dataTables.bootstrap.js',
			'assets/libs/jquery-datatables/extensions/TableTools/js/dataTables.tableTools.min.js',
			'assets/js/pages/datatables.js'
		];

		$products = $this->productmodel->getManageProducts();

		if ( count($products) ){
			$products = array_map( function($v){
				
				$row = $v;
				
				if ( $row['expired_at'] ) {
					$row['expired_at'] = formatDate(ADMIN_DATE_FORMAT, $row['expired_at'] );
				};

				if ( $row['created_at'] ) {
					$row['created_at'] = formatDate(ADMIN_DATE_FORMAT, $row['created_at'] );
				};

				if ( $row['updated_at'] ) {
					$row['updated_at'] = formatDate(ADMIN_DATE_FORMAT, $row['updated_at'] );
				};

				return $row;

			}, $products );
		}

		$this->loadTemplate('products/index', ['data'=>$products]);
	}

	private function __validation( $id = NULL ){

		$this->load->library('form_validation');

		$config = [
	        [
	        	'field' => 'title',
	            'label' => 'title',
	            'rules' => 'trim|required|max_length[50]|is_unique['.TBL_PRODUCTS.'.title]',
	            'errors' => [
	            	'is_unique' => 'This %s already exists.'
	            ],
	        ],
	        [
	           	'field' => 'category_id',
	            'label' => 'category',
	            // 'rules' => 'required',
	            'errors' => [
	            	'required' 	=> 'Please select %s field is required.',
	            ]
	        ],
	        [
	           	'field' => 'code_1',
	            'label' => 'code',
	            'rules' => 'required',
	        ],
	        [
	           	'field' => 'short_description',
	            'label' => 'short description',
	            'rules' => 'required',
	        ],
	        [
	           	'field' => 'description',
	            'label' => 'description',
	            'rules' => 'required',
	        ],
	        [
	           	'field' => 'price',
	            'label' => 'price',
	            'rules' => 'required',
	        ],
	        [
	           	'field' => 'exists_thumbnail_ids',
	            'label' => 'upload product image',
	            'rules' => 'required',
	        ],
	        /*[
	           	'field' => 'images[]',
	            'label' => 'upload image',
	            'rules' => 'callback_fileupload_check',
	        ]*/
		];

		// rd($config);
		/* title or image validation */
		if ( $id ) { 

			$config[0] = 
			[
				'field'   => 'title', 
				'label'   => 'title', 
				// 'rules'   => 'trim|required|max_length[50]|unique_other_than['.TBL_CATEGORIES.'.title, id='.$id.']',
				'rules'   => 'trim|required|max_length[50]',
				'errors' => 
				[
	            	'required' 	=> 'You must provide a %s.',
	            	'is_unique' => 'This %s already exists.'
	            ]
			];
		}
		
		$this->form_validation->set_rules($config);

		if ( $this->form_validation->run() == FALSE ) {

			$this->session->set_flashdata('data', $this->input->post() );
			$this->session->set_flashdata('alert-message', [
				'alert-type' => 'danger',
				'message' 	 => validation_errors()
			]);

			redirect( link_to_backend('products/' . ($id ? ('edit/'.$id) : 'addNew' ) ) );
		}
	}

	public function insertOfPivotCategories( $categories, $product_id )
	{
		$data = [];

		foreach ($categories as $id) {
			$data[] = [
				'category_id' => $id,
				'product_id' => $product_id,
			];
		}
		
		$this->triggermodel->insertBatch(TBL_CATEGORY_PRODUCT, $data);
	}

	public function removeOfCategoriesByProductId( $product_id )
	{
		return $this->triggermodel->delete(TBL_CATEGORY_PRODUCT, null, [
			'product_id' => $product_id
		]);
	}

	// now the callback validation that deals with the upload of files
	public function fileupload_check() {

		// we retrieve the number of files that were uploaded
		$number_of_files = sizeof($_FILES['images']['tmp_name']);

		// considering that do_upload() accepts single files, we will have to do a small hack so that we can upload multiple files. For this we will have to keep the data of uploaded files in a variable, and redo the $_FILE.
		$files = $_FILES['images'];

		// first make sure that there is no error in uploading the files
		for( $i=0;$i<$number_of_files;$i++)
		{
			if($_FILES['images']['error'][$i] != 0) 
			{
				// save the error message and return false, the validation of uploaded files failed
				$this->form_validation->set_message('fileupload_check', 'Couldn\'t upload the file(s)');
				return FALSE;
			}
		}

		// we first load the upload library
		$this->load->library('upload');

		// next we pass the upload path for the images
		$config['upload_path'] = FCPATH . 'storage/products';

		// also, we make sure we allow only certain type of images
		$config['allowed_types'] = 'gif|jpg|png';

		// now, taking into account that there can be more than one file, for each file we will have to do the upload
		for ($i = 0; $i < $number_of_files; $i++) 
		{
			$_FILES['image']['name'] 		= $files['name'][$i];
			$_FILES['image']['type'] 		= $files['type'][$i];
			$_FILES['image']['tmp_name'] 	= $files['tmp_name'][$i];
			$_FILES['image']['error'] 		= $files['error'][$i];
			$_FILES['image']['size'] 		= $files['size'][$i];

			//now we initialize the upload library
			$this->upload->initialize($config);
			if ($this->upload->do_upload('image'))
			{
				$this->_uploaded[$i] = $this->upload->data();
			}
			else
			{
				$this->form_validation->set_message('fileupload_check', $this->upload->display_errors());
				return FALSE;
			}
		}
		
		return TRUE;
	}

	public function uploadFileSummerNote()
	{
		$config['upload_path'] = './storage/summernote/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = 1024 * 8;
        $config['encrypt_name'] = TRUE;
 
        $this->load->library('upload', $config);
 
        if (!$this->upload->do_upload('file'))
        {
            $status = 'error';
            $data = $this->upload->display_errors('', '');
        }
        else
        {
        	$status = true;
            $data = $this->upload->data();   
        }

    	echo json_encode(array('status' => $status, 'data' => $data));
	}

	/**
	 * Product Upload Description
	 * 
	 */
	public function dropzoneDoUploadProductDesc()
	{
		$config['upload_path'] 		= $this->location . 'desc/';
	    $config['allowed_types'] 	= 'gif|jpg|png|jpeg';
	    $config['encrypt_name'] 	= TRUE;
	    
	    $this->load->library('upload', $config);
	    
	    if (!$this->upload->do_upload('file'))
        {
            $status 	= false;
            $message 	= $this->upload->display_errors('', '');
            r($msg);
        }
        else
        {
        	$status = true;
            $data = $this->upload->data();

            $this->triggermodel->insertOrFail(TBL_MEDIA, [
				'product_id'  => 0,
				'source_name' => $data['file_name'],
				'image' 	  => $data['file_name'],
				'is_default'  => 0,
				'type' 		  => 'pro-desc',
				'sortby' 	  => 0,
			]);

            $collection = [
				'file_id' 	=> $this->triggermodel->last_insert_id(),
				'file_name' => $data['file_name'],
				'file_size' => filesize( './storage/products/desc/'. $data['file_name']),
			];

			
			$message = 'Successfully Upload';
        }
		
		echo json_encode([
			'status' 	=> $status,
			'message' 	=> $message, 
			'collection' => [
				'images' => count($collection) ? $collection : null
			]
		]);
	}

	public function dropzoneExistsDoUploadProductDesc()
	{
		$thumnail = $this->db->where_in('id', $this->input->post('files_ids'))->select('id, image')->get(TBL_MEDIA);
		
		$medias = array_map(function($v){
			return [
				'file_id' 	=> $v['id'],
				'file_name' => $v['image'],
				'file_size' => filesize( './storage/products/desc/'. $v['image']),
			];
		}, $thumnail->result_array() );
		
		echo json_encode([
			'status' => TRUE,
			'collection' => [
				'images' => $medias
			]
		]);
	}

	public function dropzoneExistsRemoveImageProductDesc()
	{	
		$file_id 	= $this->input->post('file_id');
		$file_name 	= $this->input->post('file_name');

		@unlink( $this->location . 'desc/' . $file_name );
		$this->triggermodel->delete(TBL_MEDIA, $file_id);

		echo json_encode([
			'status' 	=> TRUE,
			'message' 	=> 'Delete Image',
		]);
	}
}
