<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends Admin_Controller {

	public function __construct(){
		parent::__construct();
	}
	
	private function formAssets(){
		$this->header_assets = [
			'assets/libs/bootstrap-validator/css/bootstrapValidator.min.css',
		];

		$this->footer_assets = [
			'assets/libs/bootstrap-validator/js/bootstrapValidator.min.js',
			'assets/js/pages/form-validation.js',
		];
	}

	public function addNew(){
		
		$this->formAssets();
		
		$this->loadTemplate('users/form');
	}

	public function save(){

		$this->__validation();

		$data = [
			'first_name' 	=> $this->input->post('first_name'),
			'last_name' 	=> $this->input->post('last_name'),
			'email' 		=> $this->input->post('email'),
			'password' 		=> salt_password($this->input->post('password')),
			'is_admin' 		=> 0,
			'created_at' 	=> date('Y-m-d H:i:s'),
			'updated_at' 	=> date('Y-m-d H:i:s')
		];

		$this->triggermodel->insertOrFail(TBL_USERS, $data);
		
		$this->session->set_flashdata('alert-message', [
			'alert-type' => 'success',
			'message' 	 => 'Your record has been successfully added.',
			'link_name' => 'View Users Manage',
			'link_url' => link_to_backend('users/manage'),
		]);

		redirect( link_to_backend('users/addNew') );
	}

	public function edit( $id ){
		
		$this->formAssets();

		$edit_data = $this->triggermodel->firstOrFail(TBL_USERS, ['where' => ['id' => $id ]]);

		$this->loadTemplate('users/form', ['edit_data' => $edit_data ]);
	}

	public function update($id){

		$this->__validation($id);

		$data = [
			'first_name' 	=> $this->input->post('first_name'),
			'last_name' 	=> $this->input->post('last_name'),
			'email' 		=> $this->input->post('email'),
			'updated_at' 	=> date('Y-m-d H:i:s')
		];

		if ( $this->input->post('password') )
		{
			$data['password'] = salt_password($this->input->post('password'));
		}

		$this->triggermodel->updateOrFail(TBL_USERS, $data, ['id' => $id]);
		
		$this->session->set_flashdata('alert-message', [
			'alert-type' => 'success',
			'message' 	 => 'Your record has been successfully updated.'
		]);

		redirect( link_to_backend('users/manage') );
	}

	public function delete( $id )
	{

		$this->triggermodel->use_soft = true;
		$this->triggermodel->delete(TBL_USERS, $id);

		$this->session->set_flashdata('alert-message', [
			'alert-type' => 'success',
			'message' 	 => 'Your record has been successfully deleted.'
		]);
		redirect( link_to_backend('users/manage') );
	}

	public function manage()
	{
		$this->header_assets = [
			'assets/libs/jquery-datatables/css/dataTables.bootstrap.css',
			'assets/libs/jquery-datatables/extensions/TableTools/css/dataTables.tableTools.css'
		];

		$this->footer_assets = [
			'assets/libs/jquery-datatables/js/jquery.dataTables.min.js',
			'assets/libs/jquery-datatables/js/dataTables.bootstrap.js',
			'assets/libs/jquery-datatables/extensions/TableTools/js/dataTables.tableTools.min.js',
			'assets/js/pages/datatables.js'
		];

		$customers = $this->triggermodel->all(TBL_USERS, [], TRUE);

		if ( count($customers) ){
			$customers = array_map( function($v){
				
				$row = $v;

				
				$row['active'] 	= $row['active'] ? 'Active' : 'Deactive';

				if ( $row['created_at'] ) {
					$row['created_at'] = formatDate(ADMIN_DATE_FORMAT, $row['created_at'] );
				};

				return $row;

			}, $customers );
		}
		// rd($customers);

		$this->loadTemplate('users/index', [
			'data' => $customers
		]);
	}

	private function __validation( $id = NULL ){

		$this->load->library('form_validation');

		$this->form_validation->set_rules('first_name', 'first name', 'trim|required|max_length[20]');
   		$this->form_validation->set_rules('last_name', 'last name', 'trim|required|max_length[20]');
   		
   		/* categories email validation */
		if ( $id ) { 
			$this->form_validation->set_rules('email', 'email', 'trim|required|valid_email|max_length[50]|unique_other_than[dc_users.email, id='.$id.']');

			$this->form_validation->set_rules('password', 'password', 'max_length[20]');
   			$this->form_validation->set_rules('confirm', 'password', 'matches[password]');

		} else  {
			
			$this->form_validation->set_rules('email', 'email', 'trim|required|valid_email|is_unique[dc_users.email]|max_length[50]');
			
			$this->form_validation->set_rules('password', 'password', 'trim|required|max_length[20]');
   			$this->form_validation->set_rules('confirm', 'password', 'trim|required|matches[password]');
		}

		if ( $this->form_validation->run() == FALSE ) {

			$this->session->set_flashdata('data', $this->input->post() );
			$this->session->set_flashdata('alert-message', [
				'alert-type' => 'danger',
				'message' 	 => validation_errors()
			]);

			redirect( link_to_backend('users/' . ($id ? ('edit/'.$id)  : 'addNew' ) ) );
		}
	}
}
