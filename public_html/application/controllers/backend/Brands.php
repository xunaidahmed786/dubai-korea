<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Brands extends Admin_Controller {

	public function __construct(){
		parent::__construct();
	}
	
	private function formAssets(){
		$this->header_assets = [
			'assets/libs/bootstrap-validator/css/bootstrapValidator.min.css',
		];

		$this->footer_assets = [
			'assets/libs/bootstrap-validator/js/bootstrapValidator.min.js',
			'assets/js/pages/form-validation.js',
		];
	}

	public function addNew(){
		
		$this->formAssets();
		
		$this->loadTemplate('brands/form');
	}

	public function save(){

		$this->__validation();

		$data = [
			'title' 		=> $this->input->post('title'),
			'slug' 			=> friendly_url($this->input->post('title')),
			'active' 		=> $this->input->post('active'),
			'created_at' 	=> date('Y-m-d H:i:s'),
			'updated_at' 	=> date('Y-m-d H:i:s')
		];

		$this->triggermodel->insertOrFail(TBL_BRANDS, $data);
		
		$this->session->set_flashdata('alert-message', [
			'alert-type' => 'success',
			'message' 	 => 'Your record has been successfully added.',
			'link_name' => 'View Brands Manage',
			'link_url' => link_to_backend('brands/manage'),
		]);

		redirect( link_to_backend('brands/addNew') );
	}

	public function edit( $id ){
		
		$this->formAssets();

		$edit_data = $this->triggermodel->firstOrFail(TBL_BRANDS, ['where' => ['id' => $id ]]);

		$this->loadTemplate('brands/form', ['edit_data' => $edit_data ]);
	}

	public function update($id){

		$this->__validation($id);

		$data = [
			'title' 		=> $this->input->post('title'),
			'slug' 			=> friendly_url($this->input->post('title')),
			'active' 		=> $this->input->post('active'),
			'updated_at' 	=> date('Y-m-d H:i:s')
		];

		$this->triggermodel->updateOrFail(TBL_BRANDS, $data, ['id' => $id]);
		
		$this->session->set_flashdata('alert-message', [
			'alert-type' => 'success',
			'message' 	 => 'Your record has been successfully updated.'
		]);

		redirect( link_to_backend('brands/manage') );
	}

	public function delete( $id )
	{

		$this->triggermodel->use_soft = true;
		$this->triggermodel->delete(TBL_BRANDS, $id);

		$this->session->set_flashdata('alert-message', [
			'alert-type' => 'success',
			'message' 	 => 'Your record has been successfully deleted.'
		]);
		redirect( link_to_backend('brands/manage') );
	}

	public function manage()
	{
		$this->header_assets = [
			'assets/libs/jquery-datatables/css/dataTables.bootstrap.css',
			'assets/libs/jquery-datatables/extensions/TableTools/css/dataTables.tableTools.css'
		];

		$this->footer_assets = [
			'assets/libs/jquery-datatables/js/jquery.dataTables.min.js',
			'assets/libs/jquery-datatables/js/dataTables.bootstrap.js',
			'assets/libs/jquery-datatables/extensions/TableTools/js/dataTables.tableTools.min.js',
			'assets/js/pages/datatables.js'
		];

		$customers = $this->triggermodel->all(TBL_BRANDS, [], TRUE);

		if ( count($customers) ){
			$customers = array_map( function($v){
				
				$row = $v;
				
				$row['active'] 	= $row['active'] ? 'Active' : 'Inactive';

				if ( $row['created_at'] ) {
					$row['created_at'] = formatDate(ADMIN_DATE_FORMAT, $row['created_at'] );
				};

				return $row;

			}, $customers );
		}
		// rd($customers);

		$this->loadTemplate('brands/index', [
			'data' => $customers
		]);
	}

	private function __validation( $id = NULL ){

		$this->load->library('form_validation');

		$config = [
	        [
	        	'field' => 'title',
	            'label' => 'title',
	            'rules' => 'trim|required|max_length[50]|is_unique['.TBL_BRANDS.'.title]',
	            'errors' => [
	            	'required' 	=> 'You must provide a %s.',
	            	'is_unique' => 'This %s already exists.'
	            ],
	        ]
		];

		/* brands validation */
		if ( $id ) { 
			$config[0] = [
				'field'   => 'title', 
				'label'   => 'title', 
				'rules'   => 'trim|required|max_length[50]|unique_other_than['.TBL_BRANDS.'.title, id='.$id.']',
				'rules'   => 'trim|required|max_length[50]',
				'errors' => [
	            	'required' 	=> 'You must provide a %s.',
	            	'is_unique' => 'This %s already exists.'
	            ]
			];
		}

		$this->form_validation->set_rules($config);

		if ( $this->form_validation->run() == FALSE ) {

			$this->session->set_flashdata('data', $this->input->post() );
			$this->session->set_flashdata('alert-message', [
				'alert-type' => 'danger',
				'message' 	 => validation_errors()
			]);

			redirect( link_to_backend('brands/' . ($id ? ('edit/'.$id)  : 'addNew' ) ) );
		}
	}
}
