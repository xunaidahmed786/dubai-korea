<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Support extends Admin_Controller {

	public function __construct(){
		parent::__construct();

		$this->load->model('ordermodel');
	}
	
	private function formAssets(){
		$this->header_assets = [
			'assets/libs/bootstrap-validator/css/bootstrapValidator.min.css',
			'assets/libs/summernote/summernote.css',

			'assets/libs/dropzone/css/dropzone.css',
		];

		$this->footer_assets = [
			'assets/libs/bootstrap-validator/js/bootstrapValidator.min.js',
			'assets/js/pages/form-validation.js',

		];
	}

	public function wholeseller()
	{
		$this->formAssets();

		$queries = $this->triggermodel->all(TBL_WHOLE_SELLER, ['orderBy' => 'DESC']);

		if ( count($queries) ){

			foreach ($queries as &$row ) {
				
				if ( $row->created_at ) {
					$row->created_at = formatDate(ADMIN_DATE_FORMAT, $row->created_at );
				};

				if ( $row->updated_at ) {
					$row->updated_at = formatDate(ADMIN_DATE_FORMAT, $row->updated_at );
				};
			}
		}

		$this->loadTemplate('supports/whole-seller-index', ['data'=>$queries]);
	}

	public function wholesellerdetails($id)
	{
		$data = $this->triggermodel->firstOrFail(TBL_WHOLE_SELLER, ['where' => ['id' => $id ]]);

		if ( !$data )
		{
			show_404();
		}

		$this->loadTemplate('supports/whole-seller-detail', ['data'=> $data]);
	}

	public function wholesellerdelete( $id )
	{
		$this->triggermodel->use_soft = true;
		$this->triggermodel->delete(TBL_WHOLE_SELLER, $id);

		$this->session->set_flashdata('alert-message', [
			'alert-type' => 'success',
			'message' 	 => 'Whole seller has been successfully deleted.'
		]);
		redirect( link_to_backend('support/wholeseller') );
	}

	public function contactus()
	{

		$this->formAssets();

		$queries = $this->triggermodel->all(TBL_CONTACT_QUERY, ['orderBy' => 'DESC']);

		if ( count($queries) ){

			foreach ($queries as &$row ) {
				
				if ( $row->created_at ) {
					$row->created_at = formatDate(ADMIN_DATE_FORMAT, $row->created_at );
				};

				if ( $row->updated_at ) {
					$row->updated_at = formatDate(ADMIN_DATE_FORMAT, $row->updated_at );
				};
			}
		}

		$this->loadTemplate('supports/contact-us-index', ['data'=>$queries]);
	}
}
