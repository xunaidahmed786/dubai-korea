<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends Admin_Controller {

	public function __construct(){
		parent::__construct();
	}
	
	private function formAssets(){
		$this->header_assets = [
			'assets/libs/bootstrap-validator/css/bootstrapValidator.min.css',
		];

		$this->footer_assets = [
			'assets/libs/bootstrap-validator/js/bootstrapValidator.min.js',
			'assets/js/pages/form-validation.js',
		];
	}

	public function addNew(){
		
		$this->formAssets();
		
		$categories = $this->triggermodel->all(TBL_CATEGORIES, [
			'select' 	=> 'id, parent_id, title'
		], TRUE);
		
		$this->loadTemplate('categories/categories_form', ['categories' => $categories]);
	}

	public function save(){

		$this->__validation();

		$data = [
			'title' 		=> $this->input->post('title'),
			'slug' 			=> friendly_url($this->input->post('title')),
			'parent_id' 	=> 0,
			'active' 		=> 1,
			'created_at' 	=> date('Y-m-d H:i:s'),
			'updated_at' 	=> date('Y-m-d H:i:s')
		];

		if ( $this->input->post('parent_id') ) {
			$data['parent_id'] = $this->input->post('parent_id');
		}

		$this->triggermodel->insertOrFail(TBL_CATEGORIES, $data);
		
		$this->session->set_flashdata('alert-message', [
			'alert-type' => 'success',
			'message' 	 => 'Your record has been successfully added.',
			'link_name' => 'View Categories Manage',
			'link_url' => link_to_backend('category/manage'),
		]);

		redirect( link_to_backend('category/addNew') );
	}

	public function edit( $id ){
		
		$this->formAssets();

		$categories = $this->triggermodel->all(TBL_CATEGORIES, [
			'select' 	=> 'id, parent_id, title'
		], TRUE);

		$edit_data = $this->triggermodel->firstOrFail(TBL_CATEGORIES, ['where' => ['id' => $id ]]);

		$this->loadTemplate('categories/categories_form', ['edit_data' => $edit_data, 'categories'=>$categories]);
	}

	public function update($id){

		$this->__validation($id);

		$data = [
			'title' 		=> $this->input->post('title'),
			'slug' 			=> friendly_url($this->input->post('title')),
			'parent_id' 	=> 0,
			'updated_at' 	=> date('Y-m-d H:i:s')
		];

		if ( $this->input->post('parent_id') ) {
			$data['parent_id'] = $this->input->post('parent_id');
		}

		$this->triggermodel->updateOrFail(TBL_CATEGORIES, $data, ['id' => $id]);
		
		$this->session->set_flashdata('alert-message', [
			'alert-type' => 'success',
			'message' 	 => 'Your record has been successfully updated.'
		]);

		redirect( link_to_backend('category/manage') );
	}

	public function delete( $id )
	{

		$this->triggermodel->use_soft = true;
		$this->triggermodel->delete(TBL_CATEGORIES, $id);

		$this->session->set_flashdata('alert-message', [
			'alert-type' => 'success',
			'message' 	 => 'Your record has been successfully deleted.'
		]);
		redirect( link_to_backend('category/manage') );
	}

	public function manage()
	{
		$this->header_assets = [
			'assets/libs/jquery-datatables/css/dataTables.bootstrap.css',
			'assets/libs/jquery-datatables/extensions/TableTools/css/dataTables.tableTools.css'
		];

		$this->footer_assets = [
			'assets/libs/jquery-datatables/js/jquery.dataTables.min.js',
			'assets/libs/jquery-datatables/js/dataTables.bootstrap.js',
			'assets/libs/jquery-datatables/extensions/TableTools/js/dataTables.tableTools.min.js',
			'assets/js/pages/datatables.js'
		];

		$categories = $this->triggermodel->all(TBL_CATEGORIES, [], TRUE);

		if ( count($categories) ){
			$categories = array_map( function($v){
				
				$row = $v;

				if ( $row['created_at'] ) {
					$row['created_at'] = formatDate(ADMIN_DATE_FORMAT, $row['created_at'] );
				};

				if ( $row['updated_at'] ) {
					$row['updated_at'] = formatDate(ADMIN_DATE_FORMAT, $row['updated_at'] );
				};

				return $row;

			}, $categories );
		}

		$this->loadTemplate('categories/index', [
			'data' => $categories
		]);
	}

	private function __validation( $id = NULL ){

		$this->load->library('form_validation');

		$config = [
	        [
	        	'field' => 'title',
	            'label' => 'title',
	            'rules' => 'trim|required|max_length[50]|is_unique['.TBL_CATEGORIES.'.title]',
	            'errors' => [
	            	'required' 	=> 'You must provide a %s.',
	            	'is_unique' => 'This %s already exists.'
	            ],
	        ],
	        [
	           	'field' => 'parent_id',
	            'label' => 'Parent',
	            'rules' => 'required',
	            'errors' => ['required' => 'You must provide a %s.'],
	        ]
		];

		/* categories email validation */
		if ( $id ) { 
			$config[0] = [
				'field'   => 'title', 
				'label'   => 'title', 
				// 'rules'   => 'trim|required|max_length[50]|unique_other_than['.TBL_CATEGORIES.'.title, id='.$id.']',
				'rules'   => 'trim|required|max_length[50]',
				'errors' => [
	            	'required' 	=> 'You must provide a %s.',
	            	'is_unique' => 'This %s already exists.'
	            ]
			];
		}

		// rd($config);

		$this->form_validation->set_rules($config);

		if ( $this->form_validation->run() == FALSE ) {

			$this->session->set_flashdata('data', $this->input->post() );
			$this->session->set_flashdata('alert-message', [
				'alert-type' => 'danger',
				'message' 	 => validation_errors()
			]);

			redirect( link_to_backend('category/' . ($id ? ('edit/'.$id)  : 'addNew' ) ) );
		}
	}
}
