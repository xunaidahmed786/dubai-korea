<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Authorized extends UnAuthorized_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct(){
		
		parent::__construct();

		$this->load->model('authorizationmodel');

		
	}
	
	public function login(){
		
		$this->checkLogin();

		$this->loadLoginTemplate('employee/login');
	}

	public function auth(){

		$this->checkLogin();

		$this->load->library('form_validation');

		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
   		$this->form_validation->set_rules('password', 'Password', 'trim|required');

   		if( $this->form_validation->run() == FALSE ){

   			$this->session->set_flashdata('data', $this->input->post() );
			$this->session->set_flashdata('alert-message', validation_errors() );
   			redirect( link_to_backend('authorized/login') );
   			return;
   		}

		$authorize = [
			'email' 	=> $this->input->post('email'),
			'password' 	=> $this->input->post('password'),
			// 'is_admin' 	=> 1,
			// 'active' 	=> 1,
		];

		if ( $this->authorizationmodel->isAuthorizationLogin($authorize) ) {
		
			redirect( link_to_backend('dashboard') );
		
		} else {

			$this->session->set_flashdata('data', $this->input->post() );
			$this->session->set_flashdata('alert-message', 'Error: Invalid email or password. Please try again' );
   			redirect( link_to_backend('authorized/login') );
   			return;
		}
	}

	public function getLogout(){

		$this->session->unset_userdata('auth', []);
		$this->session->unset_userdata('logged_in', NULL );

		redirect( link_to_backend('authorized/login') );
	}
}
