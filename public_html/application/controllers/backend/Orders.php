<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends Admin_Controller {

	public function __construct(){
		parent::__construct();

		$this->load->model('ordermodel');
	}
	
	private function formAssets(){
		$this->header_assets = [
			'assets/libs/bootstrap-validator/css/bootstrapValidator.min.css',
			'assets/libs/summernote/summernote.css',

			'assets/libs/dropzone/css/dropzone.css',
		];

		$this->footer_assets = [
			'assets/libs/bootstrap-validator/js/bootstrapValidator.min.js',
			'assets/js/pages/form-validation.js',

			'assets/libs/bootstrap-inputmask/inputmask.js',
			'assets/libs/summernote/summernote.js',
			'assets/js/pages/forms.js',

			'assets/libs/dropzone/dropzone.min.js',
		];
	}

	public function statusnow($id){

		$this->triggermodel->updateOrFail(TBL_ORDERS, [
			'order_status' => $this->input->post('order_status'),
			'notes' => $this->input->post('notes'),
			'shipped_at' => date('Y-m-d H:i:s')
		], ['id' => $id]);
		
		$this->session->set_flashdata('alert-message', [
			'alert-type' => 'success',
			'message' 	 => 'Successfully order changed status.'
		]);

		redirect( link_to_backend('orders/confirmed') );
	}

	public function status( $id ){
		
		$this->formAssets();
		
		$single_order = $this->triggermodel->firstOrFail(TBL_ORDERS, ['where'=> ['id' => $id] ]);
		if ( empty($single_order) )
		{
			redirect( link_to_backend('orders/confirmed') );
		}

		$this->loadTemplate('orders/change-status-form', ['order' => $single_order]);
	}

	public function delete( $id )
	{
		$this->triggermodel->use_soft = true;
		$this->triggermodel->delete(TBL_ORDERS, $id);

		$this->session->set_flashdata('alert-message', [
			'alert-type' => 'success',
			'message' 	 => 'Order has been successfully deleted.'
		]);
		redirect( link_to_backend('orders/confirmed') );
	}

	public function confirmed()
	{

		$this->header_assets = [
			'assets/libs/jquery-datatables/css/dataTables.bootstrap.css',
			'assets/libs/jquery-datatables/extensions/TableTools/css/dataTables.tableTools.css'
		];

		$this->footer_assets = [
			'assets/libs/jquery-datatables/js/jquery.dataTables.min.js',
			'assets/libs/jquery-datatables/js/dataTables.bootstrap.js',
			'assets/libs/jquery-datatables/extensions/TableTools/js/dataTables.tableTools.min.js',
			'assets/js/pages/datatables.js'
		];

		$orders = $this->ordermodel->getManageOrders(['pending', 'in-transition', 'processing', 'confirmed']);

		if ( count($orders) ){

			$orders = array_map( function($v){
				
				$row = $v;
				
				if ( $row['created_at'] ) {
					$row['created_at'] = formatDate(ADMIN_DATE_FORMAT, $row['created_at'] );
				};

				if ( $row['updated_at'] ) {
					$row['updated_at'] = formatDate(ADMIN_DATE_FORMAT, $row['updated_at'] );
				};

				$row['shipped_at'] = ( ($row['shipped_at']) ? formatDate(ADMIN_DATE_FORMAT, $row['shipped_at'] ) : "<small>(Not yet)</small>" );

				return $row;

			}, $orders );
		}

		// rd($orders);

		$this->loadTemplate('orders/confirmed-orders-index', ['data'=>$orders]);
	}

	public function delivered()
	{

		$this->header_assets = [
			'assets/libs/jquery-datatables/css/dataTables.bootstrap.css',
			'assets/libs/jquery-datatables/extensions/TableTools/css/dataTables.tableTools.css'
		];

		$this->footer_assets = [
			'assets/libs/jquery-datatables/js/jquery.dataTables.min.js',
			'assets/libs/jquery-datatables/js/dataTables.bootstrap.js',
			'assets/libs/jquery-datatables/extensions/TableTools/js/dataTables.tableTools.min.js',
			'assets/js/pages/datatables.js'
		];

		$orders = $this->ordermodel->getManageOrders('delivered');

		if ( count($orders) ){

			$orders = array_map( function($v){
				
				$row = $v;
				
				if ( $row['created_at'] ) {
					$row['created_at'] = formatDate(ADMIN_DATE_FORMAT, $row['created_at'] );
				};

				if ( $row['updated_at'] ) {
					$row['updated_at'] = formatDate(ADMIN_DATE_FORMAT, $row['updated_at'] );
				};

				$row['shipped_at'] = ( ($row['shipped_at']) ? formatDate(ADMIN_DATE_FORMAT, $row['shipped_at'] ) : "<small>(Not yet)</small>" );

				return $row;

			}, $orders );
		}

		$this->loadTemplate('orders/delivered-orders-index', ['data'=>$orders]);
	}

	public function details($id)
	{
		$order = $this->ordermodel->singleOrderByOrderId($id);
		if ( !$order )
		{
			show_404();
		}

		$this->loadTemplate('orders/orders-details', ['order'=> $order]);
	}
}
