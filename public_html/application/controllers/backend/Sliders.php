<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sliders extends Admin_Controller {

	private $_uploaded;

	public function __construct(){
		parent::__construct();

		$this->load->model('productmodel');
	}
	
	private function formAssets(){

		$this->header_assets = [
			'assets/libs/bootstrap-validator/css/bootstrapValidator.min.css',
		];

		$this->footer_assets = [
			'assets/libs/bootstrap-validator/js/bootstrapValidator.min.js',
			'assets/js/pages/form-validation.js',

			'assets/libs/bootstrap-inputmask/inputmask.js',
			'assets/libs/summernote/summernote.js',
			'assets/js/pages/forms.js',
		];
	}

	public function addNew(){
		
		$this->formAssets();
		
		$this->loadTemplate('sliders/form');
	}

	public function save(){

		$this->__validation();

		/**
		 * Table of Sliders
		 */
		$data = 
		[
			'title' 						=> $this->input->post('title'),
			'source' 						=> (count($this->_uploaded) ? $this->_uploaded[0]['file_name'] : NULL),
			'type_of' 						=> 'slider',
			
			'active' 						=> $this->input->post('active'),
			'created_at' 					=> date('Y-m-d H:i:s'),
			'updated_at' 					=> date('Y-m-d H:i:s')
		];

		$this->triggermodel->insertOrFail(TBL_SLIDERS, $data);

		$this->session->set_flashdata('alert-message', [
			'alert-type' => 'success',
			'message' 	 => 'Your record has been successfully added.',
			'link_name' => 'View Sliders Manage',
			'link_url' => link_to_backend('sliders/manage'),
		]);

		redirect( link_to_backend('sliders/addNew') );
	}

	public function edit( $id ){
		
		$this->formAssets();

		$edit_data = $this->triggermodel->firstOrFail(TBL_SLIDERS, ['where' => ['id' => $id ]]);

		$this->loadTemplate('sliders/form', ['edit_data' => $edit_data]);
	}

	public function update($id){

		$this->__validation($id);

		$data = 
		[
			'title' 						=> $this->input->post('title'),
			
			'active' 						=> $this->input->post('active'),
			'updated_at' 					=> date('Y-m-d H:i:s')
		];


		if ( count( $this->_uploaded ) )
		{
			$data['source'] = $this->_uploaded[0]['file_name'];
		}

		$this->triggermodel->updateOrFail(TBL_SLIDERS, $data, ['id' => $id]);
		
		$this->session->set_flashdata('alert-message', [
			'alert-type' => 'success',
			'message' 	 => 'Your record has been successfully updated.'
		]);

		redirect( link_to_backend('sliders/manage') );
	}

	public function delete( $id )
	{

		$this->triggermodel->use_soft = true;
		$this->triggermodel->delete(TBL_SLIDERS, $id);

		$this->session->set_flashdata('alert-message', [
			'alert-type' => 'success',
			'message' 	 => 'Your record has been successfully deleted.'
		]);
		redirect( link_to_backend('sliders/manage') );
	}

	public function manage()
	{
		$this->header_assets = [
			'assets/libs/jquery-datatables/css/dataTables.bootstrap.css',
			'assets/libs/jquery-datatables/extensions/TableTools/css/dataTables.tableTools.css'
		];

		$this->footer_assets = [
			'assets/libs/jquery-datatables/js/jquery.dataTables.min.js',
			'assets/libs/jquery-datatables/js/dataTables.bootstrap.js',
			'assets/libs/jquery-datatables/extensions/TableTools/js/dataTables.tableTools.min.js',
			'assets/js/pages/datatables.js'
		];

		$data = $this->triggermodel->all(TBL_SLIDERS, [], TRUE);

		if ( count($data) ){
			$data = array_map( function($v){
				
				$row = $v;
				
				if ( $row['created_at'] ) {
					$row['created_at'] = formatDate(ADMIN_DATE_FORMAT, $row['created_at'] );
				};

				if ( $row['updated_at'] ) {
					$row['updated_at'] = formatDate(ADMIN_DATE_FORMAT, $row['updated_at'] );
				};

				return $row;

			}, $data );
		}

		$this->loadTemplate('sliders/index', ['data'=>$data]);
	}

	private function __validation( $id = NULL ){

		$this->load->library('form_validation');

		$config = [
	        [
	        	'field' => 'title',
	            'label' => 'title',
	            'rules' => 'trim|required|max_length[50]|is_unique['.TBL_SLIDERS.'.title]',
	            'errors' => [
	            	'is_unique' => 'This %s already exists.'
	            ],
	        ],
	        [
	           	'field' => 'images[]',
	            'label' => 'upload image',
	            'rules' => 'callback_fileupload_check',
	        ]
		];

		// rd($config);
		/* title or image validation */
		if ( $id ) { 

			$config[0] = 
			[
				'field'   => 'title', 
				'label'   => 'title', 
				// 'rules'   => 'trim|required|max_length[50]|unique_other_than['.TBL_CATEGORIES.'.title, id='.$id.']',
				'rules'   => 'trim|required|max_length[50]',
				'errors' => 
				[
	            	'required' 	=> 'You must provide a %s.',
	            	'is_unique' => 'This %s already exists.'
	            ]
			];

			if ( isset($_FILES['images']['name'][0]) && $_FILES['images']['name'][0] == "" ){
				$config[1] = [];
			}
		}

		$this->form_validation->set_rules($config);

		if ( $this->form_validation->run() == FALSE ) {

			$this->session->set_flashdata('data', $this->input->post() );
			$this->session->set_flashdata('alert-message', [
				'alert-type' => 'danger',
				'message' 	 => validation_errors()
			]);

			redirect( link_to_backend('sliders/' . ($id ? ('edit/'.$id) : 'addNew' ) ) );
		}
	}

	// now the callback validation that deals with the upload of files
	public function fileupload_check() {

		// we retrieve the number of files that were uploaded
		$number_of_files = sizeof($_FILES['images']['tmp_name']);

		// considering that do_upload() accepts single files, we will have to do a small hack so that we can upload multiple files. For this we will have to keep the data of uploaded files in a variable, and redo the $_FILE.
		$files = $_FILES['images'];

		// first make sure that there is no error in uploading the files
		for( $i=0;$i<$number_of_files;$i++)
		{
			if($_FILES['images']['error'][$i] != 0) 
			{
				// save the error message and return false, the validation of uploaded files failed
				$this->form_validation->set_message('fileupload_check', 'Couldn\'t upload the file(s)');
				return FALSE;
			}
		}

		// we first load the upload library
		$this->load->library('upload');

		// next we pass the upload path for the images
		$config['upload_path'] = FCPATH . 'storage/sliders';

		// also, we make sure we allow only certain type of images
		$config['allowed_types'] = 'gif|jpg|png';

		// now, taking into account that there can be more than one file, for each file we will have to do the upload
		for ($i = 0; $i < $number_of_files; $i++) 
		{
			$_FILES['image']['name'] 		= $files['name'][$i];
			$_FILES['image']['type'] 		= $files['type'][$i];
			$_FILES['image']['tmp_name'] 	= $files['tmp_name'][$i];
			$_FILES['image']['error'] 		= $files['error'][$i];
			$_FILES['image']['size'] 		= $files['size'][$i];

			//now we initialize the upload library
			$this->upload->initialize($config);
			if ($this->upload->do_upload('image'))
			{
				$this->_uploaded[$i] = $this->upload->data();
			}
			else
			{
				$this->form_validation->set_message('fileupload_check', $this->upload->display_errors());
				return FALSE;
			}
		}
		
		return TRUE;
	}
}
