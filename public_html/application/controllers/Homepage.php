<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Homepage extends Public_Controller {

	public function __construct() {
		parent::__construct();
    }
    
	public function index()
	{
		$new_products 			= $this->productmodel->getNewProducts();
		$sale_products 			= $this->productmodel->getOnSaleProducts();
		$deal_products 			= $this->productmodel->getDealsProducts();
		$best_seller_products 	= $this->productmodel->getBestSellerProducts();
		// rd($deal_products);
		
		$data['new_products'] 	= $new_products;
		$data['sale_products']  = $sale_products;
		$data['deal_products']  = $deal_products;
		$data['best_seller']  	= $best_seller_products;
		// rd($data['new_products']);


		$data['sliders'] 	= $this->triggermodel->all(TBL_SLIDERS, ['where' => ['active' => 1 ]]);

		$data['meta'] 		= set_of_title_and_meta_keywords('Home');



$this->headerCSS = <<<HEADERCSS

.block-deals-of {}
.block-deals-of-opt1 .block-content{ height: 380px !important; }
.block-deals-of .product-item-opt-1 .product-item-img img { height: 215px !important; }

HEADERCSS;
$this->footerJS = <<<JAVASCRIPT

JAVASCRIPT;

		$this->loadTemplate('home', $data);
	}

	public function getCurrency($currency)
	{

		$map_index_currencires = array_index_map($this->currencies, 'symbols');

		if ( $map_index_currencires[$currency] )
		{
			$this->session->set_userdata('currency', $map_index_currencires[$currency]);
			redirect( base_url() );
		}

		$this->session->unset_userdata('currency', NULL );
		redirect( base_url() );
	}

	public function getTest()
	{


// exit();
$this->send_email('xunaidahmed@live.com', 'Test on server', $order_body);
$this->send_email('softtechnoz@gmail.com', 'Test on server', $order_body);
exit();


	}
}
