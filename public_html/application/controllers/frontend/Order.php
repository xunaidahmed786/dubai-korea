<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends Public_Controller {

	public function __construct() {
		parent::__construct();
    }

    public function postOrderPlace()
	{
		$this->load->library('form_validation');

   		$this->form_validation->set_rules('billing[first_name]', 'billing first name', 'trim|required');
   		$this->form_validation->set_rules('billing[last_name]', 'billing last name', 'trim|required');
		$this->form_validation->set_rules('billing[email_address]', 'billing email address', 'trim|required|valid_email');
   		$this->form_validation->set_rules('billing[address]', 'billing address', 'trim|required');
   		$this->form_validation->set_rules('billing[country]', 'billing country', 'trim|required');
   		$this->form_validation->set_rules('billing[state]', 'billing state', 'trim|required');
   		$this->form_validation->set_rules('billing[city]', 'billing city', 'trim|required');
   		$this->form_validation->set_rules('billing[telephone]', 'billing telephone', 'trim|required');

   		$this->form_validation->set_rules('shipping[first_name]', 'shipping first name', 'trim|required');
   		$this->form_validation->set_rules('shipping[last_name]', 'shipping last name', 'trim|required');
		$this->form_validation->set_rules('shipping[email_address]', 'shipping email address', 'trim|required|valid_email');
   		$this->form_validation->set_rules('shipping[address]', 'shipping address', 'trim|required');
   		$this->form_validation->set_rules('shipping[country]', 'shipping country', 'trim|required');
   		$this->form_validation->set_rules('shipping[state]', 'shipping state', 'trim|required');
   		$this->form_validation->set_rules('shipping[city]', 'shipping city', 'trim|required');
   		$this->form_validation->set_rules('shipping[telephone]', 'shipping telephone', 'trim|required');

   		if( $this->form_validation->run() == FALSE ){

   			$this->session->set_flashdata('data', $this->input->post() );
			$this->session->set_flashdata('alert-danger', validation_errors() );
   			redirect( site_url('checkout') );
   			return;
   		}

   		$post_data = $this->input->post();
		$this->session->set_userdata('checkout',  $post_data );

		if ( in_array($post_data['payment']['info'], ['cash_checkout', 'credit_card']) )
		{

			if ( $post_data['payment']['info'] == 'credit_card' ) 
			{
				$data['meta'] 		= set_of_title_and_meta_keywords('Pay with Card');
				$data['billing'] 	= $post_data['billing'];
				// rd($data['billing']);
				$this->loadTemplate('pay-with-card', $data);
				// redirect( site_url('place-order-confirmation') );
				return;
			}

			else if ( $post_data['payment']['info'] == 'cash_checkout' ) 
			{
				$this->postCashOnDevliery($post_data);
				return;
			}
		}

		redirect( site_url('checkout') );
		// redirect( site_url('place-order-confirmation') );
	}

	public function postPlaceOrderConfirms() {

		$this->load->library('form_validation');

   		$this->form_validation->set_rules('startToken', 'start token', 'trim|required');
   		$this->form_validation->set_rules('startEmail', 'start email', 'trim|required|valid_email');
   		
   		if( $this->form_validation->run() == FALSE ){

   			$this->session->set_flashdata('data', $this->input->post() );
			$this->session->set_flashdata('alert-danger', 'Error! Invalid request data for payment method' );
   			redirect( site_url('checkout') );
   			return;
   		}

		require_once APPPATH . 'third_party/vendor/autoload.php';

		# Initialize Start object
		Start::setApiKey(SECRET_KEY);

		# Read the fields that were automatically submitted by beautiful.js
		$token = $this->input->post('startToken');
		$email = $this->input->post('startEmail');
		$title = null;

		# Process the charge
		try {
		    
		   	$total              = currencyConvertToAmount($this->cart->total());
			$shipping_charges 	= shippingPolicyRules($total);

			$total_amount       = round($total + $shipping_charges);

		    $track_ip 			= $_SERVER["REMOTE_ADDR"];
		    $fk_user_id 		= ($this->isUserLogin() ? $this->getUserId() : $track_ip );
		    
		    $checkout 			= $this->session->userdata('checkout');

		    // rd($checkout);
		    
		    $billing 			= $checkout['billing'];
		    $shipping 			= $checkout['shipping'];
		    $payment 			= $checkout['payment'];

		    $billing_email  	= $email = $billing['email_address'];
	    	$shipping_email 	= $shipping['email_address'];

	    	$card_info 	= [
				"number" 	=> $payment['card_number'],
				"exp_month" => $payment['card_month'],
				"exp_year" 	=> $payment['card_year'],
				"cvc" 		=> $payment['card_verification']
			];

			//Create a new token for customer
			// $token = Start_Token::create($card_info);
	    	
	    	$pay_now = Start_Charge::create(array(
		        "amount"      => calculatOfPayTo($total_amount),
		        "currency"    => ($this->session->userdata('currency') ? $this->session->userdata('currency')['symbols'] : DEFAULT_CURRENCY_NAME),
		        "card"        => $token,
		        "email"       => $email,
		        "ip"          => $_SERVER["REMOTE_ADDR"],
		        "description" => "Charge Description"
		    ));

		    $order_number = random_order_number();

		    /**
		     * Order Information
		     */
		    $order_info = [
		    	'fk_user_id' 			=> $fk_user_id,
				'order_number' 			=> $order_number,
				'order_status' 			=> 'confirmed',
				'payment_method' 		=> 'Payfort',
				'tax_charges' 			=> 0.00,
				'shipping_charges' 		=> $shipping_charges,
				'discount_amount' 		=> 0.00,
				'total_amount' 			=> calculatOfReversePayTo($pay_now['amount']),
				'currency' 				=> ($this->session->userdata('currency') ? $this->session->userdata('currency')['symbols'] : DEFAULT_CURRENCY_NAME),
				'ip_address' 			=> $track_ip,
				'payment_token' 		=> $pay_now['account_id'],
				'payment_token' 		=> $pay_now['token_id'],
				'created_at' 			=> date('Y-m-d H:i:s'),
				'updated_at' 			=> date('Y-m-d H:i:s')
		    ];

		    $this->triggermodel->insertOrFail(TBL_ORDERS, $order_info);

		    $order_id = $this->triggermodel->last_insert_id();

		    /**
		     * Order Details :: Cart Items
		     */
		    $cart_items = [];

		    foreach ($this->cart->contents() as $items):
				$cart_items[] = [
					'fk_order_id' 	=> $order_id,
					'fk_product_id' => $items['id'],
					'name' 			=> $items['name'],
					'price' 		=> currencyConvertToAmount($items['original_price']),
					'sale_price' 	=> currencyConvertToAmount($items['sale_price']),
					'qty' 			=> $items['qty'],
					'subtotal' 		=> currencyConvertToAmount($items['subtotal'])
				];
			endforeach;

		    $this->triggermodel->insertBatch(TBL_ORDER_ITEMS, $cart_items);
		    
		    /**
		     * Order Transaction
		     */
		    $order_transactions = [
				'fk_order_id' 	=> $order_id,
				'account_id' 	=> $pay_now['account_id'],
				'auth_code' 	=> $pay_now['auth_code'],
				'currency' 		=> $pay_now['currency'],
				'email' 		=> $pay_now['email'],
				'status' 		=> $pay_now['state'],
				'card_detail' 	=> json_encode($pay_now['card']),
				'created_at' 	=> $pay_now['created_at'],
				'updated_at' 	=> $pay_now['updated_at'],
		    ];

		    $this->triggermodel->insertOrFail(TBL_TRANSACTION, $order_transactions);

		    /**
		     * Billing Information
		     */
		    $billing_info 	= [
				'fk_order_id' 	=> $order_id,
				'fk_user_id' 	=> $fk_user_id,
				'first_name' 	=> $billing['first_name'],
				'last_name' 	=> $billing['last_name'],
				'email' 		=> $billing['email_address'],
				'address' 		=> $billing['address'],
				'company' 		=> $billing['company_name'],
				'country' 		=> $billing['country'],
				'state' 		=> $billing['state'],
				'city' 			=> $billing['city'],
				'zip_code' 		=> $billing['postal_code'],
				'phone' 		=> $billing['telephone'],
				'fax' 			=> $billing['fax'],
		    ];

		    $this->triggermodel->insertOrFail(TBL_BILLING, $billing_info);

		    /**
		     * Shipping Information
		     */
		    $shipping_info 	= [
				'fk_order_id' 	=> $order_id,
				'fk_user_id' 	=> $fk_user_id,
				'first_name' 	=> $shipping['first_name'],
				'last_name' 	=> $shipping['last_name'],
				'email' 		=> $shipping['email_address'],
				'address' 		=> $shipping['address'],
				'company' 		=> $shipping['company_name'],
				'country' 		=> $shipping['country'],
				'state' 		=> $shipping['state'],
				'city' 			=> $shipping['city'],
				'zip_code' 		=> $shipping['postal_code'],
				'phone' 		=> $shipping['telephone'],
				'fax' 			=> $shipping['fax'],
		    ];

		    $this->triggermodel->insertOrFail(TBL_SHIPPING, $shipping_info);

		    $data['status'] 		= true;
		    $data['order_details'] 	= [
		    	'order_number' 	=> $order_number,
		    	'email' 		=> $pay_now['email'],
		    	'amount' 		=> calculatOfReversePayTo($pay_now['amount']),
		    ];

		    $title 					= 'Your Order has been received';

			 $order_invoice = [
		    	'order' 		=> $order_info,
		    	'orderProducts' => $cart_items,
		    	'customer' 		=> $this->auth(),
		    	'billing' 		=> $billing_info,
		    	'shipping' 		=> $shipping_info,
		    ];

			$this->send_email($billing_email, $title, null, 'order-invoice', $order_invoice);

			if ( $billing_email != $shipping_email ){
				$this->send_email($shipping_email, $title, null, 'order-invoice', $order_invoice);
			}
			
			$this->send_email(SUPPORT_EMAIL, $title, null, 'order-invoice', $order_invoice);

			$this->cart->destroy();

		} catch(Start_Error_Banking $e) {
		  
		  // Since it's a decline, Start_Error_Banking will be caught
			$error_code 	= $e->getErrorCode();
			$error_message 	= $e->getMessage();
		   
		    /* depending on $error_code we can show different messages */
		    if ($error_code === "card_declined") {
		       $title = 'Charge was declined';
		    } else {
		       $title = "Charge was not processed";
		    }

		    $data['errors'] 		= [
		    	'error_code' 		=> $error_code,
		    	'error_title' 		=> $title,
		    	'error_message' 	=> $error_message,
		    ];
		    
		    $data['status'] 		= false;

		} catch (Start_Error_Request $e) {
		  // Invalid parameters were supplied to Start's API
		  

			$error_code 	= $e->getErrorCode();
			$error_message 	= $e->getMessage();
		   
		    /* depending on $error_code we can show different messages */
		    if ($error_code === "card_declined") {
		       $title = 'Charge was declined';
		    } else {
		       $title = "Charge was not processed";
		    }

		    $data['errors'] 		= [
		    	'error_code' 		=> $error_code,
		    	'error_title' 		=> $title,
		    	'error_message' 	=> $error_message,
		    ];
		    
		    $data['status'] 		= false;

		} catch (Start_Error_Authentication $e) {
		  // There's a problem with that API key you provided

			$error_code 	= $e->getErrorCode();
			$error_message 	= $e->getMessage();
		   
		    /* depending on $error_code we can show different messages */
		    if ($error_code === "card_declined") {
		       $title = 'Charge was declined';
		    } else {
		       $title = "Charge was not processed";
		    }

		    $data['errors'] 		= [
		    	'error_code' 		=> $error_code,
		    	'error_title' 		=> $title,
		    	'error_message' 	=> $error_message,
		    ];
		    
		    $data['status'] 		= false;

		} catch (Start_Error $e) {
		  // Display a very generic error to the user, and maybe send
		  // yourself an email

			$error_code 	= $e->getErrorCode();
			$error_message 	= $e->getMessage();
		   
		    /* depending on $error_code we can show different messages */
		    if ($error_code === "card_declined") {
		       $title = 'Charge was declined';
		    } else {
		       $title = "Charge was not processed";
		    }

		    $data['errors'] 		= [
		    	'error_code' 		=> $error_code,
		    	'error_title' 		=> $title,
		    	'error_message' 	=> $error_message,
		    ];
		    
		    $data['status'] 		= false;

		} catch (Exception $e) {
		  // Something else happened, completely unrelated to Start
		  
		  /*print('Status is:' . $e->getHttpStatus() . "\n");
		  print('Code is:' . $e->getErrorCode() . "\n");
		  print('Message is:' . $e->getMessage() . "\n");*/

			$error_code 	= $e->getErrorCode();
			$error_message 	= $e->getMessage();
		   
		    /* depending on $error_code we can show different messages */
		    if ($error_code === "card_declined") {
		       $title = 'Charge was declined';
		    } else {
		       $title = "Charge was not processed";
		    }

		    $data['errors'] 		= [
		    	'error_code' 		=> $error_code,
		    	'error_title' 		=> $title,
		    	'error_message' 	=> $error_message,
		    ];
		    
		    $data['status'] 		= false;
		}

		
		/*
		
		$error_code 	= $e->getErrorCode();
		    $error_message 	= $e->getMessage();
		   
		    /* depending on $error_code we can show different messages 
		    /*if ($error_code === "card_declined") {
		       $title = 'Charge was declined';
		    } else {
		       $title = "Charge was not processed";
		    }*/
		    // echo "<p>".$error_message."</p>";

		    /*$data['errors'] 		= [
		    	'error_code' 		=> $error_code,
		    	'error_title' 		=> $title,
		    	'error_message' 	=> $error_message,
		    ];
		    
		    $data['status'] 		= false;

		 */

		if ( "Request params are invalid." == $error_message )
		{
			return redirect( base_url('/') );
		}

	    $data['meta'] 			= set_of_title_and_meta_keywords($title);
		$this->loadTemplate('order-success-page', $data);

	}

	private function postCashOnDevliery($post_data)
	{
		$order_number 		= random_order_number();

		$total              = currencyConvertToAmount($this->cart->total());
		$shipping_charges 	= shippingPolicyRules($total);
        
        $total_amount       = ($total + $shipping_charges);
		$track_ip 			= $_SERVER["REMOTE_ADDR"];
		$fk_user_id 		= ($this->isUserLogin() ? $this->getUserId() : $track_ip );

	    /**
	     * Order Information
	     */
	    $order_info = [
	    	'fk_user_id' 			=> $fk_user_id,
			'order_number' 			=> $order_number,
			'order_status' 			=> 'confirmed',
			'payment_method' 		=> 'Cash On Delivery',
			'tax_charges' 			=> 0.00,
			'shipping_charges' 		=> $shipping_charges,
			'discount_amount' 		=> 0.00,
			'total_amount' 			=> $total_amount,
			'currency' 				=> ($this->session->userdata('currency') ? $this->session->userdata('currency')['symbols'] : DEFAULT_CURRENCY_NAME),
			'ip_address' 			=> $track_ip,
			'payment_token' 		=> '---',
			'created_at' 			=> date('Y-m-d H:i:s'),
			'updated_at' 			=> date('Y-m-d H:i:s')
	    ];

	    $this->triggermodel->insertOrFail(TBL_ORDERS, $order_info);

	    $order_id = $this->triggermodel->last_insert_id();

	    /**
	     * Order Details :: Cart Items
	     */
	    $cart_items = [];

	    foreach ($this->cart->contents() as $items):
			$cart_items[] = [
				'fk_order_id' 	=> $order_id,
				'fk_product_id' => $items['id'],
				'name' 			=> $items['name'],
				'price' 		=> currencyConvertToAmount($items['original_price']),
				'sale_price' 	=> currencyConvertToAmount($items['sale_price']),
				'qty' 			=> $items['qty'],
				'subtotal' 		=> currencyConvertToAmount($items['subtotal'])
			];
		endforeach;

	    $this->triggermodel->insertBatch(TBL_ORDER_ITEMS, $cart_items);
	    
	    $checkout 		= $post_data;
	    $billing 		= $checkout['billing'];
	    $shipping 		= $checkout['shipping'];

	    $billing_email  = $billing['email_address'];
	    $shipping_email = $shipping['email_address'];

	    /**
	     * Billing Information
	     */
	    $billing_info 	= [
			'fk_order_id' 	=> $order_id,
			'fk_user_id' 	=> $fk_user_id,
			'first_name' 	=> $billing['first_name'],
			'last_name' 	=> $billing['last_name'],
			'email' 		=> $billing['email_address'],
			'address' 		=> $billing['address'],
			'company' 		=> $billing['company_name'],
			'country' 		=> $billing['country'],
			'state' 		=> $billing['state'],
			'city' 			=> $billing['city'],
			'zip_code' 		=> $billing['postal_code'],
			'phone' 		=> $billing['telephone'],
			'fax' 			=> $billing['fax'],
	    ];

	    $this->triggermodel->insertOrFail(TBL_BILLING, $billing_info);

	    /**
	     * Shipping Information
	     */
    	$shipping_info 	= [
			'fk_order_id' 	=> $order_id,
			'fk_user_id' 	=> $fk_user_id,
			'first_name' 	=> $shipping['first_name'],
			'last_name' 	=> $shipping['last_name'],
			'email' 		=> $shipping['email_address'],
			'address' 		=> $shipping['address'],
			'company' 		=> $shipping['company_name'],
			'country' 		=> $shipping['country'],
			'state' 		=> $shipping['state'],
			'city' 			=> $shipping['city'],
			'zip_code' 		=> $shipping['postal_code'],
			'phone' 		=> $shipping['telephone'],
			'fax' 			=> $shipping['fax'],
	    ];

	    $this->triggermodel->insertOrFail(TBL_SHIPPING, $shipping_info);

	    $data['status'] 		= true;
	    $data['order_details'] 	= [
	    	'order_number' 	=> $order_number,
	    	'email' 		=> $billing_email,
	    	'amount' 		=> $total_amount,
	    ];

	    $order_invoice = [
	    	'order' 		=> $order_info,
	    	'orderProducts' => $cart_items,
	    	'customer' 		=> $this->auth(),
	    	'billing' 		=> $billing_info,
	    	'shipping' 		=> $shipping_info,
	    ]; 

	    $title 					= 'Your Order has been received';

		$this->send_email($billing_email, $title, null, 'order-invoice', $order_invoice);

		if ( $billing_email != $shipping_email ){
			$this->send_email($shipping_email, $title, null, 'order-invoice', $order_invoice);
		}

		$this->send_email(SUPPORT_EMAIL, $title, null, 'order-invoice', $order_invoice);
		
	    $this->cart->destroy();

	    $this->session->set_flashdata('order', $data);
	    // $this->session->set_userdata('order', $data);

	   	return redirect( site_url('success-order-place') );
	}

	public function getSuccessOrderPlace()
	{

		if ( $this->session->userdata('order') )
		{
			$orders 				= $this->session->userdata('order');
			$data['meta'] 			= set_of_title_and_meta_keywords('Your Order has been received');
			$data['status'] 		= $orders['status'];
			$data['order_details'] 	= $orders['order_details'];
			$this->loadTemplate('order-success-page', $data);
			return;
		}

		return redirect( base_url('/') );
	}

}
