<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Authorized extends Public_Controller {

	public function __construct() {
		parent::__construct();

    }

    public function getSignup()
	{

		if ( $this->isUserLogin() )
		{
			redirect( site_url('shopping-cart') );
		}

		$data['email'] 		= $this->input->post('email');
		$data['meta'] 		= set_of_title_and_meta_keywords('Sign Up');

		$this->loadTemplate('signup', $data);
	}

	public function postSignup()
	{

		$this->load->library('form_validation');

   		$this->form_validation->set_rules('first_name', 'first name', 'trim|required|max_length[20]');
   		$this->form_validation->set_rules('last_name', 'last name', 'trim|required|max_length[20]');
		$this->form_validation->set_rules('email', 'email', 'trim|required|valid_email|is_unique[dc_customers.email]|max_length[50]');
   		$this->form_validation->set_rules('telephone', 'telephone', 'trim|required|max_length[20]');
   		$this->form_validation->set_rules('address', 'address', 'trim|required');
   		$this->form_validation->set_rules('city', 'city', 'trim|max_length[20]');
   		$this->form_validation->set_rules('state', 'state', 'trim|max_length[20]');
   		$this->form_validation->set_rules('country', 'state', 'trim|max_length[20]');
   		$this->form_validation->set_rules('password', 'password', 'trim|required|max_length[20]');
   		$this->form_validation->set_rules('confirm', 'password', 'trim|required|matches[password]');

   		if( $this->form_validation->run() == FALSE ){

   			$this->session->set_flashdata('data', $this->input->post() );
			$this->session->set_flashdata('alert-message', validation_errors() );
   			redirect( site_url('sign-up') );
   			return;
   		}

   		$data = [
			'first_name' 	=> $this->input->post('first_name'),
			'last_name' 	=> $this->input->post('last_name'),
			'email' 		=> $this->input->post('email'),
			'password' 		=> salt_password($this->input->post('password')),
			'telephone' 	=> $this->input->post('telephone'),
			'address' 		=> $this->input->post('address'),
			'country' 		=> $this->input->post('country'),
			'state' 		=> $this->input->post('state'),
			'city' 			=> $this->input->post('city'),
			'postal_code' 	=> $this->input->post('postal_code'),
			'active' 		=> 1,
			'created_at' 	=> date('Y-m-d H:i:s'),
			'updated_at' 	=> date('Y-m-d H:i:s')
		];

		$this->triggermodel->insertOrFail(TBL_CUSTOMERS, $data);

		$this->session->set_flashdata('alert-success', 'Congratulations! You have successfully registered.' );
		redirect( site_url('login') );
	}

	public function getLogin()
	{
		if ( $this->isUserLogin() )
		{
			redirect( site_url('shopping-cart') );
		}

		$data['meta'] 		= set_of_title_and_meta_keywords('Authentication');

		$register_url 		= site_url('sign-up');
		$guest_url 			= site_url('checkout');

		$this->footerJS = <<<JAVASCRIPT
$('#checkout_btn').on('click', function(){

	var typeOf = $(this).closest('.box-border').find('input[name=checkout_type]:checked').val();
	
	if ( 'guest' == typeOf )
	{
		href = "$guest_url";
	
	} else {
		href = "$register_url";
	}
	
	window.location = href;
	
});
JAVASCRIPT;

		$this->loadTemplate('login', $data);
	}

	public function getAuthentication()
	{

		$this->load->library('form_validation');

		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
   		$this->form_validation->set_rules('password', 'Password', 'trim|required');

   		if( $this->form_validation->run() == FALSE ){

   			$this->session->set_flashdata('data', $this->input->post() );
			$this->session->set_flashdata('alert-danger', validation_errors() );
   			redirect( site_url('login') );
   			return;
   		}
   		// rd( salt_password('admin123') );

   		$this->load->model('authorizationmodel');

   		$authorize = [
			'email' 	=> $this->input->post('email'),
			'password' 	=> $this->input->post('password'),
			'active' 	=> 1,
		];

		if ( $this->authorizationmodel->isAuthorizationLoginByCustomer($authorize) ) {
		
			redirect( site_url('shopping-cart') );
		
		} else {

			$this->session->set_flashdata('data', $this->input->post() );
			$this->session->set_flashdata('alert-danger', 'Error: Invalid email or password. Please try again' );
   			redirect( site_url('login') );
   			return;
		}
	}

	public function getLogout()
	{
		if ( $this->isUserLogin() == false )
		{
			redirect( base_url() );
		}

		$this->session->unset_userdata('auth', []);
		$this->session->unset_userdata('logged_in', NULL );

		redirect( base_url() );
	}

}
