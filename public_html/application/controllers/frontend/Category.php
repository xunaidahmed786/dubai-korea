<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends Public_Controller {

	public function __construct(){
		parent::__construct();
	}

	public function getAllCategories(){

		$data['meta'] 		= set_of_title_and_meta_keywords('About US');

		$this->loadTemplate('aboutus', $data);
	}

}
