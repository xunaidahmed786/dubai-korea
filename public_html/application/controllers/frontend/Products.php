<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends Public_Controller {

	public function __construct() {
		parent::__construct();
    }

	public function getCategoriesSearchProducts( $category_id, $keyword  ){

		$is_single_category 	= false;
		$categories_of_sidebar = buildTree($this->productmodel->getAllCategories([
            'sortBy' => 'id',
            'orderBy' => 'DESC'
        ]));

		$search_by 			= [
			'search_by' => $keyword
		];

		if ( $category_id !== 'all' )
		{
			$is_single_category 		= true;
			$search_by['category_id'] 	= $category_id;
		}

		if ( $is_single_category )
		{
			$single_category 	= $this->triggermodel->firstOrFail(TBL_CATEGORIES, [ 'where' => [
				'active' => 1,
				'slug' => $category_id
			]]);	
		}

		$products 			= $this->productmodel->getProductsList($search_by);

        if ( $is_single_category )
        {
			$data['breakcrum_active'] 			= $single_category->title;
			$data['get_of_data'] 				= $single_category;
        } 
        else
        {
        	$data['breakcrum_active'] 			= 'All';
			$data['get_of_data'] 				= (object) ['title' => 'All'];
        }
		
		$data['type_of_page'] 				= 'search_of_products_categories';
		$data['products'] 					= $products;
        $data['categories_of_sidebar']     	= $categories_of_sidebar;
		$data['meta'] 						= set_of_title_and_meta_keywords($data['breakcrum_active']);

		$this->loadTemplate('products', $data);
	}

    public function getBrandsProducts( $brand_id = null  ){

		$brand_where = [];
		$brand 		 = null;

		if ( $brand_id != 'all')
		{
			$brand_where = [
				'brand_id' => $brand_id
			];

			$brand 	= $this->triggermodel->firstOrFail(TBL_BRANDS, [ 'where' => [
				'active' => 1,
				'slug' => $brand_id
			]]);
		}

		$products 				= $this->productmodel->getBrandProductsList($brand_where);
		
        $categories_of_sidebar 	= buildTree($this->productmodel->getAllCategories([
            'sortBy' => 'id',
            'orderBy' => 'DESC'
        ]));

		$data['type_of_page'] 				= 'brands';
		$data['breakcrum_active'] 			= (isset($brand->title) ? $brand->title : 'All - Brands');
		$data['get_of_data'] 				= $brand;
		$data['products'] 					= $products;
        $data['categories_of_sidebar']     	= $categories_of_sidebar;
		$data['meta'] 						= set_of_title_and_meta_keywords($data['breakcrum_active']);

		$this->loadTemplate('products', $data);
	}

	public function getCategoriesProducts( $category_id = null  ){

		$single_category 	= $this->triggermodel->firstOrFail(TBL_CATEGORIES, [ 'where' => [
			'active' => 1,
			'slug' => $category_id
		]]);

		$products 			= $this->productmodel->getProductsList([
			'category_id' => $category_id
		]);

        $categories_of_sidebar = buildTree($this->productmodel->getAllCategories([
            'sortBy' => 'id',
            'orderBy' => 'DESC'
        ]));

		$data['type_of_page'] 				= 'categories';
		$data['breakcrum_active'] 			= $single_category->title;
		$data['get_of_data'] 				= $single_category;
		$data['products'] 					= $products;
        $data['categories_of_sidebar']     	= $categories_of_sidebar;
		$data['meta'] 						= set_of_title_and_meta_keywords($data['breakcrum_active']);

		$this->loadTemplate('products', $data);
	}

	public function getSingleProduct( $category_id, $product_id )
	{
		$data['is_product'] = false;;

		$single_product 	= $this->triggermodel->firstOrFail(TBL_PRODUCTS, [ 'where' => [
			'active' => 1,
			'slug' => $product_id
		]]);

		$data['meta'] 		= set_of_title_and_meta_keywords('Home');

		if ( $single_product )
		{

			$data['is_product'] = true;

			$single_category 	= $this->triggermodel->firstOrFail(TBL_CATEGORIES, [ 'where' => [
				'active' => 1,
				'slug' => $category_id
			]]);

			// rd($single_product->id);
			$product_image 		= $this->productmodel->getMediaByProductId( $single_product->id );
			$product_desc_image = $this->productmodel->getMediaByProductIdOrType( $single_product->id, 'pro-desc' );
			$product_reviews 	= $this->productmodel->getListOfProductReviewByProductId( $single_product->id );
			
			$data['single_product'] = [
				'category' => (array) $single_category,
				'product' => (array) $single_product,
				'images' => $product_image,
				'pro_desc_images' => $product_desc_image,
				'reviews' => $product_reviews,
			];

			// rd($data['single_product']['reviews']);

			$last_product_id 	= $data['single_product']['product']['id'];
			$products 			= $this->productmodel->getProductsList($data['single_product']['category']['slug']);

			$products = array_map(function($v) use($last_product_id) {
				
				if ( $v['products']['id'] !== $last_product_id ){
					return $v;	
				}

			}, $products);

			$data['related_products'] = array_filter($products);

			$meta_tags 			= [
				'description' 	=> $data['single_product']['product']['short_description'],
				'image' 		=> (isset($data['single_product']['images'][0]) ? storage('products/' . $data['single_product']['images'][0]['image']) : null),
				'url' 			=> site_url($data['single_product']['category']['slug'] .'/'. $data['single_product']['product']['slug'])
			];

			$data['meta'] 		= set_of_title_and_meta_keywords($data['single_product']['product']['title'], $meta_tags);
		}

$this->footerJS = <<<JAVASCRIPT

	var obj=document.body;  // obj=element for example body
	// bind mousewheel event on the mouseWheel function
	if(obj.addEventListener)
	{
	    obj.addEventListener('DOMMouseScroll',mouseWheel,false);
	    obj.addEventListener("mousewheel",mouseWheel,false);
	}
	else obj.onmousewheel=mouseWheel;

	function mouseWheel(e)
	{
	    // disabling
	    e=e?e:window.event;
	    if(e.ctrlKey)
	    {
	        if(e.preventDefault) e.preventDefault();
	        else e.returnValue=false;
	        return false;
	    }
	}

	$('.write-review').on('click', function(){
		
		$('.tab-content, .nav-pills li').removeClass('active');
		$('#reviews, .nav-pills li:nth-child(3)').addClass('active');
		
		main.to_scroll_to( $('.tab-content'), 30 );
	});
JAVASCRIPT;

		$this->loadTemplate('products/single_product_details', $data);	
	}

	public function postWholeSellerSendInquiry()
	{
		$fk_product_id 				= $this->input->post('product_id');
		$fk_product_name 			= $this->input->post('product_name');
		$fk_product_url 			= $this->input->post('product_url');

		$seller_name 				= $this->input->post('name');
		$seller_email 				= $this->input->post('email');
		$seller_phone 				= $this->input->post('phone');
		$company_name 				= $this->input->post('company_name');
		$store_website_name 		= $this->input->post('store_website_name');
		$year_established 			= $this->input->post('year_established');

		$address1 					= $this->input->post('address1');
		$address2 					= $this->input->post('address2');
		$city 						= $this->input->post('city');
		$state 						= $this->input->post('state');
		$zip_code 					= $this->input->post('zip_code');
		$country 					= $this->input->post('country');
		$country_distribute_sell	= $this->input->post('country_distribute_sell');
		$type_of_business 			= $this->input->post('type_of_business');
		$currently_sell 			= $this->input->post('currently_sell');

		$data = [

			'fk_product_id' 			=> $fk_product_id,
			'product_name' 				=> $fk_product_name,
			'seller_name' 				=> $seller_name,
			'seller_email' 				=> $seller_email,
			'seller_phone' 				=> $seller_phone,
			'company_name' 				=> $company_name,
			'store_website_name' 		=> $store_website_name,
			'year_established' 			=> $year_established,
			
			'address1' 					=> $address1,
			'address2' 					=> $address2,
			'city' 						=> $city,
			'state' 					=> $state,
			'zip_code' 					=> $zip_code,
			'country' 					=> $country,
			'country_distribute_sell' 	=> $country_distribute_sell,
			'type_of_business' 			=> $type_of_business,
			'currently_sell' 			=> $currently_sell,

			'created_at' 				=> date('Y-m-d H:i:s'),
			'updated_at' 				=> date('Y-m-d H:i:s')
		];

		$this->triggermodel->insertOrFail(TBL_WHOLE_SELLER, $data);

		// Send email to Guest
        $to_guest = '
			Hey '.$name.', <br/><br/>
			Your query has been received and following is the copy of your message. We\'ll respond you as soon as possible. <br/><br />
			
			<strong><u>Requested Message</u></strong><br/><br/>
			Email: <strong>' . $seller_email . '</strong><br />
			Phone: <strong>' . $seller_phone . '</strong><br />
			Company Name: <strong>' . $company_name . '</strong><br />
			Store or Website Name: <strong>' . $store_website_name . '</strong><br /><br />
			
			Address 1: <strong>' . $address1 . '</strong><br />
			City: <strong>' . $city . '</strong><br />
			Country: <strong>' . $country . '</strong><br />
			Country you wish to distribute to or sell to: <strong>' . $country_distribute_sell . '</strong><br />
			Type of business: <strong>' . $type_of_business . '</strong><br />
			Which other skincare brands do you currently sell?: <strong>' . $currently_sell . '</strong><br />
        ';
        $this->send_email($seller_email, 'Your query has been received', $to_guest);

        // Send email to Administrator
        $to_admin = '
			Hey Admin, <br/><br/>
			Someone has contacted you via whole seller form on your website.<br /><br />
			
			<strong><u>Requested Message</u></strong><br/><br/>
			Contact Name: <strong>' . $seller_name . '</strong><br />
			Email: <strong>' . $seller_email . '</strong><br />
			Phone: <strong>' . $seller_phone . '</strong><br />
			Company Name: <strong>' . $company_name . '</strong><br />
			Store or Website Name: <strong>' . $store_website_name . '</strong><br /><br />
			
			Address 1: <strong>' . $address1 . '</strong><br />
			City: <strong>' . $city . '</strong><br />
			Country: <strong>' . $country . '</strong><br />
			Country you wish to distribute to or sell to: <strong>' . $country_distribute_sell . '</strong><br />
			Type of business: <strong>' . $type_of_business . '</strong><br />
			Which other skincare brands do you currently sell?: <strong>' . $currently_sell . '</strong><br />
            
            Date Contacted: <strong>' . date('F d, Y') . '</strong>
        ';
        $this->send_email($this->is_admin_email, 'Whole Seller Query', $to_admin);


		$this->session->set_flashdata('alert-success', 'Whole seller query has been added' );

		redirect( $fk_product_url);
	}

	public function postProductReviews( $fk_product_id )
	{

		if ( $this->isUserLogin() == false )
		{
			redirect( site_url('login') );
		}

		$fk_product_url = $this->input->post('fk_product_url');

		$data = [
			'fk_user_id' 	=> $this->getUserId(),
			'fk_product_id' => $fk_product_id,
			'reviews' 		=> $this->input->post('comments'),
			'review_rating' => $this->input->post('rate'),
			'is_active' 	=> 1,
			'created_at' 	=> date('Y-m-d H:i:s'),
			'updated_at' 	=> date('Y-m-d H:i:s')
		];

		$this->triggermodel->insertOrFail(TBL_PRODUCT_REVIEWS, $data);

		$this->session->set_flashdata('alert-message', 'Successfully review has been added.' );
		$this->session->set_flashdata('select-tab', 'reviews' );

		redirect( $fk_product_url . '#reviews' );
	}
}
