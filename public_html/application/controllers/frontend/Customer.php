<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends Public_Controller {

	public function __construct() {
		
		parent::__construct();

		if ( $this->isUserLogin() == false )
		{
			redirect( site_url('login') );
		}

		$this->load->model(['ordermodel', 'customermodel']);
		
		$this->headerCSS = <<<HEADERCSS
.cart_summary thead {
	font-size:12px;
}
HEADERCSS;

    }

    public function getMyAccount()
	{

		$confirmed_orders = $this->ordermodel->getCustomerConfirmedOrders();
		$delivered_orders = $this->ordermodel->getCustomerDeliveredOrders();

		$data['confirmed_orders'] 	= $confirmed_orders;
		$data['delivered_orders'] 	= $delivered_orders;

		$data['meta'] 				= set_of_title_and_meta_keywords('My Account');

		$this->loadTemplate('customers/dashboard', $data);
	}

	public function getOrders()
	{

		$orders = $this->ordermodel->getCustomerAllOrders();
		
		$data['orders'] 	= $orders;
		$data['meta'] 		= set_of_title_and_meta_keywords('My Orders');

		$this->loadTemplate('customers/myorders', $data);
	}

	public function getOrderDetail($fk_order_id)
	{

		$orders 			= $this->ordermodel->singleOrderByOrderId($fk_order_id);

		if ( empty($orders) )
		{
			show_404();
		}
		
		$data['order'] 		= $orders;
		$data['meta'] 		= set_of_title_and_meta_keywords('Order Summary');

		$this->headerCSS = <<<HEADERCSS
.page-order .step {
	cursor:pointer;
}
.content{
	display:none;
}
HEADERCSS;

$this->footerJS = <<<JAVASCRIPT
$('.content:first').show();

$('.page-order li').on('click', function(){
	
	$('.page-order li').removeClass('current-step');
	$(this).addClass('current-step');

	var tab = $(this).data('id');
	$('.content').hide();
	$('#'+tab).show();
});
JAVASCRIPT;

		$this->loadTemplate('customers/order-details', $data);
	}

	public function postWishlist($fk_product_id)
	{

		/*$single_product = $this->triggermodel->firstOrFail(TBL_WISHLIST, ['where'=> ['fk_product_id' => $fk_product_id] ]);
		if ( count($single_product) )
		{
			$this->session->set_flashdata('alert-danger', 'You have already exists.' );
			redirect( site_url('wishlist') );
		}*/
		$data = [
			'fk_user_id' 	=> $this->getUserId(),
			'fk_product_id' => $fk_product_id,
			'date_created' 	=> date('Y-m-d H:i:s')
		];

		$this->triggermodel->insertOrFail(TBL_WISHLIST, $data);

		$this->session->set_flashdata('alert-success', 'You have successfully product added in wishlist list.' );

		redirect( site_url('wishlist') );
	}

	public function getWishlist()
	{

		$data['wishlists'] 	= $this->productmodel->getWishlistProducts();
		$data['meta'] 		= set_of_title_and_meta_keywords('Wishlist');

		$this->loadTemplate('wishlist', $data);
	}

	public function deleteWishlist($fk_product_id)
	{
		$this->triggermodel->delete(TBL_WISHLIST, null, ['fk_product_id' => $fk_product_id]);

		$this->session->set_flashdata('alert-success', 'You have successfully deleted.' );
		redirect( site_url('wishlist') );
	}



}
