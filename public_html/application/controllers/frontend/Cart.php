<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends Public_Controller {

	public function __construct() {
		
		parent::__construct();

		$this->load->library('cart');
    }

    public function getApiProductAddToCart($product_id)
    {
    	$to_single_product 	= $this->productmodel->getSingleProduct( $product_id );
    	
    	if ( $to_single_product )
		{
			
			unset($to_single_product['products']['data']['description']);

			$to_product 		= $to_single_product['products'];
			$data 				= [
				'id'      				=> $to_product['data']['id'],
				'code'    				=> $to_product['data']['code'],
	        	'name'    				=> $to_product['data']['title'],
	        	'short_description'    	=> $to_product['data']['short_description'],
	        	'qty'     				=> 1,
	        	'price'   				=> ( 
	        								($to_product['data']['sale_price'] =='0.00') ? 
	        								$to_product['data']['price'] : 
	        								$to_product['data']['sale_price'] 
	        							),
	        	'original_price'   		=> $to_product['data']['price'],
	        	'sale_price'   			=> $to_product['data']['sale_price'],
	        	'available_stock'   	=> $to_product['data']['available_stock'],
	        	'expired_at'    		=> $to_product['data']['expired_at'],
	        	'options' 				=> [
	        		'category_name' 	=> $to_product['category']['name'],
	        		'category_url' 		=> site_url( 'c/'.$to_product['category']['slug'] ),
	        		'product_url' 		=> site_url($to_product['category']['slug'] .'/'. $to_product['data']['slug'] ),
	        		'product_image' 	=> isset($to_product['images'][0]['image']) ? $to_product['images'][0]['image'] : null
	        	]
			];
			$this->cart->insert($data);

			$this->session->set_flashdata('alert-success', 'Added items "'.$to_product['data']['title'].'" to your cart.');
		
		} else {
			$this->session->set_flashdata('alert-danger', 'Invalid request has been detected.');	
		}
		
		redirect(  site_url('shopping-cart') );	
    }

    public function postAddToCart()
    {
    	// rd( $_POST );

    	$this->load->library('form_validation');

		$config = [
	        [
	        	'field' => 'qty',
	            'label' => 'qty',
	            'rules' => 'trim|required'
	        ],
	        [
	           	'field' => 'product_id',
	            'label' => 'product',
	            'rules' => 'required',
	            'errors' => [
	            	'required' 	=> 'Invalid request has been detected.',
	            ]
	        ]
		];

		$this->form_validation->set_rules($config);
		if ( $this->form_validation->run() == FALSE ) {

			$this->session->set_flashdata('data', $this->input->post() );
			$this->session->set_flashdata('alert-message', [
				'alert-type' => 'danger',
				'message' 	 => validation_errors()
			]);

			redirect(base_url());
		}

		// rd($this->input->post('product_id'));
		$to_single_product 	= $this->productmodel->getSingleProduct( $this->input->post('product_id') );
		$to_product 		= $to_single_product['products'];

		unset($to_product['data']['description']);

		$data 				= [
			'id'      				=> $to_product['data']['id'],
			'code'    				=> $to_product['data']['code'],
        	'name'    				=> $to_product['data']['title'],
        	'short_description'    	=> $to_product['data']['short_description'],
        	'qty'     				=> $this->input->post('qty'),
        	'price'   				=> ( 
        								($to_product['data']['sale_price'] =='0.00') ? 
        								$to_product['data']['price'] : 
        								$to_product['data']['sale_price'] 
        							),
        	'original_price'   		=> $to_product['data']['price'],
        	'sale_price'   			=> $to_product['data']['sale_price'],
        	'available_stock'   	=> $to_product['data']['available_stock'],
        	'expired_at'    		=> $to_product['data']['expired_at'],
        	'options' 				=> [
        		'category_name' 	=> $to_product['category']['name'],
        		'category_url' 		=> site_url( 'c/'.$to_product['category']['slug'] ),
        		'product_url' 		=> site_url($to_product['category']['slug'] .'/'. $to_product['data']['slug'] ),
        		'product_image' 	=> isset($to_product['images'][0]['image']) ? $to_product['images'][0]['image'] : null
        	]
		];
		// rd($data);
		// rd($to_single_product);
		$this->cart->insert($data);

		$this->session->set_flashdata('alert-success', 'Added items "'.$to_product['data']['title'].'" to your cart.');
		
		redirect( site_url('shopping-cart') );
    }

	public function getCart()
	{

		$data['meta'] 		= set_of_title_and_meta_keywords('Shopping Cart');
		$update_url_link 	= base_url('shopping-cart/update/');

		$this->footerJS = <<<JAVASCRIPT
$('.qtyclick').on('click', function(){
	var rowid = $(this).closest('tr').find('input[type="hidden"]').val();
	var qty = $(this).closest('tr').find('input[name="qty"]').val();
	window.location = "$update_url_link" + qty + "/" + rowid;
});
JAVASCRIPT;

		$this->loadTemplate('shopping-cart', $data);
	}

	public function getCheckout()
	{
		/*if ( $this->isUserLogin() == FALSE )
		{
			redirect( site_url('login') );
		}*/

		$data['meta'] 		= set_of_title_and_meta_keywords('Checkout');

		$this->loadTemplate('checkout', $data);
	}

	public function getUpdateProductByCart($qty, $cart_id)
	{
		
		// Update cart data, after cancel.
		$this->cart->update([
			'rowid' => $cart_id,
			'qty' => $qty,
		]);
		redirect( site_url('shopping-cart') );
	}

	public function getRemoveProductByCart($cart_id)
	{
		$this->cart->remove($cart_id);

		redirect( site_url('shopping-cart') );
	}

}
