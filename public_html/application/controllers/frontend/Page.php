<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends Public_Controller {

	public function __construct(){
		parent::__construct();
	}

	public function getAboutUs(){

		$data['meta'] 		= set_of_title_and_meta_keywords('About US');

		$this->loadTemplate('aboutus', $data);
	}

	public function getPrivacyPolicy(){

		$data['meta'] 		= set_of_title_and_meta_keywords('Privacy Policy');

		$this->loadTemplate('privacypolicy', $data);
	}

	public function getTermsAndCondition(){

		$data['meta'] 		= set_of_title_and_meta_keywords('Terms and Condition');

		$this->loadTemplate('temsandconditions', $data);
	}

	public function getFAQ(){

		$this->header_assets = [
			'css/bootstrap.min.css',
		];

		$this->footer_assets = [
			'js/bootstrap.min.js',
		];

		$data['meta'] 		= set_of_title_and_meta_keywords('FAQs');

		$this->loadTemplate('faqs', $data);
	}

	public function getWholeSellerInQuery(){

		$data['products'] 	= $this->triggermodel->all(TBL_PRODUCTS, [
			'select' 	=> 'id, title'
		], TRUE);

		$data['meta'] 		= set_of_title_and_meta_keywords('Wholesale enquiry form');

$this->footerJS = <<<JAVASCRIPT
$('select[name=product_id]').on('change', function(){
	$(this).val() ? $('input[name=product_name]').val($(this).find('option:selected').text()) : $('input[name=product_name]').val('');
});
JAVASCRIPT;
		$this->loadTemplate('whole-seller-page', $data);
	}

	public function getContactUS(){

		$data['meta'] 		= set_of_title_and_meta_keywords('Contact Us');

		$this->loadTemplate('contactus', $data);
	}

	public function postContactUS(){

		$this->load->library('form_validation');

   		$this->form_validation->set_rules('name', 'name', 'trim|required|max_length[30]');
   		$this->form_validation->set_rules('email', 'email', 'trim|required|valid_email|max_length[50]');
   		$this->form_validation->set_rules('telephone', 'telephone', 'trim|required|max_length[100]');
   		$this->form_validation->set_rules('message', 'message', 'trim|required|max_length[500]');
   		
   		if( $this->form_validation->run() == FALSE ){

   			$this->session->set_flashdata('data', $this->input->post() );
			$this->session->set_flashdata('alert-danger', validation_errors() );
   			redirect( site_url('contact-us') );
   			return;
   		}

		$name 				= $this->input->post('name');
		$email 				= $this->input->post('email');
		$telephone 			= $this->input->post('telephone');
		$message 			= $this->input->post('message');

		/**
	     * Contact Us Query Information
	     */
	    $query = [
	    	'name' 			=> $name,
	    	'email' 		=> $email,
	    	'telephone' 	=> $telephone,
	    	'message' 		=> $message,
			'ip_address' 	=> $_SERVER["REMOTE_ADDR"],
			'created_at' 	=> date('Y-m-d H:i:s'),
			'updated_at' 	=> date('Y-m-d H:i:s')
	    ];

	    $this->triggermodel->insertOrFail(TBL_CONTACT_QUERY, $query);

		// Send email to Guest
        $to_guest = '
			Hey '.$name.', <br/><br/>
			Your query has been received and following is the copy of your message. We\'ll respond you as soon as possible. <br/><br />
			
			<strong><u>Requested Message</u></strong><br/><br/>
			Email: <strong>' . $email . '</strong><br />
            Telephone: <strong>' . ($telephone ? $telephone : "<small>Not available</small>") . '</strong><br />
            Date Contacted: <strong>' . date('F d, Y') . '</strong><br />
            Message: <strong>' . nl2br($message) . '</strong><br />
        ';
        $this->send_email($email, 'Your query has been received', $to_guest);

        // Send email to Administrator
        $to_admin = '
			Hey Admin, <br/><br/>
			Someone has contacted you via Contact Us form on your website.<br /><br />
			
			<strong><u>Requested Message</u></strong><br/><br/>
			Full Name: <strong>' . $name . '</strong><br />
			Email: <strong>' . $email . '</strong><br />
            Telephone: <strong>' . ($telephone ? $telephone : "<small>Not available</small>") . '</strong><br />
            IP: <strong>' . $this->input->ip_address() . '</strong><br />
            Date Contacted: <strong>' . date('F d, Y') . '</strong><br />
            Message: <strong>' . nl2br($message) . '</strong><br />
        ';
        $this->send_email($this->is_admin_email, 'Contact Us Query', $to_admin);

   		$this->session->set_flashdata('alert-success', 'Contact Us query has been added.' );
		redirect( site_url('contact-us') );
	}

	public function postSubscriber()
	{
		$this->load->library('form_validation');

   		$this->form_validation->set_rules('name', 'name', 'max_length[30]');
   		// $this->form_validation->set_rules('email', 'email', 'required|valid_email|max_length[50]|is_unique['.TBL_NEWSLETTERS.'.email');
   		$this->form_validation->set_rules('email', 'email', 'required|valid_email|max_length[50]');
   		
   		if( $this->form_validation->run() == FALSE ){

   			$this->session->set_flashdata('data', $this->input->post() );
			$this->session->set_flashdata('alert-danger', validation_errors() );
   			redirect( base_url() );
   			return;
   		}

   		$email 				= $this->input->post('email');

   		/**
	     * Newsletter Information
	     */
	    $query = [
	    	'email' 		=> $email,
	    	'track_ip' 		=> $_SERVER["REMOTE_ADDR"],
			'created_at' 	=> date('Y-m-d H:i:s'),
			'updated_at' 	=> date('Y-m-d H:i:s')
	    ];

	    $this->triggermodel->insertOrFail(TBL_NEWSLETTERS, $query);

	    // Send email to Guest
        $to_guest = '
			Hey, <br/><br/>
			<strong>Thank you and welcome!</strong><br/>
			<p>
			You succesfully subscribed to the Dubaikorea Newsletter. To ensure you receive emails from Dubaikorea. <br/>
			Meanwhile, you might like to inform your friends about Dubaikorea newsletter.
			</p>
        ';
        $this->send_email($email, 'Subscriber Notification', $to_guest);

   		$this->session->set_flashdata('alert-success', 'Check you email to confirm you newsletter!' );
		redirect( base_url() );
	}

}
