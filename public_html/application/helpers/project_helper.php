<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function static_url($uri='') {
    return (is_https() ? 'https' : 'http') . '://' . (defined('ASSET_DOMAIN') ? ASSET_DOMAIN : $_SERVER['HTTP_HOST']) . substr($_SERVER['SCRIPT_NAME'], 0, strpos($_SERVER['SCRIPT_NAME'], basename($_SERVER['SCRIPT_FILENAME']))) . $uri;
}

// Snippet from PHP Share: http://www.phpshare.org
function formatSizeUnits($bytes) {
    if ($bytes >= 1073741824)
    {
        $bytes = number_format($bytes / 1073741824, 2) . ' GB';
    }
    elseif ($bytes >= 1048576)
    {
        $bytes = number_format($bytes / 1048576, 2) . ' MB';
    }
    elseif ($bytes >= 1024)
    {
        $bytes = number_format($bytes / 1024, 2) . ' KB';
    }
    elseif ($bytes > 1)
    {
        $bytes = $bytes . ' bytes';
    }
    elseif ($bytes == 1)
    {
        $bytes = $bytes . ' byte';
    }
    else
    {
        $bytes = '0 bytes';
    }

    return $bytes;
}

function remove_array_values( $item, $exists_array )
{
    $items = is_array($item) ? $item : [$item];

    $tmp = [];
    foreach ($items as $v) {
        unset($exists_array[$v]);
        $tmp = $exists_array;
    }

    return $tmp;
}

function safeSerialize($string)
{
    return base64_encode( serialize($string) );
}

function safeUnserialize($string) {
    return unserialize( base64_decode($string) );
}

function friendly_url($string){
    $string = str_replace(array('[\', \']'), '', $string);
    $string = preg_replace('/\[.*\]/U', '', $string);
    $string = preg_replace('/&(amp;)?#?[a-z0-9]+;/i', '-', $string);
    $string = htmlentities($string, ENT_COMPAT, 'utf-8');
    $string = preg_replace('/&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);/i', '\\1', $string );
    $string = preg_replace(array('/[^a-z0-9]/i', '/[-]+/') , '-', $string);
    return strtolower(trim($string, '-'));
}

function clean($string) {
    $string = str_replace(' ', '-', $string);
    $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
    return preg_replace('/-+/', '-', $string);
}

function remove_dash($string) {     
    return str_replace('-', ' ', $string);
}

function generateRandomString($length = 15) {
    $validCharacters = "abcdefghijklmnopqrstuxyvwzABCDEFGHIJKLMNOPQRSTUXYVWZ12345679";
    $validCharNumber = strlen($validCharacters);
 
    $result = "";
 
    for ($i = 0; $i < $length; $i++) {
        $index = mt_rand(0, $validCharNumber - 1);
        $result .= $validCharacters[$index];
    }
 
    return $result;
}

if ( !function_exists( 'calculateOfProductRating' ) ){
    function calculateOfProductRating($rate, $divide = 5) {
        return ($rate/$divide*100);
    }
}

if ( !function_exists( 'formatDate' ) ){
    function formatDate($format,$date) {
        return date($format, strtotime($date) );
    }
}

/**
 * Take both integer values in points methods
 */
if ( !function_exists( 'formatPoints' ) ) {
    function formatPoints($value, $digits = 0 ) {
        return number_format($value, $digits );
    }
}


/**
 * Take both numeric values & return value in percentage
 */
if ( !function_exists( 'calculatePercentage' ) ) {
    function calculatePercentage($value, $price, $digits='2') {
        return sprintf( '%.' . intval($digits) . 'f', ( 100 - ($price*100/$value) ) );
    }
}
if ( !function_exists( 'formatPercentage' ) ){
    function formatPercentage( $value, $digits='2' ) {
        return sprintf( '%.' . intval($digits) . 'f', $value ) . '%';
    }
}


/* Adding DateTime for DateFormat */
if ( !function_exists( 'addingByDateTime' ) ) {
    function addingByDateTime($date, $days = '1 day', $format = 'jS M, Y') {
        $date = new DateTime($date);
        $date->modify('+'.$days);
        
        return $date->format($format);
    }
}

if ( !function_exists( 'getPercentage' ) ) {
    function getPercentage($value, $price) {
        return formatPercentage( calculatePercentage( $value, $price ) );
    }
}

if ( !function_exists( 'removePriceFormat' ) ) {
    function removePriceFormat($value) {
        return number_format($value);
    }
}

if ( !function_exists( 'shippingPolicyRules' ) ) {

    /*
    - Above 299DHMS- delivery will be free 
    - Above $80- delivery will be free)
    - Order equal to 100DHMS- 15DHMS delivery charges
    - Order Equal to $27- $4 delivery charges
    - Order more than 299DHMS and less than 100DHMS - 12DHMS delivery charges
    - Order more than $80 and less than $27- $3 delivery charges
     */
    function shippingPolicyRules($amount) {

        $symbol     = 'AED';
        $conversion = 1;

        $CI         =& get_instance();
        $currency   = $CI->session->userdata('currency');

        if ( $currency )
        {
            $symbol     = $currency['symbols'];
            $conversion = $currency['amount'];
        }

        // $calc = ($amount * $conversion);

        $shipping_charges = '0.00';

        if ( $symbol == 'AED' )
        {
            $shipping_charges = '0';

            // Order equal to 100DHMS- 15DHMS delivery charges
            if ( round($amount) <= 99 )
            {
                $shipping_charges = '15';
            
            }

            // Order more than 299DHMS and less than 100DHMS - 12DHMS delivery charges
            else if ( round($amount) < 299 ) 
            {
                $shipping_charges = '12';
            }
        } 
        else if ( $symbol == 'USD' )
        {
            // Above $80- delivery will be free)
            
            // Order Equal to $27- $4 delivery charges
            if ( round($amount) <= 27 )
            {
                $shipping_charges = '4.00';
            }
            
            // Order more than $80 and less than $27- $3 delivery charges
            else if ( round($amount) <= 80 )
            {
                $shipping_charges = '3.00';
            }
        }

        return $shipping_charges;
    }
}

if ( !function_exists( 'formatCurrency' ) ){
    function formatCurrency($amount, $digits=2){        

        $symbol     = 'AED';
        $conversion = 1;  

        $CI =& get_instance();  //get instance, access the CI superobject
        $currency = $CI->session->userdata('currency');

        if ( $currency )
        {
            $symbol     = $currency['symbols'];
            $conversion = $currency['amount'];
        }

        $calc = ($amount * $conversion);
        return $symbol.' ' . number_format( $calc, $digits, '.', ',' );
    }
}

if ( !function_exists( 'getSelectFormatCurrency' ) ){
    function getSelectFormatCurrency($amount){        

        $symbol     = 'AED';

        $CI =& get_instance();  //get instance, access the CI superobject
        $currency = $CI->session->userdata('currency');

        if ( $currency )
        {
            $symbol     = $currency['symbols'];
        }

        return sprintf('%s %s', $symbol, $amount);
    }
}

if ( !function_exists( 'formatCurrencyByOrders' ) ){
    function formatCurrencyByOrders($amount, $symbol){
        return sprintf('%s %s', $symbol, $amount);
    }
}

if ( !function_exists( 'currencyConvertToAmount' ) ){
    function currencyConvertToAmount($amount, $digits=2, $round = false ){
        
        $symbol     = 'AED';
        $conversion = 1;  

        $CI =& get_instance();
        $currency = $CI->session->userdata('currency');

        if ( $currency )
        {
            $symbol     = $currency['symbols'];
            $conversion = $currency['amount'];
        }

        $calc = ($amount * $conversion);

        if ( $round )
        {
            return round($calc);
        }
        
        return number_format((float) $calc, $digits, '.', '');
    }
}

if ( !function_exists( 'calculatOfPayTo' ) ){
    function calculatOfPayTo($amount){
        
        $calc = (100*$amount);        
        return round($calc);
    }
}

if ( !function_exists( 'calculatOfReversePayTo' ) ){
    function calculatOfReversePayTo($amount){
        
        $calc = ($amount/100);        
        return round($calc);
    }
}

if ( !function_exists( 'calculateOfSavePercentage' ) ){
    function calculateOfSavePercentage($new_price, $old_price, $digits=0){        
        return number_format( ((($old_price-$new_price)/$old_price)*100), $digits, '.', ',' ). '%';
    }
}

if ( !function_exists( 'calculateOfExpiryDate' ) ){

	function calculateOfExpiryDate($expire_date)
	{
		
		//$date1 = new DateTime("2017-03-06");
		//$date2 = new DateTime("2017-03-09");
		//$diff  = $date2->diff($date1)->invert;

		return true;
	}
}
if ( !function_exists( 'seoURL' ) ){
    function seoURL($word){        
        return url_title($word, '-', TRUE);
    }   
}


if ( !function_exists('caseGenerator')){
    function caseGenerator( $items = array(), $default = '' ){

        if ( count($items) > 0 && $default ) {

            $filters = array();
            foreach ($items as $key => $value) {
                if ( $default == $value ) {
                    $filters[] = $key;
                }                
            }
            return  ( !empty( $filters ) ? current($filters) : false );   
        }
        return false;
    }
}

function buildTree($elements, $parentId = 0) {

    $refs = [];
    $list = [];

    foreach ($elements as $row) {
        
        $ref = &$refs[$row['id']];

        $ref['id']          = $row['id'];
        $ref['parent_id']   = $row['parent_id'];
        $ref['title']       = $row['title'];

        if( isset($row['slug']) && $row['slug'] ){
            $ref['slug']        = $row['slug'];    
        }

        if ($row['parent_id'] == $parentId ) {
            $list[$row['id']] = &$ref;
        } else {
            $refs[$row['parent_id']]['children'][$row['id']] = &$ref;
        }
    }

    return $list;
}

function curPageURL() {
    $pageURL = 'http';
    if ( isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on")
        $pageURL .= "s";
        
    $pageURL .= "://";    
    if ($_SERVER["SERVER_PORT"] != "80") {
        $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
    } else {
        $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
    }

    return $pageURL;
}

if ( !function_exists( 'setupPaypal' ) ){
    function setupPaypal(){        
        $CI =& get_instance(); 

        $CI->load->helper('url'); // Load helpers
        $CI->config->load('paypal'); // Load PayPal library
        
        $config = array(
            'Sandbox' => $CI->config->item('Sandbox'),
            'APIUsername' => $CI->config->item('APIUsername'),
            'APIPassword' => $CI->config->item('APIPassword'),
            'APISignature' => $CI->config->item('APISignature'),
            'APISubject' => '',
            'APIVersion' => $CI->config->item('APIVersion') 
        );

        $CI->load->library('paypal/Paypal_pro', $config);
    }
}

function random_order_number() {
    return 'DC-' . strtoupper( generateRandomString( 6 ) );
}

function salt_password($password, $saltkey = ''){
	return  sha1( md5($password. ($saltkey ? $saltkey : '')) );
}


if( !function_exists("template_pagenation") ){
    function template_pagenation($page_url = '', $pageno = 0, $total_rows = 0, $limit = 0 , $returnpage = FALSE ) {
        $CI =& get_instance();
        $CI->load->library('pagination');

        if ( '' == $page_url ){
            exit('page url is missing');
        }

        $config['total_rows']        = $total_rows;
        $config['per_page']          = $limit;
        $config['base_url']          = base_url($page_url.'/page/');
        $config['display_pages']     = TRUE;
        $config['page_query_string'] = false;
        $config['use_page_numbers']  = TRUE;
        $config['num_links']         = 2;
        $config['suffix']            = '.html';

        /* Full Tag */
        $config['full_tag_open']     = '<ul class="pagination dpTable mauto TopPad-15">';
        $config['full_tag_close']    = '</ul>';

        /* Digital Link */
        $config['num_tag_open']      = '<li>';
        $config['num_tag_close']     = '</li>';

        /* Current Page */
        $config['cur_tag_open']      = '<li class="active"><a href="javascript:void()">';
        $config['cur_tag_close']     = '</a></li>';

        /* First Link */
        $config['first_link']         = 'First';
        $config['first_tag_open']     = '<li>';
        $config['first_tag_close']    = '</li>';

        /* Previous Page */
        $config['prev_link']         = '<i class="fa fa-chevron-left"></i>';
        $config['prev_tag_open']     = '<li>';
        $config['prev_tag_close']    = '</li>';

        /* Next Link */
        $config['first_url']         = site_url($page_url);
        $config['next_link']         = '<i class="fa fa-chevron-right"></i>';
        $config['next_tag_open']     = '<li>';
        $config['next_tag_close']    = '</li>';

        /* Last Link */
        $config['last_link']         = 'Last';
        $config['last_tag_open']     = '<li>';
        $config['last_tag_close']    = '</li>';

        /*Global Variables*/
        $data['page']          = $pageno;
        $data['total_rows']    = $total_rows;
        $data['total_page']    = ceil($total_rows/$limit);
        $data['page_location'] = base_url($page_url.'/page/');

        $CI->pagination->initialize($config);

        return ( $returnpage ? $CI->pagination->create_links() : '' ) ;
    }
}


if( !function_exists("template_query_pagenation") ){
    function template_query_pagenation($page_url = '', $pageno = 0, $total_rows = 0, $limit = 0 , $default_url = '' , $string_segments = 'page' ) {
        $CI =& get_instance();
        $CI->load->library('pagination');

        if ( '' == $page_url ){
            exit('page url is missing');
        }

        $config['total_rows']           = $total_rows;
        $config['per_page']             = $limit;
        $config['base_url']             = base_url($page_url);
        $config['display_pages']        = TRUE;
        $config['page_query_string']    = TRUE;
        $config['use_page_numbers']     = TRUE;
        $config['num_links']            = 2;
        $config['suffix']               = FALSE;
        $config['enable_query_strings'] = TRUE;
        $config['query_string_segment'] = $string_segments;


        /* Full Tag */
        $config['full_tag_open']     = '<ul class="pagination dpTable mauto TopPad-15">';
        $config['full_tag_close']    = '</ul>';

        /* Digital Link */
        $config['num_tag_open']      = '<li>';
        $config['num_tag_close']     = '</li>';

        /* Current Page */
        $config['cur_tag_open']      = '<li class="active"><a href="javascript:void()">';
        $config['cur_tag_close']     = '</a></li>';

        /* First Link */
        $config['first_url']          = $default_url; 
        $config['first_tag_open']     = '<li>';
        $config['first_tag_close']    = '</li>';

        /* Previous Page */
        $config['prev_link']         = '<i class="fa fa-chevron-left"></i>';
        $config['prev_tag_open']     = '<li>';
        $config['prev_tag_close']    = '</li>';


        /* Next Link */                
        $config['next_link']         = '<i class="fa fa-chevron-right"></i>';
        $config['next_tag_open']     = '<li>';
        $config['next_tag_close']    = '</li>';

        /* Last Link */
        $config['last_link']         = 'Last';
        $config['last_tag_open']     = '<li>';
        $config['last_tag_close']    = '</li>';

        /*Global Variables*/
        //$data['page']          = $pageno;
        $data['total_rows']    = $total_rows;
        $data['total_page']    = ceil($total_rows/$limit);

        $CI->pagination->initialize($config);

        return $CI->pagination->create_links();
    }
}

/**
* Replace tags
* @function parse_replace_tag
* @param (string), (option)
* @return (string)
* @author GiftCardBid - Team
**/
if ( !function_exists( 'parse_replace_tag' ) ) {
    function parse_replace_tag($string = '', $class = ''){
        $parse = '';
        if (!empty ( $string ) ) {
            if ( preg_match('/(span)/', $string) ){
                $parse = str_replace(array('[span]', '[/span]'), array('<span'.( !empty($class) ? ' class="'.$class.'"' : ''  ).'>', '</span>'), $string);
            } else if ( preg_match('/(b)/', $string) ){
                $parse = str_replace(array('[b]', '[/b]'), array('<b'.( !empty($class) ? ' class="'.$class.'"' : ''  ).'>', '</b>'), $string);
            }
        }

        return $parse;
    }
}


/**
* Explode Array
* @param (array), (string)
* @function parse_implode_array
* @return string
* @author GiftCardBid - Team
**/
if ( !function_exists( 'parse_implode_array' ) ) {
    function parse_implode_array($stringArray = array(), $explodeType = ',', $return_parse = ''){        
        if ( is_array( $stringArray )&&count($stringArray) > 0 ) {            
            $return_parse = implode($explodeType,$stringArray);
        }
        return $return_parse;
    }
}

/**
 * default date format function
 *
 * @return (string)
 **/
function escape_valid_dateFormat($dateFormat) {    
    if ( $dateFormat=='0000-00-00 00:00:00' or $dateFormat=='0000-00-00' or $dateFormat=='00-00-00' or $dateFormat=='' or $dateFormat=='Jan 01, 1970 01:00 AM' || $dateFormat=='Jan 01, 1970' ){
        return '<i>(not yet)</i>';
    } 

    return $dateFormat;
}

function generatePayPalPayLink( $token ) {
    $CI =& get_instance();
    $CI->load->config('paypal');

    return $CI->config->item('Sandbox') ? 'https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&useraction=continue&token=' . $token : 'https://www.paypal.com/cgi-bin/webscr?cmd=_express-checkout&useraction=continue&token=' . $token;

}

if ( ! function_exists( 'dump' ) ) {
    function dump( $var, $exit = false, $label = false, $echo = true ) {

        if(ENVIRONMENT != 'development') return;

        // Store dump in variable
        ob_start();
        var_dump( $var );
        $output = ob_get_clean();
        $label  = $label ? $label . ' ' : '';

        // Location and line-number
        $line      = '';
        $separator = "<strong style='color:blue'>" . str_repeat( "-", 100 ) . "</strong>" . PHP_EOL;
        $caller    = debug_backtrace();
        if ( count( $caller ) > 0 ) {
            $tmp_r = $caller[ 0 ];
            $line .= "<strong style='color:blue'>Location:</strong> => <span style='color:red'>" . $tmp_r[ 'file' ] . '</span>';
            $line .= " (" . $tmp_r[ 'line' ] . ')';
        }

        // Add formatting
        $output = preg_replace( "/\]\=\>\n(\s+)/m", "] => ", $output );
        $output = '<pre style="background: #FFFEEF; color: #000; border: 1px dotted #000; padding: 10px; margin: 10px 0; text-align: left;">'
            . $label
            . $line
            . PHP_EOL
            . $separator
            . $output
            . '</pre>';

        // Output
        if ( $echo == true ) {
            echo $output;

            if ( $exit ) {
                die();
            }
        } else {
            return $output;
        }
    }
}