<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * All in one helper
 */

//Global Variables


if ( ! function_exists('defaultValue') ) {
    function defaultValue($str, $default='') {
        if ( $str == '' || $str == NULL || $str == FALSE ) {
            return $default;
        }

        return $str;
    }
}


if ( ! function_exists( 'v' ) ) {
    function v($array,$option='e') {
        var_dump($array);
        if($option=='e')
        {
        exit;
        }
    }
}
    
if ( ! function_exists( 'r' ) ) {
    function r($array,$force=false,$option='e') {
        if( ENVIRONMENT != 'development' && $force == false ) {
            return;
        }
        echo '<pre>';
        // print_r($array);
        $caller = debug_backtrace();
        $separator = "<strong style='color:blue'>".str_repeat("-", 100) ."</strong>".PHP_EOL;
        $line = "<pre>";
        if (count( $caller ) > 0) {
            $tmp_r = $caller[0];
            $line .= "<strong style='color:blue'>Location:</strong> => <span style='color:red'>" . $tmp_r['file'] . '</span>';
            $line .= " (" . $tmp_r['line'] . ')';
        }
        $line .= PHP_EOL;
        $line .= $separator;
        $line .= print_r($array,1);
        $line .= PHP_EOL . $separator;
        echo $line;
        if($option=='e'&&$option!==0)
        {
        exit;
        }
    }
}


/**
 * Last Query function
 *
 * @return (bool) optional
 * @author Junaid Ahmed
 **/
if ( !function_exists("lastQuery") ){
    function lastQuery( $bool = false ) {
        echo "<pre>";       
        $CI =&get_instance(); 
        print_r( $CI->db->last_query() );
        echo "</pre>";

        return ( ( $bool ) ? exit() : '' );
    }
}

/**
 * Database Prefix function
 *
 * @return (string) or (bool)
 * @author Junaid Ahmed
 **/
if ( !function_exists("prefix") ){
    function prefix( $table = '' ) {
        $CI =&get_instance();
        return ( ( $table ) ? $CI->db->dbprefix($table) : false );
    }
}

if ( ! function_exists( 'rd' ) ) {
    function rd( $var, $exit = true, $force = false, $label = false, $echo = true ) {

        if( ENVIRONMENT != 'development' && $force == false ) {
            return;
        }

        // Store dump in variable
        ob_start();
        if (!is_bool($var))
            print_r($var);
        else
            var_dump( $var );

        $output = ob_get_clean();
        $label  = $label ? $label . ' ' : '';

        // Location and line-number
        $line      = '';
        $separator = "<strong style='color:blue'>" . str_repeat( "-", 100 ) . "</strong>" . PHP_EOL;
        $caller    = debug_backtrace();
        if ( count( $caller ) > 0 ) {
            $tmp_r = $caller[ 0 ];
            $line .= "<strong style='color:blue'>Location:</strong> => <span style='color:red'>" . $tmp_r[ 'file' ] . '</span>';
            $line .= " (" . $tmp_r[ 'line' ] . ')';
        }

        // Add formatting
        $output = preg_replace( "/\]\=\>\n(\s+)/m", "] => ", $output );
        $output = '<pre style="background: #FFFEEF; color: #000; border: 1px dotted #000; padding: 10px; margin: 10px 0; text-align: left;">'
            . $label
            . $line
            . PHP_EOL
            . $separator
            . $output
            . '</pre>';

        // Output
        if ( $echo == true ) {
            echo $output;

            if ( $exit ) {
                die();
            }
        } else {
            return $output;
        }
    }
}

if ( ! function_exists('HTML_validation_errors') ) {
    function HTML_validation_errors() {
        return nl2br(trim(strip_tags( validation_errors(), '\n' )));
    }
}

/**
 * Generate Array to MySQL Case Converter
 * Mostly used on Record status (tinyint)
 * @example: array('0'=>'Disable', '1'=>'Enable')
 */
if ( ! function_exists('MySQLCaseGenerator') ) {
    function MySQLCaseGenerator($array, $column, $alias='') {
        if ( is_array($array) && count($array)>0 && $column != '' ) {
            $alias || $alias = $column; // Rewrite alias

            // Generate SQL Query for ROLE Alias
            $role_array = array_map(function($v, $k) {
                return "WHEN '$k' THEN '$v'";
            }, $array, array_keys($array));

            return "CASE($column) " . implode(' ', $role_array) . " END AS " . $alias;
        }

        return $column;
    }
}

/**
 * Most required array helper but not-available on native.
 */
if ( ! function_exists('array_index_map') ) {
    function array_index_map($array, $index, $remove_index = false) {
        $newArray = array();
        foreach ($array as $key => $arr) {
            if ( isset($arr[$index]) ) {
                $_tmp = $arr[$index];
                
                if ( $remove_index ) {
                    unset($arr[$index]);
                }

                $newArray[ $_tmp ] = $arr;
            }
        }

        return $newArray;
    }
}


/**
 * Most required array helper but not-available on native.
 */
if ( ! function_exists('group_array_index_map') ) {
    function group_array_index_map($array, $index, $remove_index = false) {
        
        $newArray = array();
        foreach ($array as $key => $arr) {
            if ( isset($arr[$index]) ) {
                
                $_tmp = $arr[$index];
                
                if ( $remove_index ) {
                    unset($arr[$index]);
                }

                $newArray[ $_tmp ][] = $arr;
            }
        }

        return $newArray;
    }
}

/**
 * Most required array helper but not-available on native.
 * It supports dimensional array, for example: Index value exist more than one then it'll craete dimensional array
 */
if ( ! function_exists('array_index_map_multi') ) {
    function array_index_map_multi($array, $index, $remove_index = false) {
        $newArray = array();
        foreach ( (array) $array as $key => $arr) {
            if ( isset($arr[$index]) ) {
                $_tmp = $arr[$index];
                
                if ( $remove_index ) {
                    unset($arr[$index]);
                }

                $newArray[ $_tmp ][] = $arr;
            }
        }

        return $newArray;
    }
}

/**
 * Most required object helper but not-available on native.
 */
if ( ! function_exists('object_index_map') ) {
    function object_index_map($object, $index, $remove_index = false) {
        $newArray = new StdClass;
        foreach ($object as $key => $obj) {
            if ( isset($obj->$index) ) {
                $_tmp = $obj->$index;
                
                if ( $remove_index ) {
                    unset($obj->$index);
                }

                $newArray->$_tmp = $obj;
            }
        }

        return $newArray;
    }
}

/**
 * Simple function to detect if file exist
 */
if ( ! function_exists('isImageExist') ) {
    function isImageExist($file, $noimage){
        return is_file($file) ? $file : $noimage;
    }
}

if ( ! function_exists('isRemoteImage') ) {
    function isRemoteImage( $imageURL ){
        $headers = array(
            "Range: bytes=0-32768"
        );
        $curl = curl_init($imageURL);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $data = curl_exec($curl);
        curl_close($curl);

        return (FALSE !== imagecreatefromstring($data));
    }
}

if ( ! function_exists('isNotEmpty') ) {
    function isNotEmpty($array, $index, $keyword) {
        $count = 0;
        foreach ($array as $single_array) {
            if ( $single_array->{$index} == $keyword ) {
                $count++;
            }
        }
        return ($count>0) ? TRUE : FALSE;
    }
}

if ( !function_exists('secure_site_url') ) {
    function secure_site_url( $uri = '' ) {
        return str_replace( 'https://', 'http://', site_url( $uri ) );
    }
}

if ( !function_exists('secure_base_url') ) {
    function secure_base_url( $uri = '' ) {
        return str_replace( 'https://', 'http://', base_url( $uri ) );
    }
}

if ( ! function_exists('parseString') ) {
    function parseString() {
        $stringPrefix = '%table';
        $stringSuffix = '%';

        $parameters   = func_get_args();
        $string       = array_shift($parameters);
        $replacement  = array_map(function($value, $key) use ($stringPrefix, $stringSuffix) {            
            return $stringPrefix . ($key+1) . $stringSuffix;
        }, $parameters, array_keys($parameters) );

        return str_replace( $replacement, $parameters, $string );
    }
}


if (!function_exists('array_column')) {
    /**
     * Returns the values from a single column of the input array, identified by
     * the $columnKey.
     *
     * Optionally, you may provide an $indexKey to index the values in the returned
     * array by the values from the $indexKey column in the input array.
     *
     * @param array $input A multi-dimensional array (record set) from which to pull
     *                     a column of values.
     * @param mixed $columnKey The column of values to return. This value may be the
     *                         integer key of the column you wish to retrieve, or it
     *                         may be the string key name for an associative array.
     * @param mixed $indexKey (Optional.) The column to use as the index/keys for
     *                        the returned array. This value may be the integer key
     *                        of the column, or it may be the string key name.
     * @return array
     */
    function array_column($input = null, $columnKey = null, $indexKey = null)
    {
        // Using func_get_args() in order to check for proper number of
        // parameters and trigger errors exactly as the built-in array_column()
        // does in PHP 5.5.
        $argc = func_num_args();
        $params = func_get_args();

        if ($argc < 2) {
            trigger_error("array_column() expects at least 2 parameters, {$argc} given", E_USER_WARNING);
            return null;
        }

        if (!is_array($params[0])) {
            trigger_error(
                'array_column() expects parameter 1 to be array, ' . gettype($params[0]) . ' given',
                E_USER_WARNING
            );
            return null;
        }

        if (!is_int($params[1])
            && !is_float($params[1])
            && !is_string($params[1])
            && $params[1] !== null
            && !(is_object($params[1]) && method_exists($params[1], '__toString'))
        ) {
            trigger_error('array_column(): The column key should be either a string or an integer', E_USER_WARNING);
            return false;
        }

        if (isset($params[2])
            && !is_int($params[2])
            && !is_float($params[2])
            && !is_string($params[2])
            && !(is_object($params[2]) && method_exists($params[2], '__toString'))
        ) {
            trigger_error('array_column(): The index key should be either a string or an integer', E_USER_WARNING);
            return false;
        }

        $paramsInput = $params[0];
        $paramsColumnKey = ($params[1] !== null) ? (string) $params[1] : null;

        $paramsIndexKey = null;
        if (isset($params[2])) {
            if (is_float($params[2]) || is_int($params[2])) {
                $paramsIndexKey = (int) $params[2];
            } else {
                $paramsIndexKey = (string) $params[2];
            }
        }

        $resultArray = array();

        foreach ($paramsInput as $row) {
            $key = $value = null;
            $keySet = $valueSet = false;

            if ($paramsIndexKey !== null && array_key_exists($paramsIndexKey, $row)) {
                $keySet = true;
                $key = (string) $row[$paramsIndexKey];
            }

            if ($paramsColumnKey === null) {
                $valueSet = true;
                $value = $row;
            } elseif (is_array($row) && array_key_exists($paramsColumnKey, $row)) {
                $valueSet = true;
                $value = $row[$paramsColumnKey];
            }

            if ($valueSet) {
                if ($keySet) {
                    $resultArray[$key] = $value;
                } else {
                    $resultArray[] = $value;
                }
            }

        }

        return $resultArray;
    }

}

/*
 * cURL Functionality
 * Need explanation?
 */
if ( ! function_exists('cURL') ) {
    function cURL($url, $post=false, $header=false) {
        $ch = curl_init($url);
        if ($post !== false) {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        }
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_HEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        return $result;
    }
}

if ( ! function_exists('leftTrim') ) {
    function leftTrim($str, $removal='') {
        return (substr($str, 0, strlen($removal)) == $removal) ? substr($str, strlen($removal)) : $str;
    }
}

if ( ! function_exists('rightTrim') ) {
    function rightTrim($str, $removal='') {
        return (substr($str, -strlen($removal)) == $removal) ? substr($str, 0, -strlen($removal)) : $str;
    }
}

if ( ! function_exists('currentCallMethod') ) {
    function currentCallMethod() {
        $CI =& get_instance();
        return $CI->router->fetch_method();
    }
}

if ( ! function_exists('currentCallController') ) {
    function currentCallController() {
        $CI =& get_instance();
        return $CI->router->fetch_class();
    }
}

function sendCSVHeaders( $filename ) {
    $filename = $filename . '-export_' . date('d-M-y') . '.csv';

    header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
    header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header('Content-Encoding: UTF-8');
    header('Content-type: text/csv; charset=UTF-8');
    header("Content-Disposition: attachment;filename={$filename}");
    header("Content-Transfer-Encoding: binary");

    ini_set('memory_limit', '512M');
    ini_set('max_execution_time', '180');

    echo "\xEF\xBB\xBF"; // UTF-8 BOM

}

function sendXLSXHeaders( $filename ) {
    $filename = $filename . '-export_' . date('d-M-y') . '.xlsx';

    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="'.$filename.'"');
    header('Cache-Control: max-age=0');

    ini_set('memory_limit', '512M');
    ini_set('max_execution_time', '180');
}

function sendPDFHeaders( $filename ) {
    $filename = $filename . '.pdf';

    header('Content-Type: application/pdf');
    header('Content-Disposition: attachment;filename="'.$filename.'"');
    header('Cache-Control: max-age=0');

    ini_set('memory_limit', '512M');
    ini_set('max_execution_time', '180');
}