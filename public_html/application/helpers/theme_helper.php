<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/**
 * Global Helper Method
 */
function storage($dir_with_file)
{
	return base_url('storage/'. ltrim($dir_with_file, '/'));
}


/**
 * Frontend Helper Method
 */
function include_frontend_component($view_name, $data = [])
{
    // return include(APPPATH.'views/frontend/components/'.$view_name.'.php');

    $CI =& get_instance();
    return $CI->load->view('frontend/components/'.$view_name, $data);
}


function load_frontend_components($view_name, $data = []){

    $CI =& get_instance();
    return $CI->load->view('frontend/components/'.$view_name, $data);
}

function load_frontend_view($view_name, $data = []){

    $CI =& get_instance();
    return $CI->load->view('frontend/'.ltrim($view_name, '/'), $data);
}



/**
 * Backend Helper Method
 */

function link_to_backend( $link )
{
	return site_url('/backend/' . $link);
}

function include_backend_self($view_name)
{
    return include(APPPATH.'views/backend/default/'.$view_name.'.php');
}

function include_backend_component($view_name)
{
    return include(APPPATH.'views/backend/default/components/'.$view_name.'.php');
}
