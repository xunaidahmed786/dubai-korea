<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


function not_available_for_table()
{
	echo '<small style="font-size:12px;"><i>(Not Available)</i></small>';
}

function is_status_visibility( $bool )
{
	return ( $bool ? 'Yes' : 'No' );
}

function backend_amount_sign($sign = null)
{
	return ($sign ? $sign : 'AED');
}

function backend_amount_format( $amount, $sign = null )
{
	return sprintf('%s %s', $sign, number_format($amount, 2));
}

function check_amount( $amount )
{
	return ( ($amount=='0.00' || $amount=='0') ? FALSE : TRUE );
}

function backend_image_exists( $image )
{	
	$default = '';

	if ( $image ){
		$default = 	base_url('storage/products/'. $image);	
	}

	return $default;
}

function remove_image_from_dir($source_arr, $dir)
{
	$source = is_array($source_arr) ? $source_arr : [$source_arr];

	foreach ($source as $image) {
		@unlink( FCPATH . 'storage/'. $dir . '/' . $image  );
	}
}

function action_buttons($module, $id) 
{

	$html = '
	<div class="btn-group">
	  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
		<i class="fa fa-cog"></i> Action <span class="caret"></span>
	  </button>
	  <ul class="dropdown-menu primary" role="menu">
		<li><a href="'.link_to_backend($module.'/edit/'.$id).'">Edit</a></li>
		<li><a href="'.link_to_backend($module.'/delete/'.$id).'">Delete</a></li>
	  </ul>
	</div>';

	return $html;
}

function alert_messages($alert) {

	$html = '';
	switch ($alert['alert-type']) {
		case 'success':
			$html = '<div class="alert alert-success">'.$alert['message'].' '. 
			(isset($alert['link_name']) ? '<a href="'.$alert['link_url'].'" class="alert-link">'.$alert['link_name'].'</a>' : '') .' </div>';
			break;

		case 'info':
			$html = '<div class="alert alert-info">'.$alert['message'].' '. 
			(isset($alert['link_name']) ? '<a href="'.$alert['link_url'].'" class="alert-link">'.$alert['link_name'].'</a>' : '') .' </div>';
			break;

		case 'warning':
			$html = '<div class="alert alert-warning">'.$alert['message'].' '. 
			(isset($alert['link_name']) ? '<a href="'.$alert['link_url'].'" class="alert-link">'.$alert['link_name'].'</a>' : '') .' </div>';
			break;

		case 'danger':
			$html = '<div class="alert alert-danger">'.$alert['message'].' '. 
			(isset($alert['link_name']) ? '<a href="'.$alert['link_url'].'" class="alert-link">'.$alert['link_name'].'</a>' : '') .' </div>';
			break;
		
		default:
			$html = '<div class="alert alert-danger nomargin">Lorem ipsum dolor sit amet <a href="#" class="alert-link">Alert Link</a>.</div>';
			break;
	}

	return $html;
}