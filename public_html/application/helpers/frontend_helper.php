<?php

function set_of_title_and_meta_keywords($title, $tags = []){

	return [
		'site_title' => $title,
		'properties' =>
		[
			'title' 		 => $title,
			'description'	 => ( isset($tags['description']) ? $tags['description'] : null),
			'keywords'	 	 => ( isset($tags['keywords']) ? $tags['keywords'] : null),
			'url'	 		 => ( isset($tags['url']) ? $tags['url'] : null),
			'image' 		 => ( isset($tags['image']) ? $tags['image'] : null),
		]
	];
}