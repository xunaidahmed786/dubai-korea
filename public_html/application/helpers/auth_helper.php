<?php


function ci(){
	$CI =& get_instance();
	return $CI;
}

function check_login(){
	$CI = ci();
	return $CI->session->userdata('logged_in');
}

function auth(){

	if ( check_login() ){
		$CI = ci();
		return $CI->session->userdata('auth');		
	}
}

function first_name(){
	return auth()->first_name;
}

function last_name(){
	return auth()->last_name;
}