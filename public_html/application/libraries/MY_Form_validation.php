<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Form_validation extends CI_Form_validation {

    public function __construct($rules = array()) {
        parent::__construct($rules);
    }

    /**
     * Error Array
     *
     * Returns the error messages as an array
     *
     * @return  array
     */
    function error_array()
    {
        if (count($this->_error_array) === 0)
        {
            return array();
        }
        return $this->_error_array;
    }

    /**
     * Return the data after validation
     *
     * @return  array
     */
    public function field_data($field = NULL)
    {
        if($field)
        {
            return isset($_POST[$field]) ? $_POST[$field] : '';
        }

        $return = array();
        if(is_array($this->_field_data))
        {
            foreach($this->_field_data as $key => $val)
            {
                if ($val['is_array'] == TRUE)
                {
                    $return[$val['keys'][0]] = $this->field_data($val['keys'][0]);
                }
                else
                {
                    $return[$key] = isset($_POST[$key]) ? $_POST[$key] : '';
                }
            }
        }
        return $return;
    }

    /**
     * Check for valid Full Name
     *
     * @access public
     * @return bool
     */
    public function valid_fullname($str) {
        if ( preg_match('/^[A-Za-z\s\.]+$/', $str) )
            return TRUE;

        $this->set_message('valid_fullname', 'Full Name is not valid!');
        return FALSE;
    }

    /**
     * Check for valid Phone Number
     *
     * @access public
     * @return bool
     */
    public function valid_phone($str) {
        if ( preg_match('/^[\+0-9\(\)\.-]+$/', $str) )
            return TRUE;

        $this->set_message('valid_phone', 'Phone Number may only contain ( ).-+ and numbers.');
        return FALSE;
    }

    /**
     * Check for valid Phone Number
     *
     * @access public
     * @return bool
     */
    public function valid_usphone($str) {
        if ( preg_match('/^\([0-9]{3}\)\s[0-9]{3}-[0-9]{4}$/', $str) )
            return TRUE;

        $this->set_message('valid_usphone', 'Mobile Number may only contain parenthesis, space, hyphen and numbers.');
        return FALSE;
    }

    /**
     * Check for Alpha Numeric Phone
     *
     * @access public
     * @return bool
     */
    public function alphanum_space($str) {
        if ( preg_match('/^[0-9A-Za-z\s]+$/', $str) )
            return TRUE;

        $this->set_message('alphanum_space', '%s only allowed Alpha Numeric with space!');
        return FALSE;
    }

    /**
     * Check for unique value except mentioned where clause row
     * Mostly used for update email or phone number not having more than one!
     *
     * @access public
     * @return bool
     */
    public function unique_other_than($str, $data) {
        list($field, $col_value)=explode(',', $data);
        list($table, $field)=explode('.', $field);
        list($col, $value)=explode('=', $col_value);
        $query = $this->CI->db->limit(1)->where($col.' !=', $value, false)->get_where($table, array($field => $str));

        if ( $query->num_rows() === 0 )
            return TRUE;

        $this->set_message('unique_other_than', '%s already exist in our system!');
        return FALSE;
    }

    /**
     * Detect if single row exist on database table as per where clause
     *
     * @access public
     * @return bool
     */
    public function single_exist($str, $field) {
        list($table, $field)=explode('.', $field);
        $query = $this->CI->db->limit(1)->get_where($table, array($field => $str));

        $this->set_message('single_exist', '%s record not exist!');

        return $query->num_rows() === 1;
    }

    /**
     * Detect if rows (usually more than one) exist on database table as per where clause
     *
     * @access public
     * @return bool
     */
    public function multi_exist($str, $field) {
        list($table, $field)=explode('.', $field);
        $query = $this->CI->db->get_where($table, array($field => $str));

        $this->set_message('multi_exist', '%s record not exist!');

        return $query->num_rows() > 1;
    }

    /**
     * Validate if input contain only allowed characters?
     *
     * @access public
     * @return bool
     */
    public function allowed_char($str, $allowed) {
        $this->set_message('allowed_char', '%s is invalid!');
        return (bool) in_array( $str, str_getcsv($allowed), TRUE );
    }

    /**
     * Validate dateformat
     */
    public function valid_dateformat($date, $format) {
        $dateInstance = DateTime::createFromFormat( $format, $date );
        $this->set_message('valid_dateformat', '%s not in valid format!');

        return ( $dateInstance && $dateInstance->format( $format ) == $date );
    }

    public function array_not_empty($str) {
        rd(func_get_args());
        return (bool) (is_array($str) && count($str)==0);
    }
    
    /**
     * undocumented function
     *
     * Method Name        : valid_range
     * @author              Junaid Ahmed
     * Method Created       Nov, 2015
     * Calling Arguments  : INPUT, PARAM range[0-100]
     **/
    public function valid_range($str, $param){

        $valid_range = ( preg_match('/\d-\d/', $param) ? $param : false );

        if ( $valid_range )
        {
            $range_expload = explode('-', $param);            
            if ( $range_expload[0] < $str && $str  <= $range_expload[1]  )
            {                
                return TRUE;
            }
            
            $this->set_message('valid_range', 'The %s field must be in range '.$range_expload[0].' to '.$range_expload[1]);
            return FALSE;
        }

        $this->set_message('valid_range', 'The %s field validation has failed');
        return $valid_range;        
    }

    /**
     * Validate Saller Card Type
     * @Developed project helper
     * @author GiftCardBid
     */
    public function valid_cardType($str) {

        if ( strtolower( $str ) == 'auction' ) {
            if  (  $this->CI->input->post('cardPrice') == '' || $this->CI->input->post('expiryCard') == ''  ){
                $this->set_message('valid_cardType', 'Reserved Price and Expiry datetime is required.' );
                return false;
            }
        } else {
            if  (  $this->CI->input->post('cardPrice') == '' ) {
                $this->set_message('valid_cardType', 'Reserved Price is required.' );
                return false;
            }
        }

        return true;
    }

    /**
     * Validate Set Alert Price
     * @Developed project helper
     * @author GiftCardBid
     */
    public function valid_setAlertRange() {

        $priceRange      = explode('|', $this->CI->input->post('max_range'));
        $percentageRange = explode('|', $this->CI->input->post('range'));

        if ( $priceRange[0] < 0 || $priceRange[1] > MAXIMUM_PRICE_RANGE ){
            $this->set_message('valid_setAlertRange', 'You entered invalid Price Range.' );
            return false;
        }

        if ( $percentageRange[0] < 0 || $percentageRange[1] > 100 ){
            $this->set_message('valid_setAlertRange', 'You entered invalid Percentage Range.' );
            return false;
        }

        return true;
    }

}

/* End of file MY_Form_validation.php */
/* Location: ./application/libraries/MY_Form_validation.php */