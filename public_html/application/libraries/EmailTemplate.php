<?php

class EmailTemplate {

    private $CI;

    /**
     * Container of all error messages
     * @var array
     */
    private $_errorMessages = array();

    /**
     * Default Color & Texts for Model
     */
    private $_primaryHighlightColor = '#469C23';

    /**
     * Array of data to pass on email template
     * @var array
     */
    public $templateArray = array();

    public function __construct() {
        $this->CI =& get_instance();
    }

    public function setSalutation( $greet, $name, $tinyText = '' ) {
        $this->templateArray['salutation']['greeting'] = $greet;
        $this->templateArray['salutation']['name']     = $name;

        if ( $tinyText != '' ) {
            $this->templateArray['salutation']['tinyText'] = $tinyText;
        }

        return $this;
    }

    public function setHighlightHeading( $text ) {
        $this->templateArray['highlightHeading'] = $text;

        return $this;
    }

    public function setHighlightText( $text ) {
        $this->templateArray['highlightContent'] = $this->_parseValuesForTemplate( $text );

        return $this;
    }

    public function setOtherMessage( $heading, $text = '' ) {
        $this->templateArray['messageText'][] = array(
            'heading'   =>  $heading
        );

        if ( $text != '' ) {
            $lastIndex = count($this->templateArray['messageText'])-1;

            $this->templateArray['messageText'][ $lastIndex ]['text'] = $text;
        }

        return $this;
    }

    /**
     * Will return generated HTML to send as body
     * @param  boolean $resetContainer By default it will reset property array else it will overwrite values
     * @return string                  Generated HTML body
     */
    public function compile( $resetContainer = TRUE ) {
        $this->_validateRequiredIndices();

        $generatedHTML = $this->CI->load->view( 'template/_blocks/email/email-template.php', array(
            'templateArray'       =>  $this->templateArray
        ), TRUE );

        if ( $resetContainer ) {
            $this->templateArray = array();
        }

        return $generatedHTML;
    }

    public function getErrorMessage() {
        return $this->_errorMessages;
    }

    private function _setErrorMessage($text) {
        array_push( $this->_errorMessages, $text );
    }

    private function _parseValuesForTemplate( $text ) {
        return str_replace( array(
            '{%primaryColor%}',
        ), array(
            $this->_primaryHighlightColor,
        ), $text );
    }

    private function _validateRequiredIndices() {
        if ( isset( $this->templateArray['salutation'] ) ) {
            if ( ! isset( $this->templateArray['salutation']['greeting'] ) ) {
                $this->_setErrorMessage( 'Greeting is missing!' );
                return false;
            }

            if ( ! isset( $this->templateArray['salutation']['name'] ) ) {
                $this->_setErrorMessage( 'Greeting Name is missing!' );
                return false;
            }
        }
    }
}