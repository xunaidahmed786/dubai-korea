<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Yahoo OAuth2 Provider
 *
 * @package    CodeIgniter/OAuth2
 * @category   Provider
 * @author     Phil Sturgeon
 * @copyright  (c) 2012 HappyNinjas Ltd
 * @license    http://philsturgeon.co.uk/code/dbad-license
 */

class OAuth2_Provider_Yahoo extends OAuth2_Provider
{
	/**
	 * @var  string  the method to use when requesting tokens
	 */
	public $method = 'POST';

	/**
	 * @var  string  scope separator, most use "," but some like Yahoo are spaces
	 */
	public $scope_seperator = ' ';

	public function url_authorize()
	{
		return 'https://api.login.yahoo.com/oauth2/request_auth';
	}

	public function url_access_token()
	{
		return 'https://api.login.yahoo.com/oauth2/get_token';
	}

	public function __construct(array $options = array())
	{		
		parent::__construct($options);
	}

	/*
	* Get access to the API
	*
	* @param	string	The access code
	* @return	object	Success or failure along with the response details
	*/	
	public function access($code, $options = array())
	{
		if ($code === null)
		{
			throw new OAuth2_Exception(array('message' => 'Expected Authorization Code from '.ucfirst($this->name).' is missing'));
		}

		return parent::access($code, $options);
	}
}
