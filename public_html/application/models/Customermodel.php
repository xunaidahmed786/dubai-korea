<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customermodel extends CI_Model {

    private $tblCustomer = 'dc_customers';
    
    public function __construct() {
        parent::__construct();
    }

    /**
     * List of Customer Wishlist Product Only ID's
     */
    public function getCustomerWishlistProductIds()
    {
        // rd(auth()->id);

        $product_ids = $this->triggermodel->all(TBL_WISHLIST, [
            'where' => [
                'fk_user_id' => auth()->id
            ],
            'select' => 'fk_product_id'
        ], true, false);

        if ( count($product_ids) )
        {   
            return array_map(function($v){
                return $v['fk_product_id'];
            }, $product_ids);
        }

        return [];
    }
    
    
}

/* End of file Customermodel.php */
/* Location: ./application/models/Customermodel.php */