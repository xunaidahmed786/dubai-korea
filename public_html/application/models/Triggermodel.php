<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Triggermodel extends CI_Model {

    public $use_soft = false ;

    public function __construct() {
        parent::__construct();
    }

    public function last_insert_id()
    {
        return $this->db->insert_id();
    }

    public function insertOrFail($table, $data)
    {

    	return $this->db->insert($table, $data);
    }

    public function insertBatch($table, $data ){
        return $this->db->insert_batch($table, $data);
    }

    public function firstOrFail($table, $attributes = [] )
    {

        $this->db->from($table)->where('deleted_at', NULL);

        if( isset($attributes['where']) && is_array($attributes['where']) ){
            $this->db->where($attributes['where']);
        }

        $this->db->limit(1);

        return $this->db->get()->row();
    }

    public function updateOrFail($table, $data,  $attributes ){

        return $this->db->where($attributes)->update($table, $data); 
    }

    public function count($table, $options = [])
    {
        $query = $this->db->where($options)->where('deleted_at', null)->get($table);

        return $query->num_rows();
    }

    public function all($table, $where = [],  $object = FALSE, $soft_delete = true ){

    	$this->db->from($table);

        if ( $soft_delete )
        {
            $this->db->where('deleted_at', NULL);
        }

    	if( isset($where['select']) && !is_null($where['select']) ){
    		$this->db->select($where['select']);
    	}

    	if( isset($where['where']) && is_array($where['where']) ){
    		$this->db->where($where['where']);
    	}

        if( isset($where['orderBy']) && !is_null($where['orderBy']) ){
            $this->db->order_by('id', $where['orderBy']);
        }

    	$result = $this->db->get();

    	if ( $result->num_rows() ) {

    		if ( $object ) {
    			return $result->result_array();
    		}

    		return $result->result();
    	}

    	return [];
    }

    public function delete($table, $item = null , $option = [] ) {

        if ( count($option) ) 
        {
            $this->db->where($option);
        } 
        else 
        {
            if (is_null($item))
            {
                return false;
            }
            $items = is_array($item) ? $item : [$item];

            $this->db->where_in('id', $items);
        }

        if ( $this->use_soft )
        {

            if ( $items )
            {
                foreach ($items as $i) {
                    $this->db->where('id', $i)->update($table, ['deleted_at'=> date('Y-m-d H:i:s') ]);
                }
            }
            
            return true;
        }
        else
        {
            return $this->db->delete($table);
        }
    }


}

/* End of file Triggermodel.php */
/* Location: ./application/models/Triggermodel.php */