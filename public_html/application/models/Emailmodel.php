<?php

class EmailModel extends CI_Model {

    public function __construct(){
        parent::__construct();
	   
	   $this->load->library('PHPMailer/phpmailer');
  	   $this->emailHost = "localhost";
	   $this->sendFrom = $this->siteSetting['web_title'];
   	   $this->sendFromEmail = $this->siteSetting['admin_email'];
    }

    public function sendEmail($name, $to, $subject, $body, $attach="") {
 		$mail             = new PHPMailer();
   		$mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
												   // 1 = errors and messages
												   // 2 = messages only
        if ( ENVIRONMENT == 'development' ) {
            $mail->IsSMTP();
            $mail->SMTPAuth = true;
            $mail->SMTPSecure = 'tls';
            $mail->Host = "mailtrap.io";
            $mail->Port = 2525;
            $mail->IsHTML(true);
            $mail->Username = "7ac13f93e18ef4";
            $mail->Password = "00fd5906582550";
        } else {
            $mail->IsSMTP();

            // cPanel SMTP Authentication
            /*$mail->SMTPAuth = true; 
            // $mail->SMTPSecure = 'ssl';
            $mail->Host = "mail.giftcardbid.com";
            $mail->Port = 25;
            $mail->IsHTML(true);
            // Account creatd via cPanel
            $mail->Username = "no-reply@giftcardbid.com";
            $mail->Password = "&Oae8ZDXgQio";*/

            // Client's Gmail Account
            $mail->SMTPAuth = true; 
            $mail->SMTPSecure = 'ssl';
            $mail->Host = "smtp.gmail.com";
            $mail->Port = 465;
            $mail->IsHTML(true);
            $mail->Username = "admin@giftcardbid.com";
            $mail->Password = "waleedahsan";

            $this->sendFromEmail = $mail->Username;
        }

        logIt( $body, 'Email: ' . $subject . ' -- To: ' . $to );
        $debug = debug_backtrace();
        logIt( 'Email triggered from: ' . $debug[0]['file'] . '<Br />Line: ' . $debug[0]['line'] . '<Br />Args: ' . print_r($debug[0]['args'], true), 'Email triggered location' );

   		$mail->SetFrom($mail->Username, $this->sendFrom);
		$mail->AddReplyTo($mail->Username, $this->sendFrom);
		$mail->Subject    = $subject;
		$mail->MsgHTML($body);
        $mail->AddAddress($to, $name);
		// $mail->AddAddress('ahsan@fsdsolutions.com', 'Ahsaaan');
        //$mail->AddBCC('ahsan@fsdsolutions.com', 'Ahsaan BCC');

        if ( is_array($attach) ) {
            foreach ( $attach as $file ) {
                $mail->AddAttachment($file);
            }
        } else if ( $attach != "" ) {
			$mail->AddAttachment($attach);
		}

        $send = $mail->Send();

        // var_dump($send);

		if(!$send) {
            return "Error";
		} else {
            return "Sent";
		}
    }


}