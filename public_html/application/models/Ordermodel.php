<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ordermodel extends CI_Model {

    private $tblOrders = 'dc_orders';
    
    public function __construct() {
        parent::__construct();
    }


    public function getCustomerOrders($status = 'pending', $limit = 10, $user_id = NULL )
    {
        $status     = (is_array($status) ? $status : [$status]);
        $user_id    = ( $user_id ? $user_id : auth()->id );

        $sqlQuery   = "SELECT 
            o.id, 
            o.order_number,
            o.order_status,
            o.payment_method,
            o.currency,
            CONCAT( o.currency, ' ',o.tax_charges) AS tax_charges,
            CONCAT( o.currency, ' ',o.shipping_charges) AS shipping_charges,
            CONCAT( o.currency, ' ',o.discount_amount) AS discount_amount,
            CONCAT( o.currency, ' ',o.total_amount) AS total_amount,
            o.payment_token,
            COUNT(oi.fk_product_id) AS total_items,
            t.account_id,
            o.created_at,
            o.updated_at,
            o.shipped_at
        FROM 
        ".$this->tblOrders." o 
        INNER JOIN ".TBL_ORDER_ITEMS." AS oi 
            ON oi.fk_order_id = o.id
        LEFT JOIN ".TBL_TRANSACTION." AS t 
            ON t.fk_order_id = o.id
        WHERE o.order_status in ? AND o.fk_user_id = ? GROUP BY o.id  ORDER BY o.created_at DESC LIMIT ?";

        $execute = $this->db->query($sqlQuery, array($status, $user_id, $limit));

        return $execute->result_array();

    }

    public function getCustomerConfirmedOrders()
    {
        return $this->getCustomerOrders('confirmed');
    }

    public function getCustomerDeliveredOrders()
    {
        return $this->getCustomerOrders('delivered');
    }

    public function getCustomerAllOrders()
    {
        return $this->getCustomerOrders(['delivered', 'in-transition', 'confirmed'], 100);
    }

    public function getManageOrders($status)
    {
        $status     = (is_array($status) ? $status : [$status]);
        
        $sqlQuery   = "SELECT 
            o.id, 
            c.first_name,
            c.last_name,
            CONCAT(c.first_name, ' ', c.last_name) as customer_name,
            o.fk_user_id as order_user,
            o.order_number,
            o.order_status,
            o.payment_method,
            t.currency,
            o.tax_charges,
            o.discount_amount,
            o.total_amount,
            o.payment_token,
            COUNT(oi.fk_product_id) AS total_items,
            t.account_id,
            o.created_at,
            o.updated_at,
            o.shipped_at
        FROM 
        ".$this->tblOrders." o 
        INNER JOIN ".TBL_ORDER_ITEMS." AS oi 
            ON oi.fk_order_id = o.id
        LEFT JOIN ".TBL_TRANSACTION." AS t 
            ON t.fk_order_id = o.id
        LEFT JOIN ".TBL_CUSTOMERS." AS c 
            ON c.id = o.fk_user_id
        WHERE o.order_status in ? GROUP BY o.id  ORDER BY o.created_at DESC";

        $execute = $this->db->query($sqlQuery, array($status));

        return $execute->result_array();
    }

    /**
     * Single Order Details
     * @param $order_id
     * @return (array)
     */
    public function singleOrderByOrderId( $order_id )
    {
        // rd($order_id);
        $sqlQuery   = "SELECT  
            c.id as customer_id,
            CONCAT(c.first_name, ' ', c.last_name) as customer_name,
            c.email as customer_email,
            c.telephone as customer_telephone,
            c.address as customer_address,
            c.country as customer_country,
            c.state as customer_state,
            c.city as customer_city,
            c.postal_code as customer_postal_code,
            count(oi.id) as total_items,
            o.id as order_id,
            o.order_number as order_order_number,
            o.order_status as order_order_status,
            o.payment_method as order_payment_method,
            o.tax_charges as order_tax_charges,
            o.shipping_charges as order_shipping_charges,
            o.discount_amount as order_discount_amount,
            CONCAT ( o.currency, ' ', o.total_amount) as order_total_amount,
            o.currency as order_currency,
            o.ip_address as order_ip_address,
            o.payment_token as order_payment_token,
            o.notes as order_notes,
            o.created_at as order_created_at,
            o.updated_at as order_updated_at,
            o.shipped_at as order_shipped_at,
            t.*,
           CONCAT(b.first_name, ' ', b.last_name) as billing_name,
           b.email as billing_email,
           b.address as billing_address,
           b.company as billing_company,
           b.country as billing_country,
           b.state as billing_state,
           b.city as billing_city,
           b.zip_code as billing_zip_code,
           b.phone as billing_phone,
           b.fax as billing_fax,
           CONCAT(s.first_name, ' ', s.last_name) as shipping_name,
           s.email as shipping_email,
           s.address as shipping_address,
           s.company as shipping_company,
           s.country as shipping_country,
           s.state as shipping_state,
           s.city as shipping_city,
           s.zip_code as shipping_zip_code,
           s.phone as shipping_phone,
           s.fax as shipping_fax

        FROM 
        ".$this->tblOrders." o 
        LEFT JOIN ".TBL_CUSTOMERS." AS c 
            ON c.id = o.fk_user_id
        INNER JOIN ".TBL_ORDER_ITEMS." AS oi 
            ON oi.fk_order_id = o.id
        LEFT JOIN ".TBL_TRANSACTION." AS t 
            ON t.fk_order_id = o.id
        INNER JOIN ".TBL_BILLING." AS b 
            ON b.fk_order_id = o.id
        INNER JOIN ".TBL_SHIPPING." AS s 
            ON s.fk_order_id = o.id
        WHERE o.id = ? GROUP BY o.id ";

        $execute = $this->db->query($sqlQuery, array($order_id));

        // rd($execute->row_array());
        // rd ($this->db->last_query());

        if ( $execute->num_rows() )
        {

            $result = $execute->row_array();

            $orderItemQuery = "SELECT
                oi.id as order_items_id,
                oi.fk_order_id as order_items_fk_order_id,
                oi.name as order_items_name,
                oi.price as order_items_price,
                oi.sale_price as order_items_sale_price,
                oi.qty as order_items_qty,
                oi.expire_date as order_items_expire_date,
                oi.subtotal as order_items_subtotal,
                cp.*,
                p.*

            FROM " . TBL_ORDER_ITEMS . " oi
                INNER JOIN ".TBL_PRODUCTS." AS p 
                    ON p.id = oi.fk_product_id AND p.active = 1 AND p.deleted_at IS NULL
                INNER JOIN ".TBL_CATEGORY_PRODUCT." AS cp 
                    ON cp.product_id = p.id
                WHERE  oi.fk_order_id = ?
                ORDER BY p.id DESC";

            $order_items_execute = $this->db->query($orderItemQuery, array($order_id));
            
            if ( $order_items_execute->num_rows() )
            {
                foreach ($order_items_execute->result_array() as $oiKey => $oi) {

                    // rd($oi);
                    $order['order_items'][$oiKey] = [
                        'id'            => $oi['order_items_id'],
                        'fk_order_id'   => $oi['order_items_fk_order_id'],
                        'name'          => $oi['order_items_name'],
                        'price'         => $oi['order_items_price'],
                        'sale_price'    => $oi['order_items_sale_price'],
                        'qty'           => $oi['order_items_qty'],
                        'expire_date'   => $oi['order_items_expire_date'],
                        'subtotal'      => $oi['order_items_subtotal']
                    ];

                    $order['products'][$oiKey] = [
                        'id'                => $oi['id'],
                        'category_id'       => $oi['category_id'],
                        'brand_id'          => $oi['brand_id'],
                        'title'             => $oi['title'],
                        'slug'              => $oi['slug'],
                        'code'              => $oi['code'],
                        'short_description' => $oi['short_description'],
                        'price'             => $oi['price'],
                        'sale_price'        => $oi['sale_price'],
                        'is_sale'           => $oi['is_sale'],
                    ];
                }
            }
            

            // rd($result);
            $order['customer'] = [
                'id'            => $result['customer_id'],
                'name'          => $result['customer_name'],
                'email'         => $result['customer_email'],
                'telephone'     => $result['customer_telephone'],
                'address'       => $result['customer_address'],
                'country'       => $result['customer_country'],
                'state'         => $result['customer_state'],
                'city'          => $result['customer_city'],
                'postal_code'   => $result['customer_postal_code']
            ];

            $order['order'] = [
                'id'                => $result['order_id'],
                'order_number'      => $result['order_order_number'],
                'order_status'      => $result['order_order_status'],
                'payment_method'    => $result['order_payment_method'],
                'tax_charges'       => $result['order_tax_charges'],
                'shipping_charges'  => $result['order_shipping_charges'],
                'discount_amount'   => $result['order_discount_amount'],
                'total_amount'      => $result['order_total_amount'],
                'currency'          => $result['order_currency'],
                'ip_address'        => $result['order_ip_address'],
                'payment_token'     => $result['order_payment_token'],
                'notes'             => $result['order_notes'],
                'created_at'        => $result['order_created_at'],
                'updated_at'        => $result['order_updated_at'],
                'shipped_at'        => $result['order_shipped_at'],
            ];

            $order['transactions'] = [
                'id'            => $result['id'],
                'account_id'    => $result['account_id'],
                'auth_code'     => $result['auth_code'],
                'currency'      => $result['currency'],
                'email'         => $result['email'],
                'status'        => $result['status'],
                'card_detail'   => $result['card_detail'],
                'created_at'    => $result['created_at'],
                'updated_at'    => $result['updated_at']
            ];

            $order['billing'] = [
                'name'      => $result['billing_name'],
                'email'     => $result['billing_email'],
                'address'   => $result['billing_address'],
                'company'   => $result['billing_company'],
                'country'   => $result['billing_country'],
                'state'     => $result['billing_state'],
                'city'      => $result['billing_city'],
                'zipcode'   => $result['billing_zip_code'],
                'phone'     => $result['billing_phone'],
                'fax'       => $result['billing_fax']
            ];

            if ( !is_null($result['shipping_name']) && !is_null($result['shipping_email']) && !is_null($result['shipping_address']) )
            {
                $order['shipping'] = [
                    'name'      => $result['shipping_name'],
                    'email'     => $result['shipping_email'],
                    'address'   => $result['shipping_address'],
                    'company'   => $result['shipping_company'],
                    'country'   => $result['shipping_country'],
                    'state'     => $result['shipping_state'],
                    'city'      => $result['shipping_city'],
                    'zipcode'   => $result['shipping_zip_code'],
                    'phone'     => $result['shipping_phone'],
                    'fax'       => $result['shipping_fax']
                ];    
            }
            

            return $order;
        }

        return [];
    }
    
    
}

/* End of file Ordermodel.php */
/* Location: ./application/models/Ordermodel.php */