<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Productmodel extends CI_Model {

    private $tblProducts = 'products';
    
    public function __construct() {
        parent::__construct();
    }
    
    public function getAllCategories($items =  [] )
    {

        $this->db->where(['active'=> 1, 'deleted_at' => null])->select('id, parent_id, title, slug');

        if ( isset($items['sortBy']) && isset($items['orderBy']) )
        {
             $this->db->order_by($items['sortBy'], $items['orderBy']);
        }

        if ( isset($items['limit']) )
        {
             $this->db->limit($items['limit']);
        }

        $query = $this->db->get(TBL_CATEGORIES);

        if ( $query->num_rows() )
        {
            return $query->result_array();
        }

        return [];
    }

    public function getBrandProductsList( $options )
    {

        $brand_id       = (isset($options['brand_id']) && $options['brand_id']) ? 'b.slug = "'.$options['brand_id'].'" AND' : null;

        $query = $this->db->query(
            "SELECT
                c.id as c_id,
                c.title as c_name,
                c.slug as c_slug,
                c.active as c_active,
                c.created_at as c_created_at,
                c.updated_at as c_updated_at,
                c.deleted_at as c_deleted_at,

                b.id as b_id,
                b.title as b_name,
                b.slug as b_slug,
                b.active as b_active,
                b.created_at as b_created_at,
                b.updated_at as b_updated_at,
                b.deleted_at as b_deleted_at,
            cp.*,
            p.* 
            FROM " . $this->db->dbprefix( $this->tblProducts ) . " p
            INNER JOIN ".TBL_CATEGORY_PRODUCT." AS cp 
                ON cp.product_id = p.id

            INNER JOIN ".TBL_CATEGORIES." AS c ON 
                c.id = cp.category_id AND c.active = 1 AND c.deleted_at IS NULL

            INNER JOIN ".TBL_BRANDS." AS b ON 
                b.id = p.brand_id AND b.active = 1  AND ".$brand_id." b.deleted_at IS NULL

            WHERE p.active = 1 AND p.deleted_at IS NULL GROUP BY p.id ORDER BY p.id DESC ");

        // rd($this->db->last_query());
        
        if ( $query->num_rows() ) {
            
            $result         = $query->result_array();

            $product_ids    = array_map(function($v){
                return $v['id'];
            }, $result);

            $images         = $this->getMediaByProductId($product_ids);
            $map_images     = group_array_index_map($images, 'product_id');
            $reviews        = $this->getProductRatingByProductId($product_ids);
            $map_reviews    = array_index_map($reviews, 'fk_product_id');
            $products       = [];

            foreach ($result as $key => $product) {
                
                if ( isset($product['c_id']) )
                {
                    $products[$key]['category'] = [
                        'id'            => $product['c_id'],
                        'title'         => $product['c_name'],
                        'slug'          => $product['c_slug'],
                        'active'        => $product['c_active'],
                        'created_at'    => $product['c_created_at'],
                        'updated_at'    => $product['c_updated_at']
                    ];
                }

                if ( isset($product['b_id']) )
                {
                    $products[$key]['brand'] = [
                        'id'            => $product['b_id'],
                        'title'         => $product['b_name'],
                        'slug'          => $product['b_slug'],
                        'active'        => $product['b_active'],
                        'created_at'    => $product['b_created_at'],
                        'updated_at'    => $product['b_updated_at']
                    ];
                }
                

                $products[$key]['products'] = [
                    'id'                            => $product['id'],
                    'category_id'                   => $product['category_id'],
                    'brand_id'                      => $product['brand_id'],
                    'title'                         => $product['title'],
                    'slug'                          => $product['slug'],
                    'code'                          => $product['code'],
                    'extra'                         => $product['extra'],
                    'short_description'             => $product['short_description'],
                    'price'                         => $product['price'],
                    'is_sale'                       => $product['is_sale'],
                    'sale_price'                    => $product['sale_price'],
                    'available_stock'               => $product['available_stock'],
                    'whole_sale_price'              => $product['whole_sale_price'],
                    'whole_sale_available_stock'    => $product['whole_sale_available_stock'],
                    'is_best_seller'                => $product['is_best_seller'],
                    'is_deals'                      => $product['is_deals'],
                    'active'                        => $product['active'],
                    'expired_at'                    => $product['expired_at'],
                    'created_at'                    => $product['created_at'],
                    'updated_at'                    => $product['updated_at'],
                ];

                if ( isset($map_images[$product['id']]) && count( $map_images[$product['id']] ) )
                {
                    $tmp_img = [];
                    foreach ($map_images[$product['id']] as $value)
                    {
                        $tmp_img[] = $value;
                    }

                    $products[$key]['images'] = $tmp_img;
                }

                if ( isset($map_reviews[$product['id']]) && count( $map_reviews[$product['id']] ) )
                {
                    $products[$key]['review'] = [
                        'items' => $map_reviews[$product['id']]['items'],
                        'total' => $map_reviews[$product['id']]['total'],
                    ];
                }
            }

            return $products;
        }

        return [];
    }

    public function getProductsList( $options )
    {

        $category_id    = (isset($options['category_id']) && $options['category_id']) ? 'c.slug = "'.$options['category_id'].'" AND' : null;
        $search_by      = (isset($options['search_by']) && $options['search_by']) ? 'p.title like "%'.trim($options['search_by']).'%" AND' : null;

        $query = $this->db->query(
            "SELECT
                c.id as c_id,
                c.title as c_name,
                c.slug as c_slug,
                c.active as c_active,
                c.created_at as c_created_at,
                c.updated_at as c_updated_at,
                c.deleted_at as c_deleted_at,
            cp.*,
            p.* 
            FROM " . $this->db->dbprefix( $this->tblProducts ) . " p
            INNER JOIN ".TBL_CATEGORY_PRODUCT." AS cp ON 
                cp.product_id = p.id

            INNER JOIN ".TBL_CATEGORIES." AS c ON 
                c.id = cp.category_id AND c.active = 1 AND ".$category_id." c.deleted_at IS NULL

            WHERE 
                p.active = 1 AND 
                ".$search_by."
                p.deleted_at IS NULL 
                GROUP BY p.id ORDER BY p.id DESC
            ");

        // rd($this->db->last_query());
        
        if ( $query->num_rows() ) {
            
            $result         = $query->result_array();

            $product_ids    = array_map(function($v){
                return $v['id'];
            }, $result);

            $images         = $this->getMediaByProductId($product_ids);
            $map_images     = group_array_index_map($images, 'product_id');
            $reviews        = $this->getProductRatingByProductId($product_ids);
            $map_reviews    = array_index_map($reviews, 'fk_product_id');
            $products       = [];

            foreach ($result as $key => $product) {
                
                if ( isset($product['c_id']) )
                {
                    $products[$key]['category'] = [
                        'id'            => $product['c_id'],
                        'title'         => $product['c_name'],
                        'slug'          => $product['c_slug'],
                        'active'        => $product['c_active'],
                        'created_at'    => $product['c_created_at'],
                        'updated_at'    => $product['c_updated_at']
                    ];
                }

                $products[$key]['products'] = [
                    'id'                            => $product['id'],
                    'category_id'                   => $product['category_id'],
                    'brand_id'                      => $product['brand_id'],
                    'title'                         => $product['title'],
                    'slug'                          => $product['slug'],
                    'code'                          => $product['code'],
                    'extra'                         => $product['extra'],
                    'short_description'             => $product['short_description'],
                    'price'                         => $product['price'],
                    'is_sale'                       => $product['is_sale'],
                    'sale_price'                    => $product['sale_price'],
                    'available_stock'               => $product['available_stock'],
                    'whole_sale_price'              => $product['whole_sale_price'],
                    'whole_sale_available_stock'    => $product['whole_sale_available_stock'],
                    'is_best_seller'                => $product['is_best_seller'],
                    'is_deals'                      => $product['is_deals'],
                    'active'                        => $product['active'],
                    'expired_at'                    => $product['expired_at'],
                    'created_at'                    => $product['created_at'],
                    'updated_at'                    => $product['updated_at'],
                ];

                if ( isset($map_images[$product['id']]) && count( $map_images[$product['id']] ) )
                {
                    $tmp_img = [];
                    foreach ($map_images[$product['id']] as $value)
                    {
                        $tmp_img[] = $value;
                    }

                    $products[$key]['images'] = $tmp_img;
                }

                if ( isset($map_reviews[$product['id']]) && count( $map_reviews[$product['id']] ) )
                {
                    $products[$key]['review'] = [
                        'items' => $map_reviews[$product['id']]['items'],
                        'total' => $map_reviews[$product['id']]['total'],
                    ];
                }
            }

            return $products;
        }

        return [];
    }

    public function getProductsCountByActive()
    {

        $query = $this->db->query("SELECT COUNT('p.id') as total FROM " . $this->db->dbprefix( $this->tblProducts ) . " p WHERE p.deleted_at IS NULL AND p.active = 1");

        return $query->row()->total;
    }

    public function getNewProducts( $orderBy = 'desc', $limit = 10 ){

        $query = $this->db->query(
            "SELECT
                c.id as c_id,
                c.title as c_name,
                c.slug as c_slug,
                c.active as c_active,
                c.created_at as c_created_at,
                c.updated_at as c_updated_at,
                c.deleted_at as c_deleted_at,
            cp.*,
            p.*
            FROM " . $this->db->dbprefix( $this->tblProducts ) . " p 
            INNER JOIN ".TBL_CATEGORY_PRODUCT." AS cp 
                ON cp.product_id = p.id
            INNER JOIN ".TBL_CATEGORIES." AS c 
                ON c.id = cp.category_id AND 
                c.active = 1 AND  c.deleted_at IS NULL
            WHERE  
                p.active = 1 AND p.is_sale = 0 AND p.is_deals <> 1 
            AND p.deleted_at IS NULL GROUP BY p.id ORDER BY p.id DESC limit 10 ");

        if ( $query->num_rows() ) {
            
            $result         = $query->result_array();

            $product_ids    = array_map(function($v){
                return $v['id'];
            }, $result);

            $images         = $this->getMediaByProductId($product_ids);
            $map_images     = group_array_index_map($images, 'product_id');
            $reviews        = $this->getProductRatingByProductId($product_ids);
            $map_reviews    = array_index_map($reviews, 'fk_product_id');
            // rd($map_reviews);
            $products       = [];

            foreach ($result as $key => $product) {
                $products[$key]['category'] = [
                    'id'            => $product['c_id'],
                    'title'         => $product['c_name'],
                    'slug'          => $product['c_slug'],
                    'active'        => $product['c_active'],
                    'created_at'    => $product['c_created_at'],
                    'updated_at'    => $product['c_updated_at']
                ];

                $products[$key]['products'] = [
                    'id'                            => $product['id'],
                    'product_type'                  => 1,
                    'category_id'                   => $product['category_id'],
                    'brand_id'                      => $product['brand_id'],
                    'title'                         => $product['title'],
                    'slug'                          => $product['slug'],
                    'code'                          => $product['code'],
                    'extra'                         => $product['extra'],
                    'short_description'             => $product['short_description'],
                    'price'                         => $product['price'],
                    'is_sale'                       => $product['is_sale'],
                    'sale_price'                    => $product['sale_price'],
                    'available_stock'               => $product['available_stock'],
                    'whole_sale_price'              => $product['whole_sale_price'],
                    'whole_sale_available_stock'    => $product['whole_sale_available_stock'],
                    'is_best_seller'                => $product['is_best_seller'],
                    'is_deals'                      => $product['is_deals'],
                    'active'                        => $product['active'],
                    'expired_at'                    => $product['expired_at'],
                    'created_at'                    => $product['created_at'],
                    'updated_at'                    => $product['updated_at'],
                ];

                if ( isset($map_images[$product['id']]) && count( $map_images[$product['id']] ) )
                {
                    $tmp_img = [];
                    foreach ($map_images[$product['id']] as $value)
                    {
                        $tmp_img[] = $value;
                    }

                    $products[$key]['images'] = $tmp_img;
                }

                if ( isset($map_reviews[$product['id']]) && count( $map_reviews[$product['id']] ) )
                {
                    $products[$key]['review'] = [
                        'items' => $map_reviews[$product['id']]['items'],
                        'total' => $map_reviews[$product['id']]['total'],
                    ];
                }
            }

            return $products;
        }

        return [];
    }

    public function getOnSaleProducts( $orderBy = 'desc', $limit = 10 ){

        $query = $this->db->query(
            "SELECT
                c.id as c_id,
                c.title as c_name,
                c.slug as c_slug,
                c.active as c_active,
                c.created_at as c_created_at,
                c.updated_at as c_updated_at,
                c.deleted_at as c_deleted_at,
            cp.*,
            p.*
            FROM " . $this->db->dbprefix( $this->tblProducts ) . " p 
            INNER JOIN ".TBL_CATEGORY_PRODUCT." AS cp 
                ON cp.product_id = p.id
            INNER JOIN ".TBL_CATEGORIES." AS c 
                ON c.id = cp.category_id AND 
                c.active = 1 AND  c.deleted_at IS NULL
            WHERE  
                p.active = 1 AND p.is_sale = 1 AND p.is_deals <> 1 AND
                p.deleted_at IS NULL GROUP BY p.id ORDER BY p.id DESC limit 10", $orderBy, $limit);
        
        // rd($this->db->last_query());
        if ( $query->num_rows() ) {
            
            $result         = $query->result_array();

            $product_ids    = array_map(function($v){
                return $v['id'];
            }, $result);

            $images         = $this->getMediaByProductId($product_ids);
            $map_images     = group_array_index_map($images, 'product_id');
            $reviews        = $this->getProductRatingByProductId($product_ids);
            $map_reviews    = array_index_map($reviews, 'fk_product_id');
            $products       = [];

            foreach ($result as $key => $product) {
                $products[$key]['category'] = [
                    'id'            => $product['c_id'],
                    'title'         => $product['c_name'],
                    'slug'          => $product['c_slug'],
                    'active'        => $product['c_active'],
                    'created_at'    => $product['c_created_at'],
                    'updated_at'    => $product['c_updated_at']
                ];

                $products[$key]['products'] = [
                    'id'                            => $product['id'],
                    'category_id'                   => $product['category_id'],
                    'brand_id'                      => $product['brand_id'],
                    'title'                         => $product['title'],
                    'slug'                          => $product['slug'],
                    'code'                          => $product['code'],
                    'extra'                         => $product['extra'],
                    'short_description'             => $product['short_description'],
                    'price'                         => $product['price'],
                    'is_sale'                       => $product['is_sale'],
                    'sale_price'                    => $product['sale_price'],
                    'available_stock'               => $product['available_stock'],
                    'whole_sale_price'              => $product['whole_sale_price'],
                    'whole_sale_available_stock'    => $product['whole_sale_available_stock'],
                    'is_best_seller'                => $product['is_best_seller'],
                    'is_deals'                      => $product['is_deals'],
                    'active'                        => $product['active'],
                    'expired_at'                    => $product['expired_at'],
                    'created_at'                    => $product['created_at'],
                    'updated_at'                    => $product['updated_at'],
                ];

                if ( isset($map_images[$product['id']]) && count( $map_images[$product['id']] ) )
                {
                    $tmp_img = [];
                    foreach ($map_images[$product['id']] as $value)
                    {
                        $tmp_img[] = $value;
                    }

                    $products[$key]['images'] = $tmp_img;
                }

                if ( isset($map_reviews[$product['id']]) && count( $map_reviews[$product['id']] ) )
                {
                    $products[$key]['review'] = [
                        'items' => $map_reviews[$product['id']]['items'],
                        'total' => $map_reviews[$product['id']]['total'],
                    ];
                }
            }

            return $products;
        }

        return [];
    }

    public function getBestSellerProducts( $orderBy = 'desc', $limit = 10 ){

        $query = $this->db->query(
            "SELECT
                c.id as c_id,
                c.title as c_name,
                c.slug as c_slug,
                c.active as c_active,
                c.created_at as c_created_at,
                c.updated_at as c_updated_at,
                c.deleted_at as c_deleted_at,
            cp.*,
            p.*
            FROM " . $this->db->dbprefix( $this->tblProducts ) . " p 
            INNER JOIN ".TBL_CATEGORY_PRODUCT." AS cp 
                ON cp.product_id = p.id
            INNER JOIN ".TBL_CATEGORIES." AS c 
                ON c.id = cp.category_id AND 
                c.active = 1 AND  c.deleted_at IS NULL
            WHERE  
            p.active = 1 AND p.is_best_seller = 1 AND p.deleted_at IS NULL GROUP BY p.id ORDER BY p.id DESC limit 10", $orderBy, $limit);
        // rd($this->db->last_query());
        if ( $query->num_rows() ) {
            
            $result         = $query->result_array();

            $product_ids    = array_map(function($v){
                return $v['id'];
            }, $result);

            $images         = $this->getMediaByProductId($product_ids);
            $map_images     = group_array_index_map($images, 'product_id');
            $reviews        = $this->getProductRatingByProductId($product_ids);
            $map_reviews    = array_index_map($reviews, 'fk_product_id');
            $products       = [];

            foreach ($result as $key => $product) {
                $products[$key]['category'] = [
                    'id'            => $product['c_id'],
                    'title'         => $product['c_name'],
                    'slug'          => $product['c_slug'],
                    'active'        => $product['c_active'],
                    'created_at'    => $product['c_created_at'],
                    'updated_at'    => $product['c_updated_at']
                ];

                $products[$key]['products'] = [
                    'id'                            => $product['id'],
                    'category_id'                   => $product['category_id'],
                    'brand_id'                      => $product['brand_id'],
                    'title'                         => $product['title'],
                    'slug'                          => $product['slug'],
                    'code'                          => $product['code'],
                    'extra'                         => $product['extra'],
                    'short_description'             => $product['short_description'],
                    'price'                         => $product['price'],
                    'is_sale'                       => $product['is_sale'],
                    'sale_price'                    => $product['sale_price'],
                    'available_stock'               => $product['available_stock'],
                    'whole_sale_price'              => $product['whole_sale_price'],
                    'whole_sale_available_stock'    => $product['whole_sale_available_stock'],
                    'is_best_seller'                => $product['is_best_seller'],
                    'is_deals'                      => $product['is_deals'],
                    'active'                        => $product['active'],
                    'expired_at'                    => $product['expired_at'],
                    'created_at'                    => $product['created_at'],
                    'updated_at'                    => $product['updated_at'],
                ];

                if ( isset($map_images[$product['id']]) && count( $map_images[$product['id']] ) )
                {
                    $tmp_img = [];
                    foreach ($map_images[$product['id']] as $value)
                    {
                        $tmp_img[] = $value;
                    }

                    $products[$key]['images'] = $tmp_img;
                }

                if ( isset($map_reviews[$product['id']]) && count( $map_reviews[$product['id']] ) )
                {
                    $products[$key]['review'] = [
                        'items' => $map_reviews[$product['id']]['items'],
                        'total' => $map_reviews[$product['id']]['total'],
                    ];
                }
            }

            return $products;
        }

        return [];
    }

    public function getDealsProducts( $orderBy = 'desc', $limit = 10 ){

        $query = $this->db->query(
            "SELECT
                c.id as c_id,
                c.title as c_name,
                c.slug as c_slug,
                c.active as c_active,
                c.created_at as c_created_at,
                c.updated_at as c_updated_at,
                c.deleted_at as c_deleted_at,
            cp.*,
            p.*
            FROM " . $this->db->dbprefix( $this->tblProducts ) . " p 
            INNER JOIN ".TBL_CATEGORY_PRODUCT." AS cp 
                ON cp.product_id = p.id
            INNER JOIN ".TBL_CATEGORIES." AS c 
                ON c.id = cp.category_id AND 
                c.active = 1 AND  c.deleted_at IS NULL
            WHERE  
            p.active = 1  AND p.is_deals = 1 AND p.deleted_at IS NULL GROUP BY p.id ORDER BY p.id DESC limit 10", $orderBy, $limit);
        // rd($this->db->last_query());
        if ( $query->num_rows() ) {
            
            $result         = $query->result_array();

            $product_ids    = array_map(function($v){
                return $v['id'];
            }, $result);

            $images         = $this->getMediaByProductId($product_ids);
            $map_images     = group_array_index_map($images, 'product_id');
            $reviews        = $this->getProductRatingByProductId($product_ids);
            $map_reviews    = array_index_map($reviews, 'fk_product_id');
            $products       = [];

            foreach ($result as $key => $product) {
                $products[$key]['category'] = [
                    'id'            => $product['c_id'],
                    'title'         => $product['c_name'],
                    'slug'          => $product['c_slug'],
                    'active'        => $product['c_active'],
                    'created_at'    => $product['c_created_at'],
                    'updated_at'    => $product['c_updated_at']
                ];

                $products[$key]['products'] = [
                    'id'                            => $product['id'],
                    'category_id'                   => $product['category_id'],
                    'brand_id'                      => $product['brand_id'],
                    'title'                         => $product['title'],
                    'slug'                          => $product['slug'],
                    'code'                          => $product['code'],
                    'extra'                         => $product['extra'],
                    'short_description'             => $product['short_description'],
                    'price'                         => $product['price'],
                    'is_sale'                       => $product['is_sale'],
                    'sale_price'                    => $product['sale_price'],
                    'available_stock'               => $product['available_stock'],
                    'whole_sale_price'              => $product['whole_sale_price'],
                    'whole_sale_available_stock'    => $product['whole_sale_available_stock'],
                    'is_best_seller'                => $product['is_best_seller'],
                    'is_deals'                      => $product['is_deals'],
                    'active'                        => $product['active'],
                    'expired_at'                    => $product['expired_at'],
                    'created_at'                    => $product['created_at'],
                    'updated_at'                    => $product['updated_at'],
                ];

                if ( isset($map_images[$product['id']]) && count( $map_images[$product['id']] ) )
                {
                    $tmp_img = [];
                    foreach ($map_images[$product['id']] as $value)
                    {
                        $tmp_img[] = $value;
                    }

                    $products[$key]['images'] = $tmp_img;
                }

                if ( isset($map_reviews[$product['id']]) && count( $map_reviews[$product['id']] ) )
                {
                    $products[$key]['review'] = [
                        'items' => $map_reviews[$product['id']]['items'],
                        'total' => $map_reviews[$product['id']]['total'],
                    ];
                }
            }

            return $products;
        }

        return [];
    }

    public function getMediaByProductId( $id )
    {
        $product_ids    = is_array($id) ? $id : [$id];
        $query          = $this->db->where_in('product_id', $product_ids)->get(TBL_IMAGES);

        if ( $query->num_rows() )
        {
            return $query->result_array();
        }

        return [];
    }

    public function getMediaByProductIdOrType( $id, $type )
    {
        $product_ids    = is_array($id) ? $id : [$id];
        $query          = $this->db->where('type', $type)->where_in('product_id', $product_ids)->order_by('sortBy', 'ASC')->get(TBL_MEDIA);

        if ( $query->num_rows() )
        {
            return $query->result_array();
        }

        return [];
    }

    public function getManageProducts()
    {

        $query = $this->db->query(
            "SELECT
                c.id as c_id,
                GROUP_CONCAT(DISTINCT c.title SEPARATOR '<br/>') as c_name,
                c.slug as c_slug,
                c.active as c_active,
                c.created_at as c_created_at,
                c.updated_at as c_updated_at,
                c.deleted_at as c_deleted_at,
            cp.*,
            p.* 
            FROM " . $this->db->dbprefix( $this->tblProducts ) . " p 
            INNER JOIN ".TBL_CATEGORY_PRODUCT." AS cp ON cp.product_id = p.id
            INNER JOIN ".TBL_CATEGORIES." AS c ON c.id = cp.category_id
            WHERE c.deleted_at IS NULL AND p.deleted_at IS NULL GROUP BY p.id ");

        if ( $query->num_rows() ) {
            return $query->result_array();
        }

        return [];
    }

    public function getSingleProduct( $product_id, $active = false  ){

        $is_active = NULL;

        if ( $active ) {
            $is_active = ' p.active = 1 AND';
        }

        $query = $this->db->query(
            "SELECT
                c.id as c_id,
                c.title as c_name,
                c.slug as c_slug,
                c.active as c_active,
                c.created_at as c_created_at,
                c.updated_at as c_updated_at,
                c.deleted_at as c_deleted_at,
            cp.*,
            p.* 
            FROM " . $this->db->dbprefix( $this->tblProducts ) . " p 
            INNER JOIN ".TBL_CATEGORY_PRODUCT." AS cp 
                ON cp.product_id = p.id
            INNER JOIN ".TBL_CATEGORIES." AS c 
                ON c.id = cp.category_id AND c.deleted_at IS NULL
            WHERE 
                p.id = ?  AND 
                ".$is_active."
                p.deleted_at IS NULL  
            LIMIT 1",
            array( $product_id )
        );

        $row = $query->row_array();

        if ( $row ) {
            
            $pro_images = $this->db->from(TBL_IMAGES)->where('product_id', $row['id'])->get();
            $pro_desc   = $this->db->from(TBL_MEDIA)->where('product_id', $row['id'])->where('type', 'pro-desc')->get();

            $data   = remove_array_values(['c_id', 'c_name', 'c_slug', 'c_active', 'c_created_at', 'c_updated_at', 'c_deleted_at'], $row);

            return [
                'products' => [
                    'data' => $data,
                    'category' => [
                        'id'         => $row['c_id'],
                        'name'       => $row['c_name'],
                        'slug'       => $row['c_slug'],
                        'active'     => $row['c_active'],
                        'created_at' => $row['c_created_at'],
                        'updated_at' => $row['c_updated_at'],
                        'deleted_at' => $row['c_deleted_at']
                    ],
                    'images' => $pro_images->result_array(),
                    'pro_desc' => $pro_desc->result_array(),
                ]
            ];

        }

        return NULL;
    }

    public function getListOfProductReviewByProductId($fk_product_id)
    {
        $query = $this->db->query(
            "SELECT
                c.id as customer_id,
                CONCAT(c.first_name, ' ',c.last_name) as customer_name,
                c.email,
                pr.id,
                pr.reviews,
                pr.review_rating,
                pr.created_at

            FROM " . TBL_PRODUCT_REVIEWS . " pr 
            INNER JOIN ".TBL_CUSTOMERS." AS c ON c.id = pr.fk_user_id
            WHERE  pr.is_active = 1 AND c.active = 1  AND pr.fk_product_id = ? AND
            c.deleted_at IS NULL AND pr.deleted_at IS NULL ORDER BY pr.id DESC", array($fk_product_id));

        if ( $query->num_rows() ) {
            return $query->result_array();
        }

        return [];
    }

    public function getProductRatingByProductId($fk_product_id)
    {
        $fk_product_id = (is_array($fk_product_id) ? $fk_product_id : [$fk_product_id] );

        $query = $this->db->query(
            "SELECT pr.fk_product_id, count(pr.fk_product_id) as items, sum(pr.review_rating) as total FROM " . TBL_PRODUCT_REVIEWS . " pr
            WHERE  pr.is_active = 1 AND pr.fk_product_id in ? AND pr.deleted_at IS NULL GROUP BY pr.fk_product_id ORDER BY pr.id DESC", array($fk_product_id));

        if ( $query->num_rows() ) {
            return $query->result_array();
        }

        return [];
    }

    /**
     * List of Customer Wishlists
     */
    public function getWishlistProducts(){

        $query = $this->db->query(
            "SELECT
                c.id as c_id,
                c.title as c_name,
                c.slug as c_slug,
                c.active as c_active,
                c.created_at as c_created_at,
                c.updated_at as c_updated_at,
                c.deleted_at as c_deleted_at,
            cp.*,
            p.*,
            w.id as wishlist_id
            FROM " . $this->db->dbprefix( $this->tblProducts ) . " p 
            INNER JOIN ".TBL_CATEGORY_PRODUCT." AS cp 
                ON cp.product_id = p.id
            INNER JOIN ".TBL_CATEGORIES." AS c 
                ON c.id = cp.category_id AND 
                c.active = 1 AND  c.deleted_at IS NULL
            INNER JOIN ".TBL_WISHLIST." AS w ON w.fk_product_id = p.id
            WHERE 
            p.active = 1  AND w.fk_user_id = ? AND p.deleted_at IS NULL ORDER BY w.id DESC", array(auth()->id));
        // rd($this->db->last_query());
        if ( $query->num_rows() ) {
            
            $result         = $query->result_array();

            $product_ids    = array_map(function($v){
                return $v['id'];
            }, $result);

            $images         = $this->getMediaByProductId($product_ids);
            $map_images     = group_array_index_map($images, 'product_id');
            $reviews        = $this->getProductRatingByProductId($product_ids);
            $map_reviews    = array_index_map($reviews, 'fk_product_id');
            $products       = [];

            foreach ($result as $key => $product) {

                $products[$key]['wishlist'] = [
                    'id' => $product['wishlist_id']
                ];

                $products[$key]['category'] = [
                    'id'            => $product['c_id'],
                    'title'         => $product['c_name'],
                    'slug'          => $product['c_slug'],
                    'active'        => $product['c_active'],
                    'created_at'    => $product['c_created_at'],
                    'updated_at'    => $product['c_updated_at']
                ];

                $products[$key]['products'] = [
                    'id'                            => $product['id'],
                    'category_id'                   => $product['category_id'],
                    'brand_id'                      => $product['brand_id'],
                    'title'                         => $product['title'],
                    'slug'                          => $product['slug'],
                    'code'                          => $product['code'],
                    'extra'                         => $product['extra'],
                    'short_description'             => $product['short_description'],
                    'price'                         => $product['price'],
                    'is_sale'                       => $product['is_sale'],
                    'sale_price'                    => $product['sale_price'],
                    'available_stock'               => $product['available_stock'],
                    'whole_sale_price'              => $product['whole_sale_price'],
                    'whole_sale_available_stock'    => $product['whole_sale_available_stock'],
                    'is_best_seller'                => $product['is_best_seller'],
                    'is_deals'                      => $product['is_deals'],
                    'active'                        => $product['active'],
                    'expired_at'                    => $product['expired_at'],
                    'created_at'                    => $product['created_at'],
                    'updated_at'                    => $product['updated_at'],
                ];

                if ( isset($map_images[$product['id']]) && count( $map_images[$product['id']] ) )
                {
                    $tmp_img = [];
                    foreach ($map_images[$product['id']] as $value)
                    {
                        $tmp_img[] = $value;
                    }

                    $products[$key]['images'] = $tmp_img;
                }

                if ( isset($map_reviews[$product['id']]) && count( $map_reviews[$product['id']] ) )
                {
                    $products[$key]['review'] = [
                        'items' => $map_reviews[$product['id']]['items'],
                        'total' => $map_reviews[$product['id']]['total'],
                    ];
                }
            }

            return $products;
        }

        return [];
    }


}

/* End of file Productmodel.php */
/* Location: ./application/models/Productmodel.php */