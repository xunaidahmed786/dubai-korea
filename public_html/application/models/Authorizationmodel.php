<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Authorizationmodel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function isAuthorizationLogin( $author = [], $status = FALSE )
    {

        if ( is_array($author) && count($author) ){

            if ( !isset($author['password']) ) {
                return FALSE;
            }

            $result     = $this->db->get_where(TBL_USERS, $this->__makeCredientials($author), 1);
            $response   = $result->row();

            if ($response) {

                $this->session->set_userdata('auth', $result->row());
                $this->session->set_userdata('logged_in', TRUE );

                return TRUE;
            }
        }

        return FALSE;
    }

    public function isAuthorizationLoginByCustomer( $author = [], $status = FALSE )
    {

        if ( is_array($author) && count($author) ){

            if ( !isset($author['password']) ) {
                return FALSE;
            }

            $result     = $this->db->get_where(TBL_CUSTOMERS, $this->__makeCredientials($author));
            // rd( $this->db->last_query() );
            $response   = $result->row();

            if ($response) {

                $this->session->set_userdata('auth', $result->row());
                $this->session->set_userdata('logged_in', TRUE );

                return TRUE;
            }
        }

        return FALSE;
    }

    private function __makeCredientials( $set_of_crediential ){

        $set_of_crediential['password']     = salt_password(trim($set_of_crediential['password']));
        $set_of_crediential['deleted_at']   = NULL;

        return $set_of_crediential;
    } 



    


}

/* End of file Authorizationmodel.php */
/* Location: ./application/models/Authorizationmodel.php */